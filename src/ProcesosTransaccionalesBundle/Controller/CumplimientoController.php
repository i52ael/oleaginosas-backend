<?php

namespace ProcesosTransaccionalesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use ProcesosTransaccionalesBundle\Entity\Entidad;

/**
 * @Route("/cumplimiento")
 * **/
class CumplimientoController extends Controller
{
    /**
    * @Route("/reporte", name="cumplimiento_reporte")
    */
    public function reporteCumplimiento(Request $request)    
    {
        $empresas = [];
        $entidades = $this->getDoctrine()->getRepository(Entidad::class)->findByIdFase(3);
        $id_producto = $request->query->get('producto');
        $gestion = $request->query->get('gestion');

        foreach($entidades as $entidad){
            $cantidad_mes = [];

            for($i=1; $i<=12;  $i++){
                $compra_entidad = $this->getCompraxMes($id_producto, $entidad->getId(), $i, $gestion);
                if(!empty($compra_entidad)){
                    array_push($cantidad_mes,$compra_entidad[0]['cantidad']);
                }else{
                    array_push($cantidad_mes,0);
                }
            }
                
                $nombre_empresa = $entidad->getRazonSocial();
                $enero = $cantidad_mes[0];
                $febrero = $cantidad_mes[1];
                $marzo = $cantidad_mes[2];
                $abril = $cantidad_mes[3];
                $mayo = $cantidad_mes[4];
                $junio = $cantidad_mes[5];
                $julio = $cantidad_mes[6];
                $agosto = $cantidad_mes[7];
                $septiembre = $cantidad_mes[8];
                $octubre = $cantidad_mes[9];
                $noviembre = $cantidad_mes[10];
                $diciembre = $cantidad_mes[11];
                $empresa = [$nombre_empresa,$enero,$febrero,$marzo,$abril,$mayo,$junio,$julio,$agosto,$septiembre,$octubre,$noviembre,$diciembre];
                array_push($empresas,$empresa);
            
        }
        return $this->json($empresas, Response::HTTP_OK);
    }
    
    /**
    * @Route("/reporte/campaign", name="cumplimiento_reporte_campaign")
    */
    public function reporteCumplimientoCampaign(Request $request)    
    {
        //http://localhost:8000/cumplimiento/reporte/campaign?producto=50&gestion=2017&estacion=verano
        
        $empresas = [];
        $entidades = $this->getDoctrine()->getRepository(Entidad::class)->findByIdFase(3);
        $id_producto = $request->query->get('producto');
        $gestion = $request->query->get('gestion');
        $estacion = $request->query->get('estacion');
         foreach($entidades as $entidad){

               // Compromiso
                $compromiso_entidad = $this->getCompromisoxCampaign($id_producto, $entidad->getId(), $gestion, $estacion);
                if(!empty($compromiso_entidad)){
                    $compromiso_cantidad = $compromiso_entidad[0]['cantidad_compromiso'];
                }else{
                    $compromiso_cantidad = 0;
                }
                // Compra
                $compra_entidad = $this->getCompraxCampaign($id_producto, $entidad->getId(), $gestion, $estacion);
                if(!empty($compra_entidad)){
                    $compra_cantidad = $compra_entidad[0]['cantidad'];
                }else{
                    $compra_cantidad = 0;
                }
                
                // Acopio
                $acopio_entidad = $this->getAcopioxCampaign($id_producto, $entidad->getId(), $gestion, $estacion);
                if(!empty($acopio_entidad)){
                    $acopio_cantidad = $acopio_entidad[0]['cantidad'];
                }else{
                    $acopio_cantidad = 0;
                }
                
                if($compromiso_cantidad != 0){
                    $porc_acopio_compromiso = $acopio_cantidad/$compromiso_cantidad;
                }else{
                    $porc_acopio_compromiso = 0;
                }
                
                
                $nombre_empresa = $entidad->getRazonSocial();
                $compromiso = $compromiso_cantidad;
                $compra = $compra_cantidad;
                $acopio = $acopio_cantidad;
                $acopio_respecto_compromiso = $porc_acopio_compromiso;

                $empresa = [$nombre_empresa,$compromiso,$compra,$acopio,$acopio_respecto_compromiso];
                array_push($empresas,$empresa);
            
        }
        
        
        return $this->json($empresas, Response::HTTP_OK);
    }
    
    public function getCompraxMes($id_producto,$id_entidad, $mes, $gestion){
        $em = $this->getDoctrine()->getManager();
        $sql = "select
                entidad.id,compra_detalle.id_producto, entidad.razon_social, date_trunc('month',periodo_fase.fecha_desde) AS fecha_compra, date_part('month', periodo_fase.fecha_desde) AS mes, sum(compra_detalle.cantidad) AS cantidad
                from
                transaccion
                INNER JOIN compra_detalle ON compra_detalle.id_transaccion = transaccion.id
                INNER JOIN relacion_entidad ON relacion_entidad.id = transaccion.id_relacion_entidad
                INNER JOIN entidad ON entidad.id = relacion_entidad.id_entidad
                INNER JOIN periodo_fase ON periodo_fase.id = transaccion.id_periodo_fase
                WHERE entidad.id_fase = 3
                AND compra_detalle.id_producto = ?
                AND entidad.id = ?
                AND date_part('month', periodo_fase.fecha_desde) = ?
                AND date_part('year', periodo_fase.fecha_desde) = ?  
                GROUP BY entidad.id,compra_detalle.id_producto, date_trunc('month',periodo_fase.fecha_desde),entidad.razon_social, date_part('month', periodo_fase.fecha_desde)
                LIMIT 1"                
        ;
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->bindValue(1, $id_producto);
        $stmt->bindValue(2, $id_entidad);
        $stmt->bindValue(3, $mes);
        $stmt->bindValue(4, $gestion);
        $stmt->execute();
        return $stmt->fetchAll();
    }
    
    public function getCompromisoxCampaign($id_producto,$id_entidad, $gestion, $estacion){
        $em = $this->getDoctrine()->getManager();
        $camp = ($estacion == 'verano') ? " AND campania.mes >= 1 AND campania.mes <= 6 " : " AND campania.mes >= 7 AND campania.mes <= 12 ";
        $sql = "select
                entidad.id, entidad.razon_social,sum(compromiso_entidad.compromiso_entidad) AS cantidad_compromiso, campania.gestion
                from
                compromiso_entidad
                INNER JOIN entidad ON entidad.id = compromiso_entidad.id_entidad
                INNER JOIN campania ON campania.id = compromiso_entidad.id_campania
                WHERE entidad.id_fase = 3
                AND compromiso_entidad.id_producto = ?
                AND entidad.id = ?
                AND campania.gestion = ?
                ".$camp."
                GROUP BY entidad.id, entidad.razon_social, campania.gestion";
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->bindValue(1, $id_producto);
        $stmt->bindValue(2, $id_entidad);
        $stmt->bindValue(3, $gestion);
        $stmt->execute();
        return $stmt->fetchAll();
    }
    
    public function getCompraxCampaign($id_producto,$id_entidad, $gestion, $estacion){
        $em = $this->getDoctrine()->getManager();
        
        $camp = ($estacion == 'verano') ? " AND periodo_fase.fecha_desde >= '$gestion-01-01' AND periodo_fase.fecha_desde <= '$gestion-06-30' " : " AND periodo_fase.fecha_desde >= '$gestion-07-01' AND periodo_fase.fecha_desde < '$gestion-12-31' ";

        $sql = "select
                entidad.id, entidad.razon_social, date_trunc('year',periodo_fase.fecha_desde) AS fecha_compra, sum(compra_detalle.cantidad) AS cantidad
                from
                transaccion
                INNER JOIN compra_detalle ON compra_detalle.id_transaccion = transaccion.id
                INNER JOIN relacion_entidad ON relacion_entidad.id = transaccion.id_relacion_entidad
                INNER JOIN entidad ON entidad.id = relacion_entidad.id_entidad
                INNER JOIN periodo_fase ON periodo_fase.id = transaccion.id_periodo_fase
                WHERE entidad.id_fase = 3
                AND compra_detalle.id_producto = ?
                AND entidad.id = ?
                ".$camp."
                GROUP BY entidad.id, entidad.razon_social, date_trunc('year',periodo_fase.fecha_desde)";
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->bindValue(1, $id_producto);
        $stmt->bindValue(2, $id_entidad);
        $stmt->execute();
        return $stmt->fetchAll();
    }
    
    public function getAcopioxCampaign($id_producto,$id_entidad, $gestion, $estacion){
        $em = $this->getDoctrine()->getManager();
        
        $camp = ($estacion == 'verano') ? " AND periodo_fase.fecha_desde >= '$gestion-01-01' AND periodo_fase.fecha_desde <= '$gestion-06-30' " : " AND periodo_fase.fecha_desde >= '$gestion-07-01' AND periodo_fase.fecha_desde < '$gestion-12-31' ";

        $sql = "select
                entidad.id, entidad.razon_social, date_trunc('year',periodo_fase.fecha_desde) AS fecha_compra, sum(acopio_detalle.cantidad_neta) AS cantidad
                from
                transaccion
                INNER JOIN acopio_detalle ON acopio_detalle.id_transaccion = transaccion.id
                INNER JOIN relacion_entidad ON relacion_entidad.id = transaccion.id_relacion_entidad
                INNER JOIN entidad ON entidad.id = relacion_entidad.id_entidad
                INNER JOIN periodo_fase ON periodo_fase.id = transaccion.id_periodo_fase
                WHERE entidad.id_fase = 3
                AND acopio_detalle.id_producto = ?
                AND entidad.id = ?
                ".$camp."
                GROUP BY entidad.id, entidad.razon_social, date_trunc('year',periodo_fase.fecha_desde)";
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->bindValue(1, $id_producto);
        $stmt->bindValue(2, $id_entidad);
        $stmt->execute();
        return $stmt->fetchAll();
    }
    
}
