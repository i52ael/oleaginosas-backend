<?php

namespace ProcesosTransaccionalesBundle\Controller;

use ProcesosTransaccionalesBundle\Manager\TransaccionManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use JMS\DiExtraBundle\Annotation as DI;
use ProcesosTransaccionalesBundle\Controller\DefaultController;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

use ProductoBundle\Manager\EntidadManager;
use ProductoBundle\Manager\TipoEntidadManager;
use ProductoBundle\Manager\TipoDocumentoManager;
use ProductoBundle\Manager\PersonaManager;
use ProductoBundle\Manager\RelacionEntidadManager;
use ProductoBundle\Manager\RegimenManager;

/**
 * @Route("/procesos-transaccionales")
 **/
class PrintController extends Controller
{

    protected $transaccionManager;

    protected $entidadManager;
    protected $tipoEntidadManager;
    protected $tipoDocumentoManager;
    protected $personaManager;
    protected $relacionManager;
    protected $regimenManager;
    /**
     * @DI\InjectParams({
     * "transaccionManager"=@DI\Inject("api.manager.transaccion"),
     * "entidadManager"=@DI\Inject("api.manager.entidad"),
     * "tipoDocumentoManager"=@DI\Inject("api.manager.tipoDocumento"),
     * "tipoEntidadManager"=@DI\Inject("api.manager.tipoEntidad"),
     * "personaManager"=@DI\Inject("api.manager.persona"),
     * "relacionEntidadManager"=@DI\Inject("api.manager.relacion"),
     * "regimenManager"=@DI\Inject("api.manager.regimen")
     * })
     *
     */
    public function __construct(TransaccionManager $transaccionManager, EntidadManager $entidadManager,TipoEntidadManager  $tipoEntidadManager,
                                TipoDocumentoManager $tipoDocumentoManager,PersonaManager $personaManager,
                                RelacionEntidadManager $relacionEntidadManager,RegimenManager $regimenManager)
    {
        $this->transaccionManager=$transaccionManager;

        $this->entidadManager=$entidadManager;
        $this->tipoEntidadManager=$tipoEntidadManager;
        $this->tipoDocumentoManager= $tipoDocumentoManager;
        $this->personaManager=$personaManager;
        $this->relacionManager=$relacionEntidadManager;
        $this->regimenManager=$regimenManager;
    }

    /**
     * @Security("is_granted('ROLE_USER')")
     * @Route("/generarAcceso")
     * @Method("GET")
     */
    public function generarAcceso()
    {
        $fecha=new \DateTime();
        return $this->json($fecha->getTimestamp()+5*60, Response::HTTP_OK);//damos 5 minutos para que pueda descargar con el enlace
    }
    /**
     * @Route("/printTransactions")
     * @Method("GET")
     **/
    public function printTransactions(Request $request)
    {
       $c_defaultController=new DefaultController($this->transaccionManager);
       $obj_datos=new \stdClass();
       $obj_datos->datos=new \stdClass();
       $obj_datos->datos->compra=$request->get("compra");
       $obj_datos->datos->venta=$request->get("venta");
       $obj_datos->datos->acopio=$request->get("acopio");
       $obj_datos->datos->produccion=$request->get("produccion");

       return $c_defaultController->imprimirTransacciones($obj_datos);
    }
    /**
     * @Route("/downloadTemplate")
     *
     */
    public function downloadTemplate(Request $request)
    {
        ini_set('memory_limit', '-1');
        $fecha=new \DateTime();
        $fecha_tk=new \DateTime();
        $fecha_tk->setTimestamp($request->get('tk'));
        if($fecha_tk->getTimestamp()>$fecha->getTimestamp())
        {
            $id_fase=$request->get('fase');
            // ask the service for a excel object
            if($id_fase==3)
                $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject("template_industria.xlsx");
            else
                $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject("template_asociacion.xlsx");
            // create the writer
            $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel2007');
            // create the response
            $response = $this->get('phpexcel')->createStreamedResponse($writer);
            // adding headers
            $dispositionHeader = $response->headers->makeDisposition(
                ResponseHeaderBag::DISPOSITION_ATTACHMENT,
                'plantilla.xlsx'
            );
            $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
            $response->headers->set('Pragma', 'public');
            $response->headers->set('Cache-Control', 'maxage=1');
            $response->headers->set('Content-Disposition', $dispositionHeader);

            return $response;
        }
        else
            return new Response("El enlace solo esta vigente por 5 minutos, ya pasaron mas de 5 minutos", 200);



    }
    /**
     * @Route("/downloadTemplateEntidadMasivo")
     * @Method("GET")
     */
    public function downloadTemplateEntidadMasivo(Request $request)
    {
        ini_set('memory_limit', '-1');
        $fecha=new \DateTime();
        $fecha_tk=new \DateTime();
        $fecha_tk->setTimestamp($request->get('tk'));
        if($fecha_tk->getTimestamp()>$fecha->getTimestamp())
        {

            $sw_tipo=$request->get('tipo');
            // ask the service for a excel object
            /*
             * {data: 1, label:'Cliente'},
               {data: 2, label:'Personas proveedoras de soya'},
               {data: 3, label:'Industrias'},
               {data: 4, label:'Asociaciones'}
             * */
            $file_name = "plantilla.xlsx";
            switch($sw_tipo)
            {
                case 1:
                case 2: $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject("./template/entidad_cliente_proveedor.xlsx");
                        $file_name = "plantilla_cliente_proveedor.xlsx";
                        break;
                case 3:
                case 4: $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject("./template/entidad_industria_asociacion.xlsx");
                        $file_name = "plantilla_industria_asociacion.xlsx";
                        break;
            }

            // create the writer
            $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel2007');
            // create the response
            $response = $this->get('phpexcel')->createStreamedResponse($writer);
            // adding headers
            $dispositionHeader = $response->headers->makeDisposition(
                ResponseHeaderBag::DISPOSITION_ATTACHMENT,
                $file_name
            );
            $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
            $response->headers->set('Pragma', 'public');
            $response->headers->set('Cache-Control', 'maxage=1');
            $response->headers->set('Content-Disposition', $dispositionHeader);

            return $response;
        }
        else
            return new Response("El enlace solo esta vigente por 5 minutos, ya pasaron mas de 5 minutos", 200);

    }
    /**
     * @Route("/downloadTemplateTransaccionMasivo")
     * @Method("GET")
     */
    public function downloadTemplateTransaccionMasivo(Request $request)
    {
        ini_set('memory_limit', '-1');
        $fecha=new \DateTime();
        $fecha_tk=new \DateTime();
        $fecha_tk->setTimestamp($request->get('tk'));
        if($fecha_tk->getTimestamp()>$fecha->getTimestamp())
        {
            $id_fase=$request->get('fase');
            $id_tipo_transaccion=$request->get('transaccion');
            $file_name = "plantilla.xlsx";
            switch ($id_fase)
            {
                case 3: switch ($id_tipo_transaccion)
                        {
                            case 1: //compra
                                    $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject("./template/template_industria_compra.xlsx"); $file_name = "industria_compra.xlsx"; break;
                            case 2: //venta
                                    $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject("./template/template_industria_ventas.xlsx"); $file_name = "industria_venta.xlsx"; break;
                            case 3: //produccion
                                    $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject("./template/template_industria_produccion.xlsx"); $file_name = "industria_produccion.xlsx"; break;
                            case 4: //acopio
                                    $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject("./template/template_industria_acopio.xlsx"); $file_name = "industria_acopio.xlsx"; break;
                            default: return new Response("No existe un archivo para este tipo", 200);
                        }
                    break;
                //comentado porque falta
                /*case 4: switch ($id_tipo_transaccion)
                        {
                            case 1: //compra
                                    $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject("./template/template_asociacion_compra.xlsx"); break;
                            case 3: //produccion
                                    $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject("./template/template_asociacion_compra.xlsx"); break;
                            default: return new Response("No existe un archivo para este tipo", 200);
                        }
                    break;*/
                default: return new Response("No existe un archivo para este tipo", 200);
            }

            // create the writer
            $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel2007');
            // create the response
            $response = $this->get('phpexcel')->createStreamedResponse($writer);
            // adding headers
            $dispositionHeader = $response->headers->makeDisposition(
                ResponseHeaderBag::DISPOSITION_ATTACHMENT,
                $file_name
            );
            $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
            $response->headers->set('Pragma', 'public');
            $response->headers->set('Cache-Control', 'maxage=1');
            $response->headers->set('Content-Disposition', $dispositionHeader);

            return $response;
        }
        else
            return new Response("El enlace solo esta vigente por 5 minutos, ya pasaron mas de 5 minutos", 200);

    }
}
