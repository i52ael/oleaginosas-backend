<?php

namespace ProcesosTransaccionalesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use ProcesosTransaccionalesBundle\Entity\RelacionEntidad;
use ProcesosTransaccionalesBundle\Entity\VentaDetalle;
use ProcesosTransaccionalesBundle\Manager\SIEXCOTCPDF;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use JMS\DiExtraBundle\Annotation as DI;
use Doctrine\ORM\EntityManager;
use ProcesosTransaccionalesBundle\Manager\TransaccionManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Lexik\Bundle\JWTAuthenticationBundle\TokenExtractor\AuthorizationHeaderTokenExtractor;


/**
 * @Security("is_granted('ROLE_USER')")
 * @Route("/To-Do")
 **/
class TodoController extends Controller
{
    protected $transaccionManager;

    /**
     * @DI\InjectParams({
     * "transaccionManager"=@DI\Inject("api.manager.transaccion")
     * })
     *
     */
    public function __construct(TransaccionManager $transaccionManager)
    {
        $this->transaccionManager=$transaccionManager;
    }

    /**
     * @Route("/addIssues")
     * @Method("POST")
     */
    public function addIssues(Request $request)
    {
        $extractor = new AuthorizationHeaderTokenExtractor(
            'Bearer',
            'Authorization'
        );
        $upload = $request->request->get('upload');
        $token = $extractor->extract($request);
        $data = $this->container->get('lexik_jwt_authentication.encoder')->decode($token);
        set_time_limit(3600);
        if($upload == 'null')
        {
            $result_img['nameFile'] = '';
            $result_img['pathFolder'] = '';
        }
        else
        {
            $result_img = $this->transaccionManager->uploadImgIssues($request->files->get('upload'), $data['idUser']);
            if( !empty($result_img['error']))
                return $this->json($result_img['error'], Response::HTTP_SEE_OTHER);
        }
        $this->transaccionManager->verificarToDo();

        $obs = $request->request->get('obs');
        $ruta = $request->request->get('ruta');

        $this->transaccionManager->addIssues($ruta, $result_img['nameFile'], $result_img['pathFolder'], $data['idUser'], $obs, 1);
        return $this->json(array('ok'=>1), Response::HTTP_OK);
    }
}
