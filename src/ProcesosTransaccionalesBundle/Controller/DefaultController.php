<?php

namespace ProcesosTransaccionalesBundle\Controller;

use ProcesosTransaccionalesBundle\Entity\RelacionEntidad;
use ProcesosTransaccionalesBundle\Entity\VentaDetalle;
use ProcesosTransaccionalesBundle\Manager\SIEXCOTCPDF;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use JMS\DiExtraBundle\Annotation as DI;
use Doctrine\ORM\EntityManager;
use ProcesosTransaccionalesBundle\Manager\TransaccionManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Lexik\Bundle\JWTAuthenticationBundle\TokenExtractor\AuthorizationHeaderTokenExtractor;

use ProductoBundle\Manager\EntidadManager;
use ProductoBundle\Manager\TipoEntidadManager;
use ProductoBundle\Manager\TipoDocumentoManager;
use ProductoBundle\Manager\PersonaManager;
use ProductoBundle\Manager\RelacionEntidadManager;
use ProductoBundle\Manager\RegimenManager;
use ProductoBundle\Manager\ReportesManager;


/**
 * @Security("is_granted('ROLE_USER')")
 * @Route("/procesos-transaccionales")
 * **/
class DefaultController extends Controller
{
    use \ProductoBundle\Helper\Helper;
    protected $transaccionManager;

    protected $entidadManager;
    protected $tipoEntidadManager;
    protected $tipoDocumentoManager;
    protected $personaManager;
    protected $relacionManager;
    protected $regimenManager;
    protected $reportesManager;
    /**
     * @DI\InjectParams({
     * "transaccionManager"=@DI\Inject("api.manager.transaccion"),
     * "entidadManager"=@DI\Inject("api.manager.entidad"),
     * "tipoDocumentoManager"=@DI\Inject("api.manager.tipoDocumento"),
     * "tipoEntidadManager"=@DI\Inject("api.manager.tipoEntidad"),
     * "personaManager"=@DI\Inject("api.manager.persona"),
     * "relacionEntidadManager"=@DI\Inject("api.manager.relacion"),
     * "regimenManager"=@DI\Inject("api.manager.regimen"),
     * "reportesManager" = @DI\Inject("api.manager.reportes")
     * })
     */
    public function __construct( TransaccionManager $transaccionManager, EntidadManager $entidadManager,TipoEntidadManager  $tipoEntidadManager,
                                TipoDocumentoManager $tipoDocumentoManager,PersonaManager $personaManager,
                                RelacionEntidadManager $relacionEntidadManager,RegimenManager $regimenManager, ReportesManager $reportesManager)
    {
        $this->transaccionManager=$transaccionManager;

        $this->entidadManager=$entidadManager;
        $this->tipoEntidadManager=$tipoEntidadManager;
        $this->tipoDocumentoManager= $tipoDocumentoManager;
        $this->personaManager=$personaManager;
        $this->relacionManager=$relacionEntidadManager;
        $this->regimenManager=$regimenManager;
        $this->reportesManager = $reportesManager;
    }

    /**
     * @Route("/findEntidadXNumeroIdentificacion")
     * @Method("POST")
     **/
    public function findEntidadXNumeroIdentificacion(Request  $request)
    {
        $arr=json_decode($request->getContent());
        $result=$this->transaccionManager->findEntidadXNumeroIdentificacion($arr[0]);
        if(!empty($result['error']))
            return $this->json($result['error'],Response::HTTP_NOT_FOUND);
        return $this->json($result,Response::HTTP_OK);
    }

    /**
     * @Route("/saveTransaccion")
     * @Method("POST")
     **/
    public function saveTransaccion(Request $request)
    {
        $arr_datos=json_decode($request->getContent());
        if(empty($arr_datos->body))
            return $this->json('Por favor verifique, se envio la transaccion sin items',Response::HTTP_SEE_OTHER);
        $arr=$this->transaccionManager->guardar($arr_datos->head,$arr_datos->body);
        if(!empty($arr['error']))
            return $this->json($arr['error'],Response::HTTP_SEE_OTHER);

        return $this->json($arr,Response::HTTP_OK);
    }

    /**
     * @Route("/findTipoTransacciones")
     * @Method("POST")
     **/
    public function findTipoTransacciones(Request $request)
    {
        $arr=json_decode($request->getContent());
        $result=$this->transaccionManager->findTipoTransaccion($arr[0]);
        if(!empty($result['error']))
            return $this->json($result['error'],Response::HTTP_SEE_OTHER);

        return $this->json($result,Response::HTTP_OK);
    }

    /**
     * @Route("/findTipoTransaccionesAndColumns")
     * @Method("POST")
     **/
    public function findTipoTransaccionesAndColumns(Request $request)
    {
        $arr=json_decode($request->getContent());
        $result=$this->transaccionManager->findTipoTransaccionesAndColumns($arr[0]);
        if(!empty($result['error']))
            return $this->json($result['error'],Response::HTTP_SEE_OTHER);

        return $this->json($result,Response::HTTP_OK);
    }

    /**
     * @Route("/findProductoXDescrip")
     * @Method("POST")
     **/
    public function findProductoXDescrip(Request $request)
    {
        $obj=json_decode($request->getContent());
        if($obj->id_tipo_transaccion==2)//cuando sea venta solo debe mostrar los subproductos propios de la industria
            $result=$this->transaccionManager->findProductoXDescripIdEntidad($obj->descrip, $obj->id_entidad, $obj->id_tipo_transaccion,$obj->id_fase);
        else
            $result=$this->transaccionManager->findProductoXDescrip($obj->descrip,$obj->id_tipo_transaccion,$obj->id_fase);
        if(!empty($result['error']))
            return $this->json($result['error'],Response::HTTP_SEE_OTHER);

        return $this->json($result,Response::HTTP_OK);

    }

    /**
     * @Route("/init")
     * @Method("POST")
     **/
    public function init(Request $request)//aqui hay que enviar el usuario o algo que identifique a la entidad y cargar todos los datos iniciales
    {
        $extractor = new AuthorizationHeaderTokenExtractor(
            'Bearer',
            'Authorization'
        );
        $token = $extractor->extract($request);
        $data= $this->container->get('lexik_jwt_authentication.encoder')->decode($token);
        if($data['tipo']=='f')//consultamos si es funcionario
        {
            $result['data']=$data;
        }
        else
        {
            $result=$this->transaccionManager->init($data['idEntidad']);
            if(!empty($result['error']))
                return $this->json($result['error'],Response::HTTP_SEE_OTHER);
            $result['data']=$data;
        }

        return $this->json($result,Response::HTTP_OK);
    }

    /**
     * @Route("/findTransaccion")
     * @Method("POST")
     **/
    public function findTransaccion(Request $request)
    {
        $obj_datos=json_decode($request->getContent());
        $result=$this->transaccionManager->findTransaccion($obj_datos);
        if(!empty($result['error']))
            return $this->json($result['error'],Response::HTTP_SEE_OTHER);
        return $this->json($result,Response::HTTP_OK);
    }

    /**
     * @Route("/deleteTransacion")
     * @Method("POST")
     **/
    public function deleteTransacion(Request $request)
    {
        $obj_datos=json_decode($request->getContent());
        $result=$this->transaccionManager->deleteTransaccion($obj_datos->id_transaccion);
        if(!empty($result['error']))
            return $this->json($result['error'],Response::HTTP_SEE_OTHER);

        return $this->json($result ,Response::HTTP_OK);
    }

    /**
     * @Route("/findTransaccionEdit")
     * @Method("POST")
     **/
    public function findTransaccionEdit(Request $request)
    {
        $obj_datos=json_decode($request->getContent());
        $result=$this->transaccionManager->findTransaccionEdit($obj_datos->id_transaccion);
        if(!empty($result['error']))
            return $this->json($result['error'],Response::HTTP_SEE_OTHER);

        return $this->json($result ,Response::HTTP_OK);
    }

    /**
     * @Route("/initCPeriodo")
     * @Method("POST")
     **/
    public function initCPeriodo(Request $request)
    {
        $obj_datos=json_decode($request->getContent());
        $result=$this->transaccionManager->initCPeriodo($obj_datos->id_fase);
        if(!empty($result))
            return $this->json($result,Response::HTTP_OK);
        else
            return $this->json('No se encontro los datos',Response::HTTP_NOT_FOUND);
    }

    /**
     * @Route("/initAddTransaccion")
     * @Method("POST")
     **/
    public function initAddTransaccion(Request $request)
    {
        $obj_datos=json_decode($request->getContent());
        $result=$this->transaccionManager->initAddTransaccion($obj_datos->id_relacion_entidad,$obj_datos->id_tipo_transaccion,$obj_datos->id_fase);
        if(!empty($result['error']))
            return $this->json($result['error'],Response::HTTP_SEE_OTHER);

        return $this->json($result,Response::HTTP_OK);

    }

    /**
     * @Route("/consultaXPeriodoFrecuencia")
     * @Method("POST")
     **/
    public function consultaXPeriodoFrecuencia(Request $request)
    {
        $obj_datos=json_decode($request->getContent());
        $row = $this->transaccionManager->findIdPeriodoFaseXFrecuencia($obj_datos->frecuencia, $obj_datos->fecha, $obj_datos->id_fase);
        if(!empty($row['error']))
            return $this->json($row['error'],Response::HTTP_SEE_OTHER);

        $result=$this->transaccionManager->consultaXPeriodo($obj_datos->id_relacion_entidad,$obj_datos->id_fase, null, $row['id']);
        if(!empty($result['error']))
            return $this->json($result['error'],Response::HTTP_SEE_OTHER);
        return $this->json($result,Response::HTTP_OK);
    }

    /**
     * @Route("/consultaXPeriodo")
     * @Method("POST")
     **/
    public function consultaXPeriodo(Request $request)
    {
        $obj_datos=json_decode($request->getContent());
        $result=$this->transaccionManager->consultaXPeriodo($obj_datos->id_relacion_entidad,$obj_datos->id_fase, $obj_datos->limit);
        if(!empty($result['error']))
            return $this->json($result['error'],Response::HTTP_SEE_OTHER);
        return $this->json($result,Response::HTTP_OK);
    }

    /**
     * @Route("/cbxEntidad")
     * @Method("POST")
     **/
    public function cbxEntidad(Request $request)
    {

        $obj_datos=json_decode($request->getContent());
        $result=$this->transaccionManager->cbxEntidad(null,$obj_datos->descrip);
        if(!empty($result['error']))
            return $this->json($result['error'],Response::HTTP_SEE_OTHER);
        return $this->json($result, Response::HTTP_OK);
    }

    /**
     * @Route("/imprimirTransacciones")
     * @Method("POST")
     **/
    public function imprimirTransaccion(Request $request)
    {
        $obj_datos=json_decode($request->getContent());
        return $this->imprimirTransacciones($obj_datos);
    }
    public function imprimirTransacciones($obj_datos)
    {
        if(!empty($obj_datos->datos->compra)
            || !empty($obj_datos->datos->venta)
            || !empty($obj_datos->datos->produccion)
            || !empty($obj_datos->datos->acopio))
        {
            $pdf = new SIEXCOTCPDF('L','cm');
            $pdf->SetAuthor("SIEXCO");
            $pdf->SetTitle("transacciones");
            $pdf->SetSubject("transacciones");
            $pdf->SetFont('times',''/*'BI'*/,8);
            $pdf->setCellHeightRatio(1);

            $fill=array('col1'=>211,'col2'=>47, 'col3'=>47);
            $textColor=array('col1'=>255,'col2'=>255,'col3'=>255);
            $font=array('family'=>'times','style'=>'BI','size'=>9);

            if(!empty($obj_datos->datos->compra))
            {
                $result = $this->transaccionManager->findTransaccionEdit(str_replace("CO", "", $obj_datos->datos->compra));
                if (!empty($result['error']))
                    return $this->json($result['error'], Response::HTTP_SEE_OTHER);

                $pdf->CellHead(array('w'=>1, 'h'=>0,'txt'=> '', 'border'=>0,'ln'=>1));
                $pdf->CellHead(array('w'=>1, 'h'=>0,'txt'=> '', 'border'=>0,'ln'=>1));
                $pdf->CellHead(array('w'=>1, 'h'=>0,'txt'=> '', 'border'=>0,'ln'=>1));
                $pdf->CellHead(array('w'=>27, 'h'=>0,'txt'=> 'REPORTE DE COMPRA', 'border'=>0,'ln'=>1,'align'=> 'C','font'=>$font));
                $pdf->CellHead(array('w'=>4, 'h'=>0,'txt'=> 'RAZON SOCIAL ', 'border'=>0,'ln'=>0,'align'=> 'R','font'=>$font));
                $pdf->CellHead(array('w'=>20.5, 'h'=>0,'txt'=> $result['head']['razon_social'], 'border'=>1,'ln'=>1,'font'=>$font));
                $pdf->CellHead(array('w'=>4, 'h'=>0,'txt'=> 'PERIODO ', 'border'=>0,'ln'=>0,'align'=> 'R','font'=>$font));
                $pdf->CellHead(array('w'=>20.5, 'h'=>0,'txt'=> $result['head']['descrip_periodo_fase'], 'border'=>1,'ln'=>1,'font'=>$font));

                $pdf->CellHead(array('w'=>3,'h'=>0, 'txt'=>'NIT/CI','border'=>1,'ln'=>0,'align'=>'','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
                $pdf->CellHead(array('w'=>7,'h'=>0, 'txt'=>'RAZON SOCIAL','border'=>1,'ln'=>0,'align'=>'','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
                $pdf->CellHead(array('w'=>3,'h'=>0, 'txt'=>'MES ACOPIO','border'=>1,'ln'=>0,'align'=>'','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));

                $pdf->CellHead(array('w'=>7,'h'=>0, 'txt'=>'PRODUCTO','border'=>1,'ln'=>0,'align'=>'','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
                $pdf->CellHead(array('w'=>2.5,'h'=>0, 'txt'=>'CANTIDAD','border'=>1,'ln'=>0,'align'=>'','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
                $pdf->CellHead(array('w'=>2.5,'h'=>0, 'txt'=>'PRECIO U.','border'=>1,'ln'=>0,'align'=>'','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
                $pdf->CellHead(array('w'=>2.5,'h'=>0, 'txt'=>'PRECIO TOTAL','border'=>1,'ln'=>1,'align'=>'','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));

                $pdf->AddPage();
                $precio_total=0;
                //for($i=0;$i<50;$i++)
                foreach($result['body'] as $row)
                {
                    $pdf->Cell(3,0,$row['numero_identificacion'],'LR');
                    $pdf->Cell(7,0,$row['razon_social'],'LR');
                    $pdf->Cell(3,0,$row['print_mes_acopio'],'LR');
                    $pdf->Cell(7,0,$row['descrip_producto'],'LR');
                    $pdf->Cell(2.5,0,$row['cantidad'],'LR',0,'R');
                    $pdf->Cell(2.5,0,$row['precio_unitario'],'LR',0,'R');
                    $pdf->Cell(2.5,0,$row['precio_total'],'LR',1,'R');
                    $precio_total+=$row['precio_total'];

                }

                $pdf->SetFont('times', 'B', 8);
                $pdf->Cell(20,0,'','T',0,'');
                $pdf->Cell(2.5,0,'','T',0,'R');
                $pdf->Cell(2.5,0,'TOTAL','T',0,'R');
                $pdf->Cell(2.5,0,$precio_total,'T',1,'R');
                $pdf->SetFont('times','',8);

            }

            if(!empty($obj_datos->datos->venta))
            {
                $result = $this->transaccionManager->findTransaccionEdit(str_replace("VE", "", $obj_datos->datos->venta));
                if (!empty($result['error']))
                    return $this->json($result['error'], Response::HTTP_SEE_OTHER);

                $pdf->CellHead(array('w'=>1, 'h'=>0,'txt'=> '', 'border'=>0,'ln'=>1),true );
                $pdf->CellHead(array('w'=>1, 'h'=>0,'txt'=> '', 'border'=>0,'ln'=>1));
                $pdf->CellHead(array('w'=>1, 'h'=>0,'txt'=> '', 'border'=>0,'ln'=>1));
                $pdf->CellHead(array('w'=>27, 'h'=>0,'txt'=> 'REPORTE DE VENTA', 'border'=>0,'ln'=>1,'align'=> 'C','font'=>$font));
                $pdf->CellHead(array('w'=>4, 'h'=>0,'txt'=> 'RAZON SOCIAL ', 'border'=>0,'ln'=>0,'align'=> 'R','font'=>$font));
                $pdf->CellHead(array('w'=>20.5, 'h'=>0,'txt'=> $result['head']['razon_social'], 'border'=>1,'ln'=>1,'font'=>$font));
                $pdf->CellHead(array('w'=>4, 'h'=>0,'txt'=> 'PERIODO ', 'border'=>0,'ln'=>0,'align'=> 'R','font'=>$font));
                $pdf->CellHead(array('w'=>20.5, 'h'=>0,'txt'=> $result['head']['descrip_periodo_fase'], 'border'=>1,'ln'=>1,'font'=>$font));

                $pdf->CellHead(array('w'=>3,'h'=>0, 'txt'=>'NIT/CI','border'=>1,'ln'=>0,'align'=>'','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
                $pdf->CellHead(array('w'=>7,'h'=>0, 'txt'=>'RAZON SOCIAL','border'=>1,'ln'=>0,'align'=>'','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
                $pdf->CellHead(array('w'=>7,'h'=>0, 'txt'=>'PRODUCTO','border'=>1,'ln'=>0,'align'=>'','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
                $pdf->CellHead(array('w'=>2.5,'h'=>0, 'txt'=>'NRO. FACTURA','border'=>1,'ln'=>0,'align'=>'','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
                $pdf->CellHead(array('w'=>2.5,'h'=>0, 'txt'=>'CANTIDAD','border'=>1,'ln'=>0,'align'=>'','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
                $pdf->CellHead(array('w'=>2.5,'h'=>0, 'txt'=>'PRECIO U.','border'=>1,'ln'=>0,'align'=>'','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
                $pdf->CellHead(array('w'=>2.5,'h'=>0, 'txt'=>'PRECIO TOTAL','border'=>1,'ln'=>1,'align'=>'','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));

                $pdf->AddPage();
                $precio_total=0;
                //for($i=0;$i<50;$i++)
                foreach($result['body'] as $row)
                {
                    $pdf->Cell(3,0,$row['numero_identificacion'],'LR');
                    $pdf->Cell(7,0,$row['razon_social'],'LR');
                    $pdf->Cell(7,0,$row['descrip_producto'],'LR');
                    $pdf->Cell(2.5,0,$row['nro_factura'],'LR',0,'R');
                    $pdf->Cell(2.5,0,$row['cantidad'],'LR',0,'R');
                    $pdf->Cell(2.5,0,$row['precio_unitario'],'LR',0,'R');
                    $pdf->Cell(2.5,0,$row['precio_total'],'LR',1,'R');
                    $precio_total+=$row['precio_total'];

                }
                $pdf->SetFont('times', 'B', 8);
                $pdf->Cell(17,0,'','T',0,'');
                $pdf->Cell(2.5,0,'','T',0,'R');
                $pdf->Cell(2.5,0,'','T',0,'R');
                $pdf->Cell(2.5,0,'TOTAL','T',0,'R');
                $pdf->Cell(2.5,0,$precio_total,'T',1,'R');
                $pdf->SetFont('times', '', 8);

            }

            if(!empty($obj_datos->datos->produccion))
            {

                $result = $this->transaccionManager->findTransaccionEdit(str_replace("PRO", "", $obj_datos->datos->produccion));
                if (!empty($result['error']))
                    return $this->json($result['error'], Response::HTTP_SEE_OTHER);

                $pdf->CellHead(array('w'=>1, 'h'=>0,'txt'=> '', 'border'=>0,'ln'=>1),true );
                $pdf->CellHead(array('w'=>1, 'h'=>0,'txt'=> '', 'border'=>0,'ln'=>1));
                $pdf->CellHead(array('w'=>1, 'h'=>0,'txt'=> '', 'border'=>0,'ln'=>1));
                $pdf->CellHead(array('w'=>27, 'h'=>0,'txt'=> 'REPORTE DE PRODUCCION', 'border'=>0,'ln'=>1,'align'=> 'C','font'=>$font));
                $pdf->CellHead(array('w'=>4, 'h'=>0,'txt'=> 'RAZON SOCIAL ', 'border'=>0,'ln'=>0,'align'=> 'R','font'=>$font));
                $pdf->CellHead(array('w'=>20.5, 'h'=>0,'txt'=> $result['head']['razon_social'], 'border'=>1,'ln'=>1,'font'=>$font));
                $pdf->CellHead(array('w'=>4, 'h'=>0,'txt'=> 'PERIODO ', 'border'=>0,'ln'=>0,'align'=> 'R','font'=>$font));
                $pdf->CellHead(array('w'=>20.5, 'h'=>0,'txt'=> $result['head']['descrip_periodo_fase'], 'border'=>1,'ln'=>1,'font'=>$font));

                $pdf->CellHead(array('w'=>7,'h'=>0, 'txt'=>'PRODUCTO','border'=>1,'ln'=>0,'align'=>'','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
                $pdf->CellHead(array('w'=>4,'h'=>0, 'txt'=>'CANTIDAD PRODUCCION','border'=>1,'ln'=>0,'align'=>'','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
                $pdf->CellHead(array('w'=>4,'h'=>0, 'txt'=>'CANTIDAD STOCK','border'=>1,'ln'=>1,'align'=>'','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));

                $pdf->AddPage();

                //for($i=0;$i<50;$i++)
                foreach($result['body'] as $row)
                {
                    $pdf->Cell(7,0,$row['descrip_producto'],'LR');
                    $pdf->Cell(4,0,$row['cantidad'],'LR',0,'R');
                    $pdf->Cell(4,0,$row['cantidad_stock'],'LR',1,'R');
                    //$pdf->Ln();

                }
                $pdf->Cell(7,0,'','T',0,'');
                $pdf->Cell(4,0,'','T',0,'R');
                $pdf->Cell(4,0,'','T',1,'R');

            }

            if(!empty($obj_datos->datos->acopio))
            {
                $result = $this->transaccionManager->findTransaccionEdit(str_replace("ACO", "", $obj_datos->datos->acopio));
                if (!empty($result['error']))
                    return $this->json($result['error'], Response::HTTP_SEE_OTHER);

                $pdf->CellHead(array('w'=>1, 'h'=>0,'txt'=> '', 'border'=>0,'ln'=>1), true);
                $pdf->CellHead(array('w'=>1, 'h'=>0,'txt'=> '', 'border'=>0,'ln'=>1));
                $pdf->CellHead(array('w'=>1, 'h'=>0,'txt'=> '', 'border'=>0,'ln'=>1));
                $pdf->CellHead(array('w'=>27, 'h'=>0,'txt'=> 'REPORTE DE ACOPIO', 'border'=>0,'ln'=>1,'align'=> 'C','font'=>$font));
                $pdf->CellHead(array('w'=>4, 'h'=>0,'txt'=> 'RAZON SOCIAL ', 'border'=>0,'ln'=>0,'align'=> 'R','font'=>$font));
                $pdf->CellHead(array('w'=>20.5, 'h'=>0,'txt'=> $result['head']['razon_social'], 'border'=>1,'ln'=>1,'font'=>$font));
                $pdf->CellHead(array('w'=>4, 'h'=>0,'txt'=> 'PERIODO ', 'border'=>0,'ln'=>0,'align'=> 'R','font'=>$font));
                $pdf->CellHead(array('w'=>20.5, 'h'=>0,'txt'=> $result['head']['descrip_periodo_fase'], 'border'=>1,'ln'=>1,'font'=>$font));

                $pdf->CellHead(array('w'=>3,'h'=>0, 'txt'=>'NIT/CI','border'=>1,'ln'=>0,'align'=>'','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
                $pdf->CellHead(array('w'=>7,'h'=>0, 'txt'=>'RAZON SOCIAL','border'=>1,'ln'=>0,'align'=>'','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
                $pdf->CellHead(array('w'=>7,'h'=>0, 'txt'=>'PRODUCTO','border'=>1,'ln'=>0,'align'=>'','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
                $pdf->CellHead(array('w'=>3.5,'h'=>0, 'txt'=>'CANTIDAD BRUTA','border'=>1,'ln'=>0,'align'=>'','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
                $pdf->CellHead(array('w'=>3.5,'h'=>0, 'txt'=>'CANTIDAD NETA.','border'=>1,'ln'=>0,'align'=>'','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
                $pdf->CellHead(array('w'=>3.5,'h'=>0, 'txt'=>'CANTIDAD LIQUIDO','border'=>1,'ln'=>1,'align'=>'','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));

                $pdf->AddPage();//la funcion de adicionar pagina debe ir siempre despues de los CellHead porque si no se desconfigura

                //for($i=0;$i<50;$i++)
                foreach($result['body'] as $row)
                {
                    $pdf->Cell(3,0,$row['numero_identificacion'],'LR');
                    $pdf->Cell(7,0,$row['razon_social'],'LR');
                    $pdf->Cell(7,0,$row['descrip_producto'],'LR');
                    $pdf->Cell(3.5,0,$row['cantidad_bruta'],'LR',0,'R');
                    $pdf->Cell(3.5,0,$row['cantidad_neta'],'LR',0,'R');
                    $pdf->Cell(3.5,0,$row['cantidad_liquido'],'LR',1,'R');

                }
                $pdf->SetFont('times', 'B', 8);
                $pdf->Cell(17,0,'','T',0,'');
                $pdf->Cell(3.5,0,'','T',0,'R');
                $pdf->Cell(3.5,0,'','T',0,'R');
                $pdf->Cell(3.5,0,'','T',1,'R');
                $pdf->SetFont('times','',8);

            }

            $str=$pdf->Output('reporte.pdf','S');
            return new Response($str, 200, array('Content-Type' => 'application/pdf'));
        }
        else
            return $this->json('No existen transacciones',Response::HTTP_SEE_OTHER);

    }

    /**
     * @Route("/generarPeriodoFase")
     * @Method("POST")
     **/
    public function generarPeriodoFase(Request $request)
    {
        $obj_datos = json_decode($request->getContent());
        $result = $this->transaccionManager->generarPeriodoFase($obj_datos->gestion, $obj_datos->id_fase, $obj_datos->id_periodo );
        if(!empty($result['error']))
            return $this->json($result['error'], Response::HTTP_SEE_OTHER);
        return $this->json($result, Response::HTTP_OK);
    }

    /**
     * @Route("/cbxFechaAcopio")
     * @Method("POST")
     **/
    public function cbxFechaAcopio()
    {
        $result = $this->transaccionManager->cbxFechaAcopio();
        if(!empty($result['error']))
            return $this->json($result['error'], Response::HTTP_SEE_OTHER);
        return $this->json($result, Response::HTTP_OK);
    }

    /**
     * @Route("/uploadXls")
     * @Method("POST")
     **/
    public function uploadXls(Request $request)
    {
        $id_entidad=$request->request->get('id_entidad');
        $id_tipo_transaccion=$request->request->get('id_tipo_transaccion');
        $id_periodo_fase=$request->request->get('id_periodo_fase');

        $extractor = new AuthorizationHeaderTokenExtractor(
            'Bearer',
            'Authorization'
        );
        $token = $extractor->extract($request);
        $data= $this->container->get('lexik_jwt_authentication.encoder')->decode($token);

        $arr_upload=$this->transaccionManager->uploadExcel($id_entidad, $id_tipo_transaccion,$id_periodo_fase ,$data['idUser'],$request->files->get('uploads'));
        if(!empty($arr_upload['error']))
            return $this->json($arr_upload['error'], Response::HTTP_SEE_OTHER);

        $result=$this->transaccionManager->importarDatosExcel($id_entidad,
                                                            $id_tipo_transaccion,
                                                            $id_periodo_fase,
                                                            $this->get('phpexcel')->createPHPExcelObject($arr_upload['pathFolder'].'/'.$arr_upload['nameFile']));
        if(!empty($result['error']))
            return $this->json($result['error'], Response::HTTP_SEE_OTHER);
        if(!empty($result['sw_error']))//esto es para ver si borramos el archivo subido
        {
            $arr_delete=$this->transaccionManager->deleteUpload($arr_upload['id_archivo_excel']);
            if(!empty($arr_delete['error']))
                return $this->json($arr_delete['error'], Response::HTTP_SEE_OTHER);
        }
        return $this->json(array('arr'=>$result['arr'],'sw_error'=>$result['sw_error']), Response::HTTP_OK);
    }

    /**
     * @Route("/getFileLoadData")
     * @Method("POST")
     **/
    public function getFileLoadData(Request $request)
    {
        $obj_data = json_decode($request->getContent());
        return  $this->json($this->transaccionManager->getFileLoadData($obj_data->id_entidad, $obj_data->id_periodo_fase), Response::HTTP_OK);
    }

    /**
     * @Route("/importarDatosExcel")
     * @Method("POST")
     **/
    public function importarDatosExcel(Request $request)
    {
        $obj_datos = json_decode($request->getContent());
        //$obj_datos->id_archivo_excel;

        $result=$this->transaccionManager->importarDatosExcel($obj_datos->id_entidad,
                                                            $obj_datos->id_tipo_transaccion,
                                                            $obj_datos->id_periodo_fase,
                                                            $this->get('phpexcel')->createPHPExcelObject($obj_datos->pathFolder.'/'.$obj_datos->nameFile));
        if(!empty($result['error']))
            return $this->json($result['error'], Response::HTTP_SEE_OTHER);
        return $this->json(array('arr'=>$result['arr']), Response::HTTP_OK);
    }

    /**
     * @Route("/cambiarValidado")
     * @Method("POST")
     **/
    public function cambiarValidado(Request $request)
    {
        $obj_datos = json_decode($request->getContent());

        $result=$this->transaccionManager->cambiarValidado($obj_datos->id_transaccion);
        if(!empty($result['error']))
            return $this->json($result['error'], Response::HTTP_SEE_OTHER);

        return $this->json($result, Response::HTTP_OK);
    }

    /**
     * @Route("/reporteControlBanda")
     * @Method("POST")
     **/
    public function reporteControlBanda(Request $request)
    {
        $obj_datos = json_decode($request->getContent());
        $result=$this->transaccionManager->reporteControlBanda($obj_datos->id_moneda,$obj_datos->id_entidad,$obj_datos->id_producto,$obj_datos->desde,$obj_datos->hasta);
        if(!empty($result['error']))
            return $this->json($result['error'], Response::HTTP_SEE_OTHER);

        return $this->json($result, Response::HTTP_OK);
    }

    /**
     * @Route("/cbxMoneda")
     * @Method("POST")
     **/
    public function cbxMoneda()
    {
        $result=$this->transaccionManager->cbxMoneda();
        if(!empty($result['error']))
            return $this->json($result['error'], Response::HTTP_SEE_OTHER);

        return $this->json($result, Response::HTTP_OK);
    }
    /**
     * @Route("/exportarXlsSaldoExportable")
     * @Method("POST")
     */
    public function exportarXlsSaldoExportable(Request $request){
        $path = $request->server->get('DOCUMENT_ROOT').$request->getBasePath();
        $inicio_mes = $request->request->get('inicio_mes');
        $inicio_gest =  $request->request->get('inicio_gest');
        $fin_mes = $request->request->get('fin_mes');
        $fin_gest = $request->request->get('fin_gest');
        $id_producto = $request->request->get('id_producto');
        $id_entidad = $request->request->get('id_entidad');

        $resultado = $this->reportesManager->calculaValoresSaldoExportable($inicio_mes, $inicio_gest, $fin_mes, $fin_gest, $id_producto, $id_entidad);

        $datos_servicio = $this->reportesManager->resumenSaldoExportable($inicio_mes, $inicio_gest, $fin_mes, $fin_gest, $id_producto, $id_entidad);

        $result = $this->transaccionManager->exportarXlsSaldosExportables($this->get('phpexcel'), $path,
                                            $inicio_mes, $inicio_gest, $fin_mes, $fin_gest,
                                            $id_producto, $id_entidad, $resultado, $datos_servicio,
                                            $this->reportesManager);

        return $this->json($result, Response::HTTP_OK); 
    }
    /**
     * @Route("/uploadXlsEntidad")
     * @Method("POST")
     **/
    public function uploadXlsEntidad(Request $request)
    {
        set_time_limit(3600*2);
        $user=$this->ObtenerUser($request);
        // @param $sw_tipo_entidad 1 = cliente, 2 = proveedor, 3 = industria
        $id_entidad = $request->request->get('id_entidad');
        $idTipoEntidad = $request->request->get('id_tipo_entidad');
        $sw_tipo_entidad = $request->request->get('sw_tipo_entidad');
        $rango_inicio = $request->request->get('rango_inicio');
        $rango_fin = $request->request->get('rango_fin');
        $extractor = new AuthorizationHeaderTokenExtractor(
            'Bearer',
            'Authorization'
        );
        $token = $extractor->extract($request);
        $data= $this->container->get('lexik_jwt_authentication.encoder')->decode($token);
        $arr_upload=$this->transaccionManager->uploadExcelEntidad($id_entidad,$data['idUser'],$request->files->get('uploads'));
        if(!empty($arr_upload['error']))
            return $this->json($arr_upload['error'], Response::HTTP_SEE_OTHER);
        switch ($sw_tipo_entidad)
        {
            case 1: //cliente
            case 2: //proveedor
                    $result = $this->transaccionManager->importarDatosExcelClienteProveedor($this->get('phpexcel')->createPHPExcelObject($arr_upload['pathFolder'].'/'.$arr_upload['nameFile']),
                                                                                   $this->entidadManager,
                                                                                   $this->tipoEntidadManager,
                                                                                   $this->tipoDocumentoManager,
                                                                                   $this->personaManager,
                                                                                   $this->relacionManager,
                                                                                   $this->regimenManager,
                                                                                   $user,
                                                                                    (($sw_tipo_entidad==1)?4:2),
                                                                                   $rango_inicio,
                                                                                   $rango_fin );// idFase = 4 comercializacion;  idFase = 2 primaria
                    if(!empty($result['error']))
                        return $this->json($result['error'], Response::HTTP_SEE_OTHER);

                    break;
            case 3:
            case 4: $result = $this->transaccionManager->importarDatosExcelIndustriasAsociaciones($this->get('phpexcel')->createPHPExcelObject($arr_upload['pathFolder'].'/'.$arr_upload['nameFile']),
                                                                                    $this->entidadManager,
                                                                                    $this->tipoEntidadManager,
                                                                                    $this->tipoDocumentoManager,
                                                                                    $this->personaManager,
                                                                                    $this->relacionManager,
                                                                                    $this->regimenManager,
                                                                                    $user,
                                                                                    (($sw_tipo_entidad==3)?3:4),
                                                                                    $idTipoEntidad,
                                                                                    $rango_inicio,
                                                                                    $rango_fin);
                    if(!empty($result['error']))
                        return $this->json($result['error'], Response::HTTP_SEE_OTHER);
                    break;
            default: return $this->json("No existe la opcion para el 'tipo de entidad' seleccionado", Response::HTTP_SEE_OTHER);
        }


        return $this->json(array('arr'=>$arr_upload), Response::HTTP_OK);

/*
        $result=$this->transaccionManager->importarDatosExcel($id_entidad,
            $id_tipo_transaccion,
            $id_periodo_fase,
            $this->get('phpexcel')->createPHPExcelObject($arr_upload['pathFolder'].'/'.$arr_upload['nameFile']));
        if(!empty($result['error']))
            return $this->json($result['error'], Response::HTTP_SEE_OTHER);
        if(!empty($result['sw_error']))//esto es para ver si borramos el archivo subido
        {
            $arr_delete=$this->transaccionManager->deleteUpload($arr_upload['id_archivo_excel']);
            if(!empty($arr_delete['error']))
                return $this->json($arr_delete['error'], Response::HTTP_SEE_OTHER);
        }
*/
        return $this->json(array('arr'=>$result['arr'],'sw_error'=>$result['sw_error']), Response::HTTP_OK);
    }
    /**
     * @Route("/uploadXlsTransaction")
     * @Method("POST")
     **/
    public function uploadXlsTransaction(Request $request)
    {
        set_time_limit(3600*2);
        $user=$this->ObtenerUser($request);
        $id_entidad = $request->request->get('id_entidad');
        $id_fase = $request->request->get('id_fase');
        $id_tipo_transaccion = $request->request->get('id_tipo_transaccion');
        $extractor = new AuthorizationHeaderTokenExtractor(
            'Bearer',
            'Authorization'
        );
        $token = $extractor->extract($request);
        $data= $this->container->get('lexik_jwt_authentication.encoder')->decode($token);
        $arr_upload=$this->transaccionManager->uploadExcelEntidad($id_entidad,$data['idUser'],$request->files->get('uploads'));
        if(!empty($arr_upload['error']))
            return $this->json($arr_upload['error'], Response::HTTP_SEE_OTHER);
        switch ($id_tipo_transaccion)
        {
            case 1: //COMRA
                $result = $this->transaccionManager->importarDatosExcelTransactionCompra($id_tipo_transaccion, $this->get('phpexcel')->createPHPExcelObject($arr_upload['pathFolder'].'/'.$arr_upload['nameFile']));
                if(!empty($result['error']))
                    return $this->json($result['error'], Response::HTTP_SEE_OTHER);
                break;
            case 2: //VENTA
                $result = $this->transaccionManager->importarDatosExcelTransactionVenta($id_tipo_transaccion, $this->get('phpexcel')->createPHPExcelObject($arr_upload['pathFolder'].'/'.$arr_upload['nameFile']));
                if(!empty($result['error']))
                    return $this->json($result['error'], Response::HTTP_SEE_OTHER);

                break;
            case 3: //PRODUCCION
                $result = $this->transaccionManager->importarDatosExcelTransactionProduccion($id_tipo_transaccion, $this->get('phpexcel')->createPHPExcelObject($arr_upload['pathFolder'].'/'.$arr_upload['nameFile']));
                if(!empty($result['error']))
                    return $this->json($result['error'], Response::HTTP_SEE_OTHER);

                break;
            case 4: //ACOPIO
                $result = $this->transaccionManager->importarDatosExcelTransactionAcopio($id_tipo_transaccion, $this->get('phpexcel')->createPHPExcelObject($arr_upload['pathFolder'].'/'.$arr_upload['nameFile']));
                if(!empty($result['error']))
                    return $this->json($result['error'], Response::HTTP_SEE_OTHER);
                break;

            default: return $this->json("No existe la opcion para el 'tipo de entidad' seleccionado", Response::HTTP_SEE_OTHER);
        }


        return $this->json(array('arr'=>$arr_upload), Response::HTTP_OK);

        //return $this->json(array('arr'=>$result['arr'],'sw_error'=>$result['sw_error']), Response::HTTP_OK);
    }
    /**
     * @Route("/getRangoModulos")
     * @Method("POST")
     **/
    public function getRangoModulos(Request $request)
    {
        $id_modulo=$request->request->get('id_modulo');

        $result = $this->transaccionManager->getRangoModulos($id_modulo);
        if(!empty($result['error']))
           return $this->json($result['error'], Response::HTTP_SEE_OTHER);

        return $this->json($result, Response::HTTP_OK);
    }
    /**
     * @Route("/monitoreoTransacciones")
     * @Method("POST")
     **/
    public function monitoreoTransacciones(Request $request) {
        $id_fase = $request->request->get('id_fase');
        $fecha = $request->request->get('fecha');
        $frecuencia = $request->request->get('frecuencia');
        $fromRow = $request->request->get('fromRow');
        $pageSize = $request->request->get('pageSize');
        $result = $this->transaccionManager->monitoreoTransacciones($frecuencia, $fecha, $id_fase, $fromRow, $pageSize);
        if(!empty($result['error']))
            return $this->json($result['error'], Response::HTTP_SEE_OTHER);

        return $this->json($result, Response::HTTP_OK);
    }
    /**
     * @Route("/consultaFrecuencia")
     * @Method("POST")
     **/
    public function consultaFrecuencia(Request $request) {
        $id_fase = $request->request->get('id_fase');
        $result = $this->transaccionManager->consultaFrecuencia($id_fase);
        if(!empty($result['error']))
            return $this->json($result['error'], Response::HTTP_SEE_OTHER);

        return $this->json($result, Response::HTTP_OK);
    }
}
