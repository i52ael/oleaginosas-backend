<?php
namespace ProcesosTransaccionalesBundle\Controller;

use ProcesosTransaccionalesBundle\Manager\RipManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use JMS\DiExtraBundle\Annotation as DI;
use ProcesosTransaccionalesBundle\Manager\TransaccionManager;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Lexik\Bundle\JWTAuthenticationBundle\TokenExtractor\AuthorizationHeaderTokenExtractor;

use ProductoBundle\Manager\EntidadManager;
use ProcesosTransaccionalesBundle\Manager\ToolsManager;
use ProcesosTransaccionalesBundle\Manager\SIEXCOTCPDF;


/**
 * @Security("is_granted('ROLE_USER')")
 * @Route("/informacionProductiva")
 * **/
class RipController extends Controller {

    use \ProductoBundle\Helper\Helper;
    protected $transaccionManager;
    protected $ripManager;
    /**
     * @DI\InjectParams({
     * "transaccionManager"=@DI\Inject("api.manager.transaccion"),
     * "ripManager"=@DI\Inject("api.manager.rip")
     * })
     */
    public function __construct( TransaccionManager $transaccionManager,RipManager  $ripManager){
        $this->transaccionManager=$transaccionManager;
        $this->ripManager=$ripManager;
    }

    /**
     * @Route("/listaInformacionProductiva")
     * @Method("POST")
     **/
    public function listaInformacionProductiva(Request  $request){
        $obj_datos=json_decode($request->getContent());
        set_time_limit(3600*2);
        $user=$this->ObtenerUser($request);
        $result = $this->ripManager->listaInformacionProductiva($obj_datos);
        if(!empty($result['error'])){
            return $this->json($result['error'], Response::HTTP_SEE_OTHER);
        }
	    return $this->json($result,Response::HTTP_OK);
    }

    /**
     * @Route("/cbxTipo")
     * @Method("POST")
     **/
    public function cbxTipo(Request  $request)
    {
        $obj_datos = json_decode($request->getContent());
        $result = $this->ripManager->cbxTipo($obj_datos->tipo);
        if(!empty($result['error']))
            return $this->json($result['error'], Response::HTTP_SEE_OTHER);

        return $this->json($result, Response::HTTP_OK);
    }

    /**
     * @Route("/guardarCentroAlmacenamiento")
     * @Method("POST")
     **/
    public function guardarCentroAlmacenamiento(Request  $request){
        $obj_datos=json_decode($request->getContent());
        set_time_limit(3600*2);
        $user=$this->ObtenerUser($request);
        $id_centro = $this->ripManager->guardarCentroAlmacenamiento($obj_datos);
        if(!empty($result['error'])){
            return $this->json($result['error'], Response::HTTP_SEE_OTHER);
        }
        $obj_datos->id = $id_centro['id'];
        $toolsManager = new ToolsManager();
        $result = $toolsManager->objToArray($obj_datos);
        return $this->json($result,Response::HTTP_OK);
    }

    /**
     * @Route("/listaCentroAlmacenamiento")
     * @Method("POST")
     **/
    public function listaCentroAlmacenamiento(Request  $request){
        $obj_datos=json_decode($request->getContent());
        set_time_limit(3600*2);
        $user=$this->ObtenerUser($request);
        $result = $this->ripManager->listaCentroAlmacenamiento($obj_datos);
        if(!empty($result['error'])){
            return $this->json($result['error'], Response::HTTP_SEE_OTHER);
        }
	    return $this->json($result,Response::HTTP_OK);
    }

    /**
     * @Route("/eliminarCentroAlmacenamiento")
     * @Method("POST")
     **/
    public function eliminarCentroAlmacenamiento(Request  $request){
        $obj_datos=json_decode($request->getContent());
        set_time_limit(3600*2);
        $user=$this->ObtenerUser($request);
        $result = $this->ripManager->eliminarCentroAlmacenamiento($obj_datos);
        if(!empty($result['error'])){
            return $this->json($result['error'], Response::HTTP_SEE_OTHER);
        }
	    return $this->json($result,Response::HTTP_OK);
    }

    /**
     * @Route("/guardarLineaProduccion")
     * @Method("POST")
     **/
    public function guardarLineaProduccion(Request  $request){
        $obj_datos=json_decode($request->getContent());
        set_time_limit(3600*2);
        $user=$this->ObtenerUser($request);
        $result = $this->ripManager->guardarLineaProduccion($obj_datos);
        if(!empty($result['error'])){
            return $this->json($result['error'], Response::HTTP_SEE_OTHER);
        }
        $toolsManager = new ToolsManager();
        $obj_datos->id = $result['id'];
        $result = $toolsManager->objToArray($obj_datos);
        return $this->json($result,Response::HTTP_OK);
    }

    /**
     * @Route("/eliminarLineaProduccion")
     * @Method("POST")
     **/
    public function eliminarLineaProduccion(Request  $request){
        $obj_datos=json_decode($request->getContent());
        set_time_limit(3600*2);
        $user=$this->ObtenerUser($request);
        $result = $this->ripManager->eliminarLineaProduccion($obj_datos);
        if(!empty($result['error'])){
            return $this->json($result['error'], Response::HTTP_SEE_OTHER);
        }
	    return $this->json($result,Response::HTTP_OK);
    }

    /**
     * @Route("/listarLineaProduccion")
     * @Method("POST")
     **/
    public function listarLineaProduccion(Request  $request){
        $obj_datos=json_decode($request->getContent());
        set_time_limit(3600*2);
        $user=$this->ObtenerUser($request);
        $result = $this->ripManager->listarLineaProduccion($obj_datos);
        if(!empty($result['error'])){
            return $this->json($result['error'], Response::HTTP_SEE_OTHER);
        }
        return $this->json($result,Response::HTTP_OK);
    }

    /**
     * @Route("/guardarAlmacenamiento")
     * @Method("POST")
     **/
    public function guardarAlmacenamiento(Request  $request){
        $obj_datos=json_decode($request->getContent());
        set_time_limit(3600*2);
        $user=$this->ObtenerUser($request);
        $result = $this->ripManager->guardarAlmacenamiento($obj_datos,$user);
        if(!empty($result['error'])){
            return $this->json($result['error'], Response::HTTP_SEE_OTHER);
        }
        $obj_datos->id = $result['id'];
        $toolsManager = new ToolsManager();
        $result = $toolsManager->objToArray($obj_datos);
        return $this->json($result,Response::HTTP_OK);

        // return $this->json($result,Response::HTTP_OK);
    }

    /**
     * @Route("/listarAlmacenamiento")
     * @Method("POST")
     **/
    public function listarAlmacenamiento(Request  $request){
        $obj_datos=json_decode($request->getContent());
        set_time_limit(3600*2);
        $user=$this->ObtenerUser($request);
        $result = $this->ripManager->listarAlmacenamiento($obj_datos);
        if(!empty($result['error'])){
            return $this->json($result['error'], Response::HTTP_SEE_OTHER);
        }
        return $this->json($result,Response::HTTP_OK);
    }

    /**
     * @Route("/guardarCapacidad")
     * @Method("POST")
     **/
    public function guardarCapacidad(Request  $request){
        $obj_datos=json_decode($request->getContent());
        set_time_limit(3600*2);
        $user=$this->ObtenerUser($request);
        $result = $this->ripManager->guardarCapacidad($obj_datos,$user);
        if(!empty($result['error'])){
            return $this->json($result['error'], Response::HTTP_SEE_OTHER);
        }
        $obj_datos->id = $result['id'];
        $toolsManager = new ToolsManager();
        $result = $toolsManager->objToArray($obj_datos);
        return $this->json($result,Response::HTTP_OK);
    }

    /**
     * @Route("/eliminarCapacidad")
     * @Method("POST")
     **/
    public function eliminarCapacidad(Request  $request){
        $obj_datos=json_decode($request->getContent());
        // set_time_limit(3600*2);
        // $user=$this->ObtenerUser($request);
        $result = $this->ripManager->eliminarCapacidad($obj_datos);
        if(!empty($result['error'])){
            return $this->json($result['error'], Response::HTTP_SEE_OTHER);
        }
        return $this->json($result,Response::HTTP_OK);
    }

    /**
     * @Route("/listarCapacidad")
     * @Method("POST")
     **/
    public function listarCapacidad(Request  $request){
        $obj_datos=json_decode($request->getContent());
        set_time_limit(3600*2);
        $user=$this->ObtenerUser($request);
        $result = $this->ripManager->listarCapacidad($obj_datos, isset($obj_datos->sw)?$obj_datos->sw:false);
        if(!empty($result['error'])){
            return $this->json($result['error'], Response::HTTP_SEE_OTHER);
        }
        return $this->json($result,Response::HTTP_OK);
    }

    /**
     * @Route("/guardarMantenimiento")
     * @Method("POST")
     **/
    public function guardarMantenimiento(Request  $request){
        $obj_datos=json_decode($request->getContent());
        set_time_limit(3600*2);
        $user=$this->ObtenerUser($request);
        $result = $this->ripManager->guardarMantenimiento($obj_datos,$user);
        if(!empty($result['error'])){
            return $this->json($result['error'], Response::HTTP_SEE_OTHER);
        }
        $obj_datos->id = $result['id'];
        $toolsManager = new ToolsManager();
        $result = $toolsManager->objToArray($obj_datos);
        return $this->json($result,Response::HTTP_OK);
    }

    /**
     * @Route("/eliminarMantenimiento")
     * @Method("POST")
     **/
    public function eliminarMantenimiento(Request  $request){
        $obj_datos=json_decode($request->getContent());
        $result = $this->ripManager->eliminarMantenimiento($obj_datos);
        if(!empty($result['error'])){
            return $this->json($result['error'], Response::HTTP_SEE_OTHER);
        }
        return $this->json($result,Response::HTTP_OK);
    }

    /**
     * @Route("/listarMantenimiento")
     * @Method("POST")
     **/
    public function listarMantenimiento(Request  $request){
        $obj_datos=json_decode($request->getContent());
        set_time_limit(3600*2);
        $user=$this->ObtenerUser($request);
        $result = $this->ripManager->listarMantenimiento($obj_datos);
        if(!empty($result['error'])){
            return $this->json($result['error'], Response::HTTP_SEE_OTHER);
        }
        return $this->json($result,Response::HTTP_OK);
    }

    /**
     * @Route("/guardarFactoresTransformacion")
     * @Method("POST")
     **/
    public function guardarFactoresTransformacion(Request  $request){
        $obj_datos=json_decode($request->getContent());
        set_time_limit(3600*2);
        $user=$this->ObtenerUser($request);
        $result = $this->ripManager->guardarFactoresTransformacion($obj_datos,$user);
        if(!empty($result['error'])){
            return $this->json($result['error'], Response::HTTP_SEE_OTHER);
        }
        $obj_datos->id = $result['id'];
        $toolsManager = new ToolsManager();
        $result = $toolsManager->objToArray($obj_datos);
        return $this->json($result,Response::HTTP_OK);
    }

    /**
     * @Route("/eliminarFactoresTransformacion")
     * @Method("POST")
     **/
    public function eliminarFactoresTransformacion(Request  $request){
        $obj_datos=json_decode($request->getContent());
        $result = $this->ripManager->eliminarFactoresTransformacion($obj_datos);
        if(!empty($result['error'])){
            return $this->json($result['error'], Response::HTTP_SEE_OTHER);
        }
        return $this->json($result,Response::HTTP_OK);
    }

    /**
     * @Route("/listarFactoresTransformacion")
     * @Method("POST")
     **/
    public function listarFactoresTransformacion(Request  $request){
        $obj_datos=json_decode($request->getContent());
        set_time_limit(3600*2);
        $user=$this->ObtenerUser($request);
        $result = $this->ripManager->listarFactoresTransformacion($obj_datos);
        if(!empty($result['error'])){
            return $this->json($result['error'], Response::HTTP_SEE_OTHER);
        }
        return $this->json($result,Response::HTTP_OK);
    }

    /**
     * @Route("/cbxCentroAlmacenamiento")
     * @Method("POST")
     **/
    public function cbxCentroAlmacenamiento(Request  $request){
        $obj_datos=json_decode($request->getContent());
        set_time_limit(3600*2);
        $user=$this->ObtenerUser($request);
        $result = $this->ripManager->cbxCentroAlmacenamiento($obj_datos, isset($obj_datos->sw)?$obj_datos->sw:false);
        if(!empty($result['error'])){
            return $this->json($result['error'], Response::HTTP_SEE_OTHER);
        }
        $toolsManager = new ToolsManager();
        $result = $toolsManager->objToArray($result);
        return $this->json($result,Response::HTTP_OK);
    }

    /**
     * @Route("/dgCentroAlmacenamiento")
     * @Method("POST")
     **/
    public function dgCentroAlmacenamiento(Request  $request){
        $obj_datos=json_decode($request->getContent());
        set_time_limit(3600*2);
        $result = $this->ripManager->dgCentroAlmacenamiento($obj_datos);
        if(!empty($result['error'])){
            return $this->json($result['error'], Response::HTTP_SEE_OTHER);
        }
        $toolsManager = new ToolsManager();
        $result = $toolsManager->objToArray($result);
        return $this->json($result,Response::HTTP_OK);
    }

    /**
     * @Route("/guardarInfoProductiva")
     * @Method("POST")
     **/
    public function guardarInfoProductiva(Request  $request)
    {
        $obj_datos=json_decode($request->getContent());
        set_time_limit(3600*2);
        $user=$this->ObtenerUser($request);
        $result = $this->ripManager->guardarInfoProductiva($obj_datos, $user);
        if(!empty($result['error'])){
            return $this->json($result['error'], Response::HTTP_SEE_OTHER);
        }
        $toolsManager = new ToolsManager();
        $result = $toolsManager->objToArray($result);
        return $this->json($result,Response::HTTP_OK);
    }

    /**
     * @Route("/imprimir")
     * @Method("POST")
     */
    public function imprimir(Request $request) 
    {

        $obj_datos=json_decode($request->getContent());        

        $arr_entidad = $this->ripManager->getEntidad($obj_datos);
        if(!empty($arr['error']))
            return $this->json($arr_entidad['error'], Response::HTTP_SEE_OTHER);

        $pdf = new SIEXCOTCPDF('P','cm');
        $pdf->setLogoMinisterio(15);
        $pdf->SetAuthor("SIEXCO");
        $pdf->SetTitle("transacciones");
        $pdf->SetSubject("transacciones");
        $pdf->SetFont('times',''/*'BI'*/,8);
        $pdf->setCellHeightRatio(1);

        $fill=array('col1'=>211,'col2'=>47, 'col3'=>47);
        $textColor=array('col1'=>255,'col2'=>255,'col3'=>255);
        $font=array('family'=>'times','style'=>'BI','size'=>9);

        $pdf->CellHead(array('w'=>1, 'h'=>0,'txt'=> '', 'border'=>0,'ln'=>1));
        $pdf->CellHead(array('w'=>1, 'h'=>0,'txt'=> '', 'border'=>0,'ln'=>1));
        $pdf->CellHead(array('w'=>1, 'h'=>0,'txt'=> '', 'border'=>0,'ln'=>1));
        $pdf->CellHead(array('w'=>1, 'h'=>0,'txt'=> '', 'border'=>0,'ln'=>1));

        $pdf->CellHead(array('w'=>4, 'h'=>0,'txt'=> '', 'border'=>0,'ln'=>0,'align'=> 'C','font'=>$font));
        $pdf->CellHead(array('w'=>11, 'h'=>0,'txt'=> 'REGISTRO DE INFORMACION PRODUCTIVA', 'border'=>1,'ln'=>1,'align'=> 'C', 'fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
        $pdf->CellHead(array('w'=>1, 'h'=>0,'txt'=> '', 'border'=>0,'ln'=>1));

        $pdf->CellHead(array('w'=>6, 'h'=>0,'txt'=> '1. IDENTIFICACIÓN DE LA EMPRESA ', 'border'=>0,'ln'=>1,'align'=> 'C','font'=>$font));
        $pdf->CellHead(array('w'=>2, 'h'=>0,'txt'=> "", 'border'=>0,'ln'=>1,'font'=>$font));
        $pdf->CellHead(array('w'=>2, 'h'=>0,'txt'=>  'Razon Social: ', 'border'=>0,'ln'=>0,'align'=> 'L','font'=>$font));
        $pdf->CellHead(array('w'=>16, 'h'=>0,'txt'=> $arr_entidad['razon_social'], 'border'=>1,'ln'=>1,'align'=> 'L','font'=>$font));
        $pdf->CellHead(array('w'=>20.5, 'h'=>0,'txt'=> "", 'border'=>0,'ln'=>1,'font'=>$font));

        $pdf->AddPage();
        
        if(!empty($dato)) {
            $pdf->CellHead(array('w'=>4, 'h'=>0,'txt'=> '', 'border'=>0,'ln'=>0,'align'=> 'C','font'=>$font));
            $pdf->CellHead(array('w'=>11, 'h'=>0,'txt'=> 'REGISTRO DE INFORMACION PRODUCTIVA', 'border'=>1,'ln'=>1,'align'=> 'C', 'fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
            $pdf->CellHead(array('w'=>1, 'h'=>0,'txt'=> '', 'border'=>0,'ln'=>1));
        }

        $obj_datos=json_decode($request->getContent());        
        $obj_datos->id_rip_tipo = 1;

        $arr = $this->ripManager->listarAlmacenamiento($obj_datos);
        if(!empty($arr['error']))
            return $this->json($arr['error'], Response::HTTP_SEE_OTHER);

        $pdf->Cell(14,0.5,'2. CAPACIDAD DE ALMACENAMIENTO', 0,1,'L');
        
        $pdf->Cell(0.5,0,'',0,0,'L');
        $pdf->Cell(10,0,'2.1. ALMACENAMIENTO EN CENTROS DE ACOPIO',0,1,'L');

        $pdf->Cell(1,0,'',0,1,'L');

        $pdf->CellStyle(15,0,'Centro de Almacenamiento', 1,0,'L',true, array(255, 255, 255),array(211, 47, 47),25);
        $pdf->CellStyle(4,0, 'Capacidad',1,1,'R',true,  array(255, 255, 255), array(211,47,47));


        foreach ($arr as $row)
        {
             $pdf->Cell(15,0,$row['descrip'],1,0,'L');
             $pdf->cell(4,0,$row['capacidad'],1,1,'R');
        } 
        
        $pdf->Cell(14,0,'',' ',1,'');
        $pdf->Cell(14,0,'Nota.- Informacion Reportada por la Empresa',0,1,'');

        $obj_datos=json_decode($request->getContent());        
        $obj_datos->id_rip_tipo = 2;

        $arr = $this->ripManager->listarAlmacenamiento($obj_datos);
        if(!empty($arr['error']))
            return $this->json($arr['error'], Response::HTTP_SEE_OTHER);

        $pdf->Cell(0.5,0,'',' ',1,'');
        $pdf->Cell(10,0,'2.2. ALMACENAMIENTO EN PLANTAS ',0,1,'L');
        $pdf->Cell(0.5,0.5,'',' ',0,'');
        $pdf->Cell(10,0.5,'ALMACENAMIENTO DE GRANO',0,1,'L');

        $pdf->CellStyle(15,0, 'Centro de Almacenamiento', 1,0,'L',true, array(255,255,255), array(211,47,47));
        $pdf->CellStyle(4,0, 'Capacidad',1,1,'R',true, array(255,255,255), array(211,47,47));

        foreach ($arr as $row)
        {
             $pdf->Cell(15,0,$row['descrip'],1,0,'L');
             $pdf->cell(4,0,$row['capacidad'],1,1,'R');
        } 
        
        $pdf->Cell(14,0,'',' ',1,'');
        $pdf->Cell(14,0,'Nota.- Informacion Reportada por la Empresa',0,1,'');


        $obj_datos=json_decode($request->getContent());        
        $obj_datos->id_rip_tipo = 3;

        $arr = $this->ripManager->listarCapacidad($obj_datos);
        if(!empty($arr['error']))
            return $this->json($arr['error'], Response::HTTP_SEE_OTHER);

        $pdf->Cell(14,0,'','',1,'');
 
        $pdf->Cell(0.5,0.5,'',' ',0,'');
        $pdf->Cell(10,0.5,'ALMACENAMIENTO DE PRODUCTOS Y SUBPRODUCTOS',0,1,'L');

        $pdf->CellStyle(11,0, 'Centro de Almacenamiento', 1,0,'L', true, array(255,255,255),array(211,47,47));
        $pdf->CellStyle(4,0, 'Producto/Subproducto',1,0,'L', true, array(255,255,255),array(211,47,47));
        $pdf->CellStyle(4,0, 'Capacidad',1,1,'R',true,array(255,255,255),array(211,47,47));

        foreach ($arr as $row)
        {
             $pdf->Cell(11,0,$row['descrip'],1,0,'L');
             $pdf->cell(4,0,$row['nombre_producto'],1,0,'L');
             $pdf->cell(4,0,$row['almacenamiento'],1,1,'R');
        } 

        $pdf->Cell(14,0,'',' ',1,'');
        $pdf->Cell(14,0,'Nota.- Informacion Reportada por la Empresa',0,1,'');

        $pdf->Cell(1,0,'',0,1,'');
        $pdf->Cell(14,0.5,'3. CAPACIDAD DE PRODUCCION', 0,1,'L');
        
        $obj_datos=json_decode($request->getContent());        

        $arr = $this->ripManager->listarMantenimiento($obj_datos);
        if(!empty($arr['error']))
            return $this->json($arr['error'], Response::HTTP_SEE_OTHER);
        
        $pdf->Cell(0.5,0.5,'',' ',0,'');
        $pdf->Cell(10,0.5,'MESES DE MANTENIMIENTO',0,1,'L');

        $pdf->CellStyle(11,0, 'Centro de Almacenamiento', 1,0,'L', true, array(255,255,255), array(211,47,47));
        $pdf->CellStyle(4,0, 'Mes',1,0,'L', true, array(255,255,255), array(211,47,47));
        $pdf->CellStyle(4,0, 'Dias',1,1,'R', true, array(255,255,255), array(211,47,47));
    
        foreach ($arr as $row)
        {
            $pdf->Cell(11,0,$row['descrip'],1,0,'L');
            $pdf->cell(4,0,$row['descrip_mes'],1,0,'L');
            $pdf->cell(4,0,$row['dias'],1,1,'R');
        } 

        $pdf->Cell(14,0,'',' ',1,'');
        $pdf->Cell(14,0,'Nota.- Informacion Reportada por la Empresa',0,1,'');

        $obj_datos=json_decode($request->getContent());        
        $obj_datos->id_rip_tipo = 4;

        $arr = $this->ripManager->listarCapacidad($obj_datos, true);
        if(!empty($arr['error']))
            return $this->json($arr['error'], Response::HTTP_SEE_OTHER);

       
        $pdf->Cell(0.5,0.5,'',' ',0,'');
        $pdf->Cell(10,0.5,'CAPACIDAD DE PRODUCCION',0,1,'L');

        $pdf->CellStyle(7,0, 'Lineas Produccion', 1,0,'L', true, array(255,255,255),array(211,47,47));
        $pdf->CellStyle(6,0, 'Producto/SubProducto',1,0,'L', true, array(255,255,255), array(211,47,47));
        $pdf->CellStyle(2,0, 'Por Dia',1,0,'R', true, array(255,255,255),array(211,47,47));
        $pdf->CellStyle(2,0, 'Por Mes',1,0,'R', true, array(255,255,255),array(211,47,47));
        $pdf->CellStyle(2,0, 'Por Año',1,1,'R', true, array(255,255,255),array(211,47,47));

        foreach ($arr as $row)
        {
            $pdf->Cell(7,0,$row['descripcion'],1,0,'L');
            $pdf->cell(6,0,$row['nombre_producto'],1,0,'L');
            $pdf->cell(2,0,$row['dia'],1,0,'R');
            $pdf->cell(2,0,$row['mes'],1,0,'R');
            $pdf->cell(2,0,$row['gestion'],1,1,'R');
        } 
        $pdf->Cell(14,0,'',' ',1,'');
        $pdf->Cell(14,0,'Nota.- Informacion Reportada por la Empresa',0,1,'');

        $pdf->Cell(1,0,'',0,1,'');
        $pdf->Cell(14,0.5,'4. FACTORES DE TRANSFORMACION DE GRANO EN SUBPRODUCTOS DE SOYA', 0,1,'L');
        
        $obj_datos=json_decode($request->getContent());        

        $arr = $this->ripManager->listarFactoresTransformacion($obj_datos);
        if(!empty($arr['error']))
            return $this->json($arr['error'], Response::HTTP_SEE_OTHER);
        

        $pdf->Cell(0.5,0.5,'',' ',0,'');
        $pdf->Cell(10,0.5,'FACTORES DE TRANSFORMACION',0,1,'L');

        
        $pdf->CellStyle(15,0, 'SubProducto', 1,0,'L', true, array(255,255,255),array(211,47,47));
        $pdf->CellStyle(4,0, '%',1,1,'R', true, array(255,255,255),array(211,47,47));

        foreach ($arr as $row)
        {
            $pdf->Cell(15,0,$row['nombre_producto'],1,0,'L');
            $pdf->cell(4,0,$row['factor'],1,1,'R');
        } 
     
        $pdf->Cell(14,0,'',' ',1,'');
        $pdf->Cell(14,0,'Nota.- Informacion Reportada por la Empresa',0,1,'');

        $pdf->Cell(14,0,'',' ',1,'');
        $pdf->Cell(19,0.5,'5. OBSERVACIONES.(Caracteristicas de recepcion de grano, requisitos, otros detalles de importancia.)',0,1,'L');

        $pdf->Cell(1,0,'',0,0,'');
        $pdf->Cell(17,0,$arr_entidad['observaciones'],1,0,'L');

        $str=$pdf->Output('reporte.pdf','S');

        return new Response($str, 200, array('Content-Type' => 'application/pdf'));
    }


}
