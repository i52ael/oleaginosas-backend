<?php

namespace ProcesosTransaccionalesBundle\Controller;

use ProcesosTransaccionalesBundle\Entity\RelacionEntidad;
use ProcesosTransaccionalesBundle\Entity\VentaDetalle;
use ProcesosTransaccionalesBundle\Entity\Asignacion;
use ProcesosTransaccionalesBundle\Entity\Requerimiento;
use ProcesosTransaccionalesBundle\Entity\RequerimientoDetalle;
use ProcesosTransaccionalesBundle\Entity\Entidad;
use ProcesosTransaccionalesBundle\Entity\Producto;
use ProcesosTransaccionalesBundle\Entity\Parrilleros;
use ProcesosTransaccionalesBundle\Entity\TipoPecuarioVocacion;
use ProcesosTransaccionalesBundle\Manager\SIEXCOTCPDF;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use JMS\DiExtraBundle\Annotation as DI;
use Doctrine\ORM\EntityManager;
use ProcesosTransaccionalesBundle\Manager\TransaccionManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Lexik\Bundle\JWTAuthenticationBundle\TokenExtractor\AuthorizationHeaderTokenExtractor;

/**
 * @Route("/procesos-transaccionales")
 * **/
class AsignacionController extends Controller
{
    /**
    * @Route("/saveAsignacion", name="save_asignacion")
    * @Method("POST")
    */
    public function saveAsignacion(Request $request)
    {
        $asignacion_data = json_decode($request->getContent());

        if(empty($asignacion_data))
            return $this->json('Por favor verifique, se envio la asignacion',Response::HTTP_SEE_OTHER);

        $entityManager = $this->getDoctrine()->getManager();

        $asignacion = new Asignacion();
        $entidad = $this->getDoctrine()->getRepository(Entidad::class)->find($asignacion_data->id_entidad_proveedor);
        $producto = $this->getDoctrine()->getRepository(Producto::class)->find($asignacion_data->id_producto);
        $productor = $this->getDoctrine()->getRepository(Producto::class)->find($asignacion_data->id_productor);

        $asignacion->setNroAnimales($asignacion_data->nro_animales);
        $asignacion->setPesoAnimales($asignacion_data->peso_animales);
        $asignacion->setNroAnimalesMadres($asignacion_data->nro_animales_madres);
        $asignacion->setConsumoAb($asignacion_data->consumo_ab);
        $asignacion->setPorcentageHssAb($asignacion_data->porcentage_hss_ab);
        $asignacion->setCantidadAsignada($asignacion_data->cantidad_asignada);
        $asignacion->setLimiteAsignacion($asignacion_data->limite_asignacion);
        $asignacion->setFechaInicioAsignacion(new \DateTime($asignacion_data->fecha_inicio_asignacion));
        $asignacion->setFechaFinalAsignacion(new \DateTime($asignacion_data->fecha_final_asignacion));
        $asignacion->setIdEntidadProveedor($entidad);
        $asignacion->setIdProducto($producto);
        $asignacion->setIdProductor($productor);


        $entityManager->persist($asignacion);

        $entityManager->flush();

        return $this->json($asignacion->getId(),Response::HTTP_OK);

    }

    /**
     * @Route("/uploadXls/asignacion")
     **/
    public function uploadXls(Request $request)
    {

        /*$id_productor =$request->request->get('id_productor');
        $id_producto =$request->request->get('id_producto');
        $id_entidad =$request->request->get('id_entidad');
        $id_periodo =$request->request->get('id_periodo');
        $z =$request->request->get('z');
        $consumo_dia =$request->request->get('consumo_dia');
        $periodo_gestion = explode('/', $request->request->get('periodo_gestion'));
        // $upload_file = $request->files->get('uploads');
        // if(empty($upload_file)){
        //     return $this->json('Falta archivo', Response::HTTP_BAD_REQUEST);
        // }
        // $nameFolder="upload";
        // $pathFolder="./".$nameFolder.'/'.$id_entidad;
        // if(!is_dir($pathFolder)) mkdir($pathFolder,0777,true);
        // $fecha=date('Ymd_Gis');
        // $arr_file_name=explode(".",$upload_file->getClientOriginalName());
        // $nameFile = 'Asignacion_'.$fecha.'.'.$arr_file_name[count($arr_file_name)-1];
        // $upload_file->move($pathFolder,$nameFile);// copiamos el archivo del buffer

        // $upload_file = $request->files->get('uploads');
        // if(empty($upload_file)){
            // return $this->json('Falta archivo'.$id_entidad, Response::HTTP_BAD_REQUEST);
        // }
        $nameFolder="upload";
        $pathFolder="./".$nameFolder.'/'.$id_entidad;

        // if(!is_dir($pathFolder)) mkdir($pathFolder,0777,true);
        // $fecha=date('Ymd_Gis');
        // $arr_file_name=explode(".",$upload_file->getClientOriginalName());

        // $nameFile = 'Propuesta_1_Requerimiento_SOYA_Avicultores.xlsx';
        // $nameFile = 'Propuesta_1_Requerimiento_SOYA_Instituciones.xlsx';
        $nameFile = 'Propuesta_1_Requerimiento_SOYA_Lecheros.xlsx';
        // $upload_file->move($pathFolder,$nameFile);// copiamos el archivo del buffer

        //Tratamiento excel
        $path = $pathFolder.'/'.$nameFile;
        // return $this->json('Falta archivo '.$path, Response::HTTP_BAD_REQUEST);
        $phpExcel = $this->get('phpexcel')->createPHPExcelObject($path);
        $worksheet=$phpExcel->getSheet(0);
        $last_row= $worksheet->getHighestDataRow();
        $last_column = $worksheet->getHighestColumn();*/

        $id_productor =$request->request->get('id_productor');
        $id_producto =$request->request->get('id_producto');
        $id_entidad =$request->request->get('id_entidad');
        $id_periodo =$request->request->get('id_periodo');
        $z =$request->request->get('z');
        $consumo_dia =$request->request->get('consumo_dia');
        $periodo_gestion = explode('/', $request->request->get('periodo_gestion'));
        $upload_file = $request->files->get('uploads');
        if(empty($upload_file)){
            return $this->json('Falta archivo', Response::HTTP_BAD_REQUEST);
        }
        $nameFolder="upload";
        $pathFolder="./".$nameFolder.'/'.$id_entidad;
        if(!is_dir($pathFolder)) mkdir($pathFolder,0777,true);
        $fecha=date('Ymd_Gis');
        $arr_file_name=explode(".",$upload_file->getClientOriginalName());
        $nameFile = 'Asignacion_'.$fecha.'.'.$arr_file_name[count($arr_file_name)-1];
        $upload_file->move($pathFolder,$nameFile);// copiamos el archivo del buffer
        //Tratamiento excel
        $path = $pathFolder.'/'.$nameFile;
        $phpExcel = $this->get('phpexcel')->createPHPExcelObject($path);
        $worksheet=$phpExcel->getSheet(0);
        $last_row= $worksheet->getHighestDataRow();
        $last_column = $worksheet->getHighestColumn();

        $asociacion = [
            'id_entidad'=>$id_entidad,
            'id_producto'=>$id_producto,
            'id_productor'=>$id_productor,
            'porc_hss'=>$z,
            'consumo_diario'=>$consumo_dia,
            'periodo'=>$periodo_gestion[0],
            'gestion'=>$periodo_gestion[1],
        ];

        $array_data = [];
        $estado = true;
        if($id_productor == 1){ /// avicultor
            for($i=17;$i<=$last_row;$i++){

             $ci_socio = $worksheet->getCell('B'.$i)->getValue();
             $paterno_socio = $worksheet->getCell('C'.$i)->getValue();
             $materno_socio = $worksheet->getCell('D'.$i)->getValue();
             $nombre_socio = $worksheet->getCell('E'.$i)->getValue();
             $nombre_granja = $worksheet->getCell('F'.$i)->getValue();
             $senasag_granja = (is_null($worksheet->getCell('G'.$i)->getValue())) ? 0 : $worksheet->getCell('G'.$i)->getValue();
             $tipo_produccion = $worksheet->getCell('H'.$i)->getValue();
             $aves_carga_produccion = $worksheet->getCell('I'.$i)->getValue();
             $cargas_produccion = $worksheet->getCell('J'.$i)->getValue();
             $total_aves_granja = $worksheet->getCell('K'.$i)->getValue();
             $requerimiento_mensual = $worksheet->getCell('L'.$i)->getValue();
             $requerimiento_semestral = floatVal($requerimiento_mensual) * 6;

             // Buscamos el id del socio con el CI en la entidad, en caso de no escontrar este es 0
             $entidad_socio = $this->getDoctrine()->getRepository(Entidad::class)->findByNumeroIdentificacion(str_replace(' ','', $ci_socio));
             $id_entidad_socio = (empty($entidad_socio)) ? 0 : $entidad_socio[0]->getId();

             // Buscamos el id des socio con el SENASAG en la entidad, en caso de no escontrar este es 0

             $entidad_granja = $this->getDoctrine()->getRepository(Entidad::class)->findByRegistroSanitario(str_replace(' ','',$senasag_granja));

             $id_entidad_granja = (empty($entidad_granja)) ? 0 : $entidad_granja[0]->getId();
             $id_departamento_granja = (empty($entidad_granja)) ? 0 : $entidad_granja[0]->getIdDepartamento();

             //Hacemos el calculo segun su clasificacion
             if($tipo_produccion == strtoupper('PA')){
                 $tipo = 'Parrillero';
                 $id_tipo = 1;
                 $calculo_asignacion = $this->calculoAsignacionPolloParrillero($total_aves_granja, $id_departamento_granja, $z);
                 $ica = $calculo_asignacion['ica'];
                 $peso = $calculo_asignacion['peso'];
                 $calculo = $calculo_asignacion['calculo'];
                 $asignacion_final = ($calculo > $requerimiento_mensual) ? $requerimiento_mensual: $calculo;
             }elseif ($tipo_produccion == strtoupper('PO')) {
                 $tipo = 'Ponedoras';
                 $id_tipo = 2;
                 $calculo_asignacion = $this->calculoAsignacionPolloPonedor($total_aves_granja, $consumo_dia, $z);
                 $ica = $calculo_asignacion['ica'];
                 $peso = $calculo_asignacion['peso'];
                 $calculo = $calculo_asignacion['calculo'];
                 $asignacion_final = ($calculo > $requerimiento_mensual) ? $requerimiento_mensual: $calculo;
             }elseif ($tipo_produccion == strtoupper('PR')) {
                 $tipo = 'Productor';
                 $id_tipo = 3;
                 $calculo_asignacion = $this->calculoAsignacionPolloReproductor($total_aves_granja, $consumo_dia, $z);
                 $ica = $calculo_asignacion['ica'];
                 $peso = $calculo_asignacion['peso'];
                 $calculo = $calculo_asignacion['calculo'];
                 $asignacion_final = ($calculo > $requerimiento_mensual) ? $requerimiento_mensual: $calculo;
             }else{
                 $tipo = 'Error';
                 $id_tipo = 0;
                 $calculo_asignacion = 'Error';
                 $ica = 'Error';
                 $peso = 'Error';
                 $calculo = 'Error';
                 $asignacion_final = 'Error';
             }

             if($id_entidad_socio == 0 || $id_entidad_granja == 0){
                 $estado = false;
                 $tipo = 'Error';
                 $id_tipo = 0;
                 $calculo_asignacion = 'Error';
                 $ica = 'Error';
                 $peso = 'Error';
                 $calculo = 'Error';
                 $asignacion_final = 'No se encontro granja o socio';
             }
             $row_data = ['ci_socio'=>$ci_socio,
                 // 'paterno_socio'=>$paterno_socio, //Revisar
                 'materno_socio'=>$materno_socio,
                 'nombre_socio'=>$nombre_socio,
                 'nombre_granja'=>$nombre_granja,
                 'senasag'=>$senasag_granja,
                 'produccion'=>$tipo_produccion,
                 'aves_carga'=>$aves_carga_produccion,
                 'nro_cargas'=>$cargas_produccion,
                 'total_aves'=>$total_aves_granja,
                 'req_mensual'=>$requerimiento_mensual,
                 'req_semestral'=>$requerimiento_semestral,
                 'id_socio'=>$id_entidad_socio,
                 'id_granja'=>$id_entidad_granja,
                 'id_departamento'=>$id_departamento_granja,
                 'tipo_ave'=>$tipo,
                 'id_tipo'=>$id_tipo,
                 'ica'=>$ica,
                 'peso'=>$peso,
                 'calculo'=>$calculo,
                 'asignacion_final'=> $asignacion_final,
                 'asociacion'=>$asociacion];

             array_push($array_data, $row_data);
         }
        } elseif ($id_productor == 2) { ///porcicultor
            for($i=17;$i<=$last_row;$i++){

             $ci_socio = $worksheet->getCell('B'.$i)->getValue();
             $paterno_socio = $worksheet->getCell('C'.$i)->getValue();
             $materno_socio = $worksheet->getCell('D'.$i)->getValue();
             $nombre_socio = $worksheet->getCell('E'.$i)->getValue();
             $nombre_granja = $worksheet->getCell('F'.$i)->getValue();
             $senasag_granja = (is_null($worksheet->getCell('G'.$i)->getValue())) ? 0 : $worksheet->getCell('G'.$i)->getValue();

             $nro_madres = $worksheet->getCell('H'.$i)->getValue();
             $ciclos = $worksheet->getCell('I'.$i)->getValue();

             $requerimiento_mensual = $worksheet->getCell('J'.$i)->getValue();
             $requerimiento_semestral = $requerimiento_mensual*6;

             // Buscamos el id del socio con el CI en la entidad, en caso de no escontrar este es 0
             $entidad_socio = $this->getDoctrine()->getRepository(Entidad::class)->findByNumeroIdentificacion(str_replace(' ','', $ci_socio));
             $id_entidad_socio = (empty($entidad_socio)) ? 0 : $entidad_socio[0]->getId();

             // Buscamos el id des socio con el SENASAG en la entidad, en caso de no escontrar este es 0

             $entidad_granja = $this->getDoctrine()->getRepository(Entidad::class)->findByRegistroSanitario(str_replace(' ','',$senasag_granja));

             $id_entidad_granja = (empty($entidad_granja)) ? 0 : $entidad_granja[0]->getId();
             $id_departamento_granja = (empty($entidad_granja)) ? 0 : $entidad_granja[0]->getIdDepartamento();

             //Hacemos el calculo
             $calculo = $nro_madres*$ciclos/10;
             $asignacion_final = ($calculo > $requerimiento_mensual) ? $requerimiento_mensual: $calculo;


             if($id_entidad_socio == 0 || $id_entidad_granja == 0){
                 $estado = false;
                 $tipo = 'Error';
                 $id_tipo = 0;
                 $calculo_asignacion = 'Error';
                 $ica = 'Error';
                 $peso = 'Error';
                 $calculo = 'Error';
                 $asignacion_final = 'No se encontro granja o socio';
             }
             $row_data = ['ci_socio'=>$ci_socio,
                 'paterno_socio'=>$paterno_socio,
                 'materno_socio'=>$materno_socio,
                 'nombre_socio'=>$nombre_socio,
                 'nombre_granja'=>$nombre_granja,
                'senasag'=>$senasag_granja,
                 'produccion'=>'',
                 'cerdos_carga'=>$nro_madres,
                 'nro_cargas'=>$ciclos,
                 'total_cerdos'=>$nro_madres*$ciclos,
                 'req_mensual'=>$requerimiento_mensual,
                 'req_semestral'=>$requerimiento_semestral,
                 'id_socio'=>$id_entidad_socio,
                 'id_granja'=>$id_entidad_granja,
                 'id_departamento'=>$id_departamento_granja,
                 'tipo_ave'=>'Cerdo',
                 'id_tipo'=>4,
                 'ica'=>0,
                 'peso'=>0,
                 'calculo'=>$calculo,
                 'asignacion_final'=> $asignacion_final,
                 'asociacion'=>$asociacion];

             array_push($array_data, $row_data);
         }
        } elseif ($id_productor == 3) { // Lecheros
           for($i=17;$i<=$last_row;$i++){

                $ci_socio = $worksheet->getCell('B'.$i)->getValue();
                $paterno_socio = $worksheet->getCell('C'.$i)->getValue();
                $materno_socio = $worksheet->getCell('D'.$i)->getValue();
                $nombre_socio = $worksheet->getCell('E'.$i)->getValue();
                $nombre_granja = $worksheet->getCell('F'.$i)->getValue();
                $senasag_granja = (is_null($worksheet->getCell('G'.$i)->getValue())) ? 0 : $worksheet->getCell('G'.$i)->getValue();

                $nro_cabezas_hato = $worksheet->getCell('H'.$i)->getValue();
                $nro_vacas_produccion = $worksheet->getCell('I'.$i)->getValue();
                $litros = $worksheet->getCell('J'.$i)->getValue();

                $requerimiento_mensual = $worksheet->getCell('K'.$i)->getValue();
                $requerimiento_semestral = floatVal($requerimiento_mensual)*6;

                // Buscamos el id del socio con el CI en la entidad, en caso de no escontrar este es 0
                $entidad_socio = $this->getDoctrine()->getRepository(Entidad::class)->findByNumeroIdentificacion(str_replace(' ','', $ci_socio));
                $id_entidad_socio = (empty($entidad_socio)) ? 0 : $entidad_socio[0]->getId();

                // Buscamos el id des socio con el SENASAG en la entidad, en caso de no escontrar este es 0

                $entidad_granja = $this->getDoctrine()->getRepository(Entidad::class)->findByRegistroSanitario(str_replace(' ','',$senasag_granja));

                $id_entidad_granja = (empty($entidad_granja)) ? 0 : $entidad_granja[0]->getId();
                $id_departamento_granja = (empty($entidad_granja)) ? 0 : $entidad_granja[0]->getIdDepartamento();

                //Hacemos el calculo
                $calculo = $requerimiento_mensual;
                $asignacion_final = ($calculo > $requerimiento_mensual) ? $requerimiento_mensual: $calculo;


                if($id_entidad_socio == 0 || $id_entidad_granja == 0){
                    $estado = false;
                    $error = '';
                    if($id_entidad_socio == 0 ){
                        $error = 'Error Socio';
                    }
                    if($id_entidad_granja == 0 ){
                        $error = 'Error Granja';
                    }
                    $calculo = 'Error';
                    $asignacion_final = $error;
                }
                $row_data = [
                    'ci_socio'=>$ci_socio,
                    'paterno_socio'=>$paterno_socio,
                    'materno_socio'=>$materno_socio,
                    'nombre_socio'=>$nombre_socio,
                    'nombre_granja'=>$nombre_granja,
                    'senasag'=>$senasag_granja,
                    'req_mensual'=>$requerimiento_mensual,
                    'req_semestral'=>$requerimiento_semestral,
                    'id_socio'=>$id_entidad_socio,
                    'id_granja'=>$id_entidad_granja,
                    'id_tipo'=>4,
                    'id_departamento'=>$id_departamento_granja,
                    'nro_cabezas_hato'=>$nro_cabezas_hato,
                    'nro_vacas_produccion'=>$nro_vacas_produccion,
                    'litros'=>$litros,
                    'calculo'=>$calculo,
                    'asignacion_final'=> $asignacion_final,
                    'asociacion'=>$asociacion
                ];

                array_push($array_data, $row_data);
            }
        } elseif ($id_productor == 4) { /// Apoyo a la Producción

            for($i=17;$i<=$last_row;$i++){

                $ci_solicitante = $worksheet->getCell('B'.$i)->getValue();
                $paterno_solicitante = $worksheet->getCell('C'.$i)->getValue();
                $materno_solicitante = $worksheet->getCell('D'.$i)->getValue();
                $nombres_solicitante = $worksheet->getCell('E'.$i)->getValue();
                $nombre_institucion = $worksheet->getCell('F'.$i)->getValue();
                $nit_institucion = (is_null($worksheet->getCell('G'.$i)->getValue())) ? 0 : $worksheet->getCell('G'.$i)->getValue();

                $requerimiento_mensual = $worksheet->getCell('H'.$i)->getValue();
                $requerimiento_semestral = floatVal($requerimiento_mensual)*6;

                // Buscamos el id del socio con el CI en la entidad, en caso de no escontrar este es 0
                $entidad_solicitante = $this->getDoctrine()->getRepository(Entidad::class)->findByNumeroIdentificacion(str_replace(' ','', $ci_solicitante));
                $id_entidad_solicitante = (empty($entidad_solicitante)) ? 0 : $entidad_solicitante[0]->getId();

                // Buscamos el id des socio con el SENASAG en la entidad, en caso de no escontrar este es 0
                $entidad_institucion = $this->getDoctrine()->getRepository(Entidad::class)->findByNumeroIdentificacion(str_replace(' ','',$nit_institucion));
                $id_entidad_institucion = (empty($entidad_institucion)) ? 0 : $entidad_institucion[0]->getId();

                //Hacemos el calculo
                $calculo = $requerimiento_mensual;
                $asignacion_final = ($calculo > $requerimiento_mensual) ? $requerimiento_mensual: $calculo;

                if($id_entidad_solicitante == 0 || $id_entidad_institucion == 0){
                    $estado = false;
                    $error = '';
                    if($id_entidad_solicitante == 0){
                        $error = ' Error Solicitante';
                    }
                    if($id_entidad_institucion == 0){
                        $error = 'Error Institución';
                    }
                    $requerimiento_mensual = $error;
                    $requerimiento_semestral = $error;
                    $calculo = 'Error';
                }
                $row_data = [
                    'ci_socio'=>$ci_solicitante,
                    'paterno_socio'=>$paterno_solicitante,
                    'materno_socio'=>$materno_solicitante,
                    'nombres_socio'=>$nombres_solicitante,
                    'nombre_granja'=>$nombre_institucion,
                    'nit_institucion'=>$nit_institucion,
                    'req_mensual'=>$requerimiento_mensual,
                    'req_semestral'=>$requerimiento_semestral,
                    'id_socio'=>$id_entidad_solicitante,
                    'id_granja'=>$id_entidad_institucion,
                    'id_tipo'=>4,
                    'asociacion'=>$asociacion,
                    'calculo'=>$calculo,
                    'asignacion_final'=> $asignacion_final
                ];
                array_push($array_data, $row_data);
            }

        }else{
            return $this->json('Falta parametro productor', Response::HTTP_BAD_REQUEST);
        }

         //Borramos el archivo
         // unlink($pathFolder.'/'.$nameFile);

        // return $this->json($array_data, Response::HTTP_OK);
        return $this->json(array("arr"=>$array_data,"estado"=>$estado), Response::HTTP_OK);
    }

    // public function uploadXls(Request $request)
    // {
    //
    //     $id_productor =$request->request->get('id_productor');
    //     $id_producto =$request->request->get('id_producto');
    //     $id_entidad =$request->request->get('id_entidad');
    //     $id_periodo =$request->request->get('id_periodo');
    //     $z =$request->request->get('z');
    //     $consumo_dia =$request->request->get('consumo_dia');
    //     $periodo_gestion = explode('/', $request->request->get('periodo_gestion'));
    //     $upload_file = $request->files->get('uploads');
    //     if(empty($upload_file)){
    //         return $this->json('Falta archivo', Response::HTTP_BAD_REQUEST);
    //     }
    //     $nameFolder="upload";
    //     $pathFolder="./".$nameFolder.'/'.$id_entidad;
    //     if(!is_dir($pathFolder)) mkdir($pathFolder,0777,true);
    //     $fecha=date('Ymd_Gis');
    //     $arr_file_name=explode(".",$upload_file->getClientOriginalName());
    //     $nameFile = 'Asignacion_'.$fecha.'.'.$arr_file_name[count($arr_file_name)-1];
    //     $upload_file->move($pathFolder,$nameFile);// copiamos el archivo del buffer
    //     //Tratamiento excel
    //     $path = $pathFolder.'/'.$nameFile;
    //     $phpExcel = $this->get('phpexcel')->createPHPExcelObject($path);
    //     $worksheet=$phpExcel->getSheet(0);
    //     $last_row= $worksheet->getHighestDataRow();
    //     $last_column = $worksheet->getHighestColumn();
    //
    //     $asociacion = [
    //         'id_entidad'=>$id_entidad,
    //         'id_producto'=>$id_producto,
    //         'id_productor'=>$id_productor,
    //         'porc_hss'=>$z,
    //         'consumo_diario'=>$consumo_dia,
    //         'periodo'=>$periodo_gestion[0],
    //         'gestion'=>$periodo_gestion[1],
    //     ];
    //
    //     $array_data = [];
    //
    //     if($id_productor == 'avicultor'){
    //         for($i=17;$i<=$last_row;$i++){
    //
    //          $ci_socio = $worksheet->getCell('B'.$i)->getValue();
    //          $paterno_socio = $worksheet->getCell('C'.$i)->getValue();
    //          $materno_socio = $worksheet->getCell('D'.$i)->getValue();
    //          $nombre_socio = $worksheet->getCell('E'.$i)->getValue();
    //          $nombre_granja = $worksheet->getCell('F'.$i)->getValue();
    //          $senasag_granja = (is_null($worksheet->getCell('G'.$i)->getValue())) ? 0 : $worksheet->getCell('G'.$i)->getValue();
    //          $tipo_produccion = $worksheet->getCell('H'.$i)->getValue();
    //          $aves_carga_produccion = $worksheet->getCell('I'.$i)->getValue();
    //          $cargas_produccion = $worksheet->getCell('J'.$i)->getValue();
    //          $total_aves_granja = $worksheet->getCell('K'.$i)->getValue();
    //          $requerimiento_mensual = $worksheet->getCell('L'.$i)->getValue();
    //          $requerimiento_semestral = floatVal($requerimiento_mensual) * 6;
    //
    //          // Buscamos el id del socio con el CI en la entidad, en caso de no escontrar este es 0
    //          $entidad_socio = $this->getDoctrine()->getRepository(Entidad::class)->findByNumeroIdentificacion(str_replace(' ','', $ci_socio));
    //          $id_entidad_socio = (empty($entidad_socio)) ? 0 : $entidad_socio[0]->getId();
    //
    //          // Buscamos el id des socio con el SENASAG en la entidad, en caso de no escontrar este es 0
    //
    //          $entidad_granja = $this->getDoctrine()->getRepository(Entidad::class)->findByRegistroSanitario(str_replace(' ','',$senasag_granja));
    //
    //          $id_entidad_granja = (empty($entidad_granja)) ? 0 : $entidad_granja[0]->getId();
    //          $id_departamento_granja = (empty($entidad_granja)) ? 0 : $entidad_granja[0]->getIdDepartamento();
    //
    //          //Hacemos el calculo segun su clasificacion
    //          if($tipo_produccion == strtoupper('PA')){
    //              $tipo = 'Parrillero';
    //              $id_tipo = 1;
    //              $calculo_asignacion = $this->calculoAsignacionPolloParrillero($total_aves_granja, $id_departamento_granja, $z);
    //              $ica = $calculo_asignacion['ica'];
    //              $peso = $calculo_asignacion['peso'];
    //              $calculo = $calculo_asignacion['calculo'];
    //              $asignacion_final = ($calculo > $requerimiento_mensual) ? $requerimiento_mensual: $calculo;
    //          }elseif ($tipo_produccion == strtoupper('PO')) {
    //              $tipo = 'Ponedoras';
    //              $id_tipo = 2;
    //              $calculo_asignacion = $this->calculoAsignacionPolloPonedor($total_aves_granja, $consumo_dia, $z);
    //              $ica = $calculo_asignacion['ica'];
    //              $peso = $calculo_asignacion['peso'];
    //              $calculo = $calculo_asignacion['calculo'];
    //              $asignacion_final = ($calculo > $requerimiento_mensual) ? $requerimiento_mensual: $calculo;
    //          }elseif ($tipo_produccion == strtoupper('PR')) {
    //              $tipo = 'Productor';
    //              $id_tipo = 3;
    //              $calculo_asignacion = $this->calculoAsignacionPolloReproductor($total_aves_granja, $consumo_dia, $z);
    //              $ica = $calculo_asignacion['ica'];
    //              $peso = $calculo_asignacion['peso'];
    //              $calculo = $calculo_asignacion['calculo'];
    //              $asignacion_final = ($calculo > $requerimiento_mensual) ? $requerimiento_mensual: $calculo;
    //          }else{
    //              $tipo = 'Error';
    //              $id_tipo = 0;
    //              $calculo_asignacion = 'Error';
    //              $ica = 'Error';
    //              $peso = 'Error';
    //              $calculo = 'Error';
    //              $asignacion_final = 'Error';
    //          }
    //
    //          if($id_entidad_socio == 0 || $id_entidad_granja == 0){
    //
    //              $tipo = 'Error';
    //              $id_tipo = 0;
    //              $calculo_asignacion = 'Error';
    //              $ica = 'Error';
    //              $peso = 'Error';
    //              $calculo = 'Error';
    //              $asignacion_final = 'No se encontro granja o socio';
    //          }
    //          $row_data = ['ci_socio'=>$ci_socio,
    //              // 'paterno_socio'=>$paterno_socio, //Revisar
    //              'materno_socio'=>$materno_socio,
    //              'nombre_socio'=>$nombre_socio,
    //              'nombre_granja'=>$nombre_granja,
    //              'senasag'=>$senasag_granja,
    //              'produccion'=>$tipo_produccion,
    //              'aves_carga'=>$aves_carga_produccion,
    //              'nro_cargas'=>$cargas_produccion,
    //              'total_aves'=>$total_aves_granja,
    //              'req_mensual'=>$requerimiento_mensual,
    //              'req_semestral'=>$requerimiento_semestral,
    //              'id_socio'=>$id_entidad_socio,
    //              'id_granja'=>$id_entidad_granja,
    //              'id_departamento'=>$id_departamento_granja,
    //              'tipo_ave'=>$tipo,
    //              'id_tipo'=>$id_tipo,
    //              'ica'=>$ica,
    //              'peso'=>$peso,
    //              'calculo'=>$calculo,
    //              'asignacion_final'=> $asignacion_final,
    //              'asociacion'=>$asociacion];
    //
    //          array_push($array_data, $row_data);
    //      }
    //     } elseif ($id_productor == 'porcinocultor') {
    //         for($i=17;$i<=$last_row;$i++){
    //
    //          $ci_socio = $worksheet->getCell('B'.$i)->getValue();
    //          $paterno_socio = $worksheet->getCell('C'.$i)->getValue();
    //          $materno_socio = $worksheet->getCell('D'.$i)->getValue();
    //          $nombre_socio = $worksheet->getCell('E'.$i)->getValue();
    //          $nombre_granja = $worksheet->getCell('F'.$i)->getValue();
    //          $senasag_granja = (is_null($worksheet->getCell('G'.$i)->getValue())) ? 0 : $worksheet->getCell('G'.$i)->getValue();
    //
    //          $nro_madres = $worksheet->getCell('H'.$i)->getValue();
    //          $ciclos = $worksheet->getCell('I'.$i)->getValue();
    //
    //          $requerimiento_mensual = $worksheet->getCell('J'.$i)->getValue();
    //          $requerimiento_semestral = $requerimiento_mensual*6;
    //
    //          // Buscamos el id del socio con el CI en la entidad, en caso de no escontrar este es 0
    //          $entidad_socio = $this->getDoctrine()->getRepository(Entidad::class)->findByNumeroIdentificacion(str_replace(' ','', $ci_socio));
    //          $id_entidad_socio = (empty($entidad_socio)) ? 0 : $entidad_socio[0]->getId();
    //
    //          // Buscamos el id des socio con el SENASAG en la entidad, en caso de no escontrar este es 0
    //
    //          $entidad_granja = $this->getDoctrine()->getRepository(Entidad::class)->findByRegistroSanitario(str_replace(' ','',$senasag_granja));
    //
    //          $id_entidad_granja = (empty($entidad_granja)) ? 0 : $entidad_granja[0]->getId();
    //          $id_departamento_granja = (empty($entidad_granja)) ? 0 : $entidad_granja[0]->getIdDepartamento();
    //
    //          //Hacemos el calculo
    //          $calculo = $nro_madres*$ciclos/10;
    //          $asignacion_final = ($calculo > $requerimiento_mensual) ? $requerimiento_mensual: $calculo;
    //
    //
    //          if($id_entidad_socio == 0 || $id_entidad_granja == 0){
    //
    //              $tipo = 'Error';
    //              $id_tipo = 0;
    //              $calculo_asignacion = 'Error';
    //              $ica = 'Error';
    //              $peso = 'Error';
    //              $calculo = 'Error';
    //              $asignacion_final = 'No se encontro granja o socio';
    //          }
    //          $row_data = ['ci_socio'=>$ci_socio,
    //              'paterno_socio'=>$paterno_socio,
    //              'materno_socio'=>$materno_socio,
    //              'nombre_socio'=>$nombre_socio,
    //              'nombre_granja'=>$nombre_granja,
    //             'senasag'=>$senasag_granja,
    //              'produccion'=>'',
    //              'cerdos_carga'=>$nro_madres,
    //              'nro_cargas'=>$ciclos,
    //              'total_cerdos'=>$nro_madres*$ciclos,
    //              'req_mensual'=>$requerimiento_mensual,
    //              'req_semestral'=>$requerimiento_semestral,
    //              'id_socio'=>$id_entidad_socio,
    //              'id_granja'=>$id_entidad_granja,
    //              'id_departamento'=>$id_departamento_granja,
    //              'tipo_ave'=>'Cerdo',
    //              'id_tipo'=>4,
    //              'ica'=>0,
    //              'peso'=>0,
    //              'calculo'=>$calculo,
    //              'asignacion_final'=> $asignacion_final,
    //              'asociacion'=>$asociacion];
    //
    //          array_push($array_data, $row_data);
    //      }
    //     }else{
    //         return $this->json('Falta parametro productor', Response::HTTP_BAD_REQUEST);
    //     }
    //
    //      //Borramos el archivo
    //      unlink($pathFolder.'/'.$nameFile);
    //
    //     return $this->json($array_data, Response::HTTP_OK);
    // }

    /**
     * @Route("/guardar/asignacion")
     **/
    public function guardarAsignacion(Request $request)
    {
        $arr_datos=json_decode($request->getContent(), true);

        $datos_guardados = [];
        if(count($arr_datos)>0){
            $id_entidad = $arr_datos[0]['asociacion']['id_entidad'];
            $id_producto = $arr_datos[0]['asociacion']['id_producto'];
            $id_productor = $arr_datos[0]['asociacion']['id_productor'];
            $porc_hss = $arr_datos[0]['asociacion']['porc_hss'];
            $consumo_diario = $arr_datos[0]['asociacion']['consumo_diario'];
            $gestion = $arr_datos[0]['asociacion']['gestion'];
            $periodo = $arr_datos[0]['asociacion']['periodo'];

            $id_requerimiento = $this->guardarRequerimiento($id_entidad, $id_producto, $id_productor, $porc_hss, $consumo_diario, $gestion, $periodo);

            if($id_requerimiento > 0){

                foreach ($arr_datos as $value) {
                    if($id_productor == 1){ /// avicultor
                        $id_requerimiento_detalle = $this->guardarRequerimientoDetalle(
                            $value['id_tipo'],
                            $id_requerimiento,
                            $value['id_socio'],
                            $value['id_granja'],
                            $value['aves_carga'], // dato 1
                            $value['nro_cargas'], // dato 2
                            $value['total_aves'],  // dato 3
                            0, // dato 4
                            $value['ica'],
                            $value['peso'],
                            $value['calculo'],
                            0,
                            $value['req_mensual'],
                            $value['req_semestral'],
                            $value['asignacion_final']);
                    } elseif ($id_productor == 2) { /// porcicultor
                        $id_requerimiento_detalle = $this->guardarRequerimientoDetalle(
                            $value['id_tipo'],
                            $id_requerimiento,
                            $value['id_socio'],
                            $value['id_granja'],
                            $value['cerdos_carga'], // dato 1
                            $value['nro_cargas'], // dato 2
                            $value['total_cerdos'], //dato 3
                            0, // dato 4
                            $value['calculo'],
                            0,
                            0,
                            0,
                            $value['req_mensual'],
                            $value['req_semestral'],
                            $value['asignacion_final']);
                    } elseif ($id_productor == 3) { /// Lecheros
                        $id_requerimiento_detalle = $this->guardarRequerimientoDetalle(
                            $value['id_tipo'],
                            $id_requerimiento,
                            $value['id_socio'],
                            $value['id_granja'],
                            $value['nro_cabezas_hato'], // dato 1
                            $value['nro_vacas_produccion'], // dato 2
                            $value['litros'], //dato 3
                            0, // dato 4
                            $value['calculo'],
                            0,
                            0,
                            0,
                            $value['req_mensual'],
                            $value['req_semestral'],
                            $value['asignacion_final']);
                    } elseif ($id_productor == 4) { /// apoyo instituciones
                        $id_requerimiento_detalle = $this->guardarRequerimientoDetalle(
                            $value['id_tipo'],
                            $id_requerimiento,
                            $value['id_socio'],
                            $value['id_granja'],
                            0, //$value['cerdos_carga'], // dato 1
                            0, //$value['nro_cargas'], // dato 2
                            0, //$value['total_cerdos'], //dato 3
                            0, // dato 4
                            $value['calculo'],
                            0,
                            0,
                            0,
                            $value['req_mensual'],
                            $value['req_semestral'],
                            $value['asignacion_final']);
                    } else{
                        $id_requerimiento_detalle = [];
                    }

                    array_push($datos_guardados, $id_requerimiento_detalle);

                }
            }

            return $this->json($datos_guardados, Response::HTTP_OK);
        }else{
            return $this->json('Sin datos', Response::HTTP_BAD_REQUEST);
        }

    }

    /**
    * @Route("/requerimientos", name="all_requerimientos")
    */
    public function getAllRequerimiento(Request $request)
    {
       $em = $this->getDoctrine()->getManager();
        $sql = "SELECT
                requerimiento.id,requerimiento.id_productor, entidad.razon_social,producto.nombre,SUM(requerimiento_detalle.dato_calculado_asignado) AS asignado, 
                TO_CHAR(requerimiento.fecha_creacion, 'dd-mm-YYYY') AS fecha_creacion,
                rp.descripcion tipo_productor
                FROM public.requerimiento
                JOIN entidad ON entidad.id = requerimiento.id_entidad
                JOIN producto ON producto.id = requerimiento.id_producto
                JOIN requerimiento_detalle ON requerimiento_detalle.id_req = requerimiento.id
                JOIN requerimiento_productor rp ON requerimiento.id_productor = rp.id
                GROUP BY
                requerimiento.id, entidad.razon_social,producto.nombre, requerimiento.fecha_creacion, rp.descripcion";
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
       return $this->json($stmt->fetchAll(), Response::HTTP_OK);
    }

    public function calculoAsignacionPolloParrillero($pollos_total, $id_departamento, $z ){
        if($id_departamento != 0){
            $parrillero = $this->getDoctrine()->getRepository(Parrilleros::class)->findByIdDepartamento($id_departamento);
            $peso_ave = $parrillero[0]->getPesoAve();
            $ica = $parrillero[0]->getIca();
            $calculo_asignacion = $pollos_total/12*$peso_ave*$ica*$z/1000;
        }else{
            $peso_ave = 'Error';
            $ica = 'Error';
            $calculo_asignacion = 'Error';
        }

        $respuesta = ['peso'=>$peso_ave, 'ica' => $ica, 'calculo'=>$calculo_asignacion];
        return $respuesta;

    }

    public function calculoAsignacionPolloPonedor($pollos_total, $consumo_dia, $z ){
        $calculo_asignacion = $pollos_total*$consumo_dia*30*$z/1000;
        $respuesta = ['peso'=>0, 'ica' => 0, 'calculo'=>$calculo_asignacion];
        return $respuesta;
    }

    public function calculoAsignacionPolloReproductor($pollos_total, $id_departamento, $z ){
        $calculo_asignacion = $pollos_total*$consumo_dia*30*$z/1000;
        $respuesta = ['peso'=>0, 'ica' => 0, 'calculo'=>$calculo_asignacion];
        return $respuesta;
    }

    public function guardarRequerimiento($id_entidad,$id_producto,$id_productor,$porc_hss,$consumo_diario,$gestion,$periodo){

        // $id_productor = ($id_productor == 'avicultor') ? 1: 2;
        $entityManager = $this->getDoctrine()->getManager();

        $requerimiento = new Requerimiento();

        $requerimiento->setIdEntidad($id_entidad);
        $requerimiento->setIdProducto($id_producto);
        $requerimiento->setIdProductor($id_productor);
        $requerimiento->setPorcHss($porc_hss);
        $requerimiento->setConsumoDiario($consumo_diario);
        $requerimiento->setGestion($gestion);
        $requerimiento->setPeriodo($periodo);
        $requerimiento->setFechaCreacion(new \DateTime());
        $requerimiento->setFechaUltimaModificacion(new \DateTime());
        $entityManager->persist($requerimiento);

        $entityManager->flush();
        return $requerimiento->getId();

    }


    public function guardarRequerimientoDetalle($id_tipo_percuario, $id_requerimiento, $id_entidad_productor, $id_entidad_granja,
            $datoProd1, $datoProd2, $datoProd3, $datoProd4, $datoCalc1, $datoCalc2, $datoCalc3, $datoCalc4,$mensual,$semestral,$calculoAsignado){

        $entityManager = $this->getDoctrine()->getManager();

        $tipo_percuario = $this->getDoctrine()->getRepository(TipoPecuarioVocacion::class)->find($id_tipo_percuario);
        $requerimiento = $this->getDoctrine()->getRepository(Requerimiento::class)->find($id_requerimiento);

        $requerimientoDetalle = new RequerimientoDetalle();

        $requerimientoDetalle->setIdReq($requerimiento);
        $requerimientoDetalle->setIdEntidadProductor($id_entidad_productor);
        $requerimientoDetalle->setIdEntidadGranja($id_entidad_granja);
        $requerimientoDetalle->setIdTipoVocacion($tipo_percuario);

        $requerimientoDetalle->setDatoProd1($datoProd1);
        $requerimientoDetalle->setDatoProd2($datoProd2);
        $requerimientoDetalle->setDatoProd3($datoProd3);
        $requerimientoDetalle->setDatoProd4($datoProd4);

        $requerimientoDetalle->setDatoCalculado1($datoCalc1);
        $requerimientoDetalle->setDatoCalculado2($datoCalc2);
        $requerimientoDetalle->setDatoCalculado3($datoCalc3);
        $requerimientoDetalle->setDatoCalculado4($datoCalc4);

        $requerimientoDetalle->setMensual($mensual);
        $requerimientoDetalle->setSemestral($semestral);

        $requerimientoDetalle->setDatoCalculadoAsignado($calculoAsignado);

        $requerimientoDetalle->setFechaCreacion(new \DateTime());
        $requerimientoDetalle->setFechaUltimaModificacion(new \DateTime());

        $entityManager->persist($requerimientoDetalle);

        $entityManager->flush();
        return $requerimientoDetalle->getId();

    }

    /**
    * @Route("/sendExcelAsignacion")
    * @Method("POST")
    **/
    public function sendExcelAsignacion(Request $request){

        $obj_datos = json_decode($request->getContent());
        $head_excel = $this->getHeadExcelAsignacion($obj_datos->id);
        $body_excel = $this->getBodyExcelAsignacion($obj_datos->id,$head_excel['tipo']);

        $pdf = new SIEXCOTCPDF('L','cm');
            $pdf->SetAuthor("SIEXCO");
            $pdf->SetTitle("transacciones");
            $pdf->SetSubject("transacciones");
            $pdf->SetFont('times',''/*'BI'*/,8);
            $pdf->setCellHeightRatio(1);

            $fill=array('col1'=>211,'col2'=>47, 'col3'=>47);
            $textColor=array('col1'=>255,'col2'=>255,'col3'=>255);
            $font=array('family'=>'times','style'=>'BI','size'=>9);

            //$tipo_productor = ($head_excel['tipo'] == 1) ? 'AVICULTORES' : 'PORCINOCULTORES';

            $pdf->CellHead(array('w'=>1, 'h'=>0,'txt'=> '', 'border'=>0,'ln'=>1));
            $pdf->CellHead(array('w'=>1, 'h'=>0,'txt'=> '', 'border'=>0,'ln'=>1));
            $pdf->CellHead(array('w'=>1, 'h'=>0,'txt'=> '', 'border'=>0,'ln'=>1));
            $pdf->CellHead(array('w'=>27, 'h'=>0,'txt'=> 'REPORTE DE ASIGNACION DE SUBPRODUCTOS DE SOYA PARA '.$head_excel['tipo_productor'], 'border'=>0,'ln'=>1,'align'=> 'C','font'=>$font));
            $pdf->CellHead(array('w'=>4, 'h'=>0,'txt'=> 'ASOCIACION ', 'border'=>0,'ln'=>0,'align'=> 'R','font'=>$font));
            $pdf->CellHead(array('w'=>20.5, 'h'=>0,'txt'=> strtoupper($head_excel['razon_social']), 'border'=>1,'ln'=>1,'font'=>$font));
            $pdf->CellHead(array('w'=>4, 'h'=>0,'txt'=> 'SUBPRODUCTO ', 'border'=>0,'ln'=>0,'align'=> 'R','font'=>$font));
            $pdf->CellHead(array('w'=>20.5, 'h'=>0,'txt'=> strtoupper($head_excel['subproducto']), 'border'=>1,'ln'=>1,'font'=>$font));
            $pdf->CellHead(array('w'=>4, 'h'=>0,'txt'=> 'GESTION ', 'border'=>0,'ln'=>0,'align'=> 'R','font'=>$font));
            $pdf->CellHead(array('w'=>8, 'h'=>0,'txt'=> $head_excel['gestion'], 'border'=>1,'ln'=>0,'font'=>$font));
            $pdf->CellHead(array('w'=>4, 'h'=>0,'txt'=> 'SEMESTRE ', 'border'=>0,'ln'=>0,'align'=> 'R','font'=>$font));
            $pdf->CellHead(array('w'=>8, 'h'=>0,'txt'=> $head_excel['periodo'], 'border'=>1,'ln'=>1,'font'=>$font));
            $pdf->CellHead(array('w'=>1, 'h'=>0,'txt'=> '', 'border'=>0,'ln'=>1));

            if($head_excel['tipo'] == 1){ //
                $pdf->CellHead(array('w'=>1.5,'h'=>0, 'txt'=>'CI','border'=>1,'ln'=>0,'align'=>'','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
                $pdf->CellHead(array('w'=>3,'h'=>0, 'txt'=>'Nombres','border'=>1,'ln'=>0,'align'=>'','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));

                $pdf->CellHead(array('w'=>3,'h'=>0, 'txt'=>'Nombre','border'=>1,'ln'=>0,'align'=>'','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
                $pdf->CellHead(array('w'=>2,'h'=>0, 'txt'=>'Senasag','border'=>1,'ln'=>0,'align'=>'','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));

                $pdf->CellHead(array('w'=>1.5,'h'=>0, 'txt'=>'Tipo','border'=>1,'ln'=>0,'align'=>'','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
                $pdf->CellHead(array('w'=>2,'h'=>0, 'txt'=>'N° Aves','border'=>1,'ln'=>0,'align'=>'','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
                $pdf->CellHead(array('w'=>2,'h'=>0, 'txt'=>'N° Carga','border'=>1,'ln'=>0,'align'=>'','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
                $pdf->CellHead(array('w'=>2,'h'=>0, 'txt'=>'Total Aves','border'=>1,'ln'=>0,'align'=>'','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
                $pdf->CellHead(array('w'=>2,'h'=>0, 'txt'=>'Mensual','border'=>1,'ln'=>0,'align'=>'','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));

                $pdf->CellHead(array('w'=>1.5,'h'=>0, 'txt'=>'Peso','border'=>1,'ln'=>0,'align'=>'','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
                $pdf->CellHead(array('w'=>1.5,'h'=>0, 'txt'=>'ICA','border'=>1,'ln'=>0,'align'=>'','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
                $pdf->CellHead(array('w'=>2,'h'=>0, 'txt'=>'Calculo','border'=>1,'ln'=>0,'align'=>'','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
                $pdf->CellHead(array('w'=>2,'h'=>0, 'txt'=>'Asignado','border'=>1,'ln'=>1,'align'=>'','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));

                $pdf->AddPage();//la funcion de adicionar pagina debe ir siempre despues de los CellHead porque si no se desconfigura

                foreach($body_excel as $row)
                {
                    $pdf->Cell(1.5,0,$row['numero_identificacion'],'LR');
                    $pdf->Cell(3,0,$row['nombre_productor'],0,'R');
                    $pdf->Cell(3,0,$row['nombre_granja'],0,'R');
                    $pdf->Cell(2,0,$row['registro_sanitario'],'LR',0,'R');
                    $pdf->Cell(1.5,0,$row['tipo'],'LR',0,'R');
                    $pdf->Cell(2,0,$row['aves_carga'],'LR',0,'R');
                    $pdf->Cell(2,0,$row['nro_carga'],'LR',0,'R');
                    $pdf->Cell(2,0,$row['total_aves'],'LR',0,'R');
                    $pdf->Cell(2,0,$row['mensual'],'LR',0,'R');
                    $pdf->Cell(1.5,0,$row['peso'],'LR',0,'R');
                    $pdf->Cell(1.5,0,$row['ica'],'LR',0,'R');
                    $pdf->Cell(2,0,$row['calculado'],'LR',0,'R');
                    $pdf->Cell(2,0,$row['dato_calculado_asignado'],'LR',1,'R');

                }

            } elseif($head_excel['tipo'] == 2){
                $pdf->CellHead(array('w'=>1.5,'h'=>0, 'txt'=>'CI','border'=>1,'ln'=>0,'align'=>'','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
                $pdf->CellHead(array('w'=>3,'h'=>0, 'txt'=>'Nombres','border'=>1,'ln'=>0,'align'=>'','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));

                $pdf->CellHead(array('w'=>3,'h'=>0, 'txt'=>'Nombre','border'=>1,'ln'=>0,'align'=>'','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
                $pdf->CellHead(array('w'=>2,'h'=>0, 'txt'=>'Senasag','border'=>1,'ln'=>0,'align'=>'','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));

                $pdf->CellHead(array('w'=>2,'h'=>0, 'txt'=>'N° Madres','border'=>1,'ln'=>0,'align'=>'','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
                $pdf->CellHead(array('w'=>2,'h'=>0, 'txt'=>'N° Ciclos','border'=>1,'ln'=>0,'align'=>'','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
                $pdf->CellHead(array('w'=>2,'h'=>0, 'txt'=>'Total','border'=>1,'ln'=>0,'align'=>'','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
                $pdf->CellHead(array('w'=>2,'h'=>0, 'txt'=>'Mensual','border'=>1,'ln'=>0,'align'=>'','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
                $pdf->CellHead(array('w'=>2,'h'=>0, 'txt'=>'Semestral','border'=>1,'ln'=>0,'align'=>'','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));

                $pdf->CellHead(array('w'=>2,'h'=>0, 'txt'=>'Calculo','border'=>1,'ln'=>0,'align'=>'','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
                $pdf->CellHead(array('w'=>2,'h'=>0, 'txt'=>'Asignado','border'=>1,'ln'=>1,'align'=>'','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));


                $pdf->AddPage();//la funcion de adicionar pagina debe ir siempre despues de los CellHead porque si no se desconfigura

                foreach($body_excel as $row)
                {

                    $pdf->Cell(1.5,0, $row['numero_identificacion'],'LR');
                    $pdf->Cell(3,0,$row['nombre_productor'],'LR',0,'R');

                    $pdf->Cell(3,0,$row['nombre_granja'],'LR',0,'R');
                    $pdf->Cell(2,0,$row['registro_sanitario'],'LR',0,'R');

                    $pdf->Cell(2,0,$row['cerdos_carga'],'LR',0,'R');
                    $pdf->Cell(2,0,$row['nro_carga'],'LR',0,'R');
                    $pdf->Cell(2,0,$row['total_cerdos'],'LR',0,'R');
                    $pdf->Cell(2,0,$row['mensual'],'LR',0,'R');
                    $pdf->Cell(2,0,$row['mensual']*6,'LR',0,'R');

                    $pdf->Cell(2,0,$row['calculado'],'LR',0,'R');
                    $pdf->Cell(2,0,$row['dato_calculado_asignado'],'LR',1,'R');

                }


            } elseif($head_excel['tipo'] == 3){ /// Lecheros
                $pdf->CellHead(array('w'=>1.7,'h'=>0, 'txt'=>'CI / NIT','border'=>1,'ln'=>0,'align'=>'C','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
                $pdf->CellHead(array('w'=>5,'h'=>0, 'txt'=>'Razón Social / Proveedor','border'=>1,'ln'=>0,'align'=>'C','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));

                $pdf->CellHead(array('w'=>5,'h'=>0, 'txt'=>'Nombre Granja','border'=>1,'ln'=>0,'align'=>'C','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
                $pdf->CellHead(array('w'=>1.5,'h'=>0, 'txt'=>'Senasag','border'=>1,'ln'=>0,'align'=>'C','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));

                $pdf->CellHead(array('w'=>3.6,'h'=>0, 'txt'=>'N° Cabezas Hato Lechero','border'=>1,'ln'=>0,'align'=>'C','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
                $pdf->CellHead(array('w'=>3.4,'h'=>0, 'txt'=>'N° Vacas En Produción','border'=>1,'ln'=>0,'align'=>'C','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
                $pdf->CellHead(array('w'=>2,'h'=>0, 'txt'=>'L Leche/Día','border'=>1,'ln'=>0,'align'=>'C','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
                $pdf->CellHead(array('w'=>1.5,'h'=>0, 'txt'=>'Mensual','border'=>1,'ln'=>0,'align'=>'C','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));

                $pdf->CellHead(array('w'=>1.5,'h'=>0, 'txt'=>'Cálculo','border'=>1,'ln'=>0,'align'=>'C','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
                $pdf->CellHead(array('w'=>2,'h'=>0, 'txt'=>'Asignado','border'=>1,'ln'=>1,'align'=>'C','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));

                $pdf->AddPage();//la funcion de adicionar pagina debe ir siempre despues de los CellHead porque si no se desconfigura

                foreach($body_excel as $row)
                {
                    $pdf->Cell(1.7,0,$row['numero_identificacion'],'LR');
                    $pdf->Cell(5,0,$row['nombre_productor'],0,'R');
                    $pdf->Cell(5,0,$row['nombre_granja'],0,'R');
                    $pdf->Cell(1.5,0,$row['registro_sanitario'],'LR',0,'R');
                    $pdf->Cell(3.6,0,$row['nro_cabezas_hato'],'LR',0,'R');
                    $pdf->Cell(3.4,0,$row['nro_vacas_produccion'],'LR',0,'R');
                    $pdf->Cell(2,0,$row['litros'],'LR',0,'R');
                    $pdf->Cell(1.5,0,$row['mensual'],'LR',0,'R');
                    $pdf->Cell(1.5,0,$row['calculado'],'LR',0,'R');
                    $pdf->Cell(2,0,$row['dato_calculado_asignado'],'LR',1,'R');

                }
            } elseif($head_excel['tipo'] == 4){ /// Instituciones
                $pdf->CellHead(array('w'=>3,'h'=>0, 'txt'=>'CI / NIT','border'=>1,'ln'=>0,'align'=>'C','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
                $pdf->CellHead(array('w'=>7,'h'=>0, 'txt'=>'Nombre Solicitante','border'=>1,'ln'=>0,'align'=>'C','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
                $pdf->CellHead(array('w'=>7,'h'=>0, 'txt'=>'Nombre Institución','border'=>1,'ln'=>0,'align'=>'C','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
                $pdf->CellHead(array('w'=>3,'h'=>0, 'txt'=>'NIT Institución','border'=>1,'ln'=>0,'align'=>'C','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
                $pdf->CellHead(array('w'=>2.4,'h'=>0, 'txt'=>'Mensual','border'=>1,'ln'=>0,'align'=>'C','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
                $pdf->CellHead(array('w'=>2.4,'h'=>0, 'txt'=>'Cálculo','border'=>1,'ln'=>0,'align'=>'C','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
                $pdf->CellHead(array('w'=>2.4,'h'=>0, 'txt'=>'Asignado','border'=>1,'ln'=>1,'align'=>'C','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));

                $pdf->AddPage();//la funcion de adicionar pagina debe ir siempre despues de los CellHead porque si no se desconfigura

                foreach($body_excel as $row)
                {
                    $pdf->Cell(3,0,$row['numero_identificacion'],'LR');
                    $pdf->Cell(7,0,$row['nombre_productor'],0,'R');
                    $pdf->Cell(7,0,$row['nombre_institucion'],0,'R');
                    $pdf->Cell(3,0,$row['nit_institucion'],'LR',0,'R');
                    $pdf->Cell(2.4,0,$row['mensual'],'LR',0,'R');
                    $pdf->Cell(2.4,0,$row['calculado'],'LR',0,'R');
                    $pdf->Cell(2.4,0,$row['dato_calculado_asignado'],'LR',1,'R');

                }
            }
            $str=$pdf->Output('reporte.pdf','S');
            return new Response($str, 200, array('Content-Type' => 'application/pdf'));
    }

    // public function sendExcelAsignacion($id_requerimiento){
    //
    //     $head_excel = $this->getHeadExcelAsignacion($id_requerimiento);
    //     $body_excel = $this->getBodyExcelAsignacion($id_requerimiento,$head_excel['tipo']);
    //
    //     $pdf = new SIEXCOTCPDF('L','cm');
    //         $pdf->SetAuthor("SIEXCO");
    //         $pdf->SetTitle("transacciones");
    //         $pdf->SetSubject("transacciones");
    //         $pdf->SetFont('times',''/*'BI'*/,8);
    //         $pdf->setCellHeightRatio(1);
    //
    //         $fill=array('col1'=>211,'col2'=>47, 'col3'=>47);
    //         $textColor=array('col1'=>255,'col2'=>255,'col3'=>255);
    //         $font=array('family'=>'times','style'=>'BI','size'=>9);
    //
    //         $tipo_productor = ($head_excel['tipo'] == 1) ? 'AVICULTORES' : 'PORCINOCULTORES';
    //
    //             $pdf->CellHead(array('w'=>1, 'h'=>0,'txt'=> '', 'border'=>0,'ln'=>1));
    //             $pdf->CellHead(array('w'=>1, 'h'=>0,'txt'=> '', 'border'=>0,'ln'=>1));
    //             $pdf->CellHead(array('w'=>1, 'h'=>0,'txt'=> '', 'border'=>0,'ln'=>1));
    //             $pdf->CellHead(array('w'=>27, 'h'=>0,'txt'=> 'REPORTE DE ASIGNACION DE SUBPRODUCTOS DE SOYA PARA '.$tipo_productor, 'border'=>0,'ln'=>1,'align'=> 'C','font'=>$font));
    //             $pdf->CellHead(array('w'=>4, 'h'=>0,'txt'=> 'ASOCIACION ', 'border'=>0,'ln'=>0,'align'=> 'R','font'=>$font));
    //             $pdf->CellHead(array('w'=>20.5, 'h'=>0,'txt'=> strtoupper($head_excel['razon_social']), 'border'=>1,'ln'=>1,'font'=>$font));
    //             $pdf->CellHead(array('w'=>4, 'h'=>0,'txt'=> 'SUBPRODUCTO ', 'border'=>0,'ln'=>0,'align'=> 'R','font'=>$font));
    //             $pdf->CellHead(array('w'=>20.5, 'h'=>0,'txt'=> strtoupper($head_excel['subproducto']), 'border'=>1,'ln'=>1,'font'=>$font));
    //             $pdf->CellHead(array('w'=>4, 'h'=>0,'txt'=> 'GESTION ', 'border'=>0,'ln'=>0,'align'=> 'R','font'=>$font));
    //             $pdf->CellHead(array('w'=>8, 'h'=>0,'txt'=> $head_excel['gestion'], 'border'=>1,'ln'=>0,'font'=>$font));
    //             $pdf->CellHead(array('w'=>4, 'h'=>0,'txt'=> 'SEMESTRE ', 'border'=>0,'ln'=>0,'align'=> 'R','font'=>$font));
    //             $pdf->CellHead(array('w'=>8, 'h'=>0,'txt'=> $head_excel['periodo'], 'border'=>1,'ln'=>1,'font'=>$font));
    //             $pdf->CellHead(array('w'=>1, 'h'=>0,'txt'=> '', 'border'=>0,'ln'=>1));
    //
    //         if($head_excel['tipo'] == 2){
    //             $pdf->CellHead(array('w'=>1.5,'h'=>0, 'txt'=>'CI','border'=>1,'ln'=>0,'align'=>'','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
    //             $pdf->CellHead(array('w'=>3,'h'=>0, 'txt'=>'Nombres','border'=>1,'ln'=>0,'align'=>'','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
    //
    //             $pdf->CellHead(array('w'=>3,'h'=>0, 'txt'=>'Nombre','border'=>1,'ln'=>0,'align'=>'','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
    //             $pdf->CellHead(array('w'=>2,'h'=>0, 'txt'=>'Senasag','border'=>1,'ln'=>0,'align'=>'','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
    //
    //             $pdf->CellHead(array('w'=>2,'h'=>0, 'txt'=>'N° Madres','border'=>1,'ln'=>0,'align'=>'','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
    //             $pdf->CellHead(array('w'=>2,'h'=>0, 'txt'=>'N° Ciclos','border'=>1,'ln'=>0,'align'=>'','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
    //             $pdf->CellHead(array('w'=>2,'h'=>0, 'txt'=>'Total','border'=>1,'ln'=>0,'align'=>'','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
    //             $pdf->CellHead(array('w'=>2,'h'=>0, 'txt'=>'Mensual','border'=>1,'ln'=>0,'align'=>'','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
    //             $pdf->CellHead(array('w'=>2,'h'=>0, 'txt'=>'Semestral','border'=>1,'ln'=>0,'align'=>'','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
    //
    //             $pdf->CellHead(array('w'=>2,'h'=>0, 'txt'=>'Calculo','border'=>1,'ln'=>0,'align'=>'','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
    //             $pdf->CellHead(array('w'=>2,'h'=>0, 'txt'=>'Asignado','border'=>1,'ln'=>1,'align'=>'','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
    //
    //
    //             $pdf->AddPage();//la funcion de adicionar pagina debe ir siempre despues de los CellHead porque si no se desconfigura
    //
    //             foreach($body_excel as $row)
    //             {
    //
    //                 $pdf->Cell(1.5,0, $row['numero_identificacion'],'LR');
    //                 $pdf->Cell(3,0,$row['nombre_productor'],'LR',0,'R');
    //
    //                 $pdf->Cell(3,0,$row['nombre_granja'],'LR',0,'R');
    //                 $pdf->Cell(2,0,$row['registro_sanitario'],'LR',0,'R');
    //
    //                 $pdf->Cell(2,0,$row['cerdos_carga'],'LR',0,'R');
    //                 $pdf->Cell(2,0,$row['nro_carga'],'LR',0,'R');
    //                 $pdf->Cell(2,0,$row['total_cerdos'],'LR',0,'R');
    //                 $pdf->Cell(2,0,$row['mensual'],'LR',0,'R');
    //                 $pdf->Cell(2,0,$row['mensual']*6,'LR',0,'R');
    //
    //                 $pdf->Cell(2,0,$row['calculado'],'LR',0,'R');
    //                 $pdf->Cell(2,0,$row['dato_calculado_asignado'],'LR',1,'R');
    //
    //             }
    //
    //
    //         }else{
    //             $pdf->CellHead(array('w'=>1.5,'h'=>0, 'txt'=>'CI','border'=>1,'ln'=>0,'align'=>'','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
    //             $pdf->CellHead(array('w'=>3,'h'=>0, 'txt'=>'Nombres','border'=>1,'ln'=>0,'align'=>'','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
    //
    //             $pdf->CellHead(array('w'=>3,'h'=>0, 'txt'=>'Nombre','border'=>1,'ln'=>0,'align'=>'','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
    //             $pdf->CellHead(array('w'=>2,'h'=>0, 'txt'=>'Senasag','border'=>1,'ln'=>0,'align'=>'','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
    //
    //             $pdf->CellHead(array('w'=>1.5,'h'=>0, 'txt'=>'Tipo','border'=>1,'ln'=>0,'align'=>'','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
    //             $pdf->CellHead(array('w'=>2,'h'=>0, 'txt'=>'N° Aves','border'=>1,'ln'=>0,'align'=>'','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
    //             $pdf->CellHead(array('w'=>2,'h'=>0, 'txt'=>'N° Carga','border'=>1,'ln'=>0,'align'=>'','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
    //             $pdf->CellHead(array('w'=>2,'h'=>0, 'txt'=>'Total Aves','border'=>1,'ln'=>0,'align'=>'','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
    //             $pdf->CellHead(array('w'=>2,'h'=>0, 'txt'=>'Mensual','border'=>1,'ln'=>0,'align'=>'','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
    //
    //             $pdf->CellHead(array('w'=>1.5,'h'=>0, 'txt'=>'Peso','border'=>1,'ln'=>0,'align'=>'','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
    //             $pdf->CellHead(array('w'=>1.5,'h'=>0, 'txt'=>'ICA','border'=>1,'ln'=>0,'align'=>'','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
    //             $pdf->CellHead(array('w'=>2,'h'=>0, 'txt'=>'Calculo','border'=>1,'ln'=>0,'align'=>'','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
    //             $pdf->CellHead(array('w'=>2,'h'=>0, 'txt'=>'Asignado','border'=>1,'ln'=>1,'align'=>'','fill'=>true,'font'=>$font,'arr_fill'=>$fill,'text'=>$textColor));
    //
    //             $pdf->AddPage();//la funcion de adicionar pagina debe ir siempre despues de los CellHead porque si no se desconfigura
    //
    //             foreach($body_excel as $row)
    //             {
    //                 $pdf->Cell(1.5,0,$row['numero_identificacion'],'LR');
    //                 $pdf->Cell(3,0,$row['nombre_productor'],0,'R');
    //                 $pdf->Cell(3,0,$row['nombre_granja'],0,'R');
    //                 $pdf->Cell(2,0,$row['registro_sanitario'],'LR',0,'R');
    //                 $pdf->Cell(1.5,0,$row['tipo'],'LR',0,'R');
    //                 $pdf->Cell(2,0,$row['aves_carga'],'LR',0,'R');
    //                 $pdf->Cell(2,0,$row['nro_carga'],'LR',0,'R');
    //                 $pdf->Cell(2,0,$row['total_aves'],'LR',0,'R');
    //                 $pdf->Cell(2,0,$row['mensual'],'LR',0,'R');
    //                 $pdf->Cell(1.5,0,$row['peso'],'LR',0,'R');
    //                 $pdf->Cell(1.5,0,$row['ica'],'LR',0,'R');
    //                 $pdf->Cell(2,0,$row['calculado'],'LR',0,'R');
    //                 $pdf->Cell(2,0,$row['dato_calculado_asignado'],'LR',1,'R');
    //
    //             }
    //
    //
    //         }
    //         $str=$pdf->Output('reporte.pdf','S');
    //         return new Response($str, 200, array('Content-Type' => 'application/pdf'));
    //     //return $this->json($id_requerimiento, Response::HTTP_OK);
    // }

    public function getHeadExcelAsignacion($id_req){
        $em = $this->getDoctrine()->getManager();
        $sql = "SELECT e.razon_social,p.nombre as subproducto, r.gestion, r.periodo, r.id_productor as tipo
                , UPPER(rp.descripcion) tipo_productor
                FROM requerimiento r
                JOIN entidad e ON e.id = r.id_entidad
                JOIN producto p ON p.id = r.id_producto
                JOIN relacion_entidad re ON re.id_entidad = r.id_entidad
                JOIN persona pe ON pe.id = re.id_persona
                JOIN requerimiento_productor rp ON r.id_productor = rp.id
                WHERE r.id = ?
                LIMIT 1";

        $stmt = $em->getConnection()->prepare($sql);
        $stmt->bindValue(1, $id_req);
        $stmt->execute();
        $resultado =  $stmt->fetchAll();

        return $resultado[0];
    }


    public function getBodyExcelAsignacion($id_req,$tipo){
        $em = $this->getDoctrine()->getManager();
        if($tipo == 1){
                $sql = "SELECT ep.numero_identificacion,
                        ep.razon_social as nombre_productor,
                        eg.razon_social as nombre_granja,
                        eg.registro_sanitario,
                        tv.descripcion as tipo,
                        dato_prod1 as aves_carga,
                        dato_prod2 as nro_carga,
                        dato_prod3 as total_aves,
                        mensual, dato_calculado1 as ica,
                        dato_calculado2 as peso,
                        dato_calculado3 as calculado,
                        dato_calculado_asignado
                        FROM requerimiento_detalle rd
                        JOIN entidad ep ON rd.id_entidad_productor = ep.id
                        JOIN entidad eg ON rd.id_entidad_granja = eg.id
                        JOIN tipo_pecuario_vocacion tv ON tv.id = rd.id_tipo_vocacion
                        WHERE id_req = ?";
        } elseif($tipo == 2){
                $sql = "SELECT ep.numero_identificacion,
                        ep.razon_social as nombre_productor,
                        eg.razon_social as nombre_granja,
                        eg.registro_sanitario,
                        tv.descripcion,
                        dato_prod1 as cerdos_carga,
                        dato_prod2 as nro_carga,
                        dato_prod3 as total_cerdos,
                        mensual, dato_calculado1 as calculado,
                        dato_calculado_asignado
                        FROM requerimiento_detalle rd
                        JOIN entidad ep ON rd.id_entidad_productor = ep.id
                        JOIN entidad eg ON rd.id_entidad_granja = eg.id
                        JOIN tipo_pecuario_vocacion tv ON tv.id = rd.id_tipo_vocacion
                        WHERE id_req = ?";
        } elseif($tipo == 3){
            $sql = "SELECT ep.numero_identificacion,
            	ep.razon_social as nombre_productor,
            	eg.razon_social as nombre_granja,
            	eg.registro_sanitario,
            	dato_prod1 as nro_cabezas_hato,
            	dato_prod2 as nro_vacas_produccion,
            	dato_prod3 as litros,
            	mensual
            	, dato_calculado1 as calculado,
            	dato_calculado_asignado
            	FROM requerimiento_detalle rd
            	JOIN entidad ep ON rd.id_entidad_productor = ep.id
            	JOIN entidad eg ON rd.id_entidad_granja = eg.id
            	JOIN tipo_pecuario_vocacion tv ON tv.id = rd.id_tipo_vocacion
            	WHERE id_req = ?";
        } elseif($tipo == 4){
            $sql = "SELECT ep.numero_identificacion,
            	ep.razon_social as nombre_productor,
            	eg.razon_social as nombre_institucion,
            	eg.numero_identificacion nit_institucion,
            	mensual
            	, dato_calculado1 as calculado,
            	dato_calculado_asignado
            	FROM requerimiento_detalle rd
            	JOIN entidad ep ON rd.id_entidad_productor = ep.id
            	JOIN entidad eg ON rd.id_entidad_granja = eg.id
            	JOIN tipo_pecuario_vocacion tv ON tv.id = rd.id_tipo_vocacion
            	WHERE id_req = ?";
        }

        $stmt = $em->getConnection()->prepare($sql);
        $stmt->bindValue(1, $id_req);
        $stmt->execute();
        $resultado =  $stmt->fetchAll();

        return $resultado;
    }

    /**
    * @Route("/asignacion/getperiodo", name="get_periodo")
    */
    public function reporteCumplimiento(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $sql = "SELECT gestion, campania
                FROM public.campania
                GROUP BY gestion, campania
                ORDER BY gestion ASC, campania DESC";

        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $resultado =  $stmt->fetchAll();

        return $this->json($resultado, Response::HTTP_OK);
    }

    /**
    * @Route("/asignacion/getReqProductor")
    */
    public function getReqProductor(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $sql = "SELECT id as data, descripcion as label
            FROM requerimiento_productor
            ORDER BY id";

        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $resultado =  $stmt->fetchAll();

        return $this->json($resultado, Response::HTTP_OK);
    }

}
