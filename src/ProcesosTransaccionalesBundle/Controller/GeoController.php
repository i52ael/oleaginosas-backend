<?php

namespace ProcesosTransaccionalesBundle\Controller;

use ProcesosTransaccionalesBundle\Manager\GeoManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use JMS\DiExtraBundle\Annotation as DI;
use Doctrine\ORM\EntityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Lexik\Bundle\JWTAuthenticationBundle\TokenExtractor\AuthorizationHeaderTokenExtractor;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;


// * @Security("is_granted('ROLE_USER')")
/**
 * @Route("/geo")
 * **/
class GeoController extends Controller
{
    use \ProductoBundle\Helper\Helper;
    protected $geoManager;

    /**
     * @DI\InjectParams({"geoManager"=@DI\Inject("api.manager.geo")})
     */
    public function __construct( GeoManager $geoManager)
    {
        $this->geoManager = $geoManager;
    }

    /**
     * @Route("/cbxDepartamentos")
     * @Method("POST")
     **/
    public function cbxDepartamentos(Request $request)
    {
        $arr=$this->geoManager->cbxDepartamentos();
        return $this->json( $arr,Response::HTTP_OK);
    }

    /**
     * @Route("/cbxMunicipio")
     * @Method("POST")
     **/
    public function cbxProvinciaMunicipio(Request $request)
    {
        $arr_datos = json_decode($request->getContent());
        $arr = $this->geoManager->cbxProvinciaMunicipio($arr_datos->id_departamento, $arr_datos->descrip);
        $this->json( print_r($arr, true),Response::HTTP_SEE_OTHER);
        if(!empty($arr['error']))
            $this->json( $arr['error'],Response::HTTP_SEE_OTHER);
        return $this->json( $arr,Response::HTTP_OK);
    }

}
