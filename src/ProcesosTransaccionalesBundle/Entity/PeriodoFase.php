<?php

namespace ProcesosTransaccionalesBundle\Entity;

/**
 * PeriodoFase
 */
class PeriodoFase
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $fechaDesde;

    /**
     * @var \DateTime
     */
    private $fechaHasta;

    /**
     * @var \ProcesosTransaccionalesBundle\Entity\Fase
     */
    private $idFase;

    /**
     * @var \ProcesosTransaccionalesBundle\Entity\Periodo
     */
    private $idPeriodo;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fechaDesde
     *
     * @param \DateTime $fechaDesde
     *
     * @return PeriodoFase
     */
    public function setFechaDesde($fechaDesde)
    {
        $this->fechaDesde = $fechaDesde;

        return $this;
    }

    /**
     * Get fechaDesde
     *
     * @return \DateTime
     */
    public function getFechaDesde()
    {
        return $this->fechaDesde;
    }

    /**
     * Set fechaHasta
     *
     * @param \DateTime $fechaHasta
     *
     * @return PeriodoFase
     */
    public function setFechaHasta($fechaHasta)
    {
        $this->fechaHasta = $fechaHasta;

        return $this;
    }

    /**
     * Get fechaHasta
     *
     * @return \DateTime
     */
    public function getFechaHasta()
    {
        return $this->fechaHasta;
    }

    /**
     * Set idFase
     *
     * @param \ProcesosTransaccionalesBundle\Entity\Fase $idFase
     *
     * @return PeriodoFase
     */
    public function setIdFase(\ProcesosTransaccionalesBundle\Entity\Fase $idFase = null)
    {
        $this->idFase = $idFase;

        return $this;
    }

    /**
     * Get idFase
     *
     * @return \ProcesosTransaccionalesBundle\Entity\Fase
     */
    public function getIdFase()
    {
        return $this->idFase;
    }

    /**
     * Set idPeriodo
     *
     * @param \ProcesosTransaccionalesBundle\Entity\Periodo $idPeriodo
     *
     * @return PeriodoFase
     */
    public function setIdPeriodo(\ProcesosTransaccionalesBundle\Entity\Periodo $idPeriodo = null)
    {
        $this->idPeriodo = $idPeriodo;

        return $this;
    }

    /**
     * Get idPeriodo
     *
     * @return \ProcesosTransaccionalesBundle\Entity\Periodo
     */
    public function getIdPeriodo()
    {
        return $this->idPeriodo;
    }
}

