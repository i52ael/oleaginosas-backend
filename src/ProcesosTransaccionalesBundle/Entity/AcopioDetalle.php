<?php

namespace ProcesosTransaccionalesBundle\Entity;

/**
 * AcopioDetalle
 */
class AcopioDetalle
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $idProducto;

    /**
     * @var \DateTime
     */
    private $fechaAcopio;

    /**
     * @var int
     */
    private $idTransaccion;

    /**
     * @var string
     */
    private $cantidadBruta;

    /**
     * @var string
     */
    private $cantidadNeta;

    /**
     * @var string
     */
    private $cantidadLiquido;

    /**
     * @var int
     */
    private $idEntidadProveedor;

    /**
     * @var int
     */
    private $idEntidadSocio;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idProducto
     *
     * @param integer $idProducto
     *
     * @return AcopioDetalle
     */
    public function setIdProducto($idProducto)
    {
        $this->idProducto = $idProducto;

        return $this;
    }

    /**
     * Get idProducto
     *
     * @return int
     */
    public function getIdProducto()
    {
        return $this->idProducto;
    }

    /**
     * Set fechaAcopio
     *
     * @param \DateTime $fechaAcopio
     *
     * @return AcopioDetalle
     */
    public function setFechaAcopio($fechaAcopio)
    {
        $this->fechaAcopio = $fechaAcopio;

        return $this;
    }

    /**
     * Get fechaAcopio
     *
     * @return \DateTime
     */
    public function getFechaAcopio()
    {
        return $this->fechaAcopio;
    }

    /**
     * Set idTransaccion
     *
     * @param integer $idTransaccion
     *
     * @return AcopioDetalle
     */
    public function setIdTransaccion($idTransaccion)
    {
        $this->idTransaccion = $idTransaccion;

        return $this;
    }

    /**
     * Get idTransaccion
     *
     * @return int
     */
    public function getIdTransaccion()
    {
        return $this->idTransaccion;
    }

    /**
     * Set cantidadBruta
     *
     * @param string $cantidadBruta
     *
     * @return AcopioDetalle
     */
    public function setCantidadBruta($cantidadBruta)
    {
        $this->cantidadBruta = $cantidadBruta;

        return $this;
    }

    /**
     * Get cantidadBruta
     *
     * @return string
     */
    public function getCantidadBruta()
    {
        return $this->cantidadBruta;
    }

    /**
     * Set cantidadNeta
     *
     * @param string $cantidadNeta
     *
     * @return AcopioDetalle
     */
    public function setCantidadNeta($cantidadNeta)
    {
        $this->cantidadNeta = $cantidadNeta;

        return $this;
    }

    /**
     * Get cantidadNeta
     *
     * @return string
     */
    public function getCantidadNeta()
    {
        return $this->cantidadNeta;
    }

    /**
     * Set cantidadLiquido
     *
     * @param string $cantidadLiquido
     *
     * @return AcopioDetalle
     */
    public function setCantidadLiquido($cantidadLiquido)
    {
        $this->cantidadLiquido = $cantidadLiquido;

        return $this;
    }

    /**
     * Get cantidadLiquido
     *
     * @return string
     */
    public function getCantidadLiquido()
    {
        return $this->cantidadLiquido;
    }

    /**
     * Set idEntidadProveedor
     *
     * @param integer $idEntidadProveedor
     *
     * @return AcopioDetalle
     */
    public function setIdEntidadProveedor($idEntidadProveedor)
    {
        $this->idEntidadProveedor = $idEntidadProveedor;

        return $this;
    }

    /**
     * Get idEntidadProveedor
     *
     * @return int
     */
    public function getIdEntidadProveedor()
    {
        return $this->idEntidadProveedor;
    }

    /**
     * Set idEntidadSocio
     *
     * @param integer $idEntidadSocio
     *
     * @return AcopioDetalle
     */
    public function setIdEntidadSocio($idEntidadSocio)
    {
        $this->idEntidadSocio = $idEntidadSocio;

        return $this;
    }

    /**
     * Get idEntidadSocio
     *
     * @return int
     */
    public function getIdEntidadSocio()
    {
        return $this->idEntidadSocio;
    }
}

