<?php

namespace ProcesosTransaccionalesBundle\Entity;

/**
 * Transaccion
 */
class Transaccion
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $idTipoTransaccion;

    /**
     * @var int
     */
    private $idRelacionEntidad;

    /**
     * @var int
     */
    private $idPeriodoFase;

    /**
     * @var int
     */
    private $idTipoCambio;

    /**
     * @var \DateTime
     */
    private $fechaRegistro;

    /**
     * @var int
     */
    private $idUsuarioUltimaModificacion;

    /**
     * @var int
     */
    private $idUsuarioCreacion;

    /**
     * @var \DateTime
     */
    private $fechaUltimaModificacion;

    /**
     * @var bool
     */
    private $eliminado;

    /**
     * @var bool
     */
    private $validado;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idTipoTransaccion
     *
     * @param integer $idTipoTransaccion
     *
     * @return Transaccion
     */
    public function setIdTipoTransaccion($idTipoTransaccion)
    {
        $this->idTipoTransaccion = $idTipoTransaccion;

        return $this;
    }

    /**
     * Get idTipoTransaccion
     *
     * @return int
     */
    public function getIdTipoTransaccion()
    {
        return $this->idTipoTransaccion;
    }

    /**
     * Set idRelacionEntidad
     *
     * @param integer $idRelacionEntidad
     *
     * @return Transaccion
     */
    public function setIdRelacionEntidad($idRelacionEntidad)
    {
        $this->idRelacionEntidad = $idRelacionEntidad;

        return $this;
    }

    /**
     * Get idRelacionEntidad
     *
     * @return int
     */
    public function getIdRelacionEntidad()
    {
        return $this->idRelacionEntidad;
    }

    /**
     * Set idPeriodoFase
     *
     * @param integer $idPeriodoFase
     *
     * @return Transaccion
     */
    public function setIdPeriodoFase($idPeriodoFase)
    {
        $this->idPeriodoFase = $idPeriodoFase;

        return $this;
    }

    /**
     * Get idPeriodoFase
     *
     * @return int
     */
    public function getIdPeriodoFase()
    {
        return $this->idPeriodoFase;
    }

    /**
     * Set idTipoCambio
     *
     * @param integer $idTipoCambio
     *
     * @return Transaccion
     */
    public function setIdTipoCambio($idTipoCambio)
    {
        $this->idTipoCambio = $idTipoCambio;

        return $this;
    }

    /**
     * Get idTipoCambio
     *
     * @return int
     */
    public function getIdTipoCambio()
    {
        return $this->idTipoCambio;
    }

    /**
     * Set fechaRegistro
     *
     * @param \DateTime $fechaRegistro
     *
     * @return Transaccion
     */
    public function setFechaRegistro($fechaRegistro)
    {
        $this->fechaRegistro = $fechaRegistro;

        return $this;
    }

    /**
     * Get fechaRegistro
     *
     * @return \DateTime
     */
    public function getFechaRegistro()
    {
        return $this->fechaRegistro;
    }

    /**
     * Set idUsuarioUltimaModificacion
     *
     * @param integer $idUsuarioUltimaModificacion
     *
     * @return Transaccion
     */
    public function setIdUsuarioUltimaModificacion($idUsuarioUltimaModificacion)
    {
        $this->idUsuarioUltimaModificacion = $idUsuarioUltimaModificacion;

        return $this;
    }

    /**
     * Get idUsuarioUltimaModificacion
     *
     * @return int
     */
    public function getIdUsuarioUltimaModificacion()
    {
        return $this->idUsuarioUltimaModificacion;
    }

    /**
     * Set idUsuarioCreacion
     *
     * @param integer $idUsuarioCreacion
     *
     * @return Transaccion
     */
    public function setIdUsuarioCreacion($idUsuarioCreacion)
    {
        $this->idUsuarioCreacion = $idUsuarioCreacion;

        return $this;
    }

    /**
     * Get idUsuarioCreacion
     *
     * @return int
     */
    public function getIdUsuarioCreacion()
    {
        return $this->idUsuarioCreacion;
    }

    /**
     * Set fechaUltimaModificacion
     *
     * @param \DateTime $fechaUltimaModificacion
     *
     * @return Transaccion
     */
    public function setFechaUltimaModificacion($fechaUltimaModificacion)
    {
        $this->fechaUltimaModificacion = $fechaUltimaModificacion;

        return $this;
    }

    /**
     * Get fechaUltimaModificacion
     *
     * @return \DateTime
     */
    public function getFechaUltimaModificacion()
    {
        return $this->fechaUltimaModificacion;
    }

    /**
     * Set eliminado
     *
     * @param boolean $eliminado
     *
     * @return Transaccion
     */
    public function setEliminado($eliminado)
    {
        $this->eliminado = $eliminado;

        return $this;
    }

    /**
     * Get eliminado
     *
     * @return bool
     */
    public function getEliminado()
    {
        return $this->eliminado;
    }

    /**
     * Set validado
     *
     * @param boolean $validado
     *
     * @return Transaccion
     */
    public function setValidado($validado)
    {
        $this->validado = $validado;

        return $this;
    }

    /**
     * Get valido
     *
     * @return bool
     */
    public function getValidado()
    {
        return $this->validado;
    }
}

