<?php

namespace ProcesosTransaccionalesBundle\Entity;

/**
 * RipAlmacenamiento
 */
class RipAlmacenamiento
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var float
     */
    private $capacidad;

    /**
     * @var int
     */
    private $idRipInfoProductiva;

    /**
     * @var int
     */
    private $idRipCentroAlmacenamiento;

    /**
     * @var int
     */
    private $idRipTipo;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set capacidad
     *
     * @param string $capacidad
     *
     * @return Entidad
     */
    public function setCapacidad($capacidad)
    {
        $this->capacidad = $capacidad;

        return $this;
    }

    /**
     * Get capacidad
     *
     * @return string
     */
    public function getCapacidad()
    {
        return $this->capacidad;
    }

    /**
     * Set idRipInfoProductiva
     *
     * @param integer $idRipInfoProductiva
     *
     * @return Entidad
     */
    public function setIdRipInfoProductiva($idRipInfoProductiva)
    {
        $this->idRipInfoProductiva = $idRipInfoProductiva;

        return $this;
    }

    /**
     * Get idRipInfoProductiva
     *
     * @return int
     */
    public function getIdRipInfoProductiva()
    {
        return $this->idRipInfoProductiva;
    }

    /**
     * Set idRipCentroAlmacenamiento
     *
     * @param integer $idRipCentroAlmacenamiento
     *
     * @return Entidad
     */
    public function setIdRipCentroAlmacenamiento($idRipCentroAlmacenamiento)
    {
        $this->idRipCentroAlmacenamiento = $idRipCentroAlmacenamiento;

        return $this;
    }

    /**
     * Get idRipCentroAlmacenamiento
     *
     * @return int
     */
    public function getIdRipCentroAlmacenamiento()
    {
        return $this->idRipCentroAlmacenamiento;
    }

    /**
     * Set idRipTipo
     *
     * @param integer $idRipTipo
     *
     * @return Entidad
     */
    public function setIdRipTipo($idRipTipo)
    {
        $this->idRipTipo = $idRipTipo;

        return $this;
    }

    /**
     * Get idRipTipo
     *
     * @return int
     */
    public function getIdRipTipo()
    {
        return $this->idRipTipo;
    }



}
