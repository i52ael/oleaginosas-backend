<?php

namespace ProcesosTransaccionalesBundle\Entity;

/**
 * RipInfoProductiva
 */
class RipMantenimiento
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $mes;

    /**
     * @var \DateTime
     */
    private $dias;

    /**
     * @var int
     */
    private $idRipCentroAlmacenamiento;

    /**
     * @var int
     */
    private $idRipInfoProductiva;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set mes
     *
     * @param integer $mes
     *
     * @return Entidad
     */
    public function setMes($mes)
    {
        $this->mes = $mes;

        return $this;
    }

    /**
     * Get mes
     *
     * @return int
     */
    public function getMes()
    {
        return $this->mes;
    }

    /**
     * Set dias
     *
     * @param integer $dias
     *
     * @return Entidad
     */
    public function setDias($dias)
    {
        $this->dias = $dias;

        return $this;
    }

    /**
     * Get dias
     *
     * @return int
     */
    public function getDias()
    {
        return $this->dias;
    }

    /**
     * Set idRipCentroAlmacenamiento
     *
     * @param integer $idRipCentroAlmacenamiento
     *
     * @return Entidad
     */
    public function setIdRipCentroAlmacenamiento($idRipCentroAlmacenamiento)
    {
        $this->idRipCentroAlmacenamiento = $idRipCentroAlmacenamiento;

        return $this;
    }

    /**
     * Get idRipCentroAlmacenamiento
     *
     * @return int
     */
    public function getIdRipCentroAlmacenamiento()
    {
        return $this->idRipCentroAlmacenamiento;
    }

    /**
     * Set idRipInfoProductiva
     *
     * @param integer $idRipInfoProductiva
     *
     * @return Entidad
     */
    public function setIdRipInfoProductiva($idRipInfoProductiva)
    {
        $this->idRipInfoProductiva = $idRipInfoProductiva;

        return $this;
    }

    /**
     * Get idRipInfoProductiva
     *
     * @return int
     */
    public function getIdRipInfoProductiva()
    {
        return $this->idRipInfoProductiva;
    }

}
