<?php

namespace ProcesosTransaccionalesBundle\Entity;

/**
 * ProduccionDetalle
 */
class ProduccionDetalle
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $cantidadProduccion;

    /**
     * @var string
     */
    private $stockFinal;

    /**
     * @var int
     */
    private $idTransaccion;

    /**
     * @var int
     */
    private $idProducto;

    /**
     * @var int
     */
    private $idEntidadSocio;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cantidadProduccion
     *
     * @param string $cantidadProduccion
     *
     * @return ProduccionDetalle
     */
    public function setCantidadProduccion($cantidadProduccion)
    {
        $this->cantidadProduccion = $cantidadProduccion;

        return $this;
    }

    /**
     * Get cantidadProduccion
     *
     * @return string
     */
    public function getCantidadProduccion()
    {
        return $this->cantidadProduccion;
    }

    /**
     * Set stockFinal
     *
     * @param string $stockFinal
     *
     * @return ProduccionDetalle
     */
    public function setStockFinal($stockFinal)
    {
        $this->stockFinal = $stockFinal;

        return $this;
    }

    /**
     * Get stockFinal
     *
     * @return string
     */
    public function getStockFinal()
    {
        return $this->stockFinal;
    }

    /**
     * Set idTransaccion
     *
     * @param integer $idTransaccion
     *
     * @return ProduccionDetalle
     */
    public function setIdTransaccion($idTransaccion)
    {
        $this->idTransaccion = $idTransaccion;

        return $this;
    }

    /**
     * Get idTransaccion
     *
     * @return int
     */
    public function getIdTransaccion()
    {
        return $this->idTransaccion;
    }

    /**
     * Set id_producto
     *
     * @param integer $idProducto
     *
     * @return ProduccionDetalle
     */
    public function setIdProducto($idProducto)
    {
        $this->idProducto = $idProducto;

        return $this;
    }

    /**
     * Get id_producto
     *
     * @return int
     */
    public function getIdProducto()
    {
        return $this->idProducto;
    }

    /**
     * Set idEntidadSocio
     *
     * @param integer $idEntidadSocio
     *
     * @return ProduccionDetalle
     */
    public function setIdEntidadSocio($idEntidadSocio)
    {
        $this->idEntidadSocio = $idEntidadSocio;

        return $this;
    }

    /**
     * Get idEntidadSocio
     *
     * @return int
     */
    public function getIdEntidadSocio()
    {
        return $this->idEntidadSocio;
    }

}

