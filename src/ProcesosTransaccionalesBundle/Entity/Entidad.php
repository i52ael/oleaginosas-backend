<?php

namespace ProcesosTransaccionalesBundle\Entity;

/**
 * Entidad
 */
class Entidad
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $razonSocial;

    /**
     * @var string
     */
    private $direccion;

    /**
     * @var string
     */
    private $telefonos;

    /**
     * @var string
     */
    private $celular;

    /**
     * @var string
     */
    private $correoElectronico;

    /**
     * @var bool
     */
    private $vigente;

    /**
     * @var bool
     */
    private $eliminado;

    /**
     * @var int
     */
    private $idMunicipio;

    /**
     * @var \DateTime
     */
    private $fechaCreacion;

    /**
     * @var \DateTime
     */
    private $fechaUltimaModificacion;

    /**
     * @var int
     */
    private $idUsuarioCreacion;

    /**
     * @var int
     */
    private $idUsuarioUltimaModificacion;

    /**
     * @var string
     */
    private $numeroIdentificacion;

    /**
     * @var string
     */
    private $fundempresa;

    /**
     * @var string
     */
    private $registroSanitario;

    /**
     * @var int
     */
    private $idTipoEntidad;

    /**
     * @var int
     */
    private $idTipoDocumento;

    /**
     * @var int
     */
    private $idFase;

    /**
     * @var int
     */
    private $idComplejo;

    /**
     * @var bool
     */
    private $beneficiario;
    
     /**
     * @var int
     */
    private $idDepartamento;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set razonSocial
     *
     * @param string $razonSocial
     *
     * @return Entidad
     */
    public function setRazonSocial($razonSocial)
    {
        $this->razonSocial = $razonSocial;

        return $this;
    }

    /**
     * Get razonSocial
     *
     * @return string
     */
    public function getRazonSocial()
    {
        return $this->razonSocial;
    }

    /**
     * Set direccion
     *
     * @param string $direccion
     *
     * @return Entidad
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;

        return $this;
    }

    /**
     * Get direccion
     *
     * @return string
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * Set telefonos
     *
     * @param string $telefonos
     *
     * @return Entidad
     */
    public function setTelefonos($telefonos)
    {
        $this->telefonos = $telefonos;

        return $this;
    }

    /**
     * Get telefonos
     *
     * @return string
     */
    public function getTelefonos()
    {
        return $this->telefonos;
    }

    /**
     * Set celular
     *
     * @param string $celular
     *
     * @return Entidad
     */
    public function setCelular($celular)
    {
        $this->celular = $celular;

        return $this;
    }

    /**
     * Get celular
     *
     * @return string
     */
    public function getCelular()
    {
        return $this->celular;
    }

    /**
     * Set correoElectronico
     *
     * @param string $correoElectronico
     *
     * @return Entidad
     */
    public function setCorreoElectronico($correoElectronico)
    {
        $this->correoElectronico = $correoElectronico;

        return $this;
    }

    /**
     * Get correoElectronico
     *
     * @return string
     */
    public function getCorreoElectronico()
    {
        return $this->correoElectronico;
    }

    /**
     * Set vigente
     *
     * @param boolean $vigente
     *
     * @return Entidad
     */
    public function setVigente($vigente)
    {
        $this->vigente = $vigente;

        return $this;
    }

    /**
     * Get vigente
     *
     * @return bool
     */
    public function getVigente()
    {
        return $this->vigente;
    }

    /**
     * Set eliminado
     *
     * @param boolean $eliminado
     *
     * @return Entidad
     */
    public function setEliminado($eliminado)
    {
        $this->eliminado = $eliminado;

        return $this;
    }

    /**
     * Get eliminado
     *
     * @return bool
     */
    public function getEliminado()
    {
        return $this->eliminado;
    }

    /**
     * Set idMunicipio
     *
     * @param integer $idMunicipio
     *
     * @return Entidad
     */
    public function setIdMunicipio($idMunicipio)
    {
        $this->idMunicipio = $idMunicipio;

        return $this;
    }

    /**
     * Get idMunicipio
     *
     * @return int
     */
    public function getIdMunicipio()
    {
        return $this->idMunicipio;
    }

    /**
     * Set fechaCreacion
     *
     * @param \DateTime $fechaCreacion
     *
     * @return Entidad
     */
    public function setFechaCreacion($fechaCreacion)
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    /**
     * Get fechaCreacion
     *
     * @return \DateTime
     */
    public function getFechaCreacion()
    {
        return $this->fechaCreacion;
    }

    /**
     * Set fechaUltimaModificacion
     *
     * @param \DateTime $fechaUltimaModificacion
     *
     * @return Entidad
     */
    public function setFechaUltimaModificacion($fechaUltimaModificacion)
    {
        $this->fechaUltimaModificacion = $fechaUltimaModificacion;

        return $this;
    }

    /**
     * Get fechaUltimaModificacion
     *
     * @return \DateTime
     */
    public function getFechaUltimaModificacion()
    {
        return $this->fechaUltimaModificacion;
    }

    /**
     * Set idUsuarioCreacion
     *
     * @param integer $idUsuarioCreacion
     *
     * @return Entidad
     */
    public function setIdUsuarioCreacion($idUsuarioCreacion)
    {
        $this->idUsuarioCreacion = $idUsuarioCreacion;

        return $this;
    }

    /**
     * Get idUsuarioCreacion
     *
     * @return int
     */
    public function getIdUsuarioCreacion()
    {
        return $this->idUsuarioCreacion;
    }

    /**
     * Set idUsuarioUltimaModificacion
     *
     * @param integer $idUsuarioUltimaModificacion
     *
     * @return Entidad
     */
    public function setIdUsuarioUltimaModificacion($idUsuarioUltimaModificacion)
    {
        $this->idUsuarioUltimaModificacion = $idUsuarioUltimaModificacion;

        return $this;
    }

    /**
     * Get idUsuarioUltimaModificacion
     *
     * @return int
     */
    public function getIdUsuarioUltimaModificacion()
    {
        return $this->idUsuarioUltimaModificacion;
    }

    /**
     * Set numeroIdentificacion
     *
     * @param string $numeroIdentificacion
     *
     * @return Entidad
     */
    public function setNumeroIdentificacion($numeroIdentificacion)
    {
        $this->numeroIdentificacion = $numeroIdentificacion;

        return $this;
    }

    /**
     * Get numeroIdentificacion
     *
     * @return string
     */
    public function getNumeroIdentificacion()
    {
        return $this->numeroIdentificacion;
    }

    /**
     * Set fundempresa
     *
     * @param string $fundempresa
     *
     * @return Entidad
     */
    public function setFundempresa($fundempresa)
    {
        $this->fundempresa = $fundempresa;

        return $this;
    }

    /**
     * Get fundempresa
     *
     * @return string
     */
    public function getFundempresa()
    {
        return $this->fundempresa;
    }

    /**
     * Set registroSanitario
     *
     * @param string $registroSanitario
     *
     * @return Entidad
     */
    public function setRegistroSanitario($registroSanitario)
    {
        $this->registroSanitario = $registroSanitario;

        return $this;
    }

    /**
     * Get registroSanitario
     *
     * @return string
     */
    public function getRegistroSanitario()
    {
        return $this->registroSanitario;
    }

    /**
     * Set idTipoEntidad
     *
     * @param integer $idTipoEntidad
     *
     * @return Entidad
     */
    public function setIdTipoEntidad($idTipoEntidad)
    {
        $this->idTipoEntidad = $idTipoEntidad;

        return $this;
    }

    /**
     * Get idTipoEntidad
     *
     * @return int
     */
    public function getIdTipoEntidad()
    {
        return $this->idTipoEntidad;
    }

    /**
     * Set idTipoDocumento
     *
     * @param integer $idTipoDocumento
     *
     * @return Entidad
     */
    public function setIdTipoDocumento($idTipoDocumento)
    {
        $this->idTipoDocumento = $idTipoDocumento;

        return $this;
    }

    /**
     * Get idTipoDocumento
     *
     * @return int
     */
    public function getIdTipoDocumento()
    {
        return $this->idTipoDocumento;
    }

    /**
     * Set idFase
     *
     * @param integer $idFase
     *
     * @return Entidad
     */
    public function setIdFase($idFase)
    {
        $this->idFase = $idFase;

        return $this;
    }

    /**
     * Get idFase
     *
     * @return int
     */
    public function getIdFase()
    {
        return $this->idFase;
    }

    /**
     * Set idComplejo
     *
     * @param integer $idComplejo
     *
     * @return Entidad
     */
    public function setIdComplejo($idComplejo)
    {
        $this->idComplejo = $idComplejo;

        return $this;
    }

    /**
     * Get idComplejo
     *
     * @return int
     */
    public function getIdComplejo()
    {
        return $this->idComplejo;
    }

    /**
     * Set beneficiario
     *
     * @param boolean $beneficiario
     *
     * @return Entidad
     */
    public function setBeneficiario($beneficiario)
    {
        $this->beneficiario = $beneficiario;

        return $this;
    }

    /**
     * Get beneficiario
     *
     * @return bool
     */
    public function getBeneficiario()
    {
        return $this->beneficiario;
    }
    
    /**
     * Get idDepartamento
     *
     * @return int
     */
    public function getIdDepartamento()
    {
        return $this->idDepartamento;
    }
    
    /**
     * Set idDepartamento
     *
     * @param integer $idDepartamento
     *
     * @return Entidad
     */
    public function setIdDepartamento($idDepartamento)
    {
        $this->idDepartamento = $idDepartamento;

        return $this;
    }
}

