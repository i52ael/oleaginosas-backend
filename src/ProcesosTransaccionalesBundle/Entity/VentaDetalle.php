<?php

namespace ProcesosTransaccionalesBundle\Entity;

/**
 * VentaDetalle
 */
class VentaDetalle
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $cantidad;

    /**
     * @var string
     */
    private $precioUnitario;

    /**
     * @var string
     */
    private $precioTotal;

    /**
     * @var int
     */
    private $idEntidadCliente;

    /**
     * @var string
     */
    private $nroFactura;

    /**
     * @var \DateTime
     */
    private $fechaVenta;

    /**
     * @var int
     */
    private $idTransaccion;

    /**
     * @var int
     */
    private $idSubproducto;

    /**
     * @var int
     */
    private $idEntidadSocio;

    /**
     * @var int
     */
    private $idMoneda;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cantidad
     *
     * @param string $cantidad
     *
     * @return VentaDetalle
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    /**
     * Get cantidad
     *
     * @return string
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * Set precioUnitario
     *
     * @param string $precioUnitario
     *
     * @return VentaDetalle
     */
    public function setPrecioUnitario($precioUnitario)
    {
        $this->precioUnitario = $precioUnitario;

        return $this;
    }

    /**
     * Get precioUnitario
     *
     * @return string
     */
    public function getPrecioUnitario()
    {
        return $this->precioUnitario;
    }

    /**
     * Set precioTotal
     *
     * @param string $precioTotal
     *
     * @return VentaDetalle
     */
    public function setPrecioTotal($precioTotal)
    {
        $this->precioTotal = $precioTotal;

        return $this;
    }

    /**
     * Get precioTotal
     *
     * @return string
     */
    public function getPrecioTotal()
    {
        return $this->precioTotal;
    }

    /**
     * Set idEntidadCliente
     *
     * @param integer $idEntidadCliente
     *
     * @return VentaDetalle
     */
    public function setIdEntidadCliente($idEntidadCliente)
    {
        $this->idEntidadCliente = $idEntidadCliente;

        return $this;
    }

    /**
     * Get idEntidadCliente
     *
     * @return int
     */
    public function getIdEntidadCliente()
    {
        return $this->idEntidadCliente;
    }

    /**
     * Set nroFactura
     *
     * @param string $nroFactura
     *
     * @return VentaDetalle
     */
    public function setNroFactura($nroFactura)
    {
        $this->nroFactura = $nroFactura;

        return $this;
    }

    /**
     * Get nroFactura
     *
     * @return string
     */
    public function getNroFactura()
    {
        return $this->nroFactura;
    }

    /**
     * Set fechaVenta
     *
     * @param \DateTime $fechaVenta
     *
     * @return VentaDetalle
     */
    public function setFechaVenta($fechaVenta)
    {
        $this->fechaVenta = $fechaVenta;

        return $this;
    }

    /**
     * Get fechaVenta
     *
     * @return \DateTime
     */
    public function getFechaVenta()
    {
        return $this->fechaVenta;
    }

    /**
     * Set idTransaccion
     *
     * @param integer $idTransaccion
     *
     * @return VentaDetalle
     */
    public function setIdTransaccion($idTransaccion)
    {
        $this->idTransaccion = $idTransaccion;

        return $this;
    }

    /**
     * Get idTransaccion
     *
     * @return int
     */
    public function getIdTransaccion()
    {
        return $this->idTransaccion;
    }

    /**
     * Set idSubproducto
     *
     * @param integer $idSubproducto
     *
     * @return VentaDetalle
     */
    public function setIdSubproducto($idSubproducto)
    {
        $this->idSubproducto = $idSubproducto;

        return $this;
    }

    /**
     * Get idSubproducto
     *
     * @return int
     */
    public function getIdSubproducto()
    {
        return $this->idSubproducto;
    }

    /**
     * Set idEntidadSocio
     *
     * @param integer $idEntidadSocio
     *
     * @return VentaDetalle
     */
    public function setIdEntidadSocio($idEntidadSocio)
    {
        $this->idEntidadSocio = $idEntidadSocio;

        return $this;
    }

    /**
     * Get idEntidadSocio
     *
     * @return int
     */
    public function getIdEntidadSocio()
    {
        return $this->idEntidadSocio;
    }

    /**
     * Set idMoneda
     *
     * @param integer $idMoneda
     *
     * @return CompraDetalle
     */
    public function setIdMoneda($idMoneda)
    {
        $this->idMoneda = $idMoneda;

        return $this;
    }

    /**
     * Get idMoneda
     *
     * @return int
     */
    public function getIdMoneda()
    {
        return $this->idMoneda;
    }
}

