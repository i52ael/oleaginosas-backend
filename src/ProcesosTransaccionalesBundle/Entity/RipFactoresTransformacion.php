<?php

namespace ProcesosTransaccionalesBundle\Entity;

/**
 * RipFactoresTransformacion
 */
class RipFactoresTransformacion
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var float
     */
    private $factor;

    /**
     * @var int
     */
    private $idRipInfoProductiva;

    /**
     * @var int
     */
    private $idProducto;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set factor
     *
     * @param float $factor
     *
     * @return Entidad
     */
    public function setFactor($factor)
    {
        $this->factor = $factor;

        return $this;
    }

    /**
     * Get factor
     *
     * @return string
     */
    public function getFactor()
    {
        return $this->factor;
    }

    /**
     * Set idRipInfoProductiva
     *
     * @param integer $idRipInfoProductiva
     *
     * @return Entidad
     */
    public function setIdRipInfoProductiva($idRipInfoProductiva)
    {
        $this->idRipInfoProductiva = $idRipInfoProductiva;

        return $this;
    }

    /**
     * Get idRipInfoProductiva
     *
     * @return int
     */
    public function getIdRipInfoProductiva()
    {
        return $this->idEntidad;
    }

    /**
     * Set idProducto
     *
     * @param integer $idProducto
     *
     * @return Entidad
     */
    public function setIdProducto($idProducto)
    {
        $this->idProducto = $idProducto;

        return $this;
    }

    /**
     * Get idProducto
     *
     * @return int
     */
    public function getIdProducto()
    {
        return $this->idProducto;
    }

}
