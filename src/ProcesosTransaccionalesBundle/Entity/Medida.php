<?php

namespace ProcesosTransaccionalesBundle\Entity;

/**
 * Medida
 */
class Medida
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $nombre;

    /**
     * @var string
     */
    private $abreviacion;

    /**
     * @var \ProcesosTransaccionalesBundle\Entity\Magnitud
     */
    private $idMagnitud;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Medida
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set abreviacion
     *
     * @param string $abreviacion
     *
     * @return Medida
     */
    public function setAbreviacion($abreviacion)
    {
        $this->abreviacion = $abreviacion;

        return $this;
    }

    /**
     * Get abreviacion
     *
     * @return string
     */
    public function getAbreviacion()
    {
        return $this->abreviacion;
    }

    /**
     * Set idMagnitud
     *
     * @param \ProcesosTransaccionalesBundle\Entity\Magnitud $idMagnitud
     *
     * @return Medida
     */
    public function setIdMagnitud(\ProcesosTransaccionalesBundle\Entity\Magnitud $idMagnitud = null)
    {
        $this->idMagnitud = $idMagnitud;

        return $this;
    }

    /**
     * Get idMagnitud
     *
     * @return \ProcesosTransaccionalesBundle\Entity\Magnitud
     */
    public function getIdMagnitud()
    {
        return $this->idMagnitud;
    }
}

