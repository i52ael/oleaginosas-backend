<?php

namespace ProcesosTransaccionalesBundle\Entity;

/**
 * FaseTipoTransaccion
 */
class FaseTipoTransaccion
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $idFase;

    /**
     * @var int
     */
    private $idTipoTransaccion;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idFase
     *
     * @param integer $idFase
     *
     * @return FaseTipoTransaccion
     */
    public function setIdFase($idFase)
    {
        $this->idFase = $idFase;

        return $this;
    }

    /**
     * Get idFase
     *
     * @return int
     */
    public function getIdFase()
    {
        return $this->idFase;
    }

    /**
     * Set idTipoTransaccion
     *
     * @param integer $idTipoTransaccion
     *
     * @return FaseTipoTransaccion
     */
    public function setIdTipoTransaccion($idTipoTransaccion)
    {
        $this->idTipoTransaccion = $idTipoTransaccion;

        return $this;
    }

    /**
     * Get idTipoTransaccion
     *
     * @return int
     */
    public function getIdTipoTransaccion()
    {
        return $this->idTipoTransaccion;
    }
}

