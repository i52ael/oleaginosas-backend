<?php

namespace ProcesosTransaccionalesBundle\Entity;

/**
 * Parrilleros
 */
class Parrilleros
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $diasCiclo = '0';

    /**
     * @var string
     */
    private $pesoAve = '0';

    /**
     * @var string
     */
    private $ica = '0';

    /**
     * @var integer
     */
    private $idDepartamento;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set diasCiclo
     *
     * @param string $diasCiclo
     *
     * @return Parrilleros
     */
    public function setDiasCiclo($diasCiclo)
    {
        $this->diasCiclo = $diasCiclo;

        return $this;
    }

    /**
     * Get diasCiclo
     *
     * @return string
     */
    public function getDiasCiclo()
    {
        return $this->diasCiclo;
    }

    /**
     * Set pesoAve
     *
     * @param string $pesoAve
     *
     * @return Parrilleros
     */
    public function setPesoAve($pesoAve)
    {
        $this->pesoAve = $pesoAve;

        return $this;
    }

    /**
     * Get pesoAve
     *
     * @return string
     */
    public function getPesoAve()
    {
        return $this->pesoAve;
    }

    /**
     * Set ica
     *
     * @param string $ica
     *
     * @return Parrilleros
     */
    public function setIca($ica)
    {
        $this->ica = $ica;

        return $this;
    }

    /**
     * Get ica
     *
     * @return string
     */
    public function getIca()
    {
        return $this->ica;
    }

    /**
     * Set idDepartamento
     *
     * @param string $idDepartamento
     *
     * @return Parrilleros
     */
    public function setIdDepartamento($idDepartamento)
    {
        $this->idDepartamento = $idDepartamento;

        return $this;
    }

    /**
     * Get idDepartamento
     *
     * @return integer
     */
    public function getIdDepartamento()
    {
        return $this->idDepartamento;
    }
}

