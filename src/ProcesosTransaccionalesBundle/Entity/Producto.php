<?php

namespace ProcesosTransaccionalesBundle\Entity;

/**
 * Producto
 */
class Producto
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $nombre;

    /**
     * @var int
     */
    private $idFase;

    /**
     * @var int
     */
    private $idComplejo;

    /**
     * @var bool
     */
    private $vigente;

    /**
     * @var bool
     */
    private $eliminado;

    /**
     * @var \DateTime
     */
    private $fechaCreacion;

    /**
     * @var \DateTime
     */
    private $fechaUltimaModificacion;

    /**
     * @var int
     */
    private $idUsuarioCreacion;

    /**
     * @var int
     */
    private $idUsuarioUltimaModificacion;

    /**
     * @var string
     */
    private $medida;

    /**
     * @var int
     */
    private $idMedida;

    /**
     * @var string
     */
    private $descripcion;

    /**
     * @var string
     */
    private $codigo;

    /**
     * @var string
     */
    private $nandina;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Producto
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set idFase
     *
     * @param integer $idFase
     *
     * @return Producto
     */
    public function setIdFase($idFase)
    {
        $this->idFase = $idFase;

        return $this;
    }

    /**
     * Get idFase
     *
     * @return int
     */
    public function getIdFase()
    {
        return $this->idFase;
    }

    /**
     * Set idComplejo
     *
     * @param integer $idComplejo
     *
     * @return Producto
     */
    public function setIdComplejo($idComplejo)
    {
        $this->idComplejo = $idComplejo;

        return $this;
    }

    /**
     * Get idComplejo
     *
     * @return int
     */
    public function getIdComplejo()
    {
        return $this->idComplejo;
    }

    /**
     * Set vigente
     *
     * @param boolean $vigente
     *
     * @return Producto
     */
    public function setVigente($vigente)
    {
        $this->vigente = $vigente;

        return $this;
    }

    /**
     * Get vigente
     *
     * @return bool
     */
    public function getVigente()
    {
        return $this->vigente;
    }

    /**
     * Set eliminado
     *
     * @param boolean $eliminado
     *
     * @return Producto
     */
    public function setEliminado($eliminado)
    {
        $this->eliminado = $eliminado;

        return $this;
    }

    /**
     * Get eliminado
     *
     * @return bool
     */
    public function getEliminado()
    {
        return $this->eliminado;
    }

    /**
     * Set fechaCreacion
     *
     * @param \DateTime $fechaCreacion
     *
     * @return Producto
     */
    public function setFechaCreacion($fechaCreacion)
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    /**
     * Get fechaCreacion
     *
     * @return \DateTime
     */
    public function getFechaCreacion()
    {
        return $this->fechaCreacion;
    }

    /**
     * Set fechaUltimaModificacion
     *
     * @param \DateTime $fechaUltimaModificacion
     *
     * @return Producto
     */
    public function setFechaUltimaModificacion($fechaUltimaModificacion)
    {
        $this->fechaUltimaModificacion = $fechaUltimaModificacion;

        return $this;
    }

    /**
     * Get fechaUltimaModificacion
     *
     * @return \DateTime
     */
    public function getFechaUltimaModificacion()
    {
        return $this->fechaUltimaModificacion;
    }

    /**
     * Set idUsuarioCreacion
     *
     * @param integer $idUsuarioCreacion
     *
     * @return Producto
     */
    public function setIdUsuarioCreacion($idUsuarioCreacion)
    {
        $this->idUsuarioCreacion = $idUsuarioCreacion;

        return $this;
    }

    /**
     * Get idUsuarioCreacion
     *
     * @return int
     */
    public function getIdUsuarioCreacion()
    {
        return $this->idUsuarioCreacion;
    }

    /**
     * Set idUsuarioUltimaModificacion
     *
     * @param integer $idUsuarioUltimaModificacion
     *
     * @return Producto
     */
    public function setIdUsuarioUltimaModificacion($idUsuarioUltimaModificacion)
    {
        $this->idUsuarioUltimaModificacion = $idUsuarioUltimaModificacion;

        return $this;
    }

    /**
     * Get idUsuarioUltimaModificacion
     *
     * @return int
     */
    public function getIdUsuarioUltimaModificacion()
    {
        return $this->idUsuarioUltimaModificacion;
    }

    /**
     * Set medida
     *
     * @param string $medida
     *
     * @return Producto
     */
    public function setMedida($medida)
    {
        $this->medida = $medida;

        return $this;
    }

    /**
     * Get medida
     *
     * @return string
     */
    public function getMedida()
    {
        return $this->medida;
    }

    /**
     * Set idMedida
     *
     * @param integer $idMedida
     *
     * @return Producto
     */
    public function setIdMedida($idMedida)
    {
        $this->idMedida = $idMedida;

        return $this;
    }

    /**
     * Get idMedida
     *
     * @return int
     */
    public function getIdMedida()
    {
        return $this->idMedida;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Producto
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set codigo
     *
     * @param string $codigo
     *
     * @return Producto
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo
     *
     * @return string
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * @param string $nandina
     * @return Producto
     */
    public function setNandina($nandina)
    {
        $this->nandina = $nandina;

        return $this;
    }

    /**
     * Get codigo
     *
     * @return string
     */
    public function getNandina()
    {
        return $this->nandina;
    }
}

