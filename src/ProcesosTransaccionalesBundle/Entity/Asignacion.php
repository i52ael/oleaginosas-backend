<?php

namespace ProcesosTransaccionalesBundle\Entity;

/**
 * Asignacion
 */
class Asignacion
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $nroAnimales = '0';

    /**
     * @var string
     */
    private $pesoAnimales = '0';

    /**
     * @var string
     */
    private $nroAnimalesMadres = '0';

    /**
     * @var string
     */
    private $consumoAb = '0';

    /**
     * @var string
     */
    private $porcentageHssAb = '0';

    /**
     * @var string
     */
    private $cantidadAsignada = '0';

    /**
     * @var string
     */
    private $limiteAsignacion = '0';

    /**
     * @var \DateTime
     */
    private $fechaInicioAsignacion;

    /**
     * @var \DateTime
     */
    private $fechaFinalAsignacion;

    /**
     * @var \ProcesosTransaccionalesBundle\Entity\Entidad
     */
    private $idEntidadProveedor;

    /**
     * @var \ProcesosTransaccionalesBundle\Entity\Producto
     */
    private $idProducto;

    /**
     * @var \ProcesosTransaccionalesBundle\Entity\Producto
     */
    private $idProductor;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nroAnimales
     *
     * @param integer $nroAnimales
     *
     * @return Asignacion
     */
    public function setNroAnimales($nroAnimales)
    {
        $this->nroAnimales = $nroAnimales;

        return $this;
    }

    /**
     * Get nroAnimales
     *
     * @return integer
     */
    public function getNroAnimales()
    {
        return $this->nroAnimales;
    }

    /**
     * Set pesoAnimales
     *
     * @param string $pesoAnimales
     *
     * @return Asignacion
     */
    public function setPesoAnimales($pesoAnimales)
    {
        $this->pesoAnimales = $pesoAnimales;

        return $this;
    }

    /**
     * Get pesoAnimales
     *
     * @return string
     */
    public function getPesoAnimales()
    {
        return $this->pesoAnimales;
    }

    /**
     * Set nroAnimalesMadres
     *
     * @param string $nroAnimalesMadres
     *
     * @return Asignacion
     */
    public function setNroAnimalesMadres($nroAnimalesMadres)
    {
        $this->nroAnimalesMadres = $nroAnimalesMadres;

        return $this;
    }

    /**
     * Get nroAnimalesMadres
     *
     * @return string
     */
    public function getNroAnimalesMadres()
    {
        return $this->nroAnimalesMadres;
    }

    /**
     * Set consumoAb
     *
     * @param string $consumoAb
     *
     * @return Asignacion
     */
    public function setConsumoAb($consumoAb)
    {
        $this->consumoAb = $consumoAb;

        return $this;
    }

    /**
     * Get consumoAb
     *
     * @return string
     */
    public function getConsumoAb()
    {
        return $this->consumoAb;
    }

    /**
     * Set porcentageHssAb
     *
     * @param string $porcentageHssAb
     *
     * @return Asignacion
     */
    public function setPorcentageHssAb($porcentageHssAb)
    {
        $this->porcentageHssAb = $porcentageHssAb;

        return $this;
    }

    /**
     * Get porcentageHssAb
     *
     * @return string
     */
    public function getPorcentageHssAb()
    {
        return $this->porcentageHssAb;
    }

    /**
     * Set cantidadAsignada
     *
     * @param string $cantidadAsignada
     *
     * @return Asignacion
     */
    public function setCantidadAsignada($cantidadAsignada)
    {
        $this->cantidadAsignada = $cantidadAsignada;

        return $this;
    }

    /**
     * Get cantidadAsignada
     *
     * @return string
     */
    public function getCantidadAsignada()
    {
        return $this->cantidadAsignada;
    }

    /**
     * Set limiteAsignacion
     *
     * @param string $limiteAsignacion
     *
     * @return Asignacion
     */
    public function setLimiteAsignacion($limiteAsignacion)
    {
        $this->limiteAsignacion = $limiteAsignacion;

        return $this;
    }

    /**
     * Get limiteAsignacion
     *
     * @return string
     */
    public function getLimiteAsignacion()
    {
        return $this->limiteAsignacion;
    }

    /**
     * Set fechaInicioAsignacion
     *
     * @param \DateTime $fechaInicioAsignacion
     *
     * @return Asignacion
     */
    public function setFechaInicioAsignacion($fechaInicioAsignacion)
    {
        $this->fechaInicioAsignacion = $fechaInicioAsignacion;

        return $this;
    }

    /**
     * Get fechaInicioAsignacion
     *
     * @return \DateTime
     */
    public function getFechaInicioAsignacion()
    {
        return $this->fechaInicioAsignacion;
    }

    /**
     * Set fechaFinalAsignacion
     *
     * @param \DateTime $fechaFinalAsignacion
     *
     * @return Asignacion
     */
    public function setFechaFinalAsignacion($fechaFinalAsignacion)
    {
        $this->fechaFinalAsignacion = $fechaFinalAsignacion;

        return $this;
    }

    /**
     * Get fechaFinalAsignacion
     *
     * @return \DateTime
     */
    public function getFechaFinalAsignacion()
    {
        return $this->fechaFinalAsignacion;
    }

    /**
     * Set idEntidadProveedor
     *
     * @param \ProcesosTransaccionalesBundle\Entity\Entidad $idEntidadProveedor
     *
     * @return Asignacion
     */
    public function setIdEntidadProveedor(\ProcesosTransaccionalesBundle\Entity\Entidad $idEntidadProveedor = null)
    {
        $this->idEntidadProveedor = $idEntidadProveedor;

        return $this;
    }

    /**
     * Get idEntidadProveedor
     *
     * @return \ProcesosTransaccionalesBundle\Entity\Entidad
     */
    public function getIdEntidadProveedor()
    {
        return $this->idEntidadProveedor;
    }

    /**
     * Set idProducto
     *
     * @param \ProcesosTransaccionalesBundle\Entity\Producto $idProducto
     *
     * @return Asignacion
     */
    public function setIdProducto(\ProcesosTransaccionalesBundle\Entity\Producto $idProducto = null)
    {
        $this->idProducto = $idProducto;

        return $this;
    }

    /**
     * Get idProducto
     *
     * @return \ProcesosTransaccionalesBundle\Entity\Producto
     */
    public function getIdProducto()
    {
        return $this->idProducto;
    }

    /**
     * Set idProductor
     *
     * @param \ProcesosTransaccionalesBundle\Entity\Producto $idProductor
     *
     * @return Asignacion
     */
    public function setIdProductor(\ProcesosTransaccionalesBundle\Entity\Producto $idProductor = null)
    {
        $this->idProductor = $idProductor;

        return $this;
    }

    /**
     * Get idProductor
     *
     * @return \ProcesosTransaccionalesBundle\Entity\Producto
     */
    public function getIdProductor()
    {
        return $this->idProductor;
    }
}



