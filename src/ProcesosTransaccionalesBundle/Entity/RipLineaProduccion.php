<?php

namespace ProcesosTransaccionalesBundle\Entity;

/**
 * RipLineaProduccion
 */
class RipLineaProduccion
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $descripcion;

    /**
     * @var bool
     */
    private $eliminado;

    /**
     * @var int
     */
    private $idRipCentroAlmacenamiento;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return RipLineaProduccion
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set eliminado
     *
     * @param boolean $eliminado
     *
     * @return RipLineaProduccion
     */
    public function setEliminado($eliminado)
    {
        $this->eliminado = $eliminado;

        return $this;
    }

    /**
     * Get eliminado
     *
     * @return bool
     */
    public function getEliminado()
    {
        return $this->eliminado;
    }

    /**
     * Set idRipCentroAlmacenamiento
     *
     * @param int $idRipCentroAlmacenamiento
     *
     * @return RipLineaProduccion
     */
    public function setIdRipCentroAlmacenamiento($idRipCentroAlmacenamiento)
    {
        $this->idRipCentroAlmacenamiento = $idRipCentroAlmacenamiento;

        return $this;
    }

    /**
     * Get idRipCentroAlmacenamiento
     *
     * @return int
     */
    public function getIdRipCentroAlmacenamiento()
    {
        return $this->idRipCentroAlmacenamiento;
    }

}
