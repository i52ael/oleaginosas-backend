<?php

namespace ProcesosTransaccionalesBundle\Entity;

/**
 * ArchivoExcel
 */
class ArchivoExcel
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $pathArchivo;

    /**
     * @var string
     */
    private $nombreArchivo;

    /**
     * @var int
     */
    private $idPeriodoFase;

    /**
     * @var int
     */
    private $idEntidad;

    /**
     * @var \DateTime
     */
    private $fechaUpload;

    /**
     * @var bool
     */
    private $eliminado;

    /**
     * @var \DateTime
     */
    private $fechaEliminado;

    /**
     * @var int
     */
    private $idUsuarioUpload;

    /**
     * @var int
     */
    private $idUsuarioEliminado;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pathArchivo
     *
     * @param string $pathArchivo
     *
     * @return ArchivoExcel
     */
    public function setPathArchivo($pathArchivo)
    {
        $this->pathArchivo = $pathArchivo;

        return $this;
    }

    /**
     * Get pathArchivo
     *
     * @return string
     */
    public function getPathArchivo()
    {
        return $this->pathArchivo;
    }

    /**
     * Set pathArchivo
     *
     * @param string $nombreArchivo
     *
     * @return ArchivoExcel
     */
    public function setNombreArchivo($nombreArchivo)
    {
        $this->nombreArchivo = $nombreArchivo;

        return $this;
    }

    /**
     * Get nombreArchivo
     *
     * @return string
     */
    public function getNombreArchivo()
    {
        return $this->nombreArchivo;
    }

    /**
     * Set idPeriodoFase
     *
     * @param integer $idPeriodoFase
     *
     * @return ArchivoExcel
     */
    public function setIdPeriodoFase($idPeriodoFase)
    {
        $this->idPeriodoFase = $idPeriodoFase;

        return $this;
    }

    /**
     * Get idPeriodoFase
     *
     * @return int
     */
    public function getIdPeriodoFase()
    {
        return $this->idPeriodoFase;
    }

    /**
     * Set idEntidad
     *
     * @param integer $idEntidad
     *
     * @return ArchivoExcel
     */
    public function setIdEntidad($idEntidad)
    {
        $this->idEntidad = $idEntidad;

        return $this;
    }

    /**
     * Get idEntidad
     *
     * @return int
     */
    public function getIdEntidad()
    {
        return $this->idEntidad;
    }

    /**
     * Set fechaUpload
     *
     * @param \DateTime $fechaUpload
     *
     * @return ArchivoExcel
     */
    public function setFechaUpload($fechaUpload)
    {
        $this->fechaUpload = $fechaUpload;

        return $this;
    }

    /**
     * Get fechaUpload
     *
     * @return \DateTime
     */
    public function getFechaUpload()
    {
        return $this->fechaUpload;
    }

    /**
     * Set eliminado
     *
     * @param boolean $eliminado
     *
     * @return ArchivoExcel
     */
    public function setEliminado($eliminado)
    {
        $this->eliminado = $eliminado;

        return $this;
    }

    /**
     * Get eliminado
     *
     * @return bool
     */
    public function getEliminado()
    {
        return $this->eliminado;
    }

    /**
     * Set fechaEliminado
     *
     * @param \DateTime $fechaEliminado
     *
     * @return ArchivoExcel
     */
    public function setFechaEliminado($fechaEliminado)
    {
        $this->fechaEliminado = $fechaEliminado;

        return $this;
    }

    /**
     * Get fechaEliminado
     *
     * @return \DateTime
     */
    public function getFechaEliminado()
    {
        return $this->fechaEliminado;
    }

    /**
     * Set idUsuarioUpload
     *
     * @param integer $idUsuarioUpload
     *
     * @return ArchivoExcel
     */
    public function setIdUsuarioUpload($idUsuarioUpload)
    {
        $this->idUsuarioUpload = $idUsuarioUpload;

        return $this;
    }

    /**
     * Get idUsuarioUpload
     *
     * @return int
     */
    public function getIdUsuarioUpload()
    {
        return $this->idUsuarioUpload;
    }

    /**
     * Set idUsuarioEliminado
     *
     * @param integer $idUsuarioEliminado
     *
     * @return ArchivoExcel
     */
    public function setIdUsuarioEliminado($idUsuarioEliminado)
    {
        $this->idUsuarioEliminado = $idUsuarioEliminado;

        return $this;
    }

    /**
     * Get idUsuarioEliminado
     *
     * @return int
     */
    public function getIdUsuarioEliminado()
    {
        return $this->idUsuarioEliminado;
    }
}

