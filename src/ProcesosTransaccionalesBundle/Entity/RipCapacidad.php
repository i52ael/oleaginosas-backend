<?php

namespace ProcesosTransaccionalesBundle\Entity;

/**
 * RipCapacidad
 */
class RipCapacidad
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var float
     */
    private $dia;

    /**
     * @var float
     */
    private $mes;

    /**
     * @var float
     */
    private $gestion;

    /**
     * @var float
     */
    private $almacenamiento;

    /**
     * @var int
     */
    private $idRipCentroAlmacenamiento;

    /**
     * @var int
     */
    private $idRipTipo;

    /**
     * @var int
     */
    private $idRipInfoProductiva;

    /**
     * @var int
     */
    private $idProducto;

    /**
     * @var int
     */
    private $idRipLineaProduccion;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dia
     *
     * @param float $dia
     *
     * @return Entidad
     */
    public function setDia($dia)
    {
        $this->dia = $dia;

        return $this;
    }

    /**
     * Get dia
     *
     * @return float
     */
    public function getDia()
    {
        return $this->dia;
    }

    /**
     * Set mes
     *
     * @param float $mes
     *
     * @return Entidad
     */
    public function setMes($mes)
    {
        $this->mes = $mes;

        return $this;
    }

    /**
     * Get mes
     *
     * @return float
     */
    public function getMes()
    {
        return $this->mes;
    }

    /**
     * Set gestion
     *
     * @param float $gestion
     *
     * @return Entidad
     */
    public function setGestion($gestion)
    {
        $this->gestion = $gestion;

        return $this;
    }

    /**
     * Get gestion
     *
     * @return float
     */
    public function getGestion()
    {
        return $this->gestion;
    }

    /**
     * Set almacenamiento
     *
     * @param float $almacenamiento
     *
     * @return Entidad
     */
    public function setAlmacenamiento($almacenamiento)
    {
        $this->almacenamiento = $almacenamiento;

        return $this;
    }

    /**
     * Get almacenamiento
     *
     * @return float
     */
    public function getAlmacenamiento()
    {
        return $this->almacenamiento;
    }

    /**
     * Set idRipCentroAlmacenamiento
     *
     * @param integer $idRipCentroAlmacenamiento
     *
     * @return Entidad
     */
    public function setIdRipCentroAlmacenamiento($idRipCentroAlmacenamiento)
    {
        $this->idRipCentroAlmacenamiento = $idRipCentroAlmacenamiento;

        return $this;
    }

    /**
     * Get idRipCentroAlmacenamiento
     *
     * @return int
     */
    public function getIdRipCentroAlmacenamiento()
    {
        return $this->idRipCentroAlmacenamiento;
    }

    /**
     * Set idRipTipo
     *
     * @param integer $idRipTipo
     *
     * @return Entidad
     */
    public function setIdRipTipo($idRipTipo)
    {
        $this->idRipTipo = $idRipTipo;

        return $this;
    }

    /**
     * Get idRipTipo
     *
     * @return int
     */
    public function getIdRipTipo()
    {
        return $this->idRipTipo;
    }

    /**
     * Set idRipInfoProductiva
     *
     * @param integer $idRipInfoProductiva
     *
     * @return Entidad
     */
    public function setIdRipInfoProductiva($idRipInfoProductiva)
    {
        $this->idRipInfoProductiva = $idRipInfoProductiva;

        return $this;
    }

    /**
     * Get idRipInfoProductiva
     *
     * @return int
     */
    public function getIdRipInfoProductiva()
    {
        return $this->idRipInfoProductiva;
    }

    /**
     * Set idProducto
     *
     * @param integer $idProducto
     *
     * @return Entidad
     */
    public function setIdProducto($idProducto)
    {
        $this->idProducto = $idProducto;

        return $this;
    }

    /**
     * Get idProducto
     *
     * @return int
     */
    public function getIdProducto()
    {
        return $this->idProducto;
    }

    /**
     * Set idRipLineaProduccion
     *
     * @param integer $idRipLineaProduccion
     *
     * @return Entidad
     */
    public function setIdRipLineaProduccion($idRipLineaProduccion)
    {
        $this->idRipLineaProduccion = $idRipLineaProduccion;

        return $this;
    }

    /**
     * Get idRipLineaProduccion
     *
     * @return int
     */
    public function getIdRipLineaProduccion()
    {
        return $this->idRipLineaProduccion;
    }

}
