<?php

namespace ProcesosTransaccionalesBundle\Entity;

/**
 * Subproducto
 */
class Subproducto
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $nombreComercial;

    /**
     * @var int
     */
    private $idEntidad;

    /**
     * @var int
     */
    private $idFase;

    /**
     * @var int
     */
    private $idProducto;

    /**
     * @var string
     */
    private $medida;

    /**
     * @var int
     */
    private $idMedida;

    /**
     * @var bool
     */
    private $eliminado;

    /**
     * @var \DateTime
     */
    private $fechaCreacion;

    /**
     * @var \DateTime
     */
    private $fechaUltimaModificacion;

    /**
     * @var int
     */
    private $idUsuarioCreacion;

    /**
     * @var int
     */
    private $idUsuarioUltimaModificacion;

    /**
     * @var string
     */
    private $densidad;

    /**
     * @var string
     */
    private $codigo;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombreComercial
     *
     * @param string $nombreComercial
     *
     * @return Subproducto
     */
    public function setNombreComercial($nombreComercial)
    {
        $this->nombreComercial = $nombreComercial;

        return $this;
    }

    /**
     * Get nombreComercial
     *
     * @return string
     */
    public function getNombreComercial()
    {
        return $this->nombreComercial;
    }

    /**
     * Set idEntidad
     *
     * @param integer $idEntidad
     *
     * @return Subproducto
     */
    public function setIdEntidad($idEntidad)
    {
        $this->idEntidad = $idEntidad;

        return $this;
    }

    /**
     * Get idEntidad
     *
     * @return int
     */
    public function getIdEntidad()
    {
        return $this->idEntidad;
    }

    /**
     * Set idFase
     *
     * @param integer $idFase
     *
     * @return Subproducto
     */
    public function setIdFase($idFase)
    {
        $this->idFase = $idFase;

        return $this;
    }

    /**
     * Get idFase
     *
     * @return int
     */
    public function getIdFase()
    {
        return $this->idFase;
    }

    /**
     * Set idProducto
     *
     * @param integer $idProducto
     *
     * @return Subproducto
     */
    public function setIdProducto($idProducto)
    {
        $this->idProducto = $idProducto;

        return $this;
    }

    /**
     * Get idProducto
     *
     * @return int
     */
    public function getIdProducto()
    {
        return $this->idProducto;
    }

    /**
     * Set medida
     *
     * @param string $medida
     *
     * @return Subproducto
     */
    public function setMedida($medida)
    {
        $this->medida = $medida;

        return $this;
    }

    /**
     * Get medida
     *
     * @return string
     */
    public function getMedida()
    {
        return $this->medida;
    }

    /**
     * Set idMedida
     *
     * @param integer $idMedida
     *
     * @return Subproducto
     */
    public function setIdMedida($idMedida)
    {
        $this->idMedida = $idMedida;

        return $this;
    }

    /**
     * Get idMedida
     *
     * @return int
     */
    public function getIdMedida()
    {
        return $this->idMedida;
    }

    /**
     * Set eliminado
     *
     * @param boolean $eliminado
     *
     * @return Subproducto
     */
    public function setEliminado($eliminado)
    {
        $this->eliminado = $eliminado;

        return $this;
    }

    /**
     * Get eliminado
     *
     * @return bool
     */
    public function getEliminado()
    {
        return $this->eliminado;
    }

    /**
     * Set fechaCreacion
     *
     * @param \DateTime $fechaCreacion
     *
     * @return Subproducto
     */
    public function setFechaCreacion($fechaCreacion)
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    /**
     * Get fechaCreacion
     *
     * @return \DateTime
     */
    public function getFechaCreacion()
    {
        return $this->fechaCreacion;
    }

    /**
     * Set fechaUltimaModificacion
     *
     * @param \DateTime $fechaUltimaModificacion
     *
     * @return Subproducto
     */
    public function setFechaUltimaModificacion($fechaUltimaModificacion)
    {
        $this->fechaUltimaModificacion = $fechaUltimaModificacion;

        return $this;
    }

    /**
     * Get fechaUltimaModificacion
     *
     * @return \DateTime
     */
    public function getFechaUltimaModificacion()
    {
        return $this->fechaUltimaModificacion;
    }

    /**
     * Set idUsuarioCreacion
     *
     * @param integer $idUsuarioCreacion
     *
     * @return Subproducto
     */
    public function setIdUsuarioCreacion($idUsuarioCreacion)
    {
        $this->idUsuarioCreacion = $idUsuarioCreacion;

        return $this;
    }

    /**
     * Get idUsuarioCreacion
     *
     * @return int
     */
    public function getIdUsuarioCreacion()
    {
        return $this->idUsuarioCreacion;
    }

    /**
     * Set idUsuarioUltimaModificacion
     *
     * @param integer $idUsuarioUltimaModificacion
     *
     * @return Subproducto
     */
    public function setIdUsuarioUltimaModificacion($idUsuarioUltimaModificacion)
    {
        $this->idUsuarioUltimaModificacion = $idUsuarioUltimaModificacion;

        return $this;
    }

    /**
     * Get idUsuarioUltimaModificacion
     *
     * @return int
     */
    public function getIdUsuarioUltimaModificacion()
    {
        return $this->idUsuarioUltimaModificacion;
    }

    /**
     * Set densidad
     *
     * @param string $densidad
     *
     * @return Subproducto
     */
    public function setDensidad($densidad)
    {
        $this->densidad = $densidad;

        return $this;
    }

    /**
     * Get densidad
     *
     * @return string
     */
    public function getDensidad()
    {
        return $this->densidad;
    }

    /**
     * Set codigo
     *
     * @param string $codigo
     *
     * @return Producto
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo
     *
     * @return string
     */
    public function getCodigo()
    {
        return $this->codigo;
    }
}

