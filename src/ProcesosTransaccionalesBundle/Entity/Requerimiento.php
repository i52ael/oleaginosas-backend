<?php

namespace ProcesosTransaccionalesBundle\Entity;

/**
 * Requerimiento
 */
class Requerimiento
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $idEntidad;

    /**
     * @var integer
     */
    private $idProducto;

    /**
     * @var integer
     */
    private $idProductor;

    /**
     * @var string
     */
    private $porcHss;
    
    /**
     * @var string
     */
    private $consumoDiario;

    /**
     * @var integer
     */
    private $gestion;

    /**
     * @var integer
     */
    private $periodo;

    /**
     * @var boolean
     */
    private $vigente = true;

    /**
     * @var boolean
     */
    private $eliminado = false;

    /**
     * @var \DateTime
     */
    private $fechaCreacion = 'now()';

    /**
     * @var \DateTime
     */
    private $fechaUltimaModificacion = 'now()';


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idEntidad
     *
     * @param integer $idEntidad
     *
     * @return Requerimiento
     */
    public function setIdEntidad($idEntidad)
    {
        $this->idEntidad = $idEntidad;

        return $this;
    }

    /**
     * Get idEntidad
     *
     * @return integer
     */
    public function getIdEntidad()
    {
        return $this->idEntidad;
    }

    /**
     * Set idProducto
     *
     * @param integer $idProducto
     *
     * @return Requerimiento
     */
    public function setIdProducto($idProducto)
    {
        $this->idProducto = $idProducto;

        return $this;
    }

    /**
     * Get idProducto
     *
     * @return integer
     */
    public function getIdProducto()
    {
        return $this->idProducto;
    }

    /**
     * Set idProductor
     *
     * @param integer $idProductor
     *
     * @return Requerimiento
     */
    public function setIdProductor($idProductor)
    {
        $this->idProductor = $idProductor;

        return $this;
    }

    /**
     * Get idProductor
     *
     * @return integer
     */
    public function getIdProductor()
    {
        return $this->idProductor;
    }

    /**
     * Set porcHss
     *
     * @param string $porcHss
     *
     * @return Requerimiento
     */
    public function setPorcHss($porcHss)
    {
        $this->porcHss = $porcHss;

        return $this;
    }

    /**
     * Get consumoDiario
     *
     * @return string
     */
    public function getConsumoDiario()
    {
        return $this->porcHss;
    }
    
     /**
     * Set consumoDiario
     *
     * @param string $consumoDiario
     *
     * @return Requerimiento
     */
    public function setConsumoDiario($consumoDiario)
    {
        $this->consumoDiario = $consumoDiario;

        return $this;
    }

    /**
     * Get porcHss
     *
     * @return string
     */
    public function getPorcHss()
    {
        return $this->porcHss;
    }

    /**
     * Set gestion
     *
     * @param integer $gestion
     *
     * @return Requerimiento
     */
    public function setGestion($gestion)
    {
        $this->gestion = $gestion;

        return $this;
    }

    /**
     * Get gestion
     *
     * @return integer
     */
    public function getGestion()
    {
        return $this->gestion;
    }

    /**
     * Set periodo
     *
     * @param integer $periodo
     *
     * @return Requerimiento
     */
    public function setPeriodo($periodo)
    {
        $this->periodo = $periodo;

        return $this;
    }

    /**
     * Get periodo
     *
     * @return integer
     */
    public function getPeriodo()
    {
        return $this->periodo;
    }

    /**
     * Set vigente
     *
     * @param boolean $vigente
     *
     * @return Requerimiento
     */
    public function setVigente($vigente)
    {
        $this->vigente = $vigente;

        return $this;
    }

    /**
     * Get vigente
     *
     * @return boolean
     */
    public function getVigente()
    {
        return $this->vigente;
    }

    /**
     * Set eliminado
     *
     * @param boolean $eliminado
     *
     * @return Requerimiento
     */
    public function setEliminado($eliminado)
    {
        $this->eliminado = $eliminado;

        return $this;
    }

    /**
     * Get eliminado
     *
     * @return boolean
     */
    public function getEliminado()
    {
        return $this->eliminado;
    }

    /**
     * Set fechaCreacion
     *
     * @param \DateTime $fechaCreacion
     *
     * @return Requerimiento
     */
    public function setFechaCreacion($fechaCreacion)
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    /**
     * Get fechaCreacion
     *
     * @return \DateTime
     */
    public function getFechaCreacion()
    {
        return $this->fechaCreacion;
    }

    /**
     * Set fechaUltimaModificacion
     *
     * @param \DateTime $fechaUltimaModificacion
     *
     * @return Requerimiento
     */
    public function setFechaUltimaModificacion($fechaUltimaModificacion)
    {
        $this->fechaUltimaModificacion = $fechaUltimaModificacion;

        return $this;
    }

    /**
     * Get fechaUltimaModificacion
     *
     * @return \DateTime
     */
    public function getFechaUltimaModificacion()
    {
        return $this->fechaUltimaModificacion;
    }
}

