<?php

namespace ProcesosTransaccionalesBundle\Entity;

/**
 * RequerimientoDetalle
 */
class RequerimientoDetalle
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $idEntidadProductor;

    /**
     * @var integer
     */
    private $idEntidadGranja;

    /**
     * @var string
     */
    private $datoProd1 = '0';

    /**
     * @var string
     */
    private $datoProd2 = '0';

    /**
     * @var string
     */
    private $datoProd3 = '0';

    /**
     * @var string
     */
    private $datoProd4 = '0';

    /**
     * @var string
     */
    private $mensual = '0';

    /**
     * @var string
     */
    private $semestral = '0';

    /**
     * @var string
     */
    private $datoCalculado1 = '0';

    /**
     * @var string
     */
    private $datoCalculado2 = '0';

    /**
     * @var string
     */
    private $datoCalculado3 = '0';

    /**
     * @var string
     */
    private $datoCalculado4 = '0';

    /**
     * @var string
     */
    private $datoCalculadoAsignado = '0';

    /**
     * @var boolean
     */
    private $vigente;

    /**
     * @var boolean
     */
    private $eliminado;

    /**
     * @var \DateTime
     */
    private $fechaCreacion = 'now()';

    /**
     * @var \DateTime
     */
    private $fechaUltimaModificacion = 'now()';

    /**
     * @var \ProcesosTransaccionalesBundle\Entity\Requerimiento
     */
    private $idReq;

    /**
     * @var \ProcesosTransaccionalesBundle\Entity\TipoPecuarioVocacion
     */
    private $idTipoVocacion;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idEntidadProductor
     *
     * @param integer $idEntidadProductor
     *
     * @return RequerimientoDetalle
     */
    public function setIdEntidadProductor($idEntidadProductor)
    {
        $this->idEntidadProductor = $idEntidadProductor;

        return $this;
    }

    /**
     * Get idEntidadProductor
     *
     * @return integer
     */
    public function getIdEntidadProductor()
    {
        return $this->idEntidadProductor;
    }

    /**
     * Set idEntidadGranja
     *
     * @param integer $idEntidadGranja
     *
     * @return RequerimientoDetalle
     */
    public function setIdEntidadGranja($idEntidadGranja)
    {
        $this->idEntidadGranja = $idEntidadGranja;

        return $this;
    }

    /**
     * Get idEntidadGranja
     *
     * @return integer
     */
    public function getIdEntidadGranja()
    {
        return $this->idEntidadGranja;
    }

    /**
     * Set datoProd1
     *
     * @param string $datoProd1
     *
     * @return RequerimientoDetalle
     */
    public function setDatoProd1($datoProd1)
    {
        $this->datoProd1 = $datoProd1;

        return $this;
    }

    /**
     * Get datoProd1
     *
     * @return string
     */
    public function getDatoProd1()
    {
        return $this->datoProd1;
    }

    /**
     * Set datoProd2
     *
     * @param string $datoProd2
     *
     * @return RequerimientoDetalle
     */
    public function setDatoProd2($datoProd2)
    {
        $this->datoProd2 = $datoProd2;

        return $this;
    }

    /**
     * Get datoProd2
     *
     * @return string
     */
    public function getDatoProd2()
    {
        return $this->datoProd2;
    }

    /**
     * Set datoProd3
     *
     * @param string $datoProd3
     *
     * @return RequerimientoDetalle
     */
    public function setDatoProd3($datoProd3)
    {
        $this->datoProd3 = $datoProd3;

        return $this;
    }

    /**
     * Get datoProd3
     *
     * @return string
     */
    public function getDatoProd3()
    {
        return $this->datoProd3;
    }

    /**
     * Set datoProd4
     *
     * @param string $datoProd4
     *
     * @return RequerimientoDetalle
     */
    public function setDatoProd4($datoProd4)
    {
        $this->datoProd4 = $datoProd4;

        return $this;
    }

    /**
     * Get datoProd4
     *
     * @return string
     */
    public function getDatoProd4()
    {
        return $this->datoProd4;
    }

    /**
     * Set mensual
     *
     * @param string $mensual
     *
     * @return RequerimientoDetalle
     */
    public function setMensual($mensual)
    {
        $this->mensual = $mensual;

        return $this;
    }

    /**
     * Get mensual
     *
     * @return string
     */
    public function getMensual()
    {
        return $this->mensual;
    }

    /**
     * Set semestral
     *
     * @param string $semestral
     *
     * @return RequerimientoDetalle
     */
    public function setSemestral($semestral)
    {
        $this->semestral = $semestral;

        return $this;
    }

    /**
     * Get semestral
     *
     * @return string
     */
    public function getSemestral()
    {
        return $this->semestral;
    }

    /**
     * Set datoCalculado1
     *
     * @param string $datoCalculado1
     *
     * @return RequerimientoDetalle
     */
    public function setDatoCalculado1($datoCalculado1)
    {
        $this->datoCalculado1 = $datoCalculado1;

        return $this;
    }

    /**
     * Get datoCalculado1
     *
     * @return string
     */
    public function getDatoCalculado1()
    {
        return $this->datoCalculado1;
    }

    /**
     * Set datoCalculado2
     *
     * @param string $datoCalculado2
     *
     * @return RequerimientoDetalle
     */
    public function setDatoCalculado2($datoCalculado2)
    {
        $this->datoCalculado2 = $datoCalculado2;

        return $this;
    }

    /**
     * Get datoCalculado2
     *
     * @return string
     */
    public function getDatoCalculado2()
    {
        return $this->datoCalculado2;
    }

    /**
     * Set datoCalculado3
     *
     * @param string $datoCalculado3
     *
     * @return RequerimientoDetalle
     */
    public function setDatoCalculado3($datoCalculado3)
    {
        $this->datoCalculado3 = $datoCalculado3;

        return $this;
    }

    /**
     * Get datoCalculado3
     *
     * @return string
     */
    public function getDatoCalculado3()
    {
        return $this->datoCalculado3;
    }

    /**
     * Set datoCalculado4
     *
     * @param string $datoCalculado4
     *
     * @return RequerimientoDetalle
     */
    public function setDatoCalculado4($datoCalculado4)
    {
        $this->datoCalculado4 = $datoCalculado4;

        return $this;
    }

    /**
     * Get datoCalculado4
     *
     * @return string
     */
    public function getDatoCalculado4()
    {
        return $this->datoCalculado4;
    }

    /**
     * Set datoCalculadoAsignado
     *
     * @param string $datoCalculadoAsignado
     *
     * @return RequerimientoDetalle
     */
    public function setDatoCalculadoAsignado($datoCalculadoAsignado)
    {
        $this->datoCalculadoAsignado = $datoCalculadoAsignado;

        return $this;
    }

    /**
     * Get datoCalculadoAsignado
     *
     * @return string
     */
    public function getDatoCalculadoAsignado()
    {
        return $this->datoCalculadoAsignado;
    }

    /**
     * Set vigente
     *
     * @param boolean $vigente
     *
     * @return RequerimientoDetalle
     */
    public function setVigente($vigente)
    {
        $this->vigente = $vigente;

        return $this;
    }

    /**
     * Get vigente
     *
     * @return boolean
     */
    public function getVigente()
    {
        return $this->vigente;
    }

    /**
     * Set eliminado
     *
     * @param boolean $eliminado
     *
     * @return RequerimientoDetalle
     */
    public function setEliminado($eliminado)
    {
        $this->eliminado = $eliminado;

        return $this;
    }

    /**
     * Get eliminado
     *
     * @return boolean
     */
    public function getEliminado()
    {
        return $this->eliminado;
    }

    /**
     * Set fechaCreacion
     *
     * @param \DateTime $fechaCreacion
     *
     * @return RequerimientoDetalle
     */
    public function setFechaCreacion($fechaCreacion)
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    /**
     * Get fechaCreacion
     *
     * @return \DateTime
     */
    public function getFechaCreacion()
    {
        return $this->fechaCreacion;
    }

    /**
     * Set fechaUltimaModificacion
     *
     * @param \DateTime $fechaUltimaModificacion
     *
     * @return RequerimientoDetalle
     */
    public function setFechaUltimaModificacion($fechaUltimaModificacion)
    {
        $this->fechaUltimaModificacion = $fechaUltimaModificacion;

        return $this;
    }

    /**
     * Get fechaUltimaModificacion
     *
     * @return \DateTime
     */
    public function getFechaUltimaModificacion()
    {
        return $this->fechaUltimaModificacion;
    }

    /**
     * Set idReq
     *
     * @param \ProcesosTransaccionalesBundle\Entity\Requerimiento $idReq
     *
     * @return RequerimientoDetalle
     */
    public function setIdReq(\ProcesosTransaccionalesBundle\Entity\Requerimiento $idReq = null)
    {
        $this->idReq = $idReq;

        return $this;
    }

    /**
     * Get idReq
     *
     * @return \ProcesosTransaccionalesBundle\Entity\Requerimiento
     */
    public function getIdReq()
    {
        return $this->idReq;
    }

    /**
     * Set idTipoVocacion
     *
     * @param \ProcesosTransaccionalesBundle\Entity\TipoPecuarioVocacion $idTipoVocacion
     *
     * @return RequerimientoDetalle
     */
    public function setIdTipoVocacion(\ProcesosTransaccionalesBundle\Entity\TipoPecuarioVocacion $idTipoVocacion = null)
    {
        $this->idTipoVocacion = $idTipoVocacion;

        return $this;
    }

    /**
     * Get idTipoVocacion
     *
     * @return \ProcesosTransaccionalesBundle\Entity\TipoPecuarioVocacion
     */
    public function getIdTipoVocacion()
    {
        return $this->idTipoVocacion;
    }
}

