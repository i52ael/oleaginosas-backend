<?php

namespace ProcesosTransaccionalesBundle\Entity;

/**
 * CompraDetalle
 */
class CompraDetalle
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $idEntidadProveedor;

    /**
     * @var string
     */
    private $cantidad;

    /**
     * @var int
     */
    private $idProducto;

    /**
     * @var string
     */
    private $precioUnitario;

    /**
     * @var string
     */
    private $precioTotal;

    /**
     * @var string
     */
    private $nroFactura;

    /**
     * @var \DateTime
     */
    private $fechaCompra;

    /**
     * @var int
     */
    private $idTransaccion;

    /**
     * @var int
     */
    private $idEntidadSocio;
    /**
     * @var int
     */
    private $idRegimen;
    /**
     * @var bool
     */
    private $beneficiario;

    /**
     * @var \DateTime
     */
    private $mesAcopio;
    /**
     * @var int
     */
    private $idMoneda;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idEntidadProveedor
     *
     * @param integer $idEntidadProveedor
     *
     * @return CompraDetalle
     */
    public function setIdEntidadProveedor($idEntidadProveedor)
    {
        $this->idEntidadProveedor = $idEntidadProveedor;

        return $this;
    }

    /**
     * Get idEntidadProveedor
     *
     * @return int
     */
    public function getIdEntidadProveedor()
    {
        return $this->idEntidadProveedor;
    }

    /**
     * Set cantidad
     *
     * @param string $cantidad
     *
     * @return CompraDetalle
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    /**
     * Get cantidad
     *
     * @return string
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * Set idProducto
     *
     * @param integer $idProducto
     *
     * @return CompraDetalle
     */
    public function setIdProducto($idProducto)
    {
        $this->idProducto = $idProducto;

        return $this;
    }

    /**
     * Get idProducto
     *
     * @return int
     */
    public function getIdProducto()
    {
        return $this->idProducto;
    }

    /**
     * Set precioUnitario
     *
     * @param string $precioUnitario
     *
     * @return CompraDetalle
     */
    public function setPrecioUnitario($precioUnitario)
    {
        $this->precioUnitario = $precioUnitario;

        return $this;
    }

    /**
     * Get precioUnitario
     *
     * @return string
     */
    public function getPrecioUnitario()
    {
        return $this->precioUnitario;
    }

    /**
     * Set precioTotal
     *
     * @param string $precioTotal
     *
     * @return CompraDetalle
     */
    public function setPrecioTotal($precioTotal)
    {
        $this->precioTotal = $precioTotal;

        return $this;
    }

    /**
     * Get precioTotal
     *
     * @return string
     */
    public function getPrecioTotal()
    {
        return $this->precioTotal;
    }

    /**
     * Set nroFactura
     *
     * @param string $nroFactura
     *
     * @return CompraDetalle
     */
    public function setNroFactura($nroFactura)
    {
        $this->nroFactura = $nroFactura;

        return $this;
    }

    /**
     * Get nroFactura
     *
     * @return string
     */
    public function getNroFactura()
    {
        return $this->nroFactura;
    }

    /**
     * Set fechaCompra
     *
     * @param \DateTime $fechaCompra
     *
     * @return CompraDetalle
     */
    public function setFechaCompra($fechaCompra)
    {
        $this->fechaCompra = $fechaCompra;

        return $this;
    }

    /**
     * Get fechaCompra
     *
     * @return \DateTime
     */
    public function getFechaCompra()
    {
        return $this->fechaCompra;
    }

    /**
     * Set idTransaccion
     *
     * @param integer $idTransaccion
     *
     * @return CompraDetalle
     */
    public function setIdTransaccion($idTransaccion)
    {
        $this->idTransaccion = $idTransaccion;

        return $this;
    }

    /**
     * Get idTransaccion
     *
     * @return int
     */
    public function getIdTransaccion()
    {
        return $this->idTransaccion;
    }

    /**
     * Set idEntidadSocio
     *
     * @param integer $idEntidadSocio
     *
     * @return CompraDetalle
     */
    public function setIdEntidadSocio($idEntidadSocio)
    {
        $this->idEntidadSocio = $idEntidadSocio;

        return $this;
    }

    /**
     * Get idEntidadSocio
     *
     * @return int
     */
    public function getIdEntidadSocio()
    {
        return $this->idEntidadSocio;
    }
    /**
     * Set idRegimen
     *
     * @param integer $idRegimen
     *
     * @return CompraDetalle
     */
    public function setIdRegimen($idRegimen)
    {
        $this->idRegimen = $idRegimen;

        return $this;
    }

    /**
     * Get idRegimen
     *
     * @return int
     */
    public function getIdRegimen()
    {
        return $this->idRegimen;
    }

    /**
     * Set Beneficiario
     *
     * @param boolean $idRegimen
     *
     * @return CompraDetalle
     */
    public function setBeneficiario($beneficiario)
    {
        $this->beneficiario = $beneficiario;

        return $this;
    }

    /**
     * Get Beneficiario
     *
     * @return boolean
     */
    public function getBeneficiario()
    {
        return $this->beneficiario;
    }

    /**
     * Set mesAcopio
     *
     * @param \DateTime $mesAcopio
     *
     * @return CompraDetalle
     */
    public function setMesAcopio($mesAcopio)
    {
        $this->mesAcopio = $mesAcopio;

        return $this;
    }

    /**
     * Get mesAcopio
     *
     * @return \DateTime
     */
    public function getMesAcopio()
    {
        return $this->mesAcopio;
    }

    /**
     * Set idMoneda
     *
     * @param integer $idMoneda
     *
     * @return CompraDetalle
     */
    public function setIdMoneda($idMoneda)
    {
        $this->idMoneda = $idMoneda;

        return $this;
    }

    /**
     * Get idMoneda
     *
     * @return int
     */
    public function getIdMoneda()
    {
        return $this->idMoneda;
    }
}

