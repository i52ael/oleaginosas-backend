<?php

namespace ProcesosTransaccionalesBundle\Entity;

/**
 * RipCentroAlmacenamiento
 */
class RipCentroAlmacenamiento
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $nombre;

    /**
     * @var string
     */
    private $direccion;

    /**
     * @var string
     */
    private $ubicacion;

    /**
     * @var float
     */
    private $latitud;

    /**
     * @var float
     */
    private $longitud;

    /**
     * @var float
     */
    private $grados;

    /**
     * @var float
     */
    private $minutos;

    /**
     * @var float
     */
    private $segundos;

    /**
     * @var int
     */
    private $idDepartamento;

    /**
     * @var int
     */
    private $idProvincia;

    /**
     * @var bool
     */
    private $eliminado;

    /**
     * @var int
     */
    private $idEntidad;

    /**
     * @var int
     */
    private $idMunicipio;

    /**
     * @var int
     */
    private $idRipTipo;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Entidad
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set direccion
     *
     * @param string $direccion
     *
     * @return Entidad
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;

        return $this;
    }

    /**
     * Get direccion
     *
     * @return string
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * Set ubicacion
     *
     * @param string $ubicacion
     *
     * @return Entidad
     */
    public function setUbicacion($ubicacion)
    {
        $this->ubicacion = $ubicacion;

        return $this;
    }

    /**
     * Get ubicacion
     *
     * @return string
     */
    public function getUbicacion()
    {
        return $this->ubicacion;
    }

    /**
     * Set latitud
     *
     * @param float $latitud
     *
     * @return Entidad
     */
    public function setLatitud($latitud)
    {
        $this->latitud = $latitud;

        return $this;
    }

    /**
     * Get latitud
     *
     * @return float
     */
    public function getLatitud()
    {
        return $this->latitud;
    }

    /**
     * Set longitud
     *
     * @param float $longitud
     *
     * @return Entidad
     */
    public function setLongitud($longitud)
    {
        $this->longitud = $longitud;

        return $this;
    }

    /**
     * Get longitud
     *
     * @return float
     */
    public function getLongitud()
    {
        return $this->longitud;
    }

    /**
     * Set grados
     *
     * @param float $grados
     *
     * @return Entidad
     */
    public function setGrados($grados)
    {
        $this->grados = $grados;

        return $this;
    }

    /**
     * Get grados
     *
     * @return float
     */
    public function getGrados()
    {
        return $this->grados;
    }

    /**
     * Set minutos
     *
     * @param float $minutos
     *
     * @return Entidad
     */
    public function setMinutos($minutos)
    {
        $this->minutos = $minutos;

        return $this;
    }

    /**
     * Get minutos
     *
     * @return float
     */
    public function getMinutos()
    {
        return $this->minutos;
    }

    /**
     * Set segundos
     *
     * @param float $segundos
     *
     * @return Entidad
     */
    public function setSegundos($segundos)
    {
        $this->segundos = $segundos;

        return $this;
    }

    /**
     * Get segundos
     *
     * @return float
     */
    public function getSegundos()
    {
        return $this->segundos;
    }

    /**
     * Set idDepartamento
     *
     * @param integer $idDepartamento
     *
     * @return Entidad
     */
    public function setIdDepartamento($idDepartamento)
    {
        $this->idDepartamento = $idDepartamento;

        return $this;
    }

    /**
     * Get idDepartamento
     *
     * @return int
     */
    public function getIdDepartamento()
    {
        return $this->idDepartamento;
    }

    /**
     * Set idProvincia
     *
     * @param integer $idProvincia
     *
     * @return Entidad
     */
    public function setIdProvincia($idProvincia)
    {
        $this->idProvincia = $idProvincia;

        return $this;
    }

    /**
     * Get idProvincia
     *
     * @return int
     */
    public function getIdProvincia()
    {
        return $this->idProvincia;
    }

    /**
     * Set eliminado
     *
     * @param boolean $eliminado
     *
     * @return Entidad
     */
    public function setEliminado($eliminado)
    {
        $this->eliminado = $eliminado;

        return $this;
    }

    /**
     * Get eliminado
     *
     * @return bool
     */
    public function getEliminado()
    {
        return $this->eliminado;
    }

    /**
     * Set idEntidad
     *
     * @param integer $idEntidad
     *
     * @return Entidad
     */
    public function setIdEntidad($idEntidad)
    {
        $this->idEntidad = $idEntidad;

        return $this;
    }

    /**
     * Get idEntidad
     *
     * @return int
     */
    public function getIdEntidad()
    {
        return $this->idEntidad;
    }

    /**
     * Set idMunicipio
     *
     * @param integer $idMunicipio
     *
     * @return Entidad
     */
    public function setIdMunicipio($idMunicipio)
    {
        $this->idMunicipio = $idMunicipio;

        return $this;
    }

    /**
     * Get idMunicipio
     *
     * @return int
     */
    public function getIdMunicipio()
    {
        return $this->idMunicipio;
    }

    /**
     * Set idRipTipo
     *
     * @param integer $idRipTipo
     *
     * @return Entidad
     */
    public function setIdRipTipo($idRipTipo)
    {
        $this->idRipTipo = $idRipTipo;

        return $this;
    }

    /**
     * Get idRipTipo
     *
     * @return int
     */
    public function getIdRipTipo()
    {
        return $this->idRipTipo;
    }


}
