<?php
namespace ProcesosTransaccionalesBundle\Manager;

use Doctrine\ORM\EntityManager;
use ProcesosTransaccionalesBundle\Entity\RipInfoProductiva;
use ProcesosTransaccionalesBundle\Entity\RipCentroAlmacenamiento;
use ProcesosTransaccionalesBundle\Entity\RipLineaProduccion;
use ProcesosTransaccionalesBundle\Entity\RipAlmacenamiento;
use ProcesosTransaccionalesBundle\Entity\RipTipo;
use ProcesosTransaccionalesBundle\Entity\RipCapacidad;
use ProcesosTransaccionalesBundle\Entity\RipMantenimiento;
use ProcesosTransaccionalesBundle\Entity\RipFactoresTransformacion;
use ProcesosTransaccionalesBundle\Entity\Entidad;
// use Doctrine\ORM\EntityNotFoundException;
class RipManager
{
    /**
     * @var EntityManager
     */
    protected $em;
    protected $repo;

    public function __construct(EntityManager $entityManager) {
	    $this->em = $entityManager;
        $this->setRepository();
    }
    protected function setRepository()
    {
	    $this->repo = $this->em->getRepository('ProcesosTransaccionalesBundle:RipInfoProductiva');
        // $this->repo=$this->em->getRepository(RipCentroAlmacenamiento::class);
    }

    public function listaInformacionProductiva($obj_datos){
        $str_where = "";
        if($obj_datos->id_entidad > 0){
            $str_where = "WHERE ip.id_entidad = '$obj_datos->id_entidad'";
        }
        $limit = "";
        if ($obj_datos->fromRow != -1 && $obj_datos->pageSize != -1){
            $limit = "LIMIT $obj_datos->pageSize OFFSET " . (intval($obj_datos->fromRow) - 1);
        }
        $query = "SELECT COUNT(*) OVER() AS total, ip.id, ip.observaciones, ip.fecha_registro, e.id as id_entidad, e.razon_social, ip.eliminado
        FROM rip_info_productiva ip INNER JOIN entidad e ON ip.id_entidad = e.id $str_where $limit";
        $statement = $this->em->getConnection()->prepare($query);
        $statement->execute();
        if ($statement->rowCount() == 0){
            return array('error'=>'No se encontraron formularios de informacion productiva.');
        } else{
            $arr = $statement->fetchAll();
            return array('total'=>$arr[0]['total'], 'arr'=>$arr);
        }
    }

    public function cbxTipo($tipo)
    {
        $statement = $this->em->getConnection()->prepare("SELECT
        rip_tipo.id as data,
        rip_tipo.descripcion as label
        FROM
        rip_tipo
        WHERE
        lower(rip_tipo.tipo) = lower('$tipo')
        ");

        $statement->execute();
        if ($statement->rowCount() == 0)
            return array();
        else
            return $statement->fetchAll();
    }

    public function listaCentroAlmacenamiento($obj_datos){
        $str_where = "";
        if($obj_datos->id_entidad > 0){
            $str_where = "WHERE ca.id_entidad = '$obj_datos->id_entidad'";
        }
        $limit = "";
        if ($obj_datos->fromRow != -1 && $obj_datos->pageSize != -1){
            $limit = "LIMIT $obj_datos->pageSize OFFSET " . (intval($obj_datos->fromRow) - 1);
        }
        $query = "SELECT count(*) OVER() total, ca.*
            , m.nombre_municipio || ' - ' || p.nombre_provincia AS descrip_municipio
            , d.nombre_departamento, t.descripcion
            FROM public.rip_centro_almacenamiento ca
            INNER JOIN geo.municipio m on m.id = ca.id_municipio
            INNER JOIN geo.provincia p on p.id = m.id_provincia
            INNER JOIN geo.departamento d on d.id = p.id_departamento
            INNER JOIN rip_tipo t on t.id = ca.id_rip_tipo
            $str_where ORDER BY ca.nombre $limit";
        $statement = $this->em->getConnection()->prepare($query);
        $statement->execute();
        if ($statement->rowCount() == 0){
            return array('error'=>'No se encontraron Centros de Almacenamiento para esta entidad.');
        } else{
            $arr = $statement->fetchAll();
            return array("total"=>$arr[0]['total'],"arr"=>$arr);
        }
    }

    public function guardarCentroAlmacenamiento($obj_datos){
        $statement = $this->em->getConnection()->prepare("select
            geo.municipio.id_provincia
            from
            geo.municipio
            where
            geo.municipio.id = $obj_datos->id_municipio
        ");

        $statement->execute();
        if ($statement->rowCount() == 0)
            $obj_datos->id_provincia = 0;
        else
        {
            $row = $statement->fetch();
            $obj_datos->id_provincia = $row['id_provincia'];
        }
        if($obj_datos->id > 0){
            $this->repo=$this->em->getRepository(RipCentroAlmacenamiento::class);
            $ripCentroAlmacenamiento=$this->repo->find($obj_datos->id);
        } else{
            $ripCentroAlmacenamiento = new RipCentroAlmacenamiento();
        }
        $ripCentroAlmacenamiento->setNombre($obj_datos->nombre);
        $ripCentroAlmacenamiento->setDireccion($obj_datos->direccion);
        $ripCentroAlmacenamiento->setUbicacion($obj_datos->ubicacion);
        $ripCentroAlmacenamiento->setLatitud($obj_datos->latitud);
        $ripCentroAlmacenamiento->setLongitud($obj_datos->longitud);
        $ripCentroAlmacenamiento->setGrados($obj_datos->grados);
        $ripCentroAlmacenamiento->setMinutos($obj_datos->minutos);
        $ripCentroAlmacenamiento->setSegundos($obj_datos->segundos);
        $ripCentroAlmacenamiento->setIdDepartamento($obj_datos->id_departamento);
        $ripCentroAlmacenamiento->setIdProvincia($obj_datos->id_provincia);
        $ripCentroAlmacenamiento->setEliminado($obj_datos->eliminado);
        $ripCentroAlmacenamiento->setIdEntidad($this->em->getReference('ProcesosTransaccionalesBundle:Entidad',$obj_datos->id_entidad));
        // $ripCentroAlmacenamiento->setIdMunicipio($this->em->getReference('ProcesosTransaccionalesBundle:Municipio',$obj_datos->id_municipio));
        $ripCentroAlmacenamiento->setIdMunicipio($obj_datos->id_municipio);// temporalmente
        $ripCentroAlmacenamiento->setIdRipTipo($this->em->getReference('ProcesosTransaccionalesBundle:RipTipo',$obj_datos->id_rip_tipo));

        $this->em->persist($ripCentroAlmacenamiento);
        $this->em->flush();
        return array('id'=>$ripCentroAlmacenamiento->getId());
    }

    public function eliminarCentroAlmacenamiento($obj_datos){
        $this->repo=$this->em->getRepository(RipCentroAlmacenamiento::class);
        $ripCentroAlmacenamiento=$this->repo->find($obj_datos->id);
        if($ripCentroAlmacenamiento){
            $ripCentroAlmacenamiento->setEliminado(!$ripCentroAlmacenamiento->getEliminado());
            $this->em->persist($ripCentroAlmacenamiento);
            $this->em->flush();
            return array('id'=>$ripCentroAlmacenamiento->getId(),"eliminado"=>$ripCentroAlmacenamiento->getEliminado());
        } else {
            return array('error'=>'Centro no encontrado');
        }
    }

    public function guardarLineaProduccion($obj_datos){
        if($obj_datos->id > 0){
            $this->repo=$this->em->getRepository(RipLineaProduccion::class);
            $ripLineaProduccion = $this->repo->find($obj_datos->id);
        } else{
            $ripLineaProduccion = new RipLineaProduccion();
        }
        $ripLineaProduccion->setDescripcion($obj_datos->descripcion);
        $ripLineaProduccion->setEliminado($obj_datos->eliminado);
        $ripLineaProduccion->setDescripcion($obj_datos->descripcion);
        $ripLineaProduccion->setIdRipCentroAlmacenamiento($this->em->getReference('ProcesosTransaccionalesBundle:RipCentroAlmacenamiento',$obj_datos->id_rip_centro_almacenamiento));
        $this->em->persist($ripLineaProduccion);
        $this->em->flush();
        return array('id'=>$ripLineaProduccion->getId());
    }

    public function eliminarLineaProduccion($obj_datos){
        $this->repo=$this->em->getRepository(RipLineaProduccion::class);
        $ripLineaProduccion=$this->repo->find($obj_datos->id);
        if($ripLineaProduccion){
            $ripLineaProduccion->setEliminado(!$ripLineaProduccion->getEliminado());
            $this->em->persist($ripLineaProduccion);
            $this->em->flush();
            return array('id'=>$ripLineaProduccion->getId(),"eliminado"=>$ripLineaProduccion->getEliminado());
        } else {
            return array('error'=>'Centro no encontrado');
        }
    }

    public function listarLineaProduccion($obj_datos){
        $query = "SELECT *
            FROM rip_linea_produccion lp
            WHERE lp.id_rip_centro_almacenamiento = '$obj_datos->id_rip_centro_almacenamiento'";
        $statement = $this->em->getConnection()->prepare($query);
        $statement->execute();
        if ($statement->rowCount() == 0){
            return array('error'=>'No se encontraron Lineas de Produccion para el Centro de Almacenamiento.');
        } else{
            $arr = $statement->fetchAll();
            return $arr;
        }
    }

    public function guardarAlmacenamiento($obj_datos,$user){

        $id_info_prod = $this->guardarInfoProductiva($obj_datos,$user);
        foreach ($obj_datos->almacenamiento as $alm) {
            // $rrr .= $v->capacidad.', ';
            if ($alm->id > 0) {
                $this->repo = $this->em->getRepository(RipAlmacenamiento::class);
                $ripAlmacenamiento = $this->repo->find($alm->id);
            } else {
                $ripAlmacenamiento = new RipAlmacenamiento();
            }
            $ripAlmacenamiento->setCapacidad($alm->capacidad);
            $ripAlmacenamiento->setIdRipInfoProductiva($this->em->getReference('ProcesosTransaccionalesBundle:RipInfoProductiva', $id_info_prod));
            $ripAlmacenamiento->setIdRipCentroAlmacenamiento($this->em->getReference('ProcesosTransaccionalesBundle:RipCentroAlmacenamiento', $alm->id_rip_centro_almacenamiento));
            $ripAlmacenamiento->setIdRipTipo($this->em->getReference('ProcesosTransaccionalesBundle:RipTipo', $alm->id_rip_tipo));

            $this->em->persist($ripAlmacenamiento);
            $this->em->flush();
            $alm->id = $ripAlmacenamiento->getId();
            $alm->id_rip_info_productiva = $id_info_prod;
        }

        return array("id"=>$id_info_prod);
    }

    public function listarAlmacenamiento($obj_datos){
        
        $query = "SELECT 
            ip.id as id_rip_info_productiva
            , alm.id, alm.capacidad
            , alm.id_rip_centro_almacenamiento
            , alm.id_rip_tipo
            , ca.nombre, ca.direccion
            , dep.nombre_departamento, pro.nombre_provincia, mun.nombre_municipio
            , ca.nombre || ' - ' || ca.direccion || ' - ' || dep.nombre_departamento || ' - ' || pro.nombre_provincia || ' - ' || mun.nombre_municipio AS descrip
            FROM rip_info_productiva ip
            LEFT JOIN rip_almacenamiento alm ON ip.id = alm.id_rip_info_productiva
            LEFT JOIN rip_centro_almacenamiento ca ON alm.id_rip_centro_almacenamiento = ca.id
            INNER JOIN geo.departamento dep on ca.id_departamento = dep.id
            INNER JOIN geo.provincia pro ON ca.id_provincia = pro.id
            INNER JOIN geo.municipio mun ON ca.id_municipio = mun.id
            WHERE 
            ip.id = '$obj_datos->id'
            AND alm.id_rip_tipo = $obj_datos->id_rip_tipo
            ";
        
        $statement = $this->em->getConnection()->prepare($query);
        $statement->execute();
        if ($statement->rowCount() == 0){
            return array('error'=>'No se encontraron Lineas de Produccion para el Centro de Almacenamiento.');
        } else{
            $arr = $statement->fetchAll();
            return $arr;
        }
    }

    public function guardarCapacidad($obj_datos,$user){

        $id_info_prod = $this->guardarInfoProductiva($obj_datos,$user);
        $id_alm = 0;
        foreach ($obj_datos->capacidad as $cap) {
            if(isset($cap->id_rip_linea_produccion) && !empty($cap->id_rip_linea_produccion))
            {
                $statement = $this->em->getConnection()->prepare("SELECT
                rip_linea_produccion.id_rip_centro_almacenamiento
                FROM
                rip_linea_produccion
                WHERE
                rip_linea_produccion.id = $cap->id_rip_linea_produccion");
                $statement->execute();
                if ($statement->rowCount() == 0){
                    $cap->id_rip_centro_almacenamiento = 0;
                } else{
                    $row= $statement->fetch();
                    $cap->id_rip_centro_almacenamiento = $row['id_rip_centro_almacenamiento'];
                }
            }
            if($cap->id > 0){
                $this->repo=$this->em->getRepository(RipCapacidad::class);
                $ripCapacidad = $this->repo->find($cap->id);
            } else{
                $ripCapacidad = new RipCapacidad();
                $ripCapacidad->setIdRipInfoProductiva($this->em->getReference('ProcesosTransaccionalesBundle:RipInfoProductiva',$id_info_prod));
            }
            $ripCapacidad->setDia($cap->dia);
            $ripCapacidad->setMes($cap->mes);
            $ripCapacidad->setGestion($cap->gestion);
            $ripCapacidad->setAlmacenamiento($cap->almacenamiento);
            $ripCapacidad->setIdRipCentroAlmacenamiento($this->em->getReference('ProcesosTransaccionalesBundle:RipCentroAlmacenamiento',$cap->id_rip_centro_almacenamiento));
            $ripCapacidad->setIdRipTipo($this->em->getReference('ProcesosTransaccionalesBundle:RipTipo',$cap->id_rip_tipo));
            $ripCapacidad->setIdProducto($this->em->getReference('ProcesosTransaccionalesBundle:Producto',$cap->id_producto));
            $ripCapacidad->setIdRipLineaProduccion(isset($cap->id_rip_linea_produccion)?$cap->id_rip_linea_produccion:0);
            $this->em->persist($ripCapacidad);
            $this->em->flush();
            $cap->id = $ripCapacidad->getId();
        }

        return array("id"=>$id_info_prod);
    }

    public function eliminarCapacidad($obj_datos){
        $id = $obj_datos->id;
        try{
            $this->repo=$this->em->getRepository(RipCapacidad::class);
            $ripCapacidad = $this->repo->find($obj_datos->id);
            $this->em->remove($ripCapacidad);
            $this->em->flush();
        } catch (\Exception $e) {
            return array("error"=> 'Error al eliminar el registro.');
        }
        return array("id"=>$id);
    }

    public function listarCapacidad($obj_datos, $sw = false){
        if ($sw)
        {
            $campo = ", rip_linea_produccion.descripcion";
            $inner = "INNER JOIN rip_linea_produccion ON rip_linea_produccion.id = cap.id_rip_linea_produccion";
        }
        else
        {
            $campo = "";
            $inner = "";
        }
        $query = "SELECT
        	cap.*
        	, alm.nombre || ' - ' || dep.nombre_departamento || ' - ' || mun.nombre_municipio  descrip
        	, pro.nombre nombre_producto
        $campo	
        FROM rip_info_productiva ip        
        INNER JOIN rip_capacidad cap ON ip.id = cap.id_rip_info_productiva
        $inner
        INNER JOIN producto pro ON cap.id_producto = pro.id
        INNER JOIN rip_centro_almacenamiento alm ON alm.id = cap.id_rip_centro_almacenamiento
        INNER JOIN geo.departamento dep on alm.id_departamento = dep.id
        INNER JOIN geo.municipio mun ON alm.id_municipio = mun.id
        WHERE 
        ip.id = '$obj_datos->id'
        AND cap.id_rip_tipo = $obj_datos->id_rip_tipo";
        $statement = $this->em->getConnection()->prepare($query);
        $statement->execute();
        if ($statement->rowCount() == 0){
            return array('error'=>'No se encontraron Lineas de Produccion para el Centro de Almacenamiento.');
        } else{
            $arr = $statement->fetchAll();
            return $arr;
        }
    }

    public function guardarMantenimiento($obj_datos,$user){
        $id_info_prod = $this->guardarInfoProductiva($obj_datos,$user);
        // foreach ($obj_datos->capacidad as $cap) {
            if($obj_datos->mantenimiento->id > 0){
                $this->repo=$this->em->getRepository(RipMantenimiento::class);
                $ripMantenimiento = $this->repo->find($obj_datos->mantenimiento->id);
            } else{
                $ripMantenimiento = new RipMantenimiento();
                $ripMantenimiento->setIdRipInfoProductiva($this->em->getReference('ProcesosTransaccionalesBundle:RipInfoProductiva',$id_info_prod));
            }
            $ripMantenimiento->setMes($obj_datos->mantenimiento->mes);
            $ripMantenimiento->setDias($obj_datos->mantenimiento->dias);
            $ripMantenimiento->setIdRipCentroAlmacenamiento($this->em->getReference('ProcesosTransaccionalesBundle:RipCentroAlmacenamiento',$obj_datos->mantenimiento->id_rip_centro_almacenamiento));

            $this->em->persist($ripMantenimiento);
            $this->em->flush();
            $obj_datos->mantenimiento->id = $ripMantenimiento->getId();
        // }
        return array("id"=>$id_info_prod);
    }

    public function eliminarMantenimiento($obj_datos){
        $id = $obj_datos->id;
        try{
            $this->repo=$this->em->getRepository(RipMantenimiento::class);
            $ripMantenimiento = $this->repo->find($obj_datos->id);
            $this->em->remove($ripMantenimiento);
            $this->em->flush();
        } catch (\Exception $e) {
            return array("error"=> 'Error al eliminar el Mantenimiento.');
        }
        return array("id"=>$id);
    }

    public function listarMantenimiento($obj_datos){
        $query = "SELECT man.*, ca.nombre || ' - ' || dep.nombre_departamento || ' - ' || mun.nombre_municipio descrip
            FROM rip_mantenimiento man
            INNER JOIN rip_centro_almacenamiento ca ON ca.id = man.id_rip_centro_almacenamiento
            LEFT JOIN geo.departamento dep ON ca.id_departamento = dep.id
	        LEFT JOIN geo.municipio mun ON ca.id_municipio = mun.id
        	WHERE 
            /*ca.id_entidad = obj_datos->id_entidad*/
        	man.id_rip_info_productiva = '$obj_datos->id'
        	ORDER BY man.id";
        $statement = $this->em->getConnection()->prepare($query);
        $statement->execute();
        if ($statement->rowCount() == 0){
            return array('error'=>'No se encontraron Lineas de Produccion para el Centro de Almacenamiento.');
        } else{
            $arr = array();
            while ($row = $statement->fetch())
            {
                $row['descrip_mes'] = $this->getMes($row['mes'], true);
                $arr[] = $row;
            }
            return $arr;
        }
    }

    public function guardarFactoresTransformacion($obj_datos,$user){
        $id_info_prod = $this->guardarInfoProductiva($obj_datos,$user);
        // foreach ($obj_datos->capacidad as $cap) {
            if($obj_datos->factores->id > 0){
                $this->repo=$this->em->getRepository(RipFactoresTransformacion::class);
                $ripFactores = $this->repo->find($obj_datos->factores->id);
            } else{
                $ripFactores = new RipFactoresTransformacion();
                $ripFactores->setIdRipInfoProductiva($this->em->getReference('ProcesosTransaccionalesBundle:RipInfoProductiva',$id_info_prod));
            }
            $ripFactores->setFactor($obj_datos->factores->factor);
            $ripFactores->setIdProducto($this->em->getReference('ProcesosTransaccionalesBundle:Producto',$obj_datos->factores->id_producto));

            $this->em->persist($ripFactores);
            $this->em->flush();
            $obj_datos->factores->id = $ripFactores->getId();
        // }
        return array("id"=>$id_info_prod);
    }

    public function eliminarFactoresTransformacion($obj_datos){
        $id = $obj_datos->id;
        try{
            $this->repo=$this->em->getRepository(RipFactoresTransformacion::class);
            $ripFactoresTransformacion = $this->repo->find($obj_datos->id);
            $this->em->remove($ripFactoresTransformacion);
            $this->em->flush();
        } catch (\Exception $e) {
            return array("error"=> 'Error al eliminar el Factor de Transformación.');
        }
        return array("id"=>$id);
    }

    public function listarFactoresTransformacion($obj_datos){
        $query = "SELECT ft.*, pr.descripcion nombre_producto
            FROM rip_factores_transformacion ft
            INNER JOIN producto pr ON ft.id_producto = pr.id
            INNER JOIN rip_info_productiva ip ON ft.id_rip_info_productiva = ip.id
            WHERE ft.id_rip_info_productiva = '$obj_datos->id'";

        $statement = $this->em->getConnection()->prepare($query);
        $statement->execute();
        if ($statement->rowCount() == 0){
            return array('error'=>'No se encontraron Factores de Transformación.');
        } else{
            $arr = $statement->fetchAll();
            return $arr;
        }
    }

    public function guardarInfoProductiva($obj_datos,$user){
        if($obj_datos->id > 0){
            $this->repo=$this->em->getRepository(RipInfoProductiva::class);
            $ripInfoProd = $this->repo->find($obj_datos->id);
        } else{
            $ripInfoProd = new RipInfoProductiva();
            $ripInfoProd->setFechaRegistro(new \DateTime());
            $ripInfoProd->setIdEntidad($this->em->getReference('ProcesosTransaccionalesBundle:Entidad',$obj_datos->id_entidad));
            $ripInfoProd->setIdUsuarioCreacion($user->idUser);
            $ripInfoProd->setEstado(1);
        }
        $ripInfoProd->setEliminado($obj_datos->eliminado);
        $ripInfoProd->setObservaciones($obj_datos->observaciones);
        $ripInfoProd->setFechaUltimaModificacion(new \DateTime());
        $ripInfoProd->setIdUsuarioUltimaModificacion($user->idUser);

        $this->em->persist($ripInfoProd);
        $this->em->flush();
        return $ripInfoProd->getId();
    }

    public function cbxCentroAlmacenamiento($obj_datos, $sw = false)
    {
        if ($sw)
        {
            $campo = ", rip_linea_produccion.descripcion, rip_linea_produccion.id AS id_rip_linea_produccion";
            $inner = "INNER JOIN rip_linea_produccion on rip_linea_produccion.id_rip_centro_almacenamiento = ca.id";
        }
        else
        {
            $campo = "";
            $inner = "";
        }
        $statement = $this->em->getConnection()->prepare("SELECT ca.id_rip_tipo, ca.id, ca.nombre || ' - ' || ca.direccion || ' - ' || dep.nombre_departamento || ' - ' || pro.nombre_provincia || ' - ' || mun.nombre_municipio AS descrip
            $campo
            FROM rip_centro_almacenamiento ca
            $inner
            INNER JOIN geo.departamento dep on ca.id_departamento = dep.id
            INNER JOIN geo.provincia pro ON ca.id_provincia = pro.id
            INNER JOIN geo.municipio mun ON ca.id_municipio = mun.id
            WHERE ca.id_entidad = '$obj_datos->id_entidad'
            AND ca.id_rip_tipo = '$obj_datos->id_rip_tipo'");

        $statement->execute();
        if ($statement->rowCount() == 0)
            return array();
        else
            return $statement->fetchAll();
    }

    public function dgCentroAlmacenamiento($obj_datos)
    {
        $statement = $this->em->getConnection()->prepare("SELECT
        '0' AS id,
        '' AS capacidad,
        '0' AS id_rip_info_productiva,
        ca.id AS id_rip_centro_almacenamiento,
        ca.id_rip_tipo,
        ca.nombre || ' - ' || ca.direccion || ' - ' || dep.nombre_departamento || ' - ' || pro.nombre_provincia || ' - ' || mun.nombre_municipio AS descrip
        FROM rip_centro_almacenamiento ca
        INNER JOIN geo.departamento dep on ca.id_departamento = dep.id
        INNER JOIN geo.provincia pro ON ca.id_provincia = pro.id
        INNER JOIN geo.municipio mun ON ca.id_municipio = mun.id
        WHERE ca.id_entidad = '$obj_datos->id_entidad'
        AND ca.id_rip_tipo = '$obj_datos->id_rip_tipo'");

        $statement->execute();
        if ($statement->rowCount() == 0)
            return array();
        else
            return $statement->fetchAll();
    }

    public function dgAlmacenamiento($obj_datos)
    {
        $statement = $this->em->getConnection()->prepare("SELECT
        '0' AS id,
        '0' AS dia,
        '0' AS mes,
        '0' as gestion,
        '' as almacenamiento,
        '0' AS id_rip_info_productiva,
        rip_centro_almacenamiento.id AS id_rip_centro_almacenamiento,
        rip_centro_almacenamiento.id_rip_tipo,
        rip_centro_almacenamiento.nombre,
        rip_tipo.descripcion
        FROM
        rip_centro_almacenamiento
        inner join rip_tipo on rip_tipo.id = rip_centro_almacenamiento.id_rip_tipo
        WHERE rip_centro_almacenamiento.id_entidad = $obj_datos->id_entidad
        AND rip_centro_almacenamiento.id_rip_tipo = $obj_datos->id_rip_tipo
                ");

        $statement->execute();
        if ($statement->rowCount() == 0)
            return array();
        else
            return $statement->fetchAll();
    }

    public function getMeses($sw = false) {
        return array(
            array('data' => 1, 'label' => ($sw?'ENE':'ENERO')),
            array('data' => 2, 'label' => ($sw?'FEB':'FEBRERO')),
            array('data' => 3, 'label' => ($sw?'MAR':'MARZO')),
            array('data' => 4, 'label' => ($sw?'ABR':'ABRIL')),
            array('data' => 5, 'label' => ($sw?'MAY':'MAYO')),
            array('data' => 6, 'label' => ($sw?'JUN':'JUNIO')),
            array('data' => 7, 'label' => ($sw?'JUL':'JULIO')),
            array('data' => 8, 'label' => ($sw?'AGO':'AGOSTO')),
            array('data' => 9, 'label' => ($sw?'SEP':'SEPTIEMBRE')),
            array('data' => 10, 'label' => ($sw?'OCT':'OCTUBRE')),
            array('data' => 11, 'label' => ($sw?'NOV':'NOVIEMBRE')),
            array('data' => 12, 'label' => ($sw?'DIC':'DICIEMBRE'))
        );
    }
    public function getMes($mes, $sw=false)
    {
        $arr = $this->getMeses($sw);
        $i = 0;
        while ($i<count($arr) && $arr[$i]['data']!=$mes)
            $i++;
        if ($i<count($arr))
            return $arr[$i]['label'];
        else
            return "";

    }
    public function getEntidad($obj_datos)
    {
        $statement = $this->em->getConnection()->prepare("SELECT entidad.id, entidad.razon_social, ip.observaciones
        FROM  rip_info_productiva ip
        INNER JOIN entidad on ip.id_entidad = entidad.id
        WHERE ip.id  = '$obj_datos->id'");

        $statement->execute();
        if ($statement->rowCount() == 0)
            return array();
        else
            return $statement->fetch();
    }

}
