<?php
/**
 * Created by PhpStorm.
 * User: israel
 * Date: 26-09-18
 * Time: 11:39 AM
 */

namespace ProcesosTransaccionalesBundle\Manager;


class ToolsManager
{
    public function objToArray($obj)
    {
        if(is_array($obj))
            return $obj;

        return json_decode(json_encode($obj), true);
    }
}