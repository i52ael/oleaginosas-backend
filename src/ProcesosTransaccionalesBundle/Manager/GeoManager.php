<?php
/**
 * Created by @i52ael
 * User: israel
 * Date: 21-09-18
 * Time: 04:41 PM
 */

namespace ProcesosTransaccionalesBundle\Manager;

use Doctrine\ORM\EntityManager;
use JMS\DiExtraBundle\Annotation as DI;

class GeoManager
{
    /**
     * @var EntityManager
     */
    protected $em;

    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }
    public function cbxDepartamentos()
    {
        $statement = $this->em->getConnection()->prepare("SELECT
        departamento.id,
        departamento.nombre_departamento
        FROM geo.departamento
        ORDER BY departamento.nombre_departamento
        ");
        $statement->execute();
        if ($statement->rowCount() == 0)
        {
            return array();
        }
        else
        {
            return $statement->fetchAll();
        }
    }
    public function cbxProvinciaMunicipio($id_departamento, $txt)
    {
        $partes = array_diff(explode(" ", $txt), array(''));
        if(count($partes) > 0)
        {
            $arr = array();
            foreach ($partes as $row)
            {
                $arr[] = "( lower(geo.municipio.nombre_municipio) ~ lower('$row') OR lower(geo.municipio.nombre_municipio) ~ lower(' $row') )";
            }
            $criteria = " AND " . implode(" AND ", $arr);
        }
        else
        {
            $criteria = "";
        }
        $statement = $this->em->getConnection()->prepare("SELECT
        geo.provincia.id AS id_provincia,
        geo.provincia.nombre_provincia,
        geo.municipio.id AS id_municipio,
        geo.municipio.nombre_municipio
        FROM
        geo.provincia
        INNER JOIN geo.municipio on geo.municipio.id_provincia = geo.provincia.id
        WHERE
        geo.provincia.id_departamento = $id_departamento
        $criteria
        ORDER BY geo.provincia.nombre_provincia, geo.municipio.nombre_municipio
        ");
        $statement->execute();
        if ($statement->rowCount() == 0)
        {
            return array();
        }
        else
        {
            $arr = array();
            while ($row = $statement->fetch())
            {
                $arr[] = array('data'=>$row['id_municipio'], 'label' => $row['nombre_municipio'].' - '.$row['nombre_provincia']);
            }
            return $arr;
            //return $statement->fetchAll();
        }
    }

}