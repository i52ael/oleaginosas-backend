<?php
/**
 * Created by PhpStorm.
 * User: israel
 * Date: 26-12-17
 * Time: 09:19 AM
 */

namespace ProcesosTransaccionalesBundle\Manager;


class SIEXCOTCPDF extends \TCPDF
{
    private $_arr_head = array();
    private $_dimencion=0;
    private $_logo_ministerio_x = 23;
    public function __construct($orientation = 'P', $unit = 'mm', $format = 'A4', $unicode = true, $encoding = 'UTF-8', $diskcache = false, $pdfa = false)
    {
        parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache, $pdfa);
    }
    private function clear()
    {
        $this->_arr_head=array();
        $this->_dimencion=0;
    }
    public function CellHead($arr,$clear=false)
    {
        if($clear===true)
            $this->clear();
        $index=count($this->_arr_head);
        foreach($arr as $key => $row)
            $this->_arr_head[$index][$key] = $row;

        if(!empty($arr['font']))
            $this->_arr_head[$index]['font']=$arr['font'];
        else
            $this->_arr_head[$index]['font']=array('family'=>$this->getFontFamily(),'style'=>'','size'=>null);

        if(!empty($arr['arr_fill']))
            $this->_arr_head[$index]['arr_fill']=$arr['arr_fill'];
        else
            $this->_arr_head[$index]['arr_fill']=array('col1'=>0,'col2'=>-1,'col3'=>-1);

        if(!empty($arr['text']))
            $this->_arr_head[$index]['text']=$arr['text'];
        else
            $this->_arr_head[$index]['text']=array('col1'=>0,'col2'=>-1,'col3'=>-1);

        if($arr['ln']===1)
            $this->_dimencion++;

    }
    public function setLogoMinisterio($x, $y=0)
    {
        $this->_logo_ministerio_x = $x;
    }
    public function Header()
    {
        $this->SetMargins(1,$this->_dimencion*0.465);
        $this->Image('bolivia.jpg',1,0.5,2.5,1.5);
        $this->Image('logo_mdpyep.jpg',$this->_logo_ministerio_x,0.5,5,1.3);

        foreach( $this->_arr_head as $row)
        {
            $row['w']=empty($row['w'])?0:$row['w'];
            $row['h']=empty($row['h'])?0:$row['h'];
            $row['txt']=empty($row['txt'])?'':$row['txt'];
            $row['border']=empty($row['border'])?0:$row['border'];
            $row['ln']=empty($row['ln'])?0:$row['ln'];
            $row['align']=empty($row['align'])?'':$row['align'];
            $row['fill']=empty($row['fill'])?false:true;


            $this->SetFont($row['font']['family'],$row['font']['style'],$row['font']['size']);
            $this->setColor('fill',$row['arr_fill']['col1'],$row['arr_fill']['col2'],$row['arr_fill']['col3']);
            $this->setColor('text',$row['text']['col1'],$row['text']['col2'],$row['text']['col3']);
            $this->Cell($row['w'],$row['h'],$row['txt'],$row['border'],$row['ln'],$row['align'],$row['fill']);
        }
    } 
    public function CellStyle($w,$h,$txt='',$border=0,$ln=0,$align='L',$fill=false,$colorText=array(0,0,0), $colorFill=array(0,0,0))
    {
        
        //$this->SetFont($fontsize);
        $this->setColor('fill',$colorFill[0], $colorFill[1], $colorFill[2]);
        $this->setColor('text',$colorText[0], $colorText[1], $colorText[2]);
        $this->Cell($w,$h,$txt,$border,$ln,$align,$fill);

        //$this->SetFont($fontsize=2);
        $this->setColor('fill',0,0,0);
        $this->setColor('text',0,0,0);
    }
}