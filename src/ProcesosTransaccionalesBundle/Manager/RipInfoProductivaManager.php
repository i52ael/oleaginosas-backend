<?php
namespace ProcesosTransaccionalesBundle\Manager;

use Doctrine\ORM\EntityManager;
// use ProcesosTransaccionalesBundle\Entity\AcopioDetalle;
// use ProcesosTransaccionalesBundle\Entity\CompraDetalle;
// use ProcesosTransaccionalesBundle\Entity\PeriodoFase;
// use ProcesosTransaccionalesBundle\Entity\ArchivoExcel;
// use ProcesosTransaccionalesBundle\Entity\ProduccionDetalle;
// use ProcesosTransaccionalesBundle\Entity\VentaDetalle;
// use ProcesosTransaccionalesBundle\Entity\Transaccion;
use ProcesosTransaccionalesBundle\Entity\RipInfoProductiva;
// use ProductoBundle\Controller\EntidadController;
// use ProductoBundle\Modelo\ModeloEntidad;

class RipInfoProductivaManager
{
    /**
     * @var EntityManager
     */
    protected $em;
    protected $repo;

    public function __construct(EntityManager $entityManager) {
	$this->em = $entityManager;
        $this->setRepository();
    }
    protected function setRepository()
    {
	$this->repo = $this->em->getRepository('ProcesosTransaccionalesBundle:RipInfoProductiva');
    }

    public function listaInformacionProductiva($obj_datos){
        $str_where = "";
        if($obj_datos->id_entidad > 0){
            $str_where = "WHERE ip.id_entidad = '$obj_datos->id_entidad'";
        }
        $limit = "";
        if ($obj_datos->fromRow != -1 && $obj_datos->pageSize != -1){
            $limit = "LIMIT $obj_datos->pageSize OFFSET " . (intval($obj_datos->fromRow) - 1);
        }
    $query = "SELECT ip.id, ip.observaciones, ip.fecha_registro, e.id as id_entidad, e.razon_social
        FROM rip_info_productiva ip INNER JOIN entidad e ON ip.id_entidad = e.id $str_where $limit";
        $statement = $this->em->getConnection()->prepare($query);
        $statement->execute();
        if ($statement->rowCount() == 0){
            return array('error'=>'No se encontraron formularios de informacion productiva.');
        } else{
            return $statement->fetchAll();
        }
    }

    public function consulta($obj_datos,$user){
        $ripInfoProductiva = new RipInfoProductiva();
        $ripInfoProductiva->setObservaciones($obj_datos->observaciones);
        $ripInfoProductiva->setFechaRegistro(new \DateTime(date("Y-m-d H:i:s")));
        $ripInfoProductiva->setIdUsuarioCreacion($user->idUser);
        $ripInfoProductiva->setEliminado(false);
        $ripInfoProductiva->setIdEntidad($this->em->getReference('ProcesosTransaccionalesBundle:Entidad',$obj_datos->id_entidad));
        $this->em->persist($ripInfoProductiva);
        $this->em->flush();
        return array('id'=>$ripInfoProductiva->getId());
    }

    public function cbxTipo($tipo)
    {
        $statement = $this->em->getConnection()->prepare("SELECT
        rip_tipo.id as data,
        rip_tipo.descripcion as label
        FROM
        rip_tipo
        WHERE
        lower(rip_tipo.tipo) = lower('$tipo')
        ");

        $statement->execute();
        if ($statement->rowCount() == 0)
            return array();
        else
            return $statement->fetchAll();
    }
}
