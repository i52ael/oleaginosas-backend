<?php
namespace ProcesosTransaccionalesBundle\Manager;

use Doctrine\ORM\EntityManager;
use ProcesosTransaccionalesBundle\Entity\AcopioDetalle;
use ProcesosTransaccionalesBundle\Entity\CompraDetalle;
use ProcesosTransaccionalesBundle\Entity\PeriodoFase;
use ProcesosTransaccionalesBundle\Entity\ArchivoExcel;
use ProcesosTransaccionalesBundle\Entity\ProduccionDetalle;
use ProcesosTransaccionalesBundle\Entity\VentaDetalle;
use ProcesosTransaccionalesBundle\Entity\Transaccion;
use ProductoBundle\Controller\EntidadController;
use ProductoBundle\Modelo\ModeloEntidad;

class TransaccionManager
{
    /**
     * @var EntityManager
     */
    protected $em;
    protected $repo;

    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
        $this->setRepository();

    }

    protected function setRepository()
    {
        $this->repo = $this->em->getRepository('ProcesosTransaccionalesBundle:Transaccion');
    }

    public function getList()
    {
        $this->qb = $this->repo->getListQB();

        return $this;
    }

    public function guardar($head, $body)
    {
        $this->em->getConnection()->beginTransaction();
        try {
            $arr = $this->guardarFn($head, $body);
            if (!empty($arr['error'])) {
                $this->em->getConnection()->rollBack();
                return $arr;
            }

            $this->em->getConnection()->commit();
        } catch (\Exception $e) {
            $this->em->getConnection()->rollBack();
            throw $e;
        }

        return $arr;
    }

    private function guardarFn($head, $body)
    {
        if (empty($head->id_periodo_fase))
            return array('error' => 'Por favor seleccione el periodo de la transaccion');
        $validado = false;
        $id_transaccion_edit = 0;
        if (!empty($head->id_transaccion))//si se esta editando una transaccion primero hacemos el eliminado logico y luego se procede a guardar como otra transaccion
        {
            $id_transaccion_edit = $head->id_transaccion;
            $statement = $this->em->getConnection()->prepare("UPDATE transaccion SET eliminado=TRUE
                                                                        WHERE
                                                                        id=$head->id_transaccion
                                                                        ");
            $statement->execute();

            $statement = $this->em->getConnection()->prepare("SELECT
            transaccion.validado
            FROM
            transaccion
            WHERE
            transaccion.id = $head->id_transaccion
            ");
            $statement->execute();
            $row = $statement->fetch();
            $validado = $row['validado'];

        }

        $arr_result=$this->saveHead($head, $validado);
        if(!empty($arr_result['error']))
            return $arr_result;

        $this->saveBody($head->id_tipo_transaccion, $arr_result['id'], $body);

        $this->em->flush();

        $obj = new \stdClass();
        $obj->id_transaccion = $arr_result['id'];
        return array('id' => $arr_result['id'], 'id_edit' => $id_transaccion_edit, 'arr' => $this->findTransaccion($obj));
    }

    /**
     * @param $head
     * @param bool $validado => toda transaccion nueva es false, porque no esta validada
     * @return array
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\ORM\ORMException
     */
    public function saveHead($head, $validado=false)
    {
        $statement = $this->em->getConnection()->prepare("SELECT
        transaccion.id
        FROM
        transaccion
        WHERE
        transaccion.id_tipo_transaccion=$head->id_tipo_transaccion
        AND transaccion.id_periodo_fase=$head->id_periodo_fase
        AND transaccion.id_relacion_entidad=$head->id_relacion_entidad
        AND transaccion.eliminado = false
        ");
        $statement->execute();

        if ($statement->rowCount() > 0)
            return array('error' => 'Ya existe la transaccion con ese tipo de transacion');

        $c_transaccion = new Transaccion();
        $c_transaccion->setFechaRegistro(new \DateTime());
        $c_transaccion->setIdUsuarioUltimaModificacion(1);
        $c_transaccion->setIdUsuarioCreacion(1);
        $c_transaccion->setFechaUltimaModificacion(new \DateTime());
        $c_transaccion->setEliminado(false);
        $c_transaccion->setIdPeriodoFase($this->em->getReference('ProcesosTransaccionalesBundle:PeriodoFase', $head->id_periodo_fase));
        $c_transaccion->setIdTipoCambio($this->em->getReference('ProcesosTransaccionalesBundle:TipoCambio', $head->id_tipo_cambio));
        $c_transaccion->setIdTipoTransaccion($this->em->getReference('ProcesosTransaccionalesBundle:TipoTransaccion', $head->id_tipo_transaccion));
        $c_transaccion->setIdRelacionEntidad($this->em->getReference('ProcesosTransaccionalesBundle:RelacionEntidad', $head->id_relacion_entidad));
        $c_transaccion->setValidado($validado);
        $this->em->persist($c_transaccion);

        return array('id'=> $c_transaccion->getId());
    }
    /**
     * @description se separo la funcion de grabado del body para que se pueda realizar el grabado masivo del excel
     */
    public function saveBody($id_tipo_transaccion, $id_head, $body)
    {
        switch ($id_tipo_transaccion)
        {
            case 1://compra

                foreach ($body as $row) {

                    $statement = $this->em->getConnection()->prepare("SELECT
                    entidad.beneficiario,
                    entidad.id_regimen
                    FROM
                    entidad
                    WHERE
                    entidad.id = $row->id_entidad
                    ");
                    $statement->execute();
                    $row_entidad = $statement->fetch();

                    $c_compraDetalle = new CompraDetalle();
                    $c_compraDetalle->setIdTransaccion($this->em->getReference('ProcesosTransaccionalesBundle:Transaccion', $id_head));
                    $c_compraDetalle->setIdEntidadProveedor($this->em->getReference('ProcesosTransaccionalesBundle:Entidad', $row->id_entidad));
                    $c_compraDetalle->setIdEntidadSocio($this->em->getReference('ProcesosTransaccionalesBundle:Entidad', $row->id_entidad_b));
                    $c_compraDetalle->setIdProducto($this->em->getReference('ProcesosTransaccionalesBundle:Producto', $row->id_producto));
                    $c_compraDetalle->setIdRegimen($this->em->getReference('ProcesosTransaccionalesBundle:Regimen', $row_entidad['id_regimen']));
                    $c_compraDetalle->setFechaCompra(new \DateTime($row->fecha));
                    $c_compraDetalle->setNroFactura($row->nro_factura);
                    $c_compraDetalle->setCantidad($row->cantidad);
                    $c_compraDetalle->setPrecioUnitario($row->precio_unitario);
                    $c_compraDetalle->setPrecioTotal($row->precio_total);
                    $c_compraDetalle->setBeneficiario($row_entidad['beneficiario']);
                    $c_compraDetalle->setMesAcopio(new \DateTime($row->mes_acopio));
                    $c_compraDetalle->setIdMoneda($this->em->getReference('ProcesosTransaccionalesBundle:TipoMoneda', $row->id_moneda));
                    $this->em->persist($c_compraDetalle);
                }
                break;
            case 2://venta

                foreach ($body as $row) {
                    $c_ventaDetalle = new VentaDetalle();
                    $c_ventaDetalle->setIdTransaccion($this->em->getReference('ProcesosTransaccionalesBundle:Transaccion', $id_head));
                    $c_ventaDetalle->setNroFactura($row->nro_factura);
                    $c_ventaDetalle->setIdEntidadCliente($this->em->getReference('ProcesosTransaccionalesBundle:Entidad', $row->id_entidad));
                    $c_ventaDetalle->setIdEntidadSocio($this->em->getReference('ProcesosTransaccionalesBundle:Entidad', $row->id_entidad_b));
                    $c_ventaDetalle->setIdSubproducto($this->em->getReference('ProcesosTransaccionalesBundle:Subproducto', $row->id_producto));
                    $c_ventaDetalle->setFechaVenta(new \DateTime($row->fecha));
                    $c_ventaDetalle->setCantidad($row->cantidad);
                    $c_ventaDetalle->setPrecioUnitario($row->precio_unitario);
                    $c_ventaDetalle->setPrecioTotal($row->precio_total);
                    $c_ventaDetalle->setIdMoneda($this->em->getReference('ProcesosTransaccionalesBundle:TipoMoneda', $row->id_moneda));
                    $this->em->persist($c_ventaDetalle);
                }
                break;
            case 3://produccion

                foreach ($body as $row) {
                    $c_produccionDetalle = new ProduccionDetalle();
                    $c_produccionDetalle->setIdEntidadSocio($this->em->getReference('ProcesosTransaccionalesBundle:Entidad', $row->id_entidad_b));
                    $c_produccionDetalle->setIdTransaccion($this->em->getReference('ProcesosTransaccionalesBundle:Transaccion', $id_head));
                    $c_produccionDetalle->setIdProducto($this->em->getReference('ProcesosTransaccionalesBundle:Subproducto', $row->id_producto));//dice id_producto pero el dato es del subproducto
                    $c_produccionDetalle->setCantidadProduccion($row->cantidad);
                    $c_produccionDetalle->setStockFinal($row->cantidad_stock);
                    $this->em->persist($c_produccionDetalle);
                }
                break;
            case 4://acopio

                foreach ($body as $row) {
                    $c_acopioDetalle = new AcopioDetalle();

                    $c_acopioDetalle->setIdEntidadSocio($this->em->getReference('ProcesosTransaccionalesBundle:Entidad', $row->id_entidad_b));
                    $c_acopioDetalle->setIdProducto($this->em->getReference('ProcesosTransaccionalesBundle:Producto', $row->id_producto));
                    $c_acopioDetalle->setIdTransaccion($this->em->getReference('ProcesosTransaccionalesBundle:Transaccion', $id_head));
                    $c_acopioDetalle->setFechaAcopio(new \DateTime($row->fecha));

                    $c_acopioDetalle->setCantidadBruta($row->cantidad_bruta);
                    $c_acopioDetalle->setCantidadLiquido($row->cantidad_liquido);
                    $c_acopioDetalle->setCantidadNeta($row->cantidad_neta);
                    $c_acopioDetalle->setIdEntidadProveedor($this->em->getReference('ProcesosTransaccionalesBundle:Entidad', $row->id_entidad));
                    $this->em->persist($c_acopioDetalle);
                }
        }
        return array('ok'=>1);
    }

    public function findEntidadXNumeroIdentificacion($numero_identificacion)
    {
        $query = "SELECT
entidad.razon_social,
entidad.numero_identificacion,
relacion_entidad.id AS id_relacion_entidad,
relacion_entidad.id_entidad AS id_entidad
FROM
entidad
INNER JOIN relacion_entidad ON relacion_entidad.id_entidad = entidad.id AND relacion_entidad.vigente = true
WHERE
entidad.eliminado=false
";
        $statement = $this->em->getConnection()->prepare($query . " AND entidad.numero_identificacion = '$numero_identificacion' ");
        $statement->execute();

        if ($statement->rowCount() == 0)
            return array('error'=>'No se encontro los datos de la entidad con numero de identificacion "'.$numero_identificacion.'"');
        else
            return $statement->fetchAll();
    }
    public function findProductoXCodigo($id_tipo_transaccion,$codigo_producto)
    {
        return $this->findProductoSW('',$id_tipo_transaccion,'',$codigo_producto, null,2);
    }
    public function findProductoXDescripIdEntidad($str_nombre,$id_entidad, $id_tipo_transaccion, $id_fase)
    {
        return $this->findProductoSW($str_nombre,$id_tipo_transaccion,$id_fase,'',$id_entidad,1);
    }
    public function findProductoXDescrip($str_nombre, $id_tipo_transaccion, $id_fase)
    {
        return $this->findProductoSW($str_nombre,$id_tipo_transaccion,$id_fase,'',null,1);
    }
    private function findProductoSW($str_nombre, $id_tipo_transaccion, $id_fase,$codigo_producto,$id_entidad,$sw_busqueda)
    {
        $arr_where=array();
        switch ($id_tipo_transaccion) {
            case 0: //queria mostrar todos aqui pero pero no se si producto o subproducto
                    $query = "SELECT
                                producto.nombre || ' ' || producto.medida || ' '  ||medida.abreviacion   as label,
                                producto.id as data
                                FROM
                                producto
                                INNER JOIN medida ON medida.id = producto.id_medida
                                WHERE
                                (LOWER(producto.nombre) ~ '" . strtolower($str_nombre) . "'
                                OR LOWER(producto.nombre) ~ ' " . strtolower($str_nombre) . "')
                                AND producto.eliminado=false";
                    break;
            case 3: //produccion
                if($sw_busqueda==1)
                {
                    if($id_fase==3)
                    {
                        $statement = $this->em->getConnection()->prepare("SELECT fase.id
                                        FROM fase
                                        WHERE fase.orden = (SELECT fase.orden-1 FROM fase WHERE fase.id =$id_fase LIMIT 1)");
                        $statement->execute();
                        if ($statement->rowCount() == 0)
                            return array('error' => 'Busqueda finalizada, no se encontro los datos');

                        $row_fase = $statement->fetch();
                        $str_where_fase="AND (producto.id_fase IN ($id_fase,$row_fase[id]) )";
                    }
                    else
                        $str_where_fase="AND producto.id_fase=$id_fase";

                    $criterio = "(LOWER(producto.nombre) ~ '" . strtolower($str_nombre) . "'
                                OR LOWER(producto.nombre) ~ ' " . strtolower($str_nombre) . "')
                                $str_where_fase";
                }
                else
                {
                    $criterio = " LOWER(producto.codigo) LIKE '". strtolower($codigo_producto)."'";
                }



                $query = "SELECT
                            producto.nombre || ' ' || producto.medida || ' '  ||medida.abreviacion   as label,
                            producto.id as data
                            FROM
                            producto
                            INNER JOIN medida ON medida.id = producto.id_medida
                            WHERE
                            $criterio
                            AND producto.eliminado=false";
                    break;
            case 1: //compra
            case 4: //acopio

                if($sw_busqueda==2)
                    $criterio=" LOWER(producto.codigo) LIKE '". strtolower($codigo_producto)."'";
                else
                {
                    $statement = $this->em->getConnection()->prepare("SELECT fase.id
                                        FROM fase
                                        WHERE fase.orden = (SELECT fase.orden-1 FROM fase WHERE fase.id =$id_fase LIMIT 1)");
                    $statement->execute();
                    if ($statement->rowCount() == 0)
                        return array('error' => 'Busqueda finalizada, no se encontro los datos');

                    $row_fase = $statement->fetch();

                    $criterio = "(LOWER(producto.nombre) ~ '" . strtolower($str_nombre) . "'
                                OR LOWER(producto.nombre) ~ ' " . strtolower($str_nombre) . "')
                                AND producto.id_fase=$row_fase[id]";
                }
                $query = "SELECT
                            producto.nombre || ' ' || producto.medida || ' '  ||medida.abreviacion   as label,
                            producto.id as data
                            FROM
                            producto
                            INNER JOIN medida ON medida.id = producto.id_medida
                            WHERE
                            $criterio
                            AND producto.eliminado=false";
                break;
            case 2: //venta
                if(!empty($id_entidad))
                    $arr_where[]="subproducto.id_entidad = '$id_entidad'";

                if($sw_busqueda==2)
                    $arr_where[]="subproducto.codigo LIKE '$codigo_producto'";
                else
                    $arr_where[]="(LOWER(subproducto.nombre_comercial) ~ '" . strtolower($str_nombre) . "'
                                OR LOWER(subproducto.nombre_comercial) ~ ' " . strtolower($str_nombre) . "')";
                if(!empty($arr_where))
                    $criterio=implode(" AND ",$arr_where);
                else
                    $criterio='';
                $query = "SELECT
                            subproducto.nombre_comercial || ' ' || subproducto.medida || ' '  ||medida.abreviacion   as label,
                            subproducto.id as data
                            FROM
                            subproducto
                            INNER JOIN medida ON medida.id = subproducto.id_medida
                            WHERE
                            $criterio
                            AND eliminado=false";

                break;
            default:
                $query = "";
        }
        $statement = $this->em->getConnection()->prepare($query);
        $statement->execute();
        if ($statement->rowCount() == 0)
           return array('error' => 'Busqueda finalizada, no se encontro los datos del producto '.$codigo_producto);

        return array('arr' => $statement->fetchAll());
    }

    public function findTipoTransaccionesAndColumns($id_fase)
    {
        $arr_tipo_transaccion = $this->findTipoTransaccion($id_fase);
        if (!empty($arr_tipo_transaccion['error']))
            return $arr_tipo_transaccion;

        $arr_columns = $this->getColumnsGrid($id_fase);
        if (!empty($arr_columns['error']))
            return $arr_columns;

        return array('arr_tipo_transaccion' => $arr_tipo_transaccion, 'arr_columns' => $arr_columns);
    }

    public function findTipoTransaccion($id_fase)
    {
        $statement = $this->em->getConnection()->prepare("SELECT
tipo_transaccion.id as data,
tipo_transaccion.descripcion as label
FROM
fase_tipo_transaccion
INNER JOIN tipo_transaccion ON tipo_transaccion.id = fase_tipo_transaccion.id_tipo_transaccion
WHERE
fase_tipo_transaccion.id_fase='$id_fase'
AND tipo_transaccion.eliminado=false
ORDER BY tipo_transaccion.orden");
        $statement->execute();
        return $statement->fetchAll();
    }

    public function init($id_entidad)
    {
        $statement = $this->em->getConnection()->prepare("SELECT
entidad.razon_social,
entidad.id_fase
FROM
entidad
WHERE
entidad.id='$id_entidad'");
        $statement->execute();
        $datos = $statement->fetch();
        $arr_tipo_transaccion = $this->findTipoTransaccion($datos['id_fase']);

        $arr_entidad = $this->cbxEntidad($id_entidad, null);

        return array('arr_tipo_transaccion' => $arr_tipo_transaccion,
            'razon_social' => $datos['razon_social'],
            'arr_entidad' => $arr_entidad,
            'dato_a' => '',
            'dato_b' => '',
            'dato_c' => '');
    }

    public function cbxEntidad($id_entidad, $str_razon)
    {

        $arr_and = array();
        if (!empty($id_entidad))
            $arr_and[] = "entidad.id=$id_entidad";

        $arr_or = array();
        if (!empty($str_razon)) {
            $arr = array_diff(explode(" ", $str_razon), array(''));
            foreach ($arr as $row) {
                $arr_or[] = "( LOWER(entidad.razon_social) ~ '" . strtolower($row) . "' OR LOWER(entidad.razon_social) ~ ' " . strtolower($row) . "')";
            }
            if (!empty($arr_or)) {
                $arr_and[] = "(" . implode(" OR ", $arr_or) . ")";
            }
        }
        if (!empty($arr_and))
            $str_where = " AND " . implode(" AND ", $arr_and);
        else
            $str_where = "";

        $statement = $this->em->getconnection()->query("SELECT
entidad.razon_social AS label,
entidad.numero_identificacion,
relacion_entidad.id AS data,
entidad.id AS id_entidad,
entidad.id_fase AS id_fase
FROM
entidad
INNER JOIN relacion_entidad ON relacion_entidad.id_entidad = entidad.id AND relacion_entidad.vigente
INNER JOIN tipo_entidad ON tipo_entidad.id = entidad.id_tipo_entidad
WHERE
entidad.eliminado=false
AND NOT tipo_entidad.individual -- solo empresas no individuales
$str_where
");
        $arr = $statement->fetchAll();
        if ($statement->rowCount() != 1)
            array_unshift($arr, array('data' => 0, 'label' => '------', 'numero_identificacion' => '', 'id_entidad' => 0, 'id_fase' => 0));
        return $arr;
    }

    public function findTransaccion($obj_datos)
    {
        $str_where = "";
        $arr_where = array();
        if (!empty($obj_datos->id_tipo_transaccion))
            $arr_where[] = "transaccion.id_tipo_transaccion=$obj_datos->id_tipo_transaccion";

        if (!empty($obj_datos->id_transaccion))
            $arr_where[] = "transaccion.id=$obj_datos->id_transaccion";

        if (!empty($obj_datos->id_relacion_entidad))
            $arr_where[] = "transaccion.id_relacion_entidad=$obj_datos->id_relacion_entidad";

        if (!empty($arr_where))
            $str_where = ' AND ' . implode(" AND ", $arr_where);
        if (!empty($obj_datos->fromRow))
            $limit = "LIMIT $obj_datos->pageSize OFFSET " . (intval($obj_datos->fromRow) - 1);
        else
            $limit = "";
        $statement = $this->em->getconnection()->query("SELECT
COUNT(*) OVER() as total_rows,
transaccion.id,
transaccion.id_tipo_transaccion,
to_char(transaccion.fecha_registro,'dd-mm-yyyy') AS fecha_registro,
transaccion.id_periodo_fase,
tipo_transaccion.descripcion AS descrip_tipo_transaccion,
tipo_transaccion.abreviatura,
to_char(periodo_fase.fecha_desde,'dd') as dia_desde,
to_char(periodo_fase.fecha_hasta,'dd') as dia_hasta,
to_char(periodo_fase.fecha_desde,'mm') as mes,
to_char(periodo_fase.fecha_desde,'yyyy') as anio,
transaccion.validado
FROM
transaccion
INNER JOIN tipo_transaccion on tipo_transaccion.id = transaccion.id_tipo_transaccion
INNER JOIN periodo_fase on periodo_fase.id = transaccion.id_periodo_fase
WHERE
transaccion.eliminado=false
$str_where
ORDER BY periodo_fase.fecha_desde ASC -- , periodo_fase.fecha_hasta DESC
$limit
");
        $arr = array();
        $total_rows = 0;
        while ($row = $statement->fetch()) {
            $total_rows = $row['total_rows'];
            $row['codigo'] = $row['abreviatura'] . $row['id'];
            $row['descrip_periodo'] = $row['dia_desde'] . ' al ' . $row['dia_hasta'] . ' ' . $this->getMes($row['mes']).'-'.$row['anio'];
            $arr[] = $row;
        }
        if (empty($arr))
            return array('error' => 'Busqueda finalizada, no se encontro los datos');
        return array('arr' => $arr, 'total_rows' => $total_rows);
    }

    private function meses($sw = 0)
    {
        $arr = array();
        $arr[] = array('', 'Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic');
        $arr[] = array('00', '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12');
        if (empty($arr[$sw]))
            return array();
        else
            return $arr[$sw];
    }

    public function getMes($index)
    {
        $index = intval($index);
        if ($index > 12 || $index < 1)
            return '';

        $arr_mes = $this->meses();
        return $arr_mes[$index];
    }

    public function deleteTransaccion($id_transaccion)
    {
        $statement = $this->em->getConnection()->prepare("SELECT
transaccion.validado
FROM
transaccion
WHERE
transaccion.id = '$id_transaccion'");

        $statement->execute();
        $row = $statement->fetch();
        if($row['validado']===true)
            return array('error'=>'La transaccion no puede ser eliminado porque ya se encuentra en estado VALIDADO');

        $s = $this->em->getReference('ProcesosTransaccionalesBundle:Transaccion', $id_transaccion);
        $s->setEliminado(true);
        //$this->em->remove($obj_transaccion);
        $this->em->persist($s);
        $this->em->flush();

        return array('id' => $id_transaccion);
    }

    public function findTransaccionEdit($id_transaccion)
    {
        $head = $this->headTransaccion($id_transaccion);
        if(!empty($head['error']))
            return $head;
        if($head['validado']===true)
            return array('error'=>'La transaccion no puede ser modificada porque ya se encuentra en estado VALIDADO');
        $statement = $this->em->getConnection()->prepare("SELECT
periodo_fase.id_fase
FROM
periodo_fase
WHERE
periodo_fase.id = '$head[id_periodo_fase]'");
        $statement->execute();
        $row = $statement->fetch();

        return array('head' => $head,
            'body' => $this->bodyTransaccion($id_transaccion),
            'arr_periodo' => $this->initAddTransaccion($head['id_relacion_entidad'], $head['id_tipo_transaccion'], $row['id_fase'], $id_transaccion));
    }

    public function headTransaccion($id_transaccion)
    {
        $statement = $this->em->getConnection()->prepare("SELECT
transaccion.id,
transaccion.id_tipo_transaccion,
transaccion.id_relacion_entidad,
transaccion.id_periodo_fase,
transaccion.id_tipo_cambio,
transaccion.fecha_registro,
entidad.razon_social,
to_char(periodo_fase.fecha_desde,'dd') as dia_desde,
to_char(periodo_fase.fecha_hasta,'dd') as dia_hasta,
to_char(periodo_fase.fecha_desde,'mm') as mes,
to_char(periodo_fase.fecha_desde,'yyyy') as anio,
transaccion.validado

FROM
transaccion
INNER JOIN relacion_entidad ON relacion_entidad.id = transaccion.id_relacion_entidad
INNER JOIN entidad ON entidad.id = relacion_entidad.id_entidad
INNER JOIN periodo_fase ON periodo_fase.id = transaccion.id_periodo_fase
WHERE
transaccion.id = '$id_transaccion'");
        $statement->execute();
        $row = $statement->fetch();
        $row['descrip_periodo_fase'] = $row['dia_desde'] . ' al ' . $row['dia_hasta'] . ' ' . $this->getMes($row['mes']).'-'.$row['anio'];
        return $row;
    }

    public function bodyTransaccion($id_transaccion)
    {
        $statement = $this->em->getConnection()->prepare("SELECT
transaccion.id_tipo_transaccion
FROM
transaccion
WHERE
transaccion.id = '$id_transaccion'");
        $statement->execute();
        $arr_transaccion = $statement->fetch();

        switch ($arr_transaccion['id_tipo_transaccion']) {
            case 1:
                $statement = $this->em->getConnection()->prepare("SELECT
                                compra_detalle.id,
                                compra_detalle.id_producto,
                                compra_detalle.cantidad,
                                compra_detalle.precio_unitario,
                                compra_detalle.precio_total,
                                compra_detalle.nro_factura,
                                compra_detalle.id_entidad_proveedor AS id_entidad,
                                entidad.razon_social,
                                compra_detalle.fecha_compra AS fecha,
                                compra_detalle.id_transaccion,
                                producto.nombre || ' ' || producto.medida || ' '  || medida.abreviacion AS descrip_producto,
                                entidad.numero_identificacion,
                                compra_detalle.id_entidad_socio AS id_entidad_b,
                                entidad_socio.razon_social AS razon_social_b,
                                entidad_socio.numero_identificacion AS numero_identificacion_b,
                                '' AS cantidad_bruta,
                                '' AS cantidad_neta,
                                '' AS cantidad_liquido,
                                compra_detalle.mes_acopio,
                                to_char(compra_detalle.mes_acopio, 'YYYY-mm') AS print_mes_acopio,
                                id_moneda,
                                tipo_moneda.abreviacion AS descrip_moneda
                                FROM
                                compra_detalle
                                INNER JOIN producto ON producto.id = compra_detalle.id_producto
                                INNER JOIN medida ON medida.id = producto.id_medida
                                INNER JOIN entidad  ON entidad.id = compra_detalle.id_entidad_proveedor
                                INNER JOIN entidad AS entidad_socio ON entidad_socio.id = compra_detalle.id_entidad_socio
                                INNER JOIN tipo_moneda ON tipo_moneda.id = compra_detalle.id_moneda

                                WHERE
                                compra_detalle.id_transaccion=$id_transaccion
                                ");
                break;
            case 2:
                $statement = $this->em->getConnection()->prepare("SELECT
                                venta_detalle.id,
                                venta_detalle.id_subproducto AS id_producto,
                                venta_detalle.cantidad,
                                venta_detalle.precio_unitario,
                                venta_detalle.precio_total,
                                venta_detalle.nro_factura,
                                venta_detalle.id_entidad_cliente AS id_entidad,
                                venta_detalle.fecha_venta AS fecha,
                                venta_detalle.id_transaccion,
                                subproducto.nombre_comercial || ' ' || subproducto.medida || ' '  || medida.abreviacion AS descrip_producto,
                                entidad.razon_social,
                                entidad.numero_identificacion,
                                entidad_socio.id AS id_entidad_b,
                                entidad_socio.razon_social AS razon_social_b,
                                entidad_socio.numero_identificacion AS numero_identificacion_b,
                                '' AS cantidad_bruta,
                                '' AS cantidad_neta,
                                '' AS cantidad_liquido,
                                '' AS mes_acopio,
                                venta_detalle.id_moneda,
                                tipo_moneda.abreviacion AS descrip_moneda
                                FROM
                                venta_detalle
                                INNER JOIN subproducto ON subproducto.id = venta_detalle.id_subproducto
                                INNER JOIN medida ON medida.id = subproducto.id_medida
                                INNER JOIN entidad  ON entidad.id = venta_detalle.id_entidad_cliente
                                INNER JOIN entidad AS entidad_socio ON entidad_socio.id = venta_detalle.id_entidad_socio
                                INNER JOIN tipo_moneda ON tipo_moneda.id = venta_detalle.id_moneda
                                WHERE
                                venta_detalle.id_transaccion=$id_transaccion
                                ");
                break;
            case 3:
                $statement = $this->em->getConnection()->prepare("SELECT
                                produccion_detalle.id,
                                produccion_detalle.id_producto AS id_producto,
                                produccion_detalle.cantidad_produccion AS cantidad,
                                '0' AS precio_unitario,
                                '0' AS precio_total,
                                ''  AS nro_factura,
                                '' AS id_entidad,
                                '' AS fecha,
                                produccion_detalle.id_transaccion,
                                producto.nombre || ' ' || producto.medida || ' '  || medida.abreviacion AS descrip_producto,
                                produccion_detalle.stock_final AS cantidad_stock,
                                entidad_socio.id AS id_entidad_b,
                                entidad_socio.razon_social AS razon_social_b,
                                entidad_socio.numero_identificacion AS numero_identificacion_b,
                                '' AS numero_identificacion,
                                '' AS cantidad_bruta,
                                '' AS cantidad_neta,
                                '' AS cantidad_liquido,
                                '' AS mes_acopio,
                                '' AS id_moneda,
                                '' AS descrip_moneda
                                FROM
                                produccion_detalle
                                INNER JOIN producto ON producto.id = produccion_detalle.id_producto
                                INNER JOIN medida ON medida.id = producto.id_medida
                                INNER JOIN entidad AS entidad_socio ON entidad_socio.id = produccion_detalle.id_entidad_socio
                                WHERE
                                produccion_detalle.id_transaccion=$id_transaccion
                                ");

                break;
            case 4:
                $statement = $this->em->getConnection()->prepare("SELECT
                                acopio_detalle.id,
                                acopio_detalle.id_producto,
                                '' AS cantidad,
                                '' AS precio_unitario,
                                '' AS precio_total,
                                '' AS nro_factura,
                                acopio_detalle.id_entidad_proveedor AS id_entidad,
                                entidad.razon_social,
                                acopio_detalle.fecha_acopio AS fecha,
                                acopio_detalle.id_transaccion,
                                producto.nombre || ' ' || producto.medida || ' '  || medida.abreviacion AS descrip_producto,
                                entidad.numero_identificacion,
                                acopio_detalle.id_entidad_socio AS id_entidad_b,
                                entidad_socio.razon_social AS razon_social_b,
                                entidad_socio.numero_identificacion AS numero_identificacion_b,
                                acopio_detalle.cantidad_bruta,
                                acopio_detalle.cantidad_neta,
                                acopio_detalle.cantidad_liquido,
                                '' AS mes_acopio,
                                '' AS id_moneda,
                                '' AS descrip_moneda
                                FROM
                                acopio_detalle
                                INNER JOIN producto ON producto.id = acopio_detalle.id_producto
                                INNER JOIN medida ON medida.id = producto.id_medida
                                INNER JOIN entidad  ON entidad.id = acopio_detalle.id_entidad_proveedor
                                INNER JOIN entidad AS entidad_socio ON entidad_socio.id = acopio_detalle.id_entidad_socio

                                WHERE
                                acopio_detalle.id_transaccion=$id_transaccion
                                ");
                break;

        }

        $statement->execute();
        $arr = array();

        while ($row = $statement->fetch()) {
            $row['dg_id'] = $row['id'];
            $arr[] = $row;

        }
        return $arr;
    }

    /**
     * @param $fecha dato fecha en formato yyyy-mm-dd ó dd-mm-yyyy
     * */
    public function getFormatDate($fecha, $sw = 1)
    {
        $arr = explode("-", $fecha);

    }

    public function getColumnsGrid($id_fase)
    {
        $statement = $this->em->getconnection()->query("SELECT
tipo_transaccion.id,
tipo_transaccion.descripcion
FROM
fase_tipo_transaccion
INNER JOIN tipo_transaccion ON tipo_transaccion.id = fase_tipo_transaccion.id_tipo_transaccion
WHERE
fase_tipo_transaccion.id_fase=$id_fase
AND tipo_transaccion.eliminado=false
");
        $arr = array();
        $arr[] = array('name' => 'descrip_periodo', 'label' => 'Periodo');
        while ($row = $statement->fetch()) {
            $arr[] = array('id' => $row['id'], 'name' => strtolower($row['descripcion']), 'label' => ucwords(strtolower($row['descripcion'])));
        }
        return $arr;
    }

    public function initCPeriodo($id_fase)
    {
        return array('arr_columns' => $this->getColumnsGrid($id_fase));
    }

    public function initAddTransaccion($id_relacion_entidad, $id_tipo_transaccion, $id_fase, $id_transaccion = 0)
    {
        if (empty($id_relacion_entidad))
            return array('error' => 'Verifique el ID de Relacion Entidad');
        if (empty($id_tipo_transaccion))
            return array('error' => 'Verifique el ID de Tipo Transaccion');
        if (empty($id_fase))
            return array('error' => 'Verifique el ID de Fase');
        if (!empty($id_transaccion))
            $str_where = "AND transaccion.id <> $id_transaccion";
        else
            $str_where = "";
        $statement = $this->em->getconnection()->query("SELECT
periodo_fase.id,
to_char(periodo_fase.fecha_desde,'dd') as dia_desde,
to_char(periodo_fase.fecha_hasta,'dd') as dia_hasta,
to_char(periodo_fase.fecha_desde,'mm') as mes,
to_char(periodo_fase.fecha_desde,'yyyy') as anio,
periodo_fase.fecha_desde,
periodo_fase.fecha_hasta
FROM
periodo_fase
WHERE
periodo_fase.id_fase = $id_fase
AND periodo_fase.fecha_desde <= now()
AND periodo_fase.id NOT IN(

SELECT
transaccion.id_periodo_fase
FROM
transaccion
WHERE
transaccion.eliminado=false
AND transaccion.id_tipo_transaccion = $id_tipo_transaccion
AND transaccion.id_relacion_entidad = $id_relacion_entidad
$str_where
GROUP BY transaccion.id_periodo_fase

)
ORDER BY periodo_fase.fecha_desde");
        $arr = array();

        while ($row = $statement->fetch()) {
            $row['data'] = $row['id'];
            $row['label'] = $row['dia_desde'] . ' al ' . $row['dia_hasta'] . ' ' . $this->getMes($row['mes']).'-'.$row['anio'];
            $arr[] = $row;
        }
        if (empty($arr))
            return array('error' => 'Busqueda finalizada, no se encontro los datos');
        return array('arr_periodo' => $arr);
    }

    public function consultaXPeriodo($id_relacion_entidad, $id_fase, $obj_limit = null, $id_periodo_fase = 0)
    {
        if($id_periodo_fase !== 0)
            $str_where = " AND periodo_fase.id = ".$id_periodo_fase;
        else
            $str_where = "";
        if (!empty($obj_limit->fromRow))
            $str_limit = "LIMIT $obj_limit->pageSize OFFSET " . (intval($obj_limit->fromRow) - 1);
        else
            $str_limit = "";
        $arr = $this->getColumnsGrid($id_fase);
        $str_select = "";
        foreach ($arr as $row) {
            if (!empty($row['id'])) {
                $str_select .= "(
SELECT
tipo_transaccion.abreviatura || transaccion.id
FROM
transaccion
INNER JOIN tipo_transaccion ON tipo_transaccion.id = transaccion.id_tipo_transaccion
WHERE
transaccion.id_periodo_fase = periodo_fase.id
AND transaccion.eliminado=false
AND transaccion.id_tipo_transaccion = $row[id]
AND transaccion.id_relacion_entidad = $id_relacion_entidad
) AS $row[name],";
            }
        }
        $statement = $this->em->getconnection()->query("SELECT
COUNT(*) OVER() as total_rows,
$str_select
periodo_fase.id,
to_char(periodo_fase.fecha_desde,'dd') as dia_desde,
to_char(periodo_fase.fecha_hasta,'dd') as dia_hasta,
to_char(periodo_fase.fecha_desde,'mm') as mes,
to_char(periodo_fase.fecha_desde,'yyyy') as anio

FROM
periodo_fase
WHERE
periodo_fase.id_fase = $id_fase
$str_where -- AND periodo_fase.id = 1
ORDER BY periodo_fase.fecha_desde
$str_limit
");
        $arr = array();
        $total_rows = 0;
        while ($row = $statement->fetch()) {
            $total_rows = $row['total_rows'];
            $row['descrip_periodo'] = $row['dia_desde'] . ' al ' . $row['dia_hasta'] . ' ' . $this->getMes($row['mes']).'-'.$row['anio'];
            $arr[] = $row;
        }

        return array('arr' => $arr, 'total_rows' => $total_rows);
    }

    public function generarPeriodoFase($gestion, $id_fase, $id_periodo)
    {
        $this->em->getConnection()->beginTransaction();
        try {
            $arr = $this->generarPeriodoFaseFn($gestion, $id_fase, $id_periodo);
            if (!empty($arr['error'])) {
                $this->em->getConnection()->rollBack();
                return $arr;
            }

            $this->em->getConnection()->commit();
        } catch (\Exception $e) {
            $this->em->getConnection()->rollBack();
            throw $e;
        }

        return $arr;
    }

    private function generarPeriodoFaseFn($gestion, $id_fase, $id_periodo)
    {
        $statement = $this->em->getConnection()->query("SELECT
                                                        periodo.frecuencia
                                                        FROM
                                                        periodo
                                                        WHERE
                                                        periodo.id = $id_periodo
                                                        ");
        $row_periodo = $statement->fetch();
        if (empty($row_periodo))
            return array('error' => 'No se encontro la frecuencia del periodo con ID ' . $id_periodo);
        $arr_meses = $this->meses(1);
        $arr = array();

        foreach ($arr_meses as $row) {
            if (intval($row)) {

                $statement = $this->em->getConnection()->query("SELECT
to_char((date_trunc('MONTH',('$gestion-$row-01')::date) + INTERVAL '1 MONTH - 1 DAY'),'dd') AS dia_fin_mes
");
                $row_fecha = $statement->fetch();
                $incremento = round(30 / $row_periodo['frecuencia']);
                $inicio = 1;

                for ($i = 0; $i < intval($row_periodo['frecuencia']); $i++) {
                    $c_periodoFase = new PeriodoFase();
                    $c_periodoFase->setIdPeriodo($this->em->getReference('ProcesosTransaccionalesBundle:Periodo', $id_periodo));
                    $c_periodoFase->setIdFase($this->em->getReference('ProcesosTransaccionalesBundle:Fase', $id_fase));
                    $c_periodoFase->setFechaDesde(new \DateTime("$gestion-$row-$inicio"));

                    if ($i + 1 == intval($row_periodo['frecuencia'])) {
                        $c_periodoFase->setFechaHasta(new \DateTime("$gestion-$row-$row_fecha[dia_fin_mes]"));
                        $arr[] = array("$gestion-$row-$inicio", "$gestion-$row-$row_fecha[dia_fin_mes]");
                    } else {
                        $c_periodoFase->setFechaHasta(new \DateTime("$gestion-$row-" . (($inicio - 1) + $incremento)));
                        $arr[] = array("$gestion-$row-$inicio", "$gestion-$row-" . (($inicio - 1) + $incremento));
                        $inicio += $incremento;
                    }

                    $this->em->persist($c_periodoFase);
                    $this->em->flush();
                }

            }

        }

        return $arr;
    }

    public function cbxFechaAcopio()
    {
        $meses_atras = 6;// cuantos meses atras se mostraran
        $statement = $this->em->getConnection()->query("SELECT
to_char(fecha_desde,'yyyy-mm') AS fecha
FROM
periodo_fase
WHERE
fecha_desde > (now() - INTERVAL '$meses_atras MONTH')
GROUP BY to_char(fecha_desde,'yyyy-mm')
ORDER BY to_char(fecha_desde,'yyyy-mm') ASC
LIMIT " . ($meses_atras + 1));
        $arr = array();
        while ($row = $statement->fetch()) {
            $row['data'] = $row['fecha'] . '-01';
            $row['label'] = $row['fecha'];
            $arr[] = $row;
        }
        return $arr;
    }
    public function uploadExcel($id_entidad,$id_tipo_transaccion,$id_periodo_fase,$id_usuario,$file)
    {
        if(empty($id_entidad))
            return array('error'=>'Error al verificar el ID entidad');

        if(empty($id_tipo_transaccion))
            return array('error'=>'Error al verificar el ID de Tipo Transaccion');

        if(empty($id_periodo_fase))
            return array('error'=>'Error al verificar el ID de Periodo Fase');

        $nameFolder="upload";
        $pathFolder="./".$nameFolder.'/'.$id_entidad;
        if(!is_dir($pathFolder))// si no existe creamos la carpeta
            mkdir($pathFolder,0777,true);


        $row_tipo_transaccion=$this->em->getRepository("ProcesosTransaccionalesBundle:TipoTransaccion")->find($id_tipo_transaccion);

        $fecha=date('Ymd_Gis');

        $arr=explode(".",$file->getClientOriginalName());

        $nameFile=$id_usuario.$row_tipo_transaccion->getAbreviatura().'_'.$fecha.'.'.$arr[count($arr)-1];//nombre del archivo
        $file->move($pathFolder,$nameFile);// copiamos el archivo del buffer

        $i=0;
        while(filesize($pathFolder.'/'.$nameFile) < $file->getClientSize())//controlamos si termino de copiar el archivo
            $i++; // se realiza un instruccion tonta solo por relleno del control

        $c_archivoExcel=new ArchivoExcel();
        $c_archivoExcel->setEliminado(false);
        $c_archivoExcel->setIdPeriodoFase($this->em->getReference('ProcesosTransaccionalesBundle:PeriodoFase',$id_periodo_fase));
        $c_archivoExcel->setIdEntidad($this->em->getReference('ProcesosTransaccionalesBundle:Entidad',$id_entidad));
        $c_archivoExcel->setFechaUpload(new \DateTime());
        $c_archivoExcel->setPathArchivo($pathFolder);
        $c_archivoExcel->setNombreArchivo($nameFile);
        $c_archivoExcel->setIdUsuarioEliminado(0);
        $c_archivoExcel->setIdUsuarioUpload($id_usuario);
        $this->em->persist($c_archivoExcel);
        $this->em->flush();

        return array('nameFile'=>$nameFile,'pathFolder'=>$pathFolder,'id_archivo_excel'=>$c_archivoExcel->getId());
    }
    private function datosExcelAcopio($id_entidad_head,$worksheet, $id_tipo_transaccion,$index,$row_periodo_fase)
    {
        $sw_error=false;
        $arr_entidad=$this->findEntidadXNumeroIdentificacion($worksheet->getCell('A'.$index)->getValue());
        if(!empty($arr_entidad[0]))
        {
            $id_entidad=$arr_entidad[0]['id_entidad'];
            $razon_social=$arr_entidad[0]['razon_social'];

        }
        else
        {
            $id_entidad='';
            $razon_social='';
            $sw_error=true;
        }

        $arr_producto=$this->findProductoXCodigo($id_tipo_transaccion,$worksheet->getCell('D'.$index)->getValue());
        if(empty($arr_producto['error']))
        {
            $descrip_producto=$arr_producto['arr'][0]['label'];
            $id_producto=$arr_producto['arr'][0]['data'];
        }
        else
        {
            $descrip_producto='';
            $id_producto='';
            $sw_error=true;
        }
        if($this->fechaValid($this->numberToDate($worksheet->getCell('C'.$index)->getValue()), $row_periodo_fase['fecha_desde'],$row_periodo_fase['fecha_hasta']))
        {
            $fecha=$this->numberToDate($worksheet->getCell('C'.$index)->getValue());
        }
        else
        {
            $fecha='';
            $sw_error=true;
        }

        return array('error'=>$sw_error,
            'dg_id'=>$index,
            'numero_identificacion'=>$worksheet->getCell('A'.$index)->getValue(),
            'id_entidad'=>$id_entidad,
            'razon_social'=>$razon_social,
            'numero_identificacion_b'=>'',
            'id_entidad_b'=>$id_entidad_head,
            'razon_social_b'=>'',
            'fecha'=>$fecha,
            'id_producto'=>$id_producto,
            'descrip_producto'=>$descrip_producto,
            'cantidad'=>'',
            'precio_unitario'=>'',
            'precio_total'=>'',
            'nro_factura'=>'',
            'cantidad_stock'=>'',
            'cantidad_bruta'=>$worksheet->getCell('E'.$index)->getCalculatedValue(),
            'cantidad_neta'=>$worksheet->getCell('F'.$index)->getCalculatedValue(),
            'cantidad_liquido'=>$worksheet->getCell('G'.$index)->getCalculatedValue(),
            'mes_acopio'=>'');
    }
    private function datosExcelCompra($id_entidad_head, $worksheet, $id_tipo_transaccion,$index,$row_periodo_fase)
    {
        $sum_error=0;
        $sw_error=false;
        $arr_entidad=$this->findEntidadXNumeroIdentificacion($worksheet->getCell('A'.$index)->getValue());
        if(!empty($arr_entidad[0]))
        {
            $id_entidad=$arr_entidad[0]['id_entidad'];
            $razon_social=$arr_entidad[0]['razon_social'];
        }
        else
        {
            $id_entidad='';
            $razon_social='';
            $sw_error=true;
            $sum_error++;
        }

        $arr_producto=$this->findProductoXCodigo($id_tipo_transaccion,$worksheet->getCell('D'.$index)->getValue());
        if(empty($arr_producto['error']))
        {
            $descrip_producto=$arr_producto['arr'][0]['label'];
            $id_producto=$arr_producto['arr'][0]['data'];
        }
        else
        {
            $descrip_producto='';
            $id_producto='';
            $sw_error=true;
            $sum_error++;
        }
        if($this->fechaValid($this->numberToDate($worksheet->getCell('E'.$index)->getValue()), $row_periodo_fase['fecha_desde'],$row_periodo_fase['fecha_hasta']))
        {
            $fecha=$this->numberToDate($worksheet->getCell('E'.$index)->getValue());
        }
        else
        {
            $fecha='';
            $sw_error=true;
            $sum_error++;
        }
        if(trim($worksheet->getCell('C'.$index)->getValue())=='')
        {
            $mes_acopio='';
            $sw_error=true;
            $sum_error++;
        }
        else
        {
            $mes_acopio=$worksheet->getCell('C'.$index)->getValue().'-01';
        }
        $moneda=$worksheet->getCell('I'.$index)->getValue();
        $statement = $this->em->getConnection()->query("SELECT
                                                        tipo_moneda.id,
                                                        tipo_moneda.abreviacion
                                                        FROM
                                                        tipo_moneda
                                                        WHERE
                                                        tipo_moneda.abreviacion LIKE '$moneda'
                                                        ");
        if($statement->rowCount())
            $row_moneda=$statement->fetch();
        else
            $row_moneda=array('id'=>0,'abreviacion'=>'');

        return array('error'=>$sw_error,
            'dg_id'=>$index,
            'numero_identificacion'=>$worksheet->getCell('A'.$index)->getValue(),
            'id_entidad'=>$id_entidad,
            'razon_social'=>$razon_social,
            'numero_identificacion_b'=>'',
            'id_entidad_b'=>$id_entidad_head,
            'razon_social_b'=>'',
            'fecha'=>$fecha,
            'id_producto'=>$id_producto,
            'descrip_producto'=>$descrip_producto,
            'cantidad'=>$worksheet->getCell('F'.$index)->getCalculatedValue(),
            'precio_unitario'=>$worksheet->getCell('G'.$index)->getCalculatedValue(),
            'precio_total'=>$worksheet->getCell('H'.$index)->getCalculatedValue(),
            'nro_factura'=>'',
            'cantidad_stock'=>'',
            'cantidad_bruta'=>'',
            'cantidad_neta'=>'',
            'cantidad_liquido'=>'',
            'id_moneda'=>$row_moneda['id'],
            'descrip_moneda'=>$row_moneda['abreviacion'],
            'mes_acopio'=>$mes_acopio);
    }
    private function datosExcelCompraFase4($worksheet, $id_tipo_transaccion,$index,$row_periodo_fase)
    {
        $sw_error=false;

        $arr_entidad=$this->findEntidadXNumeroIdentificacion($worksheet->getCell('A'.$index)->getValue());
        if(!empty($arr_entidad[0]))
        {
            $id_entidad_b=$arr_entidad[0]['id_entidad'];
            $razon_social_b=$arr_entidad[0]['razon_social'];
        }
        else
        {
            $id_entidad_b='';
            $razon_social_b='';
            $sw_error=true;
        }

        $arr_entidad=$this->findEntidadXNumeroIdentificacion($worksheet->getCell('C'.$index)->getValue());
        if(!empty($arr_entidad[0]))
        {
            $id_entidad=$arr_entidad[0]['id_entidad'];
            $razon_social=$arr_entidad[0]['razon_social'];
        }
        else
        {
            $id_entidad='';
            $razon_social='';
            $sw_error=true;
        }

        $arr_producto=$this->findProductoXCodigo($id_tipo_transaccion,$worksheet->getCell('E'.$index)->getValue());
        if(empty($arr_producto['error']))
        {
            $descrip_producto=$arr_producto['arr'][0]['label'];
            $id_producto=$arr_producto['arr'][0]['data'];
        }
        else
        {
            $descrip_producto='';
            $id_producto='';
            $sw_error=true;
        }
        if($this->fechaValid($this->numberToDate($worksheet->getCell('F'.$index)->getValue()), $row_periodo_fase['fecha_desde'],$row_periodo_fase['fecha_hasta']))
        {
            $fecha=$this->numberToDate($worksheet->getCell('F'.$index)->getValue());
        }
        else
        {
            $fecha='';
            $sw_error=true;
        }
        $moneda=$worksheet->getCell('J'.$index)->getValue();
        $statement = $this->em->getConnection()->query("SELECT
                                                        tipo_moneda.id,
                                                        tipo_moneda.abreviacion
                                                        FROM
                                                        tipo_moneda
                                                        WHERE
                                                        tipo_moneda.abreviacion LIKE '$moneda'
                                                        ");
        if($statement->rowCount())
            $row_moneda=$statement->fetch();
        else
            $row_moneda=array('id'=>0,'abreviacion'=>'');

        return array('error'=>$sw_error,
            'dg_id'=>$index,
            'numero_identificacion'=>$worksheet->getCell('C'.$index)->getValue(),
            'id_entidad'=>$id_entidad,
            'razon_social'=>$razon_social,
            'numero_identificacion_b'=>$worksheet->getCell('A'.$index)->getValue(),
            'id_entidad_b'=>$id_entidad_b,
            'razon_social_b'=>$razon_social_b,
            'fecha'=>$fecha,
            'id_producto'=>$id_producto,
            'descrip_producto'=>$descrip_producto,
            'cantidad'=>$worksheet->getCell('G'.$index)->getCalculatedValue(),
            'precio_unitario'=>$worksheet->getCell('H'.$index)->getCalculatedValue(),
            'precio_total'=>$worksheet->getCell('I'.$index)->getCalculatedValue(),
            'nro_factura'=>'',
            'cantidad_stock'=>'',
            'cantidad_bruta'=>'',
            'cantidad_neta'=>'',
            'cantidad_liquido'=>'',
            'id_moneda'=>$row_moneda['id'],
            'descrip_moneda'=>$row_moneda['abreviacion'],
            'mes_acopio'=>'');
    }
    private function datosExcelVenta($id_entidad_head,$worksheet, $id_tipo_transaccion,$index,$row_periodo_fase)
    {
        $sw_error=false;
        $arr_entidad=$this->findEntidadXNumeroIdentificacion($worksheet->getCell('A'.$index)->getValue());
        if(!empty($arr_entidad[0]))
        {
            $id_entidad=$arr_entidad[0]['id_entidad'];
            $razon_social=$arr_entidad[0]['razon_social'];

        }
        else
        {
            $id_entidad='';
            $razon_social='';
            $sw_error=true;
        }

        $arr_producto=$this->findProductoXCodigo($id_tipo_transaccion,$worksheet->getCell('D'.$index)->getValue());
        if(empty($arr_producto['error']))
        {
            $descrip_producto=$arr_producto['arr'][0]['label'];
            $id_producto=$arr_producto['arr'][0]['data'];
        }
        else
        {
            $descrip_producto='';
            $id_producto='';
            $sw_error=true;
        }

        if($this->fechaValid($this->numberToDate($worksheet->getCell('E'.$index)->getValue()), $row_periodo_fase['fecha_desde'],$row_periodo_fase['fecha_hasta']))
        {
            $fecha=$this->numberToDate($worksheet->getCell('E'.$index)->getValue());
        }
        else
        {
            $fecha='';
            $sw_error=true;
        }
        $moneda=$worksheet->getCell('I'.$index)->getValue();
        $statement = $this->em->getConnection()->query("SELECT
                                                        tipo_moneda.id,
                                                        tipo_moneda.abreviacion
                                                        FROM
                                                        tipo_moneda
                                                        WHERE
                                                        tipo_moneda.abreviacion LIKE '$moneda'
                                                        ");
        if($statement->rowCount())
            $row_moneda=$statement->fetch();
        else
            $row_moneda=array('id'=>0,'abreviacion'=>'');

        return array('error'=>$sw_error,
            'dg_id'=>$index,
            'numero_identificacion'=>$worksheet->getCell('A'.$index)->getValue(),
            'id_entidad'=>$id_entidad,
            'razon_social'=>$razon_social,
            'numero_identificacion_b'=>'',
            'id_entidad_b'=>$id_entidad_head,
            'razon_social_b'=>'',
            'fecha'=>$fecha,
            'id_producto'=>$id_producto,
            'descrip_producto'=>$descrip_producto,
            'cantidad'=>$worksheet->getCell('F'.$index)->getCalculatedValue(),
            'precio_unitario'=>$worksheet->getCell('G'.$index)->getCalculatedValue(),
            'precio_total'=>$worksheet->getCell('H'.$index)->getCalculatedValue(),
            'nro_factura'=>$worksheet->getCell('C'.$index)->getValue(),
            'cantidad_stock'=>'',
            'cantidad_bruta'=>'',
            'cantidad_neta'=>'',
            'cantidad_liquido'=>'',
            'id_moneda'=>$row_moneda['id'],
            'descrip_moneda'=>$row_moneda['abreviacion'],
            'mes_acopio'=>'');
    }
    private function datosExcelProduccion($id_entidad_head,$worksheet, $id_tipo_transaccion,$index,$row_periodo_fase)
    {
        $sum_error=0;
        $sw_error=false;

        $arr_producto=$this->findProductoXCodigo($id_tipo_transaccion,$worksheet->getCell('A'.$index)->getValue());
        if(empty($arr_producto['error']))
        {
            $descrip_producto=$arr_producto['arr'][0]['label'];
            $id_producto=$arr_producto['arr'][0]['data'];
        }
        else
        {
            $descrip_producto='';
            $id_producto='';
            $sw_error=true;
            $sum_error++;
        }
        if(trim($worksheet->getCell('B'.$index)->getValue())=='')
            $sum_error++;
        if(trim($worksheet->getCell('C'.$index)->getValue())=='')
            $sum_error++;

        if($sum_error < 3)
        {
            return array('error'=>$sw_error,
                'dg_id'=>$index,
                'numero_identificacion'=>'',
                'id_entidad'=>'',
                'razon_social'=>'',
                'numero_identificacion_b'=>'',
                'id_entidad_b'=>$id_entidad_head,
                'razon_social_b'=>'',
                'fecha'=>'',
                'id_producto'=>$id_producto,
                'descrip_producto'=>$descrip_producto,
                'cantidad'=>$worksheet->getCell('B'.$index)->getCalculatedValue(),
                'precio_unitario'=>'',
                'precio_total'=>'',
                'nro_factura'=>'',
                'cantidad_stock'=>$worksheet->getCell('C'.$index)->getCalculatedValue(),
                'cantidad_bruta'=>'',
                'cantidad_neta'=>'',
                'cantidad_liquido'=>'',
                'mes_acopio'=>'');
        }
        else
            return false;

    }
    private function datosExcelProduccionFase4($worksheet, $id_tipo_transaccion,$index,$row_periodo_fase)
    {
        $sum_error=0;
        $sw_error=false;
        $arr_entidad=$this->findEntidadXNumeroIdentificacion($worksheet->getCell('A'.$index)->getValue());
        if(!empty($arr_entidad[0]))
        {
            $id_entidad=$arr_entidad[0]['id_entidad'];
            $razon_social=$arr_entidad[0]['razon_social'];

        }
        else
        {
            $id_entidad='';
            $razon_social='';
            $sw_error=true;
        }

        $arr_producto=$this->findProductoXCodigo($id_tipo_transaccion,$worksheet->getCell('C'.$index)->getValue());
        if(empty($arr_producto['error']))
        {
            $descrip_producto=$arr_producto['arr'][0]['label'];
            $id_producto=$arr_producto['arr'][0]['data'];
        }
        else
        {
            $descrip_producto='';
            $id_producto='';
            $sw_error=true;
            $sum_error++;
        }
        if(trim($worksheet->getCell('D'.$index)->getValue())=='')
            $sum_error++;
        if(trim($worksheet->getCell('E'.$index)->getValue())=='')
            $sum_error++;

        if($sum_error < 3)
        {
            return array('error'=>$sw_error,
                'dg_id'=>$index,
                'numero_identificacion'=>'',
                'id_entidad'=>'',
                'razon_social'=>'',
                'numero_identificacion_b'=>$worksheet->getCell('A'.$index)->getValue(),
                'id_entidad_b'=>$id_entidad,
                'razon_social_b'=>$razon_social,
                'fecha'=>'',
                'id_producto'=>$id_producto,
                'descrip_producto'=>$descrip_producto,
                'cantidad'=>$worksheet->getCell('D'.$index)->getValue(),
                'precio_unitario'=>'',
                'precio_total'=>'',
                'nro_factura'=>'',
                'cantidad_stock'=>$worksheet->getCell('E'.$index)->getValue(),
                'cantidad_bruta'=>'',
                'cantidad_neta'=>'',
                'cantidad_liquido'=>'',
                'mes_acopio'=>'');
        }
        else
            return false;

    }
    public function importarDatosExcel($id_entidad,$id_tipo_transaccion, $id_periodo_fase, $phpExcelObject )
    {
        $statement = $this->em->getConnection()->query("SELECT
periodo_fase.id_fase,
periodo_fase.fecha_desde,
periodo_fase.fecha_hasta
FROM
periodo_fase
WHERE
periodo_fase.id='$id_periodo_fase'
");
        $row_periodo_fase = $statement->fetch();
        switch($row_periodo_fase['id_fase'])
        {
            case 3://industrias
                    switch($id_tipo_transaccion)
                    {
                        case 1: //compra
                            $hojaCalculo=1; break;
                        case 2: //venta
                            $hojaCalculo=2; break;
                        case 3: //produccion
                            $hojaCalculo=3; break;
                        case 4: //acopio
                            $hojaCalculo=0; break;
                        default:
                            return array('error'=>'No existe la opcion para el ID '.$id_tipo_transaccion);
                    }
                    break;
            case 4://asociacion
                    switch($id_tipo_transaccion)
                    {
                        case 1: //compra
                            $hojaCalculo=0; break;
                        case 3: //produccion
                            $hojaCalculo=1; break;
                        default:
                            return array('error'=>'No existe la opcion para el ID '.$id_tipo_transaccion);
                    }
                    break;
            default: return array('error'=>'No exite la opcion '.$row_periodo_fase['id_fase']);
        }

        $worksheet=$phpExcelObject->getSheet($hojaCalculo);//aqui le damos de que hoja de calculo va sacar los datos
        $lastrow= $worksheet->getHighestDataRow();
        $sw_error=false;
        $arr=array();
        for($i=9;$i<=$lastrow;$i++)
        {
            switch($id_tipo_transaccion)
            {
                case 1: //compra
                    if($row_periodo_fase['id_fase']==3)
                        $row = $this->datosExcelCompra($id_entidad,$worksheet,$id_tipo_transaccion,$i,$row_periodo_fase);
                    else
                        $row = $this->datosExcelCompraFase4($worksheet,$id_tipo_transaccion,$i,$row_periodo_fase);
                    break;
                case 2: //venta
                    $row = $this->datosExcelVenta($id_entidad,$worksheet,$id_tipo_transaccion,$i,$row_periodo_fase);

                    break;
                case 3: //produccion
                    if($row_periodo_fase['id_fase']==3)
                        $row = $this->datosExcelProduccion($id_entidad,$worksheet,$id_tipo_transaccion,$i,$row_periodo_fase);
                    else
                        $row = $this->datosExcelProduccionFase4($worksheet,$id_tipo_transaccion,$i,$row_periodo_fase);
                    break;
                case 4: //acopio

                    $row=$this->datosExcelAcopio($id_entidad,$worksheet,$id_tipo_transaccion,$i,$row_periodo_fase);
                    break;
                default:
                    return array('error'=>'No existe la opcion para el ID '.$id_tipo_transaccion);
            }
            if($row!==false)
            {
                $arr[]=$row;
                if(!empty($row['error']))
                    $sw_error = true;
            }

        }

        return array('arr'=>$arr,'sw_error'=>$sw_error);
    }
    private function fechaValid($fecha, $desde, $hasta)
    {
        $statement = $this->em->getConnection()->query("SELECT '".$this->formatDate($fecha)."' BETWEEN '".$this->formatDate($desde)."' AND '".$this->formatDate($hasta)."' AS verificado");
        $row=$statement->fetch();
        return $row['verificado'];
    }
    public function numberToDate($number)
    {
        $unix_date=($number-25569)*86400;
        return gmdate("Y-m-d",$unix_date);
    }
    public function formatDate($date,$sw=1)
    {

        if($sw==1)
            $statement=$this->em->getConnection()->query("SELECT to_char('$date'::date,'YYYY-mm-dd') AS fecha");
        else
            $statement=$this->em->getConnection()->query("SELECT to_char('$date'::date,'dd-mm-YYYY') AS fecha" );

        $row_periodo = $statement->fetch();
        return $row_periodo['fecha'];
    }
    public function getFileLoadData($id_entidad, $id_periodo_fase,$id_archivo_excel=0)
    {
        if(!empty($id_archivo_excel))
        {
            $str_where="archivo_excel.id=$id_archivo_excel";
        }
        else
        {
            $str_where="archivo_excel.eliminado = false
            AND archivo_excel.id_periodo_fase=$id_periodo_fase
            AND archivo_excel.id_entidad=$id_entidad";
        }
        $statement=$this->em->getConnection()->query("SELECT
true As hidden,
archivo_excel.id,
archivo_excel.path_archivo,
archivo_excel.nombre_archivo
FROM
archivo_excel
WHERE
$str_where
" );
        if($statement->rowCount()==0)
            return array('hidden'=>false,'id'=>0,'path_archivo'=>'','nombre_archivo'=>'');
        else
            return $statement->fetch();
    }
    public function deleteUpload($id_archivo_excel)
    {
        $arr_upload=$this->getFileLoadData(null ,null, $id_archivo_excel);
        $arr_upload['path_archivo'];
        $arr_upload['nombre_archivo'];
        if(unlink($arr_upload['path_archivo'].'/'.$arr_upload['nombre_archivo']))
        {
            $this->em->getConnection()->query("DELETE FROM archivo_excel
                                               WHERE
                                               archivo_excel.id='$id_archivo_excel'
                                               " );
        }


    }
    public function cambiarValidado($id_transaccion)
    {
        $statement = $this->em->getConnection()->prepare("UPDATE transaccion SET validado= not validado
                                                                        WHERE
                                                                        id=$id_transaccion
                                                                        ");
        $statement->execute();
        $statement=$this->em->getConnection()->query("SELECT
transaccion.validado
FROM
transaccion
WHERE
transaccion.id=$id_transaccion
" );
        $row= $statement->fetch();
        return array('id'=>$id_transaccion,'validado'=>$row['validado']);
    }
    private function getCotizacionMoneda($desde,$hasta)
    {
        $statement=$this->em->getConnection()->query("SELECT
tipo_cambio.id_tipo_moneda,
MAX(tipo_cambio.valorbs) AS valorbs
FROM
tipo_cambio
WHERE
 (
 '$desde' BETWEEN tipo_cambio.fecha_inicio AND tipo_cambio.fecha_final
 OR '$hasta' BETWEEN tipo_cambio.fecha_inicio AND tipo_cambio.fecha_final
 )
GROUP BY tipo_cambio.id_tipo_moneda
");
        $arr = array();
        while($row = $statement->fetch())
        {
            $arr[$row['id_tipo_moneda']]=$row['valorbs'];
        }
        return $arr;
    }
    public function reporteControlBanda($id_moneda,$id_entidad,$id_producto,$desde,$hasta)
    {
        $statement=$this->em->getConnection()->query("SELECT
                                                          to_char((date_trunc('MONTH',('$hasta-01')::date) + INTERVAL '1 MONTH - 1 DAY'),'dd') AS dia_fin_mes
                                                          ");
        $row_fecha=$statement->fetch();
        //$hasta solo se esta enviando el anio y mes hay que completar con el ultimo dia del mes
        $hasta=$hasta.'-'.$row_fecha['dia_fin_mes'];

        $arr_tipo_cambio=$this->getCotizacionMoneda($desde,$hasta);

        $arr_min=array();
        $arr_max=array();
        $arr_categorias=array();
        $arr_producto=array();
        $i=0;//esto es para que no se desborde
        do
        {
            $min= explode('-',$desde);
            $statement=$this->em->getConnection()->query("SELECT
                                                            banda_precios.valor_minimo_banda_precio,
                                                            banda_precios.valor_maximo_banda_precio,
                                                            banda_precios.tipo_moneda_producto,
                                                            banda_precios.tipo_cambio_producto
                                                            FROM
                                                            banda_precios
                                                            WHERE
                                                            banda_precios.id_producto=$id_producto
                                                            AND '$desde' BETWEEN fecha_desde_banda_precio AND fecha_hasta_banda_precio
                                                            ");
            if($statement->rowCount()==0)
                return array('error'=>'Verifique las bandas de precio, no se encontro bandas de precio en el rango establecido');
            $row=$statement->fetch();
            //falta sacar la cotizacion del enviado
            $row['valor_minimo_banda_precio'] = $this->transformar_moneda($row['valor_minimo_banda_precio'],$row['tipo_moneda_producto'],$id_moneda,$row['tipo_cambio_producto'],$arr_tipo_cambio[$id_moneda]);
            $row['valor_maximo_banda_precio'] = $this->transformar_moneda($row['valor_maximo_banda_precio'],$row['tipo_moneda_producto'],$id_moneda,$row['tipo_cambio_producto'],$arr_tipo_cambio[$id_moneda]);
            $arr_min[]=array($this->getMes($min[1]).'-'.$min[0], floatval($row['valor_minimo_banda_precio']));
            $arr_max[]=array($this->getMes($min[1]).'-'.$min[0], floatval($row['valor_maximo_banda_precio']));
            $arr_categorias[]=array($this->getMes($min[1]).'-'.$min[0]);

            $statement=$this->em->getConnection()->query("SELECT
                                                          to_char((date_trunc('MONTH',('$min[0]-$min[1]-01')::date) + INTERVAL '1 MONTH - 1 DAY'),'dd') AS dia_fin_mes
                                                          ");
            $row_fecha=$statement->fetch();
            if(!empty($id_entidad))
                $str_where="AND relacion_entidad.id_entidad=$id_entidad";
            else
                $str_where="";
            $statement=$this->em->getConnection()->query("SELECT
SUM(compra_detalle.precio_unitario) / COUNT(compra_detalle.precio_unitario) AS precio_unitario,
compra_detalle.id_moneda
FROM
compra_detalle
INNER JOIN transaccion ON transaccion.id = compra_detalle.id_transaccion
INNER JOIN relacion_entidad ON relacion_entidad.id = transaccion.id_relacion_entidad
WHERE
compra_detalle.id_producto = $id_producto
AND compra_detalle.fecha_compra BETWEEN '$min[0]-$min[1]-01' AND '$min[0]-$min[1]-$row_fecha[dia_fin_mes]'
$str_where
GROUP BY compra_detalle.id_moneda
");
            $precio_unitario=0;
            $cantidad_iteraciones=0;//este nos muestra cuantas monedas existen
            //$arr_borrame=array();
            while($row_producto=$statement->fetch())
            {
                $precio_unitario+= $this->transformar_moneda($row_producto['precio_unitario'],
                                                             $row_producto['id_moneda'],
                                                             $id_moneda,
                                                             $arr_tipo_cambio[$row_producto['id_moneda']],
                                                             $arr_tipo_cambio[$id_moneda]);
                //$precio_unitario+=$valor;
                $cantidad_iteraciones++;
                //$arr_borrame[]=array('precio'=>$row_producto['precio_unitario'],'id_moneda'=>$row_producto['id_moneda'],'transa'=>$valor);
            }
            //return array('error'=>print_r($arr_borrame,true));
            if(empty($cantidad_iteraciones))
                $cantidad_iteraciones=1;
            $arr_producto[]=array($this->getMes($min[1]).'-'.$min[0], floatval( round( $precio_unitario/$cantidad_iteraciones,2)));

            $i++;
            $statement=$this->em->getConnection()->query("SELECT to_char('$desde'::date + INTERVAL '1 month','yyyy-mm-dd') AS desde");
            $row_fecha=$statement->fetch();
            $desde=$row_fecha['desde'];
        }while((new \DateTime($row_fecha['desde'])<new \DateTime($hasta)) && $i<100);

        return array('arr_min'=>$arr_min,
                     'arr_max'=>$arr_max,
                     'arr_categorias'=>$arr_categorias,
                     'arr_producto'=>$arr_producto);

    }
    public function cbxMoneda()
    {
        $statement=$this->em->getConnection()->query("SELECT
id AS data,
nombre AS nombre,
abreviacion AS label
FROM
tipo_moneda
ORDER BY id
");
        return $statement->fetchAll();
    }
    public function transformar_moneda($monto, $moneda_old, $moneda_new, $tc_old, $tc_new)
    {
        if($moneda_old==$moneda_new)
           return round($monto,2);
        else
           return round((($monto/$tc_new)*$tc_old),2);

    }

    /**
     * @param $id_entidad
     * @param $id_usuario
     * @param $file
     * @return array
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function uploadExcelEntidad($id_entidad,$id_usuario,$file)
    {
        $nameFolder="upload";
        $pathFolder="./".$nameFolder.'/'.$id_entidad;
        if(!is_dir($pathFolder))// si no existe creamos la carpeta
            mkdir($pathFolder,0777,true);


        $fecha=date('Ymd_Gis');

        $arr=explode(".",$file->getClientOriginalName());

        $nameFile=$id_usuario.'_'.$fecha.'.'.$arr[count($arr)-1];//nombre del archivo
        $file->move($pathFolder,$nameFile);// copiamos el archivo del buffer

        $i=0;
        while(filesize($pathFolder.'/'.$nameFile) < $file->getClientSize())//controlamos si termino de copiar el archivo
            $i++; // se realiza un instruccion tonta solo por relleno del control

        return array('nameFile'=>$nameFile,'pathFolder'=>$pathFolder);
    }
    public function importarDatosExcelClienteProveedor($phpExcelObject, $c_entidadManager, $c_tipoEntidadManager, $c_tipoDocumentomanager, $c_personaManager, $c_relacionEntidadmanager, $c_regimenManager, $user,$idFase, $rango_inicio, $rango_fin)
    {
        $this->em->getConnection()->beginTransaction();

        $worksheet=$phpExcelObject->getSheet(0);//aqui le damos de que hoja de calculo va sacar los datos
        $lastrow= $worksheet->getHighestDataRow();
        $sw_error=false;
        $c_entidadController = new EntidadController($c_entidadManager, $c_tipoEntidadManager, $c_tipoDocumentomanager, $c_personaManager, $c_relacionEntidadmanager, $c_regimenManager);
        $idTipoEntidad = 5;
        //$idFase = 2 personas que llevan su soya para acopio, $idFase = 3 Industrias, $idFase = 4 clientes
        //$idFase = 4;
        if($rango_fin>0)
        {
            if($rango_fin < $lastrow)
                $lastrow = $rango_fin+4;
        }
        $arr=array();
        for($i=4+$rango_inicio; $i <= $lastrow  ;$i++)
        {
            $arr_entidad=$this->findEntidadXNumeroIdentificacion($worksheet->getCell('G'.$i)->getValue());
            if(empty($arr_entidad[0]))
            {
                try {
                    $c_obj = new ModeloEntidad();
                    $c_obj->id = 0;
                    $c_obj->razonSocial = $worksheet->getCell('A' . $i)->getValue() . ' ' . $worksheet->getCell('B' . $i)->getValue() . ' ' . $worksheet->getCell('C' . $i)->getValue();
                    $c_obj->numeroIdentificacion = $worksheet->getCell('G' . $i)->getValue();
                    $c_obj->idComplejo = 1;
                    $c_obj->idFase = $idFase;
                    $c_obj->idTipoEntidad = $idTipoEntidad;
                    $c_obj->idTipoDocumento = $worksheet->getCell('H' . $i)->getValue();
                    $c_obj->direccion = $worksheet->getCell('E' . $i)->getValue();
                    $c_obj->telefonos = $worksheet->getCell('D' . $i)->getValue();
                    $c_obj->registroSanitario = '';
                    $c_obj->fundempresa = '';
                    $c_obj->correo = $worksheet->getCell('F' . $i)->getValue();
                    $c_obj->beneficiario = false;//true = si es beneficiario, false = no es beneficiario
                    $c_obj->idRegimen = 1;//
                    $c_obj->persona = array('id' => 0,
                        'nombres' => $worksheet->getCell('A' . $i)->getValue(),
                        'primerApellido' => $worksheet->getCell('B' . $i)->getValue(),
                        'segundoApellido' => $worksheet->getCell('C' . $i)->getValue(),
                        'telefonos' => $worksheet->getCell('D' . $i)->getValue(),
                        'direccion' => $worksheet->getCell('E' . $i)->getValue(),
                        'numeroIdentificacion' => $worksheet->getCell('G' . $i)->getValue(),
                        'idTipoDocumento' => $worksheet->getCell('H' . $i)->getValue(),
                        'correo' => $worksheet->getCell('F' . $i)->getValue()
                    );

                    $arr_result = $c_entidadController->guardarEmpresaFn($c_obj, $user);
                }
                catch(Exception $e)
                {
                    $this->em->getConnection()->rollBack();
                    throw $e;
                }
            }
        }
        $this->em->getConnection()->commit();
        return array('arr'=>$arr,'sw_error'=>$sw_error);
    }
    public function importarDatosExcelIndustriasAsociaciones($phpExcelObject, $c_entidadManager, $c_tipoEntidadManager, $c_tipoDocumentomanager, $c_personaManager, $c_relacionEntidadmanager, $c_regimenManager, $user,$idFase,$idTipoEntidad, $rango_inicio, $rango_fin)
    {
        $this->em->getConnection()->beginTransaction();

        $worksheet=$phpExcelObject->getSheet(0);//aqui le damos de que hoja de calculo va sacar los datos
        $lastrow= $worksheet->getHighestDataRow();
        $sw_error=false;
        $c_entidadController = new EntidadController($c_entidadManager, $c_tipoEntidadManager, $c_tipoDocumentomanager, $c_personaManager, $c_relacionEntidadmanager, $c_regimenManager);
        if(empty($idTipoEntidad))
            $idTipoEntidad = ($idFase==3?2:4);

        if($rango_fin>0)
        {
            if($rango_fin < $lastrow)
                $lastrow = $rango_fin+10;
        }
        $arr=array();
        for($i=10+$rango_inicio; $i <= $lastrow ;$i++)
        {
            $arr_entidad=$this->findEntidadXNumeroIdentificacion($worksheet->getCell('J'.$i)->getValue());

            if(empty($arr_entidad[0]))
            {
                try {
                    $c_obj = new ModeloEntidad();
                    $c_obj->id = 0;
                    $c_obj->razonSocial = $worksheet->getCell('I' . $i)->getValue();
                    $c_obj->numeroIdentificacion = $worksheet->getCell('J' . $i)->getValue();
                    $c_obj->idComplejo = 1;
                    $c_obj->idFase = $idFase;//Q
                    $c_obj->idTipoEntidad = $idTipoEntidad;//R
                    $c_obj->idTipoDocumento = $worksheet->getCell('K' . $i)->getValue();
                    $c_obj->direccion = $worksheet->getCell('L' . $i)->getValue();
                    $c_obj->telefonos = $worksheet->getCell('M' . $i)->getValue();
                    $c_obj->registroSanitario = $worksheet->getCell('N' . $i)->getValue();
                    $c_obj->fundempresa = $worksheet->getCell('O' . $i)->getValue();
                    $c_obj->correo = $worksheet->getCell('P' . $i)->getValue();
                    $c_obj->beneficiario = false;//true = si es beneficiario, false = no es beneficiario
                    $c_obj->idRegimen = 1;//
                    $c_obj->persona = array('id' => 0,
                        'nombres' => $worksheet->getCell('A' . $i)->getValue(),
                        'primerApellido' => $worksheet->getCell('B' . $i)->getValue(),
                        'segundoApellido' => $worksheet->getCell('C' . $i)->getValue(),
                        'telefonos' => $worksheet->getCell('D' . $i)->getValue(),
                        'direccion' => $worksheet->getCell('E' . $i)->getValue(),
                        'numeroIdentificacion' => $worksheet->getCell('G' . $i)->getValue(),
                        'idTipoDocumento' => $worksheet->getCell('H' . $i)->getValue(),
                        'correo' => $worksheet->getCell('F' . $i)->getValue()
                    );

                    $arr_result = $c_entidadController->guardarEmpresaFn($c_obj, $user);
                }
                catch(Exception $e)
                {
                    $this->em->getConnection()->rollBack();
                    throw $e;
                }
            }
        }
        $this->em->getConnection()->commit();
        return array('arr'=>$arr,'sw_error'=>$sw_error);
    }
    public function importarDatosExcelTransactionVenta($id_tipo_transaccion, $phpExcelObject)
    {
        $this->em->getConnection()->beginTransaction();

        $worksheet=$phpExcelObject->getSheet(0);//aqui le damos de que hoja de calculo va sacar los datos
        $lastrow= $worksheet->getHighestDataRow();
        $sw_error=false;
        for($i = 2; $i <= $lastrow; $i++)
        {
            $fecha = $this->numberToDate($worksheet->getCell('E'.$i)->getValue());
            $arrRelacionEntidad = $this->findIdRelacionEntidad($worksheet->getCell('J'.$i)->getValue());
            if(!empty($arrRelacionEntidad['error']))
                return $arrRelacionEntidad;
            $result = $this->findTransaccionId($id_tipo_transaccion, $fecha, $arrRelacionEntidad['id']);
            if(!empty($result['error']))
            {
                $arrPeriodoFase = $this->findIdPeriodoFase($arrRelacionEntidad['id_entidad'], $fecha);
                if(!empty($arrPeriodoFase['error']))
                    return $arrPeriodoFase;

                $head = new \stdClass();
                $head->id_transaccion = 0;
                $head->id_relacion_entidad = $arrRelacionEntidad['id'];
                $head->id_periodo_fase = $arrPeriodoFase['id'];
                $head->id_tipo_cambio = 1;
                $head->id_tipo_transaccion = $id_tipo_transaccion;

                $arr_result = $this->saveHead($head);
                $id_head = $arr_result['id'];
            }
            else
                $id_head = $result['id'];

            $arrFindMoneda = $this->findIdMoneda($worksheet->getCell('I'.$i)->getValue());
            if(!empty($arrFindMoneda['error']))
                return $arrFindMoneda;

            $arr_entidad = $this->findEntidadXNumeroIdentificacion($worksheet->getCell('A'.$i)->getValue());
            if(!empty($arr_entidad['error']))
                return $arr_entidad;

            $arrProducto = $this->findProductoXCodigo($id_tipo_transaccion, $worksheet->getCell('D'.$i)->getValue());
            if(!empty($arrProducto['error']))
                return $arrProducto;

            $item = new \stdClass();
            $item->numero_identificacion = $arr_entidad[0]['numero_identificacion'];
            $item->id_entidad = $arr_entidad[0]['id_entidad'];
            //$item->razon_social = 0;
            $item->id_entidad_b = $arrRelacionEntidad['id_entidad'];
            //$item->razon_social_b = 0;
            //$item->numero_identificacion_b = '';
            $item->fecha = $fecha;
            $item->nro_factura = $worksheet->getCell('C'.$i)->getValue();
            $item->id_producto = $arrProducto['arr'][0]['data'];
            //$item->descrip_producto = '';

            $item->cantidad = $worksheet->getCell('F'.$i)->getCalculatedValue();
            $item->precio_unitario = $worksheet->getCell('G'.$i)->getCalculatedValue();
            $item->precio_total = $worksheet->getCell('H'.$i)->getCalculatedValue();

            //$item->cantidad_stock = 0;
            //$item->cantidad_bruta = 0;
            //$item->cantidad_neta = 0;
            //$item->cantidad_liquido = 0;
            //$item->mes_acopio = 0;
            $item->id_moneda = $arrFindMoneda['id'];
            //$item->descrip_moneda = '';

            $body = array($item);
            $resultBody = $this->saveBody($id_tipo_transaccion, $id_head, $body);
            if(!empty($resultBody['error']))
                return $resultBody;

            $this->em->flush();
        }

        $this->em->getConnection()->commit();
        return array('sw_error'=>$sw_error);
    }
    public function importarDatosExcelTransactionCompra($id_tipo_transaccion, $phpExcelObject)
    {
        $this->em->getConnection()->beginTransaction();

        $worksheet=$phpExcelObject->getSheet(0);//aqui le damos de que hoja de calculo va sacar los datos
        //$lastrow= $worksheet->getHighestRow();
        $lastrow = $worksheet->getHighestDataRow();
        $sw_error=false;
        for($i = 2; $i <= $lastrow; $i++)
        {
            $fecha = $this->numberToDate($worksheet->getCell('E'.$i)->getValue());
            $fecha_recepcion = $this->numberToDate($worksheet->getCell('C'.$i)->getValue());
            $arrRelacionEntidad = $this->findIdRelacionEntidad($worksheet->getCell('J'.$i)->getValue(), $i);
            if(!empty($arrRelacionEntidad['error']))
                return $arrRelacionEntidad;
            $result = $this->findTransaccionId($id_tipo_transaccion, $fecha, $arrRelacionEntidad['id']);
            if(!empty($result['error']))
            {


                $arrPeriodoFase = $this->findIdPeriodoFase($arrRelacionEntidad['id_entidad'], $fecha);
                if(!empty($arrPeriodoFase['error']))
                    return $arrPeriodoFase;

                $head = new \stdClass();
                $head->id_transaccion = 0;
                $head->id_relacion_entidad = $arrRelacionEntidad['id'];
                $head->id_periodo_fase = $arrPeriodoFase['id'];
                $head->id_tipo_cambio = 1;
                $head->id_tipo_transaccion = $id_tipo_transaccion;

                $arr_result = $this->saveHead($head);
                $id_head = $arr_result['id'];
            }
            else
                $id_head = $result['id'];

            $arrFindMoneda = $this->findIdMoneda($worksheet->getCell('I'.$i)->getValue());
            if(!empty($arrFindMoneda['error']))
                return $arrFindMoneda;

            $arr_entidad = $this->findEntidadXNumeroIdentificacion($worksheet->getCell('A'.$i)->getValue());
            if(!empty($arr_entidad['error']))
                return $arr_entidad;

            $arrProducto = $this->findProductoXCodigo($id_tipo_transaccion, $worksheet->getCell('D'.$i)->getValue());
            if(!empty($arrProducto['error']))
                return $arrProducto;

            $item = new \stdClass();
            $item->numero_identificacion = $arr_entidad[0]['numero_identificacion'];
            $item->id_entidad = $arr_entidad[0]['id_entidad'];
            //$item->razon_social = 0;
            $item->id_entidad_b = $arrRelacionEntidad['id_entidad'];
            //$item->razon_social_b = 0;
            //$item->numero_identificacion_b = '';
            $item->fecha = $fecha;
            $item->nro_factura = '';
            $item->id_producto = $arrProducto['arr'][0]['data'];
            //$item->descrip_producto = '';
            $item->cantidad = $worksheet->getCell('F'.$i)->getCalculatedValue();
            $item->precio_unitario = $worksheet->getCell('G'.$i)->getCalculatedValue();
            $item->precio_total = $worksheet->getCell('H'.$i)->getCalculatedValue();
            //$item->cantidad_stock = 0;
            //$item->cantidad_bruta = 0;
            //$item->cantidad_neta = 0;
            //$item->cantidad_liquido = 0;
            $item->mes_acopio = $fecha_recepcion;
            $item->id_moneda = $arrFindMoneda['id'];
            //$item->descrip_moneda = '';

            $body = array($item);
            $resultBody = $this->saveBody($id_tipo_transaccion, $id_head, $body);
            if(!empty($resultBody['error']))
                return $resultBody;

            $this->em->flush();
        }

        $this->em->getConnection()->commit();
        return array('sw_error'=>$sw_error);
    }
    public function importarDatosExcelTransactionAcopio($id_tipo_transaccion, $phpExcelObject)
    {
        $this->em->getConnection()->beginTransaction();

        $worksheet=$phpExcelObject->getSheet(0);//aqui le damos de que hoja de calculo va sacar los datos
        //$lastrow= $worksheet->getHighestRow();
        $lastrow = $worksheet->getHighestDataRow();
        $sw_error=false;
        for($i = 2; $i <= $lastrow; $i++)
        {
            $fecha = $this->numberToDate($worksheet->getCell('C'.$i)->getValue());
            $arrRelacionEntidad = $this->findIdRelacionEntidad($worksheet->getCell('H'.$i)->getValue(), $i);
            if(!empty($arrRelacionEntidad['error']))
                return $arrRelacionEntidad;
            $result = $this->findTransaccionId($id_tipo_transaccion, $fecha, $arrRelacionEntidad['id']);
            if(!empty($result['error']))
            {
                $arrPeriodoFase = $this->findIdPeriodoFase($arrRelacionEntidad['id_entidad'], $fecha);
                if(!empty($arrPeriodoFase['error']))
                    return $arrPeriodoFase;

                $head = new \stdClass();
                $head->id_transaccion = 0;
                $head->id_relacion_entidad = $arrRelacionEntidad['id'];
                $head->id_periodo_fase = $arrPeriodoFase['id'];
                $head->id_tipo_cambio = 1;
                $head->id_tipo_transaccion = $id_tipo_transaccion;

                $arr_result = $this->saveHead($head);
                $id_head = $arr_result['id'];
            }
            else
                $id_head = $result['id'];


            $arr_entidad = $this->findEntidadXNumeroIdentificacion($worksheet->getCell('A'.$i)->getValue());
            if(!empty($arr_entidad['error']))
                return $arr_entidad;

            $arrProducto = $this->findProductoXCodigo($id_tipo_transaccion, $worksheet->getCell('D'.$i)->getValue());
            if(!empty($arrProducto['error']))
                return $arrProducto;

            $item = new \stdClass();
            $item->numero_identificacion = $arr_entidad[0]['numero_identificacion'];
            $item->id_entidad = $arr_entidad[0]['id_entidad'];
            //$item->razon_social = 0;
            $item->id_entidad_b = $arrRelacionEntidad['id_entidad'];
            //$item->razon_social_b = 0;
            //$item->numero_identificacion_b = '';
            $item->fecha = $fecha;
            $item->nro_factura = '';
            $item->id_producto = $arrProducto['arr'][0]['data'];
            //$item->descrip_producto = '';
            //$item->cantidad = $worksheet->getCell('F'.$i)->getValue();
            //$item->precio_unitario = '';
            //$item->precio_total = '';
            //$item->cantidad_stock = 0;
            $item->cantidad_bruta = $worksheet->getCell('E'.$i)->getCalculatedValue();
            $item->cantidad_neta = $worksheet->getCell('G'.$i)->getCalculatedValue();
            $item->cantidad_liquido = $worksheet->getCell('F'.$i)->getCalculatedValue();
            $item->mes_acopio = '';
            $item->id_moneda = '';
            //$item->descrip_moneda = '';

            $body = array($item);
            $resultBody = $this->saveBody($id_tipo_transaccion, $id_head, $body);
            if(!empty($resultBody['error']))
                return $resultBody;

            $this->em->flush();
        }

        $this->em->getConnection()->commit();
        return array('sw_error'=>$sw_error);
    }
    public function importarDatosExcelTransactionProduccion($id_tipo_transaccion, $phpExcelObject)
    {
        $this->em->getConnection()->beginTransaction();

        $worksheet=$phpExcelObject->getSheet(0);//aqui le damos de que hoja de calculo va sacar los datos
        //$lastrow= $worksheet->getHighestRow();
        $lastrow = $worksheet->getHighestDataRow();
        $sw_error=false;
        for($i = 2; $i <= $lastrow; $i++)
        {
            $fecha = $this->numberToDate($worksheet->getCell('E'.$i)->getValue());
            $arrRelacionEntidad = $this->findIdRelacionEntidad($worksheet->getCell('D'.$i)->getValue(), $i);
            if(!empty($arrRelacionEntidad['error']))
                return $arrRelacionEntidad;
            $result = $this->findTransaccionId($id_tipo_transaccion, $fecha, $arrRelacionEntidad['id']);
            if(!empty($result['error']))
            {


                $arrPeriodoFase = $this->findIdPeriodoFase($arrRelacionEntidad['id_entidad'], $fecha);
                if(!empty($arrPeriodoFase['error']))
                    return $arrPeriodoFase;

                $head = new \stdClass();
                $head->id_transaccion = 0;
                $head->id_relacion_entidad = $arrRelacionEntidad['id'];
                $head->id_periodo_fase = $arrPeriodoFase['id'];
                $head->id_tipo_cambio = 1;
                $head->id_tipo_transaccion = $id_tipo_transaccion;

                $arr_result = $this->saveHead($head);
                $id_head = $arr_result['id'];
            }
            else
                $id_head = $result['id'];

/*
            $arr_entidad = $this->findEntidadXNumeroIdentificacion($worksheet->getCell('A'.$i)->getValue());
            if(!empty($arr_entidad['error']))
                return $arr_entidad;
*/

            $arrProducto = $this->findProductoXCodigo($id_tipo_transaccion, $worksheet->getCell('A'.$i)->getValue());
            if(!empty($arrProducto['error']))
                return $arrProducto;
            $item = new \stdClass();
            //$item->razon_social = 0;
            $item->id_entidad_b = $arrRelacionEntidad['id_entidad'];
            //$item->razon_social_b = 0;
            //$item->numero_identificacion_b = '';
            $item->fecha = $fecha;
            //$item->nro_factura = '';
            $item->id_producto = $arrProducto['arr'][0]['data'];
            //$item->descrip_producto = '';
            $item->cantidad = $worksheet->getCell('B'.$i)->getCalculatedValue();
            //$item->precio_unitario = '';
            //$item->precio_total = '';
            $item->cantidad_stock = $worksheet->getCell('C'.$i)->getCalculatedValue();
            //$item->cantidad_bruta = 0;
            //$item->cantidad_neta = 0;
            //$item->cantidad_liquido = 0;
            //$item->mes_acopio = '';
            //$item->id_moneda = '';
            //$item->descrip_moneda = '';

            $body = array($item);
            $resultBody = $this->saveBody($id_tipo_transaccion, $id_head, $body);
            if(!empty($resultBody['error']))
                return $resultBody;

            $this->em->flush();
        }

        $this->em->getConnection()->commit();
        return array('sw_error'=>$sw_error);
    }
    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function exportarXlsSaldosExportables($objPhp, $path, $inicio_mes, $inicio_gest, $fin_mes, $fin_gest,
                                                 $id_producto, $id_entidad, $datosExportacion, $datos_servicio,
                                                 $reportesManager){

        //$sheet->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
        //$sheet->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
        $response = null;
        try{
            $phpExcelObject = $objPhp->createPHPExcelObject();
            $phpExcelObject->getDefaultStyle()->getFont()->setName('Arial')
                ->setSize(10);
            $sheet = $phpExcelObject->setActiveSheetIndex(0);

            $sheet->getStyle("A6")->getFont()->setBold(true);
            $sheet->getStyle("A6")->getBorders()->getLeft()->getColor()->setARGB('FF993300');
            $sheet->getStyle("A6")->getBorders()->getTop()->getColor()->setARGB('FF993300');
            $sheet->getStyle("A6")->getBorders()->getBottom()->getColor()->setARGB('FF993300');
            $sheet->getStyle("A6")->getBorders()->getRight()->getColor()->setARGB('FF993300');

            $sheet->getStyle("A6")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'D88308')
                    )
                )
            );
            $sheet->getColumnDimension('A')
                ->setWidth(20);
            $sheet->setCellValue('A6', 'Producto')->mergecells('A6:A8');
            $sheet->getColumnDimension('B')
                ->setWidth(25);
            $sheet->getStyle("B6")->getFont()->setBold(true);
            $sheet->getStyle("B6")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'D88308')
                    )
                )
            );
            $sheet->setCellValue('B6', 'Empresas')->mergeCells('B6:B8');
            $sheet->getColumnDimension('C')
                ->setWidth(15);
            $sheet->getStyle("B6")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'D88308')
                    )
                )
            );
            $sheet->setCellValue('C6', 'Existencias de grano al')->mergeCells('C6:C7');
            $sheet->getStyle("C6")->getFont()->setBold(true);
            $sheet->getStyle('C6')->getAlignment()->setWrapText(true);
            $sheet->getStyle("C6")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'D88308')
                    )
                )
            );
            $sheet->getStyle("C8")->getFont()->setBold(true);
            $sheet->getStyle("C8")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'D88308')
                    )
                )
            );
            $sheet->setCellValue('C8', 't soya');
            $sheet->getColumnDimension('D')
                ->setWidth(12);
            $sheet->getColumnDimension('E')
                ->setWidth(12);
            $sheet->getStyle("D6")->getFont()->setBold(true);
            $sheet->getStyle("D6")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'D88308')
                    )
                )
            );
            $sheet->setCellValue('D6', 'Compromiso y compra de grano')->mergeCells('D6:E6');
            $sheet->getStyle("D7")->getFont()->setBold(true);
            $sheet->getStyle("D7")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'D88308')
                    )
                )
            );
            $sheet->setCellValue('D7', 'Compromiso Ver');
            $sheet->getStyle("D8")->getFont()->setBold(true);
            $sheet->getStyle("D8")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'D88308')
                    )
                )
            );
            $sheet->setCellValue('D8', 't soya');
            $sheet->getStyle("E7")->getFont()->setBold(true);
            $sheet->getStyle("E7")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'D88308')
                    )
                )
            );
            $sheet->getStyle("E8")->getFont()->setBold(true);
            $sheet->getStyle("E8")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'D88308')
                    )
                )
            );
            $sheet->setCellValue('E7', 'Compromiso Inv');
            $sheet->setCellValue('E8', 't soya');
            $sheet->getColumnDimension('F')
                ->setWidth(15);
            $sheet->getStyle("F6")->getFont()->setBold(true);
            $sheet->getStyle("F6")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'D88308')
                    )
                )
            );
            $sheet->getStyle("F8")->getFont()->setBold(true);
            $sheet->getStyle("F8")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'D88308')
                    )
                )
            );
            $sheet->getStyle('F6')->getAlignment()->setWrapText(true);
            $sheet->setCellValue('F6', 'Total Grano Disponible')->mergeCells('F6:F7');
            $sheet->setCellValue('F8', 't soya');
            $sheet->getColumnDimension('G')
                ->setWidth(15);
            $sheet->getStyle("G6")->getFont()->setBold(true);
            $sheet->getStyle("G6")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'D88308')
                    )
                )
            );
            $sheet->getStyle("G8")->getFont()->setBold(true);
            $sheet->getStyle("G8")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'D88308')
                    )
                )
            );
            $sheet->getStyle('G6')->getAlignment()->setWrapText(true);
            $sheet->setCellValue('G6', 'Factor de conversion de grano')->mergeCells('G6:G7');
            $sheet->setCellValue('G8', '%');
            $sheet->getColumnDimension('H')
                ->setWidth(14);
            $sheet->getStyle("H6")->getFont()->setBold(true);
            $sheet->getStyle("H6")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'D88308')
                    )
                )
            );
            $sheet->getStyle("H8")->getFont()->setBold(true);
            $sheet->getStyle("H8")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'D88308')
                    )
                )
            );
            $sheet->getStyle('H6')->getAlignment()->setWrapText(true);
            $sheet->setCellValue('H6', 'Existencias producto')->mergeCells('H6:H7');
            $sheet->setCellValue('H8', 't');
            $sheet->getColumnDimension('I')
                ->setWidth(14);
            $sheet->getStyle("I6")->getFont()->setBold(true);
            $sheet->getStyle("I6")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'D88308')
                    )
                )
            );
            $sheet->getStyle("I8")->getFont()->setBold(true);
            $sheet->getStyle("I8")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'D88308')
                    )
                )
            );
            $sheet->getStyle('I6')->getAlignment()->setWrapText(true);
            $sheet->setCellValue('I6', 'Produccion teorica')->mergeCells('I6:I7');
            $sheet->setCellValue('I8', 't');
            $sheet->getStyle("J6")->getFont()->setBold(true);
            $sheet->getColumnDimension('J')
                ->setWidth(14);
            $sheet->getStyle("J6")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'D88308')
                    )
                )
            );
            $sheet->getStyle("J8")->getFont()->setBold(true);
            $sheet->getStyle("J8")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'D88308')
                    )
                )
            );
            $sheet->getStyle('J6')->getAlignment()->setWrapText(true);
            $sheet->setCellValue('J6', 'Total oferta')->mergeCells('J6:J7');
            $sheet->setCellValue('J8', 't');
            $sheet->getColumnDimension('K')
                ->setWidth(15);
            $sheet->getStyle("K6")->getFont()->setBold(true);
            $sheet->getStyle("K6")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'D88308')
                    )
                )
            );
            $sheet->getStyle("K8")->getFont()->setBold(true);
            $sheet->getStyle("K8")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'D88308')
                    )
                )
            );
            $sheet->getStyle('K6')->getAlignment()->setWrapText(true);
            $sheet->setCellValue('K6', 'Cupo asignado para venta de mercado interno')->mergeCells('K6:L7');
            $sheet->setCellValue('K8', 't mes');
            $sheet->getStyle("L8")->getFont()->setBold(true);
            $sheet->getStyle("L8")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'D88308')
                    )
                )
            );
            $sheet->setCellValue('L8', 't semestre');
            $sheet->getStyle("M6")->getFont()->setBold(true);
            $sheet->getStyle("M6")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'D88308')
                    )
                )
            );
            $sheet->getStyle("M7")->getFont()->setBold(true);
            $sheet->getStyle("M7")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'D88308')
                    )
                )
            );
            $sheet->getStyle("M8")->getFont()->setBold(true);
            $sheet->getStyle("M8")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'D88308')
                    )
                )
            );
            $sheet->getStyle('M6')->getAlignment()->setHorizontal('center');
            $sheet->setCellValue('M6', 'Ventas Internas')->mergeCells('M6:S6');
            $sheet->setCellValue('M7', 'Enero');
            $sheet->setCellValue('M8', 't');
            $sheet->getStyle("N7")->getFont()->setBold(true);
            $sheet->getStyle("N7")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'D88308')
                    )
                )
            );
            $sheet->getStyle("N8")->getFont()->setBold(true);
            $sheet->getStyle("N8")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'D88308')
                    )
                )
            );
            $sheet->setCellValue('N7', 'Febrero');
            $sheet->setCellValue('N8', 't');
            $sheet->getStyle("O6")->getFont()->setBold(true);
            $sheet->getStyle("O7")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'D88308')
                    )
                )
            );
            $sheet->getStyle("O8")->getFont()->setBold(true);
            $sheet->getStyle("O8")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'D88308')
                    )
                )
            );
            $sheet->getStyle("O7")->getFont()->setBold(true);
            $sheet->setCellValue('O7', 'Marzo');
            $sheet->setCellValue('O8', 't');
            $sheet->getStyle("P7")->getFont()->setBold(true);
            $sheet->getStyle("P7")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'D88308')
                    )
                )
            );
            $sheet->getStyle("P8")->getFont()->setBold(true);
            $sheet->getStyle("P8")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'D88308')
                    )
                )
            );
            $sheet->setCellValue('P7', 'Abril');
            $sheet->setCellValue('P8', 't');
            $sheet->getStyle("Q7")->getFont()->setBold(true);
            $sheet->getStyle("Q7")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'D88308')
                    )
                )
            );
            $sheet->getStyle("Q8")->getFont()->setBold(true);
            $sheet->getStyle("Q8")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'D88308')
                    )
                )
            );
            $sheet->setCellValue('Q7', 'Mayo');
            $sheet->setCellValue('Q8', 't');
            $sheet->getStyle("R7")->getFont()->setBold(true);
            $sheet->getStyle("R7")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'D88308')
                    )
                )
            );
            $sheet->getStyle("R8")->getFont()->setBold(true);
            $sheet->getStyle("R8")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'D88308')
                    )
                )
            );
            $sheet->setCellValue('R7', 'Junio');
            $sheet->setCellValue('R8', 't');
            $sheet->getStyle("S7")->getFont()->setBold(true);
            $sheet->getStyle("S7")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'D88308')
                    )
                )
            );
            $sheet->getStyle("S8")->getFont()->setBold(true);
            $sheet->getStyle("S8")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'D88308')
                    )
                )
            );
            $sheet->setCellValue('S7', 'Ene a Jun');
            $sheet->setCellValue('S8', 't');
            $sheet->getColumnDimension('T')
                ->setWidth(16);
            $sheet->getStyle("T6")->getFont()->setBold(true);
            $sheet->getStyle("T6")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'D88308')
                    )
                )
            );
            $sheet->getStyle("T8")->getFont()->setBold(true);
            $sheet->getStyle("T8")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'D88308')
                    )
                )
            );
            $sheet->getStyle('T6')->getAlignment()->setWrapText(true);
            $sheet->getStyle('T6')->getAlignment()->setHorizontal('center');
            $sheet->setCellValue('T6', 'Cupo asignado para venta al mercado interno')->mergeCells('T6:U7');
            $sheet->setCellValue('T8', 't mes');
            $sheet->getStyle("U8")->getFont()->setBold(true);
            $sheet->getStyle("U8")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'D88308')
                    )
                )
            );
            $sheet->setCellValue('U8', 't semestre');
            $sheet->getStyle("V6")->getFont()->setBold(true);
            $sheet->getStyle("V6")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'D88308')
                    )
                )
            );
            $sheet->getStyle("V7")->getFont()->setBold(true);
            $sheet->getStyle("V7")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'D88308')
                    )
                )
            );
            $sheet->getStyle("V8")->getFont()->setBold(true);
            $sheet->getStyle("V8")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'D88308')
                    )
                )
            );
            $sheet->getStyle('V6')->getAlignment()->setHorizontal('center');
            $sheet->setCellValue('V6', 'Ventas internas II/2017')->mergeCells('V6:AB6');
            $sheet->setCellValue('V7', 'Julio');
            $sheet->setCellValue('V8', 't');
            $sheet->getStyle("W7")->getFont()->setBold(true);
            $sheet->getStyle("W7")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'D88308')
                    )
                )
            );
            $sheet->getStyle("W8")->getFont()->setBold(true);
            $sheet->getStyle("W8")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'D88308')
                    )
                )
            );
            $sheet->setCellValue('W7', 'Agosto');
            $sheet->setCellValue('W8', 't');
            $sheet->getStyle("X7")->getFont()->setBold(true);
            $sheet->getStyle("X7")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'D88308')
                    )
                )
            );
            $sheet->getStyle("X8")->getFont()->setBold(true);
            $sheet->getStyle("X8")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'D88308')
                    )
                )
            );
            $sheet->setCellValue('X7', 'Septiembre');
            $sheet->setCellValue('X8', 't');
            $sheet->getStyle("Y7")->getFont()->setBold(true);
            $sheet->getStyle("Y7")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'D88308')
                    )
                )
            );
            $sheet->getStyle("Y8")->getFont()->setBold(true);
            $sheet->getStyle("Y8")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'D88308')
                    )
                )
            );
            $sheet->setCellValue('Y7', 'Octubre');
            $sheet->setCellValue('Y8', 't');
            $sheet->getStyle("Z7")->getFont()->setBold(true);
            $sheet->getStyle("Z7")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'D88308')
                    )
                )
            );
            $sheet->getStyle("Z8")->getFont()->setBold(true);
            $sheet->getStyle("Z8")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'D88308')
                    )
                )
            );
            $sheet->setCellValue('Z7', 'Noviembre');
            $sheet->setCellValue('Z8', 't');
            $sheet->getStyle("AA7")->getFont()->setBold(true);
            $sheet->getStyle("AA7")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'D88308')
                    )
                )
            );
            $sheet->getStyle("AA8")->getFont()->setBold(true);
            $sheet->getStyle("AA8")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'D88308')
                    )
                )
            );
            $sheet->setCellValue('AA7', 'Diciembre');
            $sheet->setCellValue('AA8', 't');
            $sheet->getStyle("AB7")->getFont()->setBold(true);
            $sheet->getStyle("AB7")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'D88308')
                    )
                )
            );
            $sheet->getStyle("AB8")->getFont()->setBold(true);
            $sheet->getStyle("AB8")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'D88308')
                    )
                )
            );
            $sheet->setCellValue('AB7', 'Jul a Dic');
            $sheet->setCellValue('AB8', 't');
            $sheet->getColumnDimension('AC')
                ->setWidth(15);
            $sheet->getStyle("AC6")->getFont()->setBold(true);
            $sheet->getStyle("AC6")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'D88308')
                    )
                )
            );
            $sheet->getStyle("AC8")->getFont()->setBold(true);
            $sheet->getStyle("AC8")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'D88308')
                    )
                )
            );
            $sheet->getStyle('AC6')->getAlignment()->setWrapText(true);
            $sheet->setCellValue('AC6', 'Saldo exportable teorico (4)')->mergeCells('AC6:AC7');
            $sheet->setCellValue('AC8', 't');
            $sheet->getStyle('AD6')->getAlignment()->setWrapText(true);
            $sheet->getColumnDimension('AD')
                ->setWidth(15);
            $sheet->getStyle("AD6")->getFont()->setBold(true);
            $sheet->getStyle("AD6")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'D88308')
                    )
                )
            );
            $sheet->setCellValue('AD6', 'Cantidad Autorizada')->mergeCells('AD6:AD7');
            $sheet->getStyle("AD8")->getFont()->setBold(true);
            $sheet->getStyle("AD8")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'D88308')
                    )
                )
            );
            $sheet->setCellValue('AD8', 't');
            $sheet->getStyle('AE6')->getAlignment()->setWrapText(true);
            $sheet->getColumnDimension('AE')
                ->setWidth(15);
            $sheet->getStyle("AE6")->getFont()->setBold(true);
            $sheet->getStyle("AE6")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'D88308')
                    )
                )
            );
            $sheet->setCellValue('AE6', 'Cantidad Exportada')->mergeCells('AE6:AE7');
            $sheet->getStyle("AE8")->getFont()->setBold(true);
            $sheet->getStyle("AE8")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'D88308')
                    )
                )
            );
            $sheet->setCellValue('AE8', 't');
            $sheet->getStyle('AF6')->getAlignment()->setWrapText(true);
            $sheet->getStyle("AF6")->getFont()->setBold(true);
            $sheet->getColumnDimension('AF')
                ->setWidth(15);
            $sheet->getStyle("AF6")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'D88308')
                    )
                )
            );
            $sheet->setCellValue('AF6', 'Saldo')->mergeCells('AF6:AF7');
            $sheet->getStyle('AF8')->getAlignment()->setWrapText(true);
            $sheet->getStyle("AF8")->getFont()->setBold(true);
            $sheet->getStyle("AF8")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'D88308')
                    )
                )
            );
            $sheet->setCellValue('AF8', 't');

            $phpExcelObject->getActiveSheet()->setTitle('Simple');

            // Set active sheet index to the first sheet, so Excel opens this as the first sheet
            $phpExcelObject->setActiveSheetIndex(0);


            $drawingobject = $objPhp->createPHPExcelWorksheetDrawing();
            $drawingobject->setName('logo');
            $drawingobject->setDescription('logotipo');
            $drawingobject->setPath($path."/bolivia.jpg");
            $drawingobject->setHeight(60);
            $drawingobject->setOffsetY(20);
            $drawingobject->setCoordinates('A1');
            $drawingobject->setWorksheet($sheet);

            $drawingobject = $objPhp->createPHPExcelWorksheetDrawing();
            $drawingobject->setName('logo ministerio');
            $drawingobject->setDescription('logotipo ministerio');
            $drawingobject->setPath($path."/logo_mdpyep.jpg");
            $drawingobject->setHeight(80);
            $drawingobject->setOffsetY(20);
            $drawingobject->setCoordinates('W1');
            $drawingobject->setWorksheet($sheet);

            $indice = 9;
            $indi_inicio = 9;
            $id_producto = 0;
            $id_producto_ant = 0;
            $conta_produc = 0;

            //totales de todos los registros
            $total_exis_grano = 0;
            $total_compra_verano = 0;
            $total_compra_invierno = 0;
            $total_disponi_prod = 0;

            foreach ($datosExportacion as $row){
                $id_producto = intval($row[0]);
                if($id_producto_ant != 0 && $id_producto != $id_producto_ant){
                    //
                    $sheet->getStyle("A".$indice)->applyFromArray(
                        array(
                            'fill' => array(
                                'type' => 'solid',
                                'color' => array('rgb' => 'B4EEA6')
                            )
                        )
                    );
                    $sheet->mergeCells('A'.$indi_inicio.':A'.($indice-1));
                    $sheet->getStyle("A".$indice)->getFont()->setBold(true);
                    $sheet->setCellValue('A'.$indice, "Total Empresas")->mergeCells('A'.$indice.':B'.$indice);
                    $sheet->getStyle("C".$indice)->applyFromArray(
                        array(
                            'fill' => array(
                                'type' => 'solid',
                                'color' => array('rgb' => 'B4EEA6')
                            )
                        )
                    );
                    $sheet->setCellValue('C'.$indice, '=SUM(C'.$indi_inicio.':C'.($indice-1).')');
                    $sheet->getStyle("C".$indice)->applyFromArray(
                        array(
                            'fill' => array(
                                'type' => 'solid',
                                'color' => array('rgb' => 'B4EEA6')
                            )
                        )
                    );
                    $sheet->setCellValue('D'.$indice, '=SUM(D'.$indi_inicio.':D'.($indice-1).')');
                    $sheet->getStyle("D".$indice)->applyFromArray(
                        array(
                            'fill' => array(
                                'type' => 'solid',
                                'color' => array('rgb' => 'B4EEA6')
                            )
                        )
                    );
                    $sheet->setCellValue('E'.$indice, '=SUM(E'.$indi_inicio.':E'.($indice-1).')');
                    $sheet->getStyle("E".$indice)->applyFromArray(
                        array(
                            'fill' => array(
                                'type' => 'solid',
                                'color' => array('rgb' => 'B4EEA6')
                            )
                        )
                    );
                    $sheet->setCellValue('F'.$indice, '=SUM(F'.$indi_inicio.':F'.($indice-1).')');
                    $sheet->getStyle("F".$indice)->applyFromArray(
                        array(
                            'fill' => array(
                                'type' => 'solid',
                                'color' => array('rgb' => 'B4EEA6')
                            )
                        )
                    );
                    $sheet->setCellValue('G'.$indice, '=(SUM(G'.$indi_inicio.':G'.($indice-1).')/'.$conta_produc.')');
                    $sheet->getStyle("G".$indice)->applyFromArray(
                        array(
                            'fill' => array(
                                'type' => 'solid',
                                'color' => array('rgb' => 'B4EEA6')
                            )
                        )
                    );
                    $sheet->setCellValue('H'.$indice, '=SUM(H'.$indi_inicio.':H'.($indice-1).')');
                    $sheet->getStyle("H".$indice)->applyFromArray(
                        array(
                            'fill' => array(
                                'type' => 'solid',
                                'color' => array('rgb' => 'B4EEA6')
                            )
                        )
                    );
                    $sheet->setCellValue('I'.$indice, '=SUM(I'.$indi_inicio.':I'.($indice-1).')');
                    $sheet->getStyle("I".$indice)->applyFromArray(
                        array(
                            'fill' => array(
                                'type' => 'solid',
                                'color' => array('rgb' => 'B4EEA6')
                            )
                        )
                    );
                    $sheet->setCellValue('J'.$indice, '=SUM(J'.$indi_inicio.':J'.($indice-1).')');
                    $sheet->getStyle("J".$indice)->applyFromArray(
                        array(
                            'fill' => array(
                                'type' => 'solid',
                                'color' => array('rgb' => 'B4EEA6')
                            )
                        )
                    );
                    $sheet->setCellValue('M'.$indice, '=SUM(M'.$indi_inicio.':M'.($indice-1).')');
                    $sheet->getStyle("M".$indice)->applyFromArray(
                        array(
                            'fill' => array(
                                'type' => 'solid',
                                'color' => array('rgb' => 'B4EEA6')
                            )
                        )
                    );
                    $sheet->setCellValue('N'.$indice, '=SUM(N'.$indi_inicio.':N'.($indice-1).')');
                    $sheet->getStyle("N".$indice)->applyFromArray(
                        array(
                            'fill' => array(
                                'type' => 'solid',
                                'color' => array('rgb' => 'B4EEA6')
                            )
                        )
                    );
                    $sheet->setCellValue('O'.$indice, '=SUM(O'.$indi_inicio.':O'.($indice-1).')');
                    $sheet->getStyle("O".$indice)->applyFromArray(
                        array(
                            'fill' => array(
                                'type' => 'solid',
                                'color' => array('rgb' => 'B4EEA6')
                            )
                        )
                    );
                    $sheet->setCellValue('P'.$indice, '=SUM(P'.$indi_inicio.':P'.($indice-1).')');
                    $sheet->getStyle("P".$indice)->applyFromArray(
                        array(
                            'fill' => array(
                                'type' => 'solid',
                                'color' => array('rgb' => 'B4EEA6')
                            )
                        )
                    );
                    $sheet->setCellValue('Q'.$indice, '=SUM(Q'.$indi_inicio.':Q'.($indice-1).')');
                    $sheet->getStyle("Q".$indice)->applyFromArray(
                        array(
                            'fill' => array(
                                'type' => 'solid',
                                'color' => array('rgb' => 'B4EEA6')
                            )
                        )
                    );
                    $sheet->setCellValue('R'.$indice, '=SUM(R'.$indi_inicio.':R'.($indice-1).')');
                    $sheet->getStyle("R".$indice)->applyFromArray(
                        array(
                            'fill' => array(
                                'type' => 'solid',
                                'color' => array('rgb' => 'B4EEA6')
                            )
                        )
                    );
                    $sheet->setCellValue('S'.$indice, '=SUM(S'.$indi_inicio.':S'.($indice-1).')');
                    $sheet->getStyle("S".$indice)->applyFromArray(
                        array(
                            'fill' => array(
                                'type' => 'solid',
                                'color' => array('rgb' => 'B4EEA6')
                            )
                        )
                    );
                    $sheet->setCellValue('V'.$indice, '=SUM(V'.$indi_inicio.':V'.($indice-1).')');
                    $sheet->getStyle("V".$indice)->applyFromArray(
                        array(
                            'fill' => array(
                                'type' => 'solid',
                                'color' => array('rgb' => 'B4EEA6')
                            )
                        )
                    );
                    $sheet->setCellValue('W'.$indice, '=SUM(W'.$indi_inicio.':W'.($indice-1).')');
                    $sheet->getStyle("W".$indice)->applyFromArray(
                        array(
                            'fill' => array(
                                'type' => 'solid',
                                'color' => array('rgb' => 'B4EEA6')
                            )
                        )
                    );
                    $sheet->setCellValue('X'.$indice, '=SUM(X'.$indi_inicio.':X'.($indice-1).')');
                    $sheet->getStyle("X".$indice)->applyFromArray(
                        array(
                            'fill' => array(
                                'type' => 'solid',
                                'color' => array('rgb' => 'B4EEA6')
                            )
                        )
                    );
                    $sheet->setCellValue('Y'.$indice, '=SUM(Y'.$indi_inicio.':Y'.($indice-1).')');
                    $sheet->getStyle("Y".$indice)->applyFromArray(
                        array(
                            'fill' => array(
                                'type' => 'solid',
                                'color' => array('rgb' => 'B4EEA6')
                            )
                        )
                    );
                    $sheet->setCellValue('Z'.$indice, '=SUM(Z'.$indi_inicio.':Z'.($indice-1).')');
                    $sheet->getStyle("Z".$indice)->applyFromArray(
                        array(
                            'fill' => array(
                                'type' => 'solid',
                                'color' => array('rgb' => 'B4EEA6')
                            )
                        )
                    );
                    $sheet->setCellValue('AA'.$indice, '=SUM(AA'.$indi_inicio.':AA'.($indice-1).')');
                    $sheet->getStyle("AA".$indice)->applyFromArray(
                        array(
                            'fill' => array(
                                'type' => 'solid',
                                'color' => array('rgb' => 'B4EEA6')
                            )
                        )
                    );
                    $sheet->setCellValue('AB'.$indice, '=SUM(AB'.$indi_inicio.':AB'.($indice-1).')');
                    $sheet->getStyle("AB".$indice)->applyFromArray(
                        array(
                            'fill' => array(
                                'type' => 'solid',
                                'color' => array('rgb' => 'B4EEA6')
                            )
                        )
                    );
                    $sheet->setCellValue('AC'.$indice, '=SUM(AC'.$indi_inicio.':AC'.($indice-1).')');
                    $sheet->getStyle("AC".$indice)->applyFromArray(
                        array(
                            'fill' => array(
                                'type' => 'solid',
                                'color' => array('rgb' => 'B4EEA6')
                            )
                        )
                    );
                    $sheet->setCellValue('AD'.$indice, '=SUM(AD'.$indi_inicio.':AD'.($indice-1).')');
                    $sheet->getStyle("AD".$indice)->applyFromArray(
                        array(
                            'fill' => array(
                                'type' => 'solid',
                                'color' => array('rgb' => 'B4EEA6')
                            )
                        )
                    );
                    $sheet->setCellValue('AE'.$indice, '=SUM(AE'.$indi_inicio.':AE'.($indice-1).')');
                    $sheet->getStyle("AE".$indice)->applyFromArray(
                        array(
                            'fill' => array(
                                'type' => 'solid',
                                'color' => array('rgb' => 'B4EEA6')
                            )
                        )
                    );
                    $sheet->setCellValue('AF'.$indice, '=SUM(AF'.$indi_inicio.':AF'.($indice-1).')');
                    $sheet->getStyle("AF".$indice)->applyFromArray(
                        array(
                            'fill' => array(
                                'type' => 'solid',
                                'color' => array('rgb' => 'B4EEA6')
                            )
                        )
                    );

                    $indice = $indice + 1;

                    //
                    $conta_produc = 0;
                    $indi_inicio = $indice;
                }
                $id_producto_ant = $id_producto;
                $sheet->setCellValue('A'.$indice, $row[1]); // producto
                $sheet->getStyle('B'.$indice)->getAlignment()->setWrapText(true);
                $sheet->setCellValue('B'.$indice, $row[2]); // empresa
                $total_exis_grano = $total_exis_grano + floatval($row[3]); //[3]=>existencia_grano
                $sheet->setCellValue('C'.$indice, $row[3]);
                $total_compra_verano = $total_compra_verano + floatval($row[4]); // [4]->compra_ver
                $sheet->setCellValue('D'.$indice, $row[4]);
                $total_compra_invierno = $total_compra_invierno + floatval($row[5]); // [5]->compra_inv
                $sheet->setCellValue('E'.$indice, $row[5]);
                $total_disponible = floatval($row[3]) + floatval($row[4]) + floatval($row[5]); //- floatval($row["venta_grano"]);
                $total_disponi_prod = $total_disponi_prod + floatval($total_disponible);
                $sheet->setCellValue('F'.$indice, $total_disponible);
                $sheet->setCellValue('G'.$indice, $row[7]);
                $sheet->setCellValue('H'.$indice, $row[8]); //[8]->existencia
                $sheet->setCellValue('I'.$indice, (floatval($row[7])/100) * $total_disponible);
                $sheet->setCellValue('J'.$indice, floatval($row[8]) + (floatval($row[7])/100) * $total_disponible);
                $sheet->setCellValue('M'.$indice, $row[11]); //[11]->venta_ene
                $sheet->setCellValue('N'.$indice, $row[12]); //[12]->venta_feb
                $sheet->setCellValue('O'.$indice, $row[13]); //[13]->venta_mar
                $sheet->setCellValue('P'.$indice, $row[14]); //[14]->venta_abr
                $sheet->setCellValue('Q'.$indice, $row[15]); //[15]->venta_may
                $sheet->setCellValue('R'.$indice, $row[16]); // [16]->venta_jun
                $sheet->setCellValue('S'.$indice, floatval($row[17])); // total ventas semestre1
                $sheet->setCellValue('V'.$indice, $row[18]); //[18]->venta_jul
                $sheet->setCellValue('W'.$indice, $row[19]); //[19]->venta_ago
                $sheet->setCellValue('X'.$indice, $row[20]); //[20]->venta_sep
                $sheet->setCellValue('Y'.$indice, $row[21]); //[21]->venta_oct
                $sheet->setCellValue('Z'.$indice, $row[22]); //[22]->venta_nov
                $sheet->setCellValue('AA'.$indice, $row[23]); //[23]->venta_dic
                $sheet->setCellValue('AB'.$indice, floatval($row[24])); // ventas sem2
                $sheet->setCellValue('AC'.$indice, floatval($row[25]));

                $respuesta = $reportesManager->datosExportaciones($inicio_mes, $inicio_gest, $fin_mes, $fin_gest, $row[26], $row[27]);
                if(count($respuesta) > 0){
                  $sheet->setCellValue('AD'.$indice, $respuesta[0][0]);
                  $sheet->setCellValue('AE'.$indice, $respuesta[0][1]);
                  $sheet->setCellValue('AF'.$indice, $respuesta[0][2]);
                } else {
                  $sheet->setCellValue('AD'.$indice, 0);
                  $sheet->setCellValue('AE'.$indice, 0);
                  $sheet->setCellValue('AF'.$indice, 0);
                }

                $conta_produc = $conta_produc + 1;
                $indice = $indice + 1;
            }
            // subtotales
            $sheet->getStyle("A".$indice)->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'B4EEA6')
                    )
                )
            );
            $sheet->getStyle("A".$indice)->getFont()->setBold(true);
            $sheet->mergeCells('A'.$indi_inicio.':A'.($indice-1));
            $sheet->setCellValue('A'.$indice, "Total Empresas")->mergeCells('A'.$indice.':B'.$indice);
            $sheet->getStyle("C".$indice)->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'B4EEA6')
                    )
                )
            );
            $sheet->setCellValue('C'.$indice, '=SUM(C'.$indi_inicio.':C'.($indice-1).')'); //$total_prod_exis_grano);
            $sheet->getStyle("C".$indice)->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'B4EEA6')
                    )
                )
            );
            $sheet->setCellValue('D'.$indice, '=SUM(D'.$indi_inicio.':D'.($indice-1).')'); //$total_prod_compra_ver);
            $sheet->getStyle("D".$indice)->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'B4EEA6')
                    )
                )
            );
            $sheet->setCellValue('E'.$indice, '=SUM(E'.$indi_inicio.':E'.($indice-1).')'); //$total_prod_compra_inv);
            $sheet->getStyle("E".$indice)->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'B4EEA6')
                    )
                )
            );
            $sheet->setCellValue('F'.$indice, '=SUM(F'.$indi_inicio.':F'.($indice-1).')'); //$total_prod_exis_grano + $total_prod_compra_ver + $total_prod_compra_inv);
            $sheet->getStyle("F".$indice)->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'B4EEA6')
                    )
                )
            );
            $sheet->setCellValue('G'.$indice, '=(SUM(G'.$indi_inicio.':G'.($indice-1).')/'.$conta_produc.')'); //$prome_prod_fact_conv/$conta_produc);
            $sheet->getStyle("G".$indice)->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'B4EEA6')
                    )
                )
            );
            $sheet->setCellValue('H'.$indice, '=SUM(H'.$indi_inicio.':H'.($indice-1).')'); //$total_existencia);
            $sheet->getStyle("H".$indice)->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'B4EEA6')
                    )
                )
            );
            $sheet->setCellValue('I'.$indice, '=SUM(I'.$indi_inicio.':I'.($indice-1).')'); //$total_prod_teorica1);
            $sheet->getStyle("I".$indice)->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'B4EEA6')
                    )
                )
            );
            $sheet->setCellValue('J'.$indice, '=SUM(J'.$indi_inicio.':J'.($indice-1).')'); // $total_existencia + $total_prod_teorica1);
            $sheet->getStyle("J".$indice)->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'B4EEA6')
                    )
                )
            );
            $sheet->setCellValue('M'.$indice, '=SUM(M'.$indi_inicio.':M'.($indice-1).')'); //$total_ene);
            $sheet->getStyle("M".$indice)->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'B4EEA6')
                    )
                )
            );
            $sheet->setCellValue('N'.$indice, '=SUM(N'.$indi_inicio.':N'.($indice-1).')'); // $total_feb);
            $sheet->getStyle("N".$indice)->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'B4EEA6')
                    )
                )
            );
            $sheet->setCellValue('O'.$indice, '=SUM(O'.$indi_inicio.':O'.($indice-1).')'); //$total_mar);
            $sheet->getStyle("O".$indice)->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'B4EEA6')
                    )
                )
            );
            $sheet->setCellValue('P'.$indice, '=SUM(P'.$indi_inicio.':P'.($indice-1).')'); //$total_abr);
            $sheet->getStyle("P".$indice)->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'B4EEA6')
                    )
                )
            );
            $sheet->setCellValue('Q'.$indice, '=SUM(Q'.$indi_inicio.':Q'.($indice-1).')'); // $total_may);
            $sheet->getStyle("Q".$indice)->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'B4EEA6')
                    )
                )
            );
            $sheet->setCellValue('R'.$indice, '=SUM(R'.$indi_inicio.':R'.($indice-1).')'); //$total_jun);
            $sheet->getStyle("R".$indice)->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'B4EEA6')
                    )
                )
            );
            $sheet->setCellValue('S'.$indice, '=SUM(S'.$indi_inicio.':S'.($indice-1).')'); // $total_ene + $total_feb + $total_mar + $total_abr + $total_may + $total_jun);
            $sheet->getStyle("S".$indice)->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'B4EEA6')
                    )
                )
            );
            $sheet->setCellValue('V'.$indice, '=SUM(V'.$indi_inicio.':V'.($indice-1).')'); // $total_jul);
            $sheet->getStyle("V".$indice)->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'B4EEA6')
                    )
                )
            );
            $sheet->setCellValue('W'.$indice, '=SUM(W'.$indi_inicio.':W'.($indice-1).')'); //$total_ago);
            $sheet->getStyle("W".$indice)->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'B4EEA6')
                    )
                )
            );
            $sheet->setCellValue('X'.$indice, '=SUM(X'.$indi_inicio.':X'.($indice-1).')'); //$total_sep);
            $sheet->getStyle("X".$indice)->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'B4EEA6')
                    )
                )
            );
            $sheet->setCellValue('Y'.$indice, '=SUM(Y'.$indi_inicio.':Y'.($indice-1).')'); // $total_oct);
            $sheet->getStyle("Y".$indice)->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'B4EEA6')
                    )
                )
            );
            $sheet->setCellValue('Z'.$indice, '=SUM(Z'.$indi_inicio.':Z'.($indice-1).')'); //$total_nov);
            $sheet->getStyle("Z".$indice)->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'B4EEA6')
                    )
                )
            );
            $sheet->setCellValue('AA'.$indice, '=SUM(AA'.$indi_inicio.':AA'.($indice-1).')'); //$total_dic);
            $sheet->getStyle("AA".$indice)->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'B4EEA6')
                    )
                )
            );
            $sheet->setCellValue('AB'.$indice, '=SUM(AB'.$indi_inicio.':AB'.($indice-1).')'); //$total_jul + $total_ago + $total_sep + $total_oct + $total_nov + $total_dic);
            $sheet->getStyle("AB".$indice)->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'B4EEA6')
                    )
                )
            );

            $sheet->setCellValue('AC'.$indice, '=SUM(AC'.$indi_inicio.':AC'.($indice-1).')');
            $sheet->getStyle("AC".$indice)->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'B4EEA6')
                    )
                )
            );
            $sheet->setCellValue('AD'.$indice, '=SUM(AD'.$indi_inicio.':AD'.($indice-1).')');
            $sheet->getStyle("AD".$indice)->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'B4EEA6')
                    )
                )
            );
            $sheet->setCellValue('AE'.$indice, '=SUM(AE'.$indi_inicio.':AE'.($indice-1).')');
            $sheet->getStyle("AE".$indice)->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'B4EEA6')
                    )
                )
            );
            $sheet->setCellValue('AF'.$indice, '=SUM(AF'.$indi_inicio.':AF'.($indice-1).')');
            $sheet->getStyle("AF".$indice)->applyFromArray(
                array(
                    'fill' => array(
                        'type' => 'solid',
                        'color' => array('rgb' => 'B4EEA6')
                    )
                )
            );

            $indice = $indice + 2;
            $sheet->getStyle("B".$indice)->getFont()->setBold(true);
            $sheet->setCellValue('B'.$indice, "TOTAL");
            $sheet->getStyle("C".$indice)->getFont()->setBold(true);
            $sheet->setCellValue('C'.$indice, $total_exis_grano);
            $sheet->getStyle("D".$indice)->getFont()->setBold(true);
            $sheet->setCellValue('D'.$indice, $total_compra_verano);
            $sheet->getStyle("E".$indice)->getFont()->setBold(true);
            $sheet->setCellValue('E'.$indice, $total_compra_invierno);
            $sheet->getStyle("F".$indice)->getFont()->setBold(true);
            $sheet->setCellValue('F'.$indice, $total_disponi_prod);

            $hora = (localtime(time(),true));
            $nombre_archivo = "saldo_exportable_".date("Y")."_".date("m")."_".date("d")."_".(date("H")-1)."_".date("i").".xls";
            // create the writer
            $writer = $objPhp->createWriter($phpExcelObject, 'Excel5');
            /*if(!is_file('exportar/')){
              mkdir('exportar', 0777, true);
            }*/
            $writer->save('exportar/'.$nombre_archivo);
            //
            $path = $path.'/exportar/'.$nombre_archivo;
            $data = file_get_contents($path);

            $response = array(
                'name' => $nombre_archivo,
                'estado' => "OK"
            );
        }catch (Exception $e){
            $response = array(
                'estado' => "ERROR",
                'mensaje' => $e
            );
        }

        return $response;
    }

    public function findIdPeriodoFase ($id_entidad, $fecha)
    {
        $statement = $this->em->getConnection()->query("SELECT
periodo_fase.id
FROM
periodo_fase
INNER JOIN entidad ON entidad.id_fase = periodo_fase.id_fase
WHERE
'$fecha'::date BETWEEN periodo_fase.fecha_desde AND periodo_fase.fecha_hasta
AND entidad.id = $id_entidad");

        if ($statement->rowCount() > 0)
            return $statement->fetch();
        else
            return array('error'=>'No se pudo encontrar el periodo de la fecha '.$fecha);

    }
    public function findIdRelacionEntidad($razon_social, $index=0)
    {

        $statement = $this->em->getConnection()->query("SELECT
relacion_entidad.id,
relacion_entidad.id_entidad
FROM
entidad
INNER JOIN relacion_entidad ON entidad.id = relacion_entidad.id_entidad AND relacion_entidad.vigente = true
WHERE
trim(LOWER(entidad.razon_social)) like trim(LOWER('$razon_social'))
");
        if ($statement->rowCount() > 0)
            return $statement->fetch();
        else
            return array('error'=>'No se encontro la entidad "'.$razon_social.'" indice ='.$index);
    }
    public function findTransaccionId($id_tipo_transaccion, $fecha, $id_relacion_entidad)
    {
        $statement = $this->em->getConnection()->query("SELECT
transaccion.id
FROM
transaccion
INNER JOIN periodo_fase ON transaccion.id_periodo_fase = periodo_fase.id
WHERE
transaccion.id_tipo_transaccion = $id_tipo_transaccion
AND transaccion.id_relacion_entidad = $id_relacion_entidad
AND '$fecha'::date BETWEEN periodo_fase.fecha_desde AND periodo_fase.fecha_hasta");

        if ($statement->rowCount() > 0)
            return $statement->fetch();
        else
            return array('error'=>'No se pudo encontrar la transaccion de fecha '.$fecha);
        //$arr = $statement->fetchAll();
        //if ($statement->rowCount() != 1)

    }
    public function findIdMoneda($descrip)
    {
        $statement = $this->em->getConnection()->query("SELECT
tipo_moneda.id
FROM
tipo_moneda
WHERE
trim(LOWER(tipo_moneda.abreviacion)) = trim(LOWER('$descrip'))");

        if ($statement->rowCount() > 0)
            return $statement->fetch();
        else
            return array('error'=>'No se encontro el ID de moneda '.$descrip);
    }
    public function getRangoModulos($id_modulo)
    {
        $statement = $this->em->getConnection()->query("SELECT
id AS data,
desde || ' - ' || hasta AS label,
desde-1 AS inicio,
hasta-1 AS fin
FROM
rango_paginador
WHERE
rango_paginador.id_modulos = $id_modulo");

        if ($statement->rowCount() > 0)
            return $statement->fetchAll();
        else
            return array('error'=>'No se encontro datos de los rangos');
    }
    public function findIdPeriodoFaseXFrecuencia($frecuencia, $fecha, $id_fase)
    {
        if(intval($frecuencia)===2)// esto despues si se aumentan las frecuencias deberia ser un switch
            $fecha = $fecha . '-16';
        else
            $fecha = $fecha . '-01';

        $statement = $this->em->getConnection()->query("SELECT
        periodo_fase.id
        FROM
        periodo_fase
        WHERE
        periodo_fase.id_fase= $id_fase
        AND periodo_fase.fecha_desde ='$fecha'");

        if ($statement->rowCount() > 0)
            return $statement->fetch();
        else
            return array('error'=>'Busqueda finalizada, no se encontro datos de Periodo Fase');
    }
    public function monitoreoTransacciones($frecuencia, $fecha, $id_fase, $fromRow, $pageSize )
    {
        if ($fromRow != -1 && $pageSize != -1)
            $limit = "LIMIT $pageSize -- OFFSET " . (intval($fromRow) - 1);
        else
            $limit = "";

        $row = $this->findIdPeriodoFaseXFrecuencia($frecuencia, $fecha, $id_fase);
        if(!empty($row['error']))
            return $row;

        $statement = $this->em->getConnection()->query("SELECT
        COUNT(*) OVER() as total_rows,
        id_entidad,
        razon_social,
        numero_identificacion,
        abreviacion,
        id_relacion_entidad
        FROM
        fn_monitoreo_industria($row[id], $id_fase)
        $limit ");

        if ($statement->rowCount() > 0) {
            $arr = $statement->fetchAll();
            return array('arr' => $arr, 'total_rows' => $arr[0]['total_rows']);
        }
        else
            return array('error'=>'Busqueda finalizada, no se encontro datos');
    }
    public function consultaPeriodoFase($id_fase)
    {
        if (empty($id_fase))
            return array('error' => 'Verifique el ID de Fase');

        $statement = $this->em->getconnection()->query("SELECT
periodo_fase.id,
to_char(periodo_fase.fecha_desde,'dd') as dia_desde,
to_char(periodo_fase.fecha_hasta,'dd') as dia_hasta,
to_char(periodo_fase.fecha_desde,'mm') as mes,
to_char(periodo_fase.fecha_desde,'yyyy') as anio,
periodo_fase.fecha_desde,
periodo_fase.fecha_hasta
FROM
periodo_fase
WHERE
periodo_fase.id_fase = $id_fase
AND periodo_fase.fecha_desde <= now()
ORDER BY periodo_fase.fecha_desde");
        $arr = array();
        while ($row = $statement->fetch()) {
            $row['data'] = $row['id'];
            $row['label'] = $row['dia_desde'] . ' al ' . $row['dia_hasta'] . ' ' . $this->getMes($row['mes']).'-'.$row['anio'];
            $arr[] = $row;
        }
        if (empty($arr))
            return array('error' => 'Busqueda finalizada, no se encontro los datos de Periodo Fase');
        return $arr;
    }
    public function consultaFrecuencia($id_fase)
    {
        if (empty($id_fase))
            return array('error' => 'Verifique el ID de Fase');

        $statement = $this->em->getconnection()->query("SELECT
periodo.id,
periodo.frecuencia
FROM
periodo_fase
INNER JOIN periodo ON periodo.id = periodo_fase.id_periodo
WHERE
periodo_fase.id_fase= $id_fase
GROUP BY periodo.id");
        $arr = array();
        if( $statement->rowCount()>0)
        {
            $row = $statement->fetch();
            $i = 0;
            while ($i < intval($row['frecuencia']))
                $arr[] = array('data'=>$row['id'], 'label'=>++$i);

            return $arr;
        }
        else
            return array('error' => 'Busqueda finalizada, no se encontro los datos de Periodo Fase');

    }
    public function uploadImgIssues($file, $id_usuario)
    {
        if(empty($id_usuario))
          return array('error'=>'Error al verificar el ID de usuario');

        $nameFolder="issues";
        $pathFolder="./".$nameFolder.'/'.$id_usuario;
        if(!is_dir($pathFolder))// si no existe creamos la carpeta
            mkdir($pathFolder,0777,true);

        $fecha=date('Ymd_Gis');
        $arr=explode(".",$file->getClientOriginalName());
        $nameFile=$id_usuario.'_'.$fecha.'.'.$arr[count($arr)-1];//nombre del archivo
        $file->move($pathFolder,$nameFile);// copiamos el archivo del buffer

        $i=0;
        while(filesize($pathFolder.'/'.$nameFile) < $file->getClientSize())//controlamos si termino de copiar el archivo
            $i++; // se realiza un instruccion tonta solo por relleno del control

        return array('nameFile'=>$nameFile,'pathFolder'=>$pathFolder);
    }
    public function verificarToDo()
    {
        $statement = $this->em->getconnection()->query("SELECT to_regclass('todo.issues')");
        if( $statement->rowCount() > 0 )
        {
            $row = $statement->fetch();
            if(empty($row['to_regclass']))
            {
                $this->em->getconnection()->query("SELECT * FROM create_todo()");
                return array('estado'=>2);
            }
            else
                return array('estado'=>"1".$statement->rowCount());
        }
        else
        {
            $this->em->getconnection()->query("SELECT * FROM create_todo()");
            return array('estado'=>2);
        }
    }
    public function addIssues($ruta, $file, $path_file, $id_usuario, $obs, $id_estados)
    {
        $this->em->getconnection()->query("SELECT * FROM todo.add_issues('$ruta', '$file','$path_file','$obs',$id_usuario,$id_estados)");
    }

}
