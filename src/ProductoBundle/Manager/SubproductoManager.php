<?php
namespace ProductoBundle\Manager;

use Doctrine\ORM\EntityManager;
use ProductoBundle\Entity\Producto;
use ProductoBundle\Entity\Subproducto;
use ProductoBundle\Modelo\ModeloProducto;
use ProductoBundle\Modelo\ModeloSubproducto;
use ProductoBundle\Modelo\ModeloUsuario;
use DateTime;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Doctrine\ORM\NoResultException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
class SubproductoManager{
    
    /**
     * @var EntityManager
     */
    protected $em;
    protected $repo;
    public function __construct(EntityManager $entityManager){
        $this->em=$entityManager;
        $this->setRepository();
    }
   
    protected function setRepository()
    {
        $this->repo=$this->em->getRepository(Subproducto::class);
    }

    public function recuperar($id){
       
        $subproducto = $this->repo->find($id);
        if(null==$subproducto)
        {
        	throw new NoResultException();
        }
		$msproducto=new ModeloSubproducto();
        $msproducto->nombreComercial=$subproducto->getNombreComercial();
        $msproducto->idFase=$subproducto->getIdFase()->getId();
        $msproducto->idMedida=$subproducto->getIdMedida()->getId();
        $msproducto->medida=$subproducto->getMedida();
        $msproducto->id=$subproducto->getId();
        $msproducto->idProducto=$subproducto->getIdProducto()->getId();
        $msproducto->idEntidad=$subproducto->getIdEntidad()->getId();
        $msproducto->densidad=$subproducto->getDensidad();
        $msproducto->idMagnitud=$subproducto->getIdMedida()->getIdMagnitud()->getId();
        $msproducto->codigo=$subproducto->getCodigo();
        return $msproducto;
    }

    public function listaSubproductos($idEmpresa){

        $productos=$this->repo->listarXEntidad($idEmpresa)->getQuery()->getResult();
        return $productos;
    }
    
    
    public function guardar(ModeloSubproducto $msproducto,ModeloUsuario $user)
    {
        $dt = new DateTime();
        $dt->format('Y-m-d H:i:s');
        $subproducto=new Subproducto();
        $subproducto->setIdUsuarioCreacion($user->idUser);
        $subproducto->setFechaCreacion($dt);

        $subproducto->setIdUsuarioUltimaModificacion($user->idUser);
        $subproducto->setFechaUltimaModificacion($dt);

        $subproducto->setNombreComercial($msproducto->nombreComercial);
        $subproducto->setIdMedida($this->em->getReference('ProductoBundle:Medida',$msproducto->idMedida));
        $subproducto->setIdFase($this->em->getReference('ProductoBundle:Fase',$msproducto->idFase));
        $subproducto->setIdProducto($this->em->getReference('ProductoBundle:Producto',$msproducto->idProducto));
        $subproducto->setIdEntidad($this->em->getReference('ProductoBundle:Entidad',$msproducto->idEntidad));
        $subproducto->setMedida($msproducto->medida);
        $subproducto->setEliminado(false);
        $subproducto->setDensidad($msproducto->densidad);
        $subproducto->setCodigo($msproducto->codigo);

        $this->em->persist($subproducto);
        $this->em->flush();
        return $subproducto;
    }
    public function actualizar(ModeloSubproducto $msproducto,ModeloUsuario $user)
    {
        
        $dt = new DateTime();
        $dt->format('Y-m-d H:i:s');
        $subproducto=$this->repo->find($msproducto->id);
        if(null==$subproducto)
        {
            throw new NoResultException();
        }

        $subproducto->setIdUsuarioCreacion($user->idUser);
        $subproducto->setFechaCreacion($dt);

        $subproducto->setIdUsuarioUltimaModificacion($user->idUser);
        $subproducto->setFechaUltimaModificacion($dt);

        $subproducto->setNombreComercial($msproducto->nombreComercial);
        $subproducto->setIdMedida($this->em->getReference('ProductoBundle:Medida',$msproducto->idMedida));
        $subproducto->setIdFase($this->em->getReference('ProductoBundle:Fase',$msproducto->idFase));
        $subproducto->setIdProducto($this->em->getReference('ProductoBundle:Producto',$msproducto->idProducto));
        $subproducto->setMedida($msproducto->medida);
        $subproducto->setDensidad($msproducto->densidad);
        $subproducto->setCodigo($msproducto->codigo);

        $this->em->persist($subproducto);
        $this->em->flush();
        return $subproducto;
        
    }
    public function baja($id,ModeloUsuario $user){
        $dt = new DateTime();
        $dt->format('Y-m-d H:i:s');
        $subproducto=$this->repo->find($id);
        if(null==$subproducto )
        {
            throw new NoResultException();
            
        }
        $subproducto->setIdUsuarioUltimaModificacion($user->idUser);
        $subproducto->setFechaUltimaModificacion($dt);
        $subproducto->setEliminado(true);
        $this->em->persist($subproducto);
        $this->em->flush();
        return $subproducto;
    }
    
    
}