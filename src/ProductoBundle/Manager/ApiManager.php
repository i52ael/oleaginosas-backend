<?php
namespace ProductoBundle\Manager;


use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

abstract class ApiManager
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var EntityRepository
     */
    protected $repo;

    /**
     * @var QueryBuilder
     */
    public function __construct(EntityManagerInterface $em) {
        $this->em = $em;
        
    }

    /**
     * @param $id
     * @return null|object
     */
    public function get($id)
    {
        return $this->repo->find($id);
    }

    public function reload($entity)
    {
        $this->em->refresh($entity);
        return $entity;
    }

    public function delete($entity)
    {
        $this->em->remove($entity);
        $this->em->flush();
    }
    public function add($entity){
        $this->em->persist($entity);
        $this->em->flush();
        return $entity;
    }

    public function update($entity)
    {
        $this->em->flush();        
        return $entity;
    }
    
}