<?php
namespace ProductoBundle\Manager;

use Doctrine\ORM\EntityManager;
use ProductoBundle\Entity\TipoTransaccion;



class TipoTransaccionManager
{

	/**
	 * @var EntityManager
	 */
    protected $em;
    
	protected $repo;
	
	
	public function __construct(EntityManager $entityManager)
	{
		$this->em=$entityManager;
		$this->setRepository();
	}
	
	protected function setRepository()
	{
		$this->repo = $this->em->getRepository('ProductoBundle:TipoTransaccion');
	}
		
	
	public function listar(){
	    return $this->repo->findAll();
	}
	
	
	
}