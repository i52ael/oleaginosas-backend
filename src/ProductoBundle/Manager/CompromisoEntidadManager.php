<?php
namespace ProductoBundle\Manager;

use Doctrine\ORM\EntityManager;
use DateTime;
use Doctrine\ORM\NoResultException;
use ProductoBundle\Entity\CompromisoEntidad;
use ProductoBundle\Modelo\ModeloCompromiso;
use ProductoBundle\Modelo\ModeloUsuario;

class CompromisoEntidadManager{
    
    /**
     * @var EntityManager
     */
    protected $em;
    protected $repo;
    public function __construct(EntityManager $entityManager){
        $this->em=$entityManager;
        $this->setRepository();
    }
   
    protected function setRepository()
    {
        $this->repo=$this->em->getRepository(CompromisoEntidad::class);
    }

    /*public function recuperar($id){

       $mcupo=new ModeloProduccionAgricola();
        $cupo =  $this->repo->find($id);
        if(null==$cupo)
        {
        	throw new NoResultException();
        }
        $mcupo->id=$cupo->getId();
        $mcupo->cupo=$cupo->getCupo();
        $mcupo->idProducto=$cupo->getIdProducto()->getId();
        $mcupo->fechaDesde=$cupo->getFechaDesde()->format('Y-m-d');;
        $mcupo->fechaHasta=$cupo->getFechaHasta()->format('Y-m-d');;
        return $mcupo;
        
    }*/
    public function listarCount($idTipoTransaccion, $idEntidad,$idProducto)
    {
        $total=$this->repo->listarCount($idTipoTransaccion, $idEntidad,$idProducto)->getQuery()->getOneOrNullResult();
        return $total;
    }

    public function listar($idTipoTransaccion, $idEntidad,$idProducto,$page,$limit){
        //var_dump($idTipoTransaccion,$idEntidad,$idProducto);
        //die();
        $compromisos=$this->repo->listar($idTipoTransaccion,$idEntidad,$idProducto,$page,$limit)->getQuery()->getResult();
        return $compromisos;
    }
    
    
    public function guardar(ModeloCompromiso $mcompromiso,ModeloUsuario $user)
    {

        $dt = new DateTime();
        $dt->format('Y-m-d H:i:s');
        $compromiso=new  CompromisoEntidad();
        $compromiso->setIdUsuarioCreacion($user->idUser);
        $compromiso->setFechaCreacion($dt);

        $compromiso->setIdUsuarioUltimaModificacion($user->idUser);
        $compromiso->setFechaUltimaModificacion($dt);

        $compromiso->setCompromisoEntidad($mcompromiso->compromiso);
        $compromiso->setIdProducto($this->em->getReference('ProductoBundle:Producto',$mcompromiso->idProducto));
        $compromiso->setIdCampania($this->em->getReference('ProductoBundle:Campania',$mcompromiso->idCampania));
        $compromiso->setIdEntidad($this->em->getReference('ProductoBundle:Entidad',$mcompromiso->idEntidad));
        $compromiso->setIdTipoTransaccion($this->em->getReference('ProductoBundle:TipoTransaccion',$mcompromiso->idTipoTransaccion));

        $compromiso->setEliminado(false);
        $this->em->persist($compromiso);
        $this->em->flush();
        return $compromiso;
    }

    public function baja($id,ModeloUsuario $user){
        $dt = new DateTime();
        $dt->format('Y-m-d H:i:s');
        $compromiso=$this->repo->find($id);
        if(null==$compromiso )
        {
            throw new NoResultException();
        }
        $compromiso->setIdUsuarioUltimaModificacion($user->idUser);
        $compromiso->setFechaUltimaModificacion($dt);
        $compromiso->setEliminado(true);
        $this->em->persist($compromiso);
        $this->em->flush();
        return $compromiso;
    }
    public function verifica($idEntidad,$idProducto,$idIdtipoTrasaccion,$idCampania)
    {
        return $this->repo->verifica($idEntidad,$idProducto,$idIdtipoTrasaccion,$idCampania)->getQuery()->getResult();
    }
    public function getIdCampania($mes, $gestion)
    {
        $statement=$this->em->getConnection()->query("SELECT
campania.id
FROM
campania
WHERE
campania.mes = $mes
AND campania.gestion = $gestion");
        if($statement->rowCount()>0)
            return $statement->fetch();
        else
            return null;
    }
    
}