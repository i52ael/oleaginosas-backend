<?php
namespace ProductoBundle\Manager;

use Doctrine\ORM\EntityManager;
use ProductoBundle\Entity\Producto;
use ProductoBundle\Modelo\ModeloProducto;
use ProductoBundle\Modelo\ModeloUsuario;
use DateTime;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Doctrine\ORM\NoResultException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use ProductoBundle\Entity\BandaPrecio;
use ProductoBundle\Modelo\ModeloBandaPrecio;

class BandaPrecioManager{
    
    /**
     * @var EntityManager
     */
    protected $em;
    protected $repo;
    public function __construct(EntityManager $entityManager){
        $this->em=$entityManager;
        $this->setRepository();
    }
   
    protected function setRepository()
    {
        $this->repo=$this->em->getRepository(BandaPrecio::class);
    }

    public function recuperar($id){
       
        $bandaPrecio = /*new BandaPrecio();*/ $this->repo->find($id);        
        if(null==$bandaPrecio)
        {
        	throw new NoResultException();
        	//throw  new HttpException(500,"no se encontro algo");
        	//throw new ResourceNotFoundException();
        }
		$mbanda=new ModeloBandaPrecio();
		$mbanda->id=$bandaPrecio->getId();
		$mbanda->idProducto=$bandaPrecio->getIdProducto()->getId();
		$mbanda->nombreProducto=$bandaPrecio->getIdProducto()->getNombre();
		$mbanda->idtipoCambio=$bandaPrecio->getIdTipoCambio()->getId();
		$mbanda->fechaDesde=$bandaPrecio->getFechaDesde()->format('Y-m-d');
		$mbanda->fechaHasta=$bandaPrecio->getFechaHasta()->format('Y-m-d');;
		$mbanda->valorMinimo=$bandaPrecio->getValorMinimo();
		$mbanda->valorMaximo=$bandaPrecio->getValorMaximo();
		$mbanda->idTipoMoneda=$bandaPrecio->getIdTipoMoneda()->getId();       
        return $mbanda;
        
    }
    public function listaXProductoCount($idProducto)
    {
        $total=$this->repo->listaXProductoCount($idProducto)->getQuery()->getOneOrNullResult();
        return $total;
    }
    public function listaXProducto($idProducto,$page,$limit){

        $productos=$this->repo->listarXProducto($idProducto,$page,$limit)->getQuery()->getResult();
        return $productos;
    }
    
    
    public function guardar(ModeloBandaPrecio $mbanda,ModeloUsuario $user)
    {
        $dt = new DateTime();
        $dt->format('Y-m-d H:i:s');
        $banda=new BandaPrecio();
        $banda->setIdUsuarioCreacion($user->idUser);
        $banda->setIdUsuarioUltimaModificacion($user->idUser);  
        $banda->setFechaCreacion($dt);
        $banda->setFechaUltimaModificacion($dt);
        
        $banda->setFechaDesde($mbanda->fechaDesde);
        $banda->setFechaHasta($mbanda->fechaHasta);
        $banda->setValorMinimo($mbanda->valorMinimo);
        $banda->setValorMaximo($mbanda->valorMaximo);
        $banda->setIdProducto($this->em->getReference('ProductoBundle:Producto',$mbanda->idProducto));
        $banda->setIdTipoCambio($this->em->getReference('ProductoBundle:TipoCambio',$mbanda->idtipoCambio));
        $banda->setIdTipoMoneda($this->em->getReference('ProductoBundle:TipoMoneda',$mbanda->idTipoMoneda));
        $banda->setEliminado(false);
                
        
        $this->em->persist($banda);
        $this->em->flush();
        return $banda;
    }
    
    public function actualizar(ModeloBandaPrecio $mbanda,ModeloUsuario $user)
    {
        
        $dt = new DateTime();
        $dt->format('Y-m-d H:i:s');
        $banda=$this->repo->find($mbanda->id);
        if(null==$banda)
        {
            throw new NoResultException();
            
        }
        $banda->setIdUsuarioUltimaModificacion($user->idUser);
        $banda->setFechaUltimaModificacion($dt);        
        $banda->setFechaDesde($mbanda->fechaDesde);
        $banda->setFechaHasta($mbanda->fechaHasta);
        $banda->setValorMinimo($mbanda->valorMinimo);
        $banda->setValorMaximo($mbanda->valorMaximo);
        $banda->setIdTipoMoneda($this->em->getReference('ProductoBundle:TipoMoneda',$mbanda->idTipoMoneda));
        
        $this->em->persist($banda);
        $this->em->flush();
        return $banda;
        
    }
    
    public function baja($id,ModeloUsuario $user){
        $dt = new DateTime();
        $dt->format('Y-m-d H:i:s');
        $banda=$this->repo->find($id);
        if(null==$banda )
        {
            throw new NoResultException();
            
        }
        $banda->setIdUsuarioUltimaModificacion($user->idUser);
        $banda->setFechaUltimaModificacion($dt);
        $banda->setEliminado(true);
        $this->em->persist($banda);
        $this->em->flush();
        return $banda;
    }
    public function verificaFechas($idProducto,$fecha,$id)
    {
        return $this->repo->verificaEntreFechas($idProducto,$fecha,$id)->getQuery()->getResult();
    }
    
}