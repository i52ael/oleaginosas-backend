<?php
namespace ProductoBundle\Manager;

use Doctrine\ORM\EntityManager;
use ProductoBundle\Entity\RelacionEntidad;
use ProductoBundle\Modelo\ModeloUsuario;
use ProductoBundle\Entity\Entidad;
use DateTime;

class RelacionEntidadManager{
    
    /**
     * @var EntityManager
     */
    protected $em;
    protected $repo;
    public function __construct(EntityManager $entityManager){
        $this->em=$entityManager;
        $this->setRepository();
    }
   
    protected function setRepository()
    {
        $this->repo=$this->em->getRepository(RelacionEntidad::class);
    }

    public function recuperar($id){
       
        $relacion = $this->repo->find($id);
        if(null==$relacion)
        {
        	throw new NoResultException();
        }

        return $relacion;
        
    }
    public function relacionesVigentesPersona($idPersona)
    {
        return $this->repo->recuperarRelacionesVigentes($idPersona)->getQuery()->getResult();
    }
    public function relacionVigenteEntidad($idEntidad){
        return $this->repo->findOneBy(array("idEntidad"=>$idEntidad,"vigente"=>true));
    }
    public function guardar($idEntidad,$idPersona,$idTipoPersona,ModeloUsuario $user)
    {
        $dt = new DateTime();
        $dt->format('Y-m-d H:i:s');

        $relacion=new RelacionEntidad();
        $relacion->setIdUsuarioCreacion($user->idUser);
        $relacion->setIdUsuarioUltimaModificacion($user->idUser);
        $relacion->setFechaCreacion($dt);
        $relacion->setFechaUltimaModificacion($dt);

        $relacion->setIdEntidad($this->em->getReference('ProductoBundle:Entidad',$idEntidad));
        $relacion->setIdPersona($this->em->getReference('ProductoBundle:Persona',$idPersona));
        $relacion->setIdTipoPersona($this->em->getReference('ProductoBundle:TipoPersona',$idTipoPersona));
        $relacion->setVigente(true);
        $this->em->persist($relacion);
        $this->em->flush();
        return $relacion;
    }
    
    public function quitarVigente($idEntidad,$idTipoPersona,ModeloUsuario $user)
    {
        //$relacion=$this->repo->recuperarVigente($idEntidad,$idTipoPersona)->getQuery()->getOneOrNullResult();
        $relacion=$this->repo->findOneBy(array("idEntidad"=>$idEntidad,"idTipoPersona"=>$idTipoPersona,"vigente"=>true));
        if(null!=$relacion)
        {
            $dt = new DateTime();
            $dt->format('Y-m-d H:i:s');


            $relacion->setIdUsuarioCreacion($user->idUser);
            $relacion->setIdUsuarioUltimaModificacion($user->idUser);
            $relacion->setFechaCreacion($dt);
            $relacion->setFechaUltimaModificacion($dt);

            $relacion->setVigente(false);
            $this->em->persist($relacion);
            $this->em->flush();
            return $relacion;
        }
        return null;

        
    }
    public function recuperaVigente($idEntidad,$idTipoPersona){

        $relacion=$this->repo->findOneBy(array("idEntidad"=>$idEntidad,"idTipoPersona"=>$idTipoPersona,"vigente"=>true));
        if(null==$relacion){
            return null;
        }
        return $relacion;

    }

    /*public function baja($id,ModeloUsuario $user){
        $dt = new DateTime();
        $dt->format('Y-m-d H:i:s');
        
        $entidad=$this->repo->find($id);
        if(null==$entidad )
        {
            throw new NoResultException();
            
        }
        $entidad->setIdUsuarioUltimaModificacion($user->idUser);
        $entidad->setFechaUltimaModificacion($dt);
        $entidad->setVigente(false);
        $entidad->setEliminado(true);
        $this->em->persist($entidad);
        $this->em->flush();
        return $entidad;
    }*/
    
    
}