<?php
namespace ProductoBundle\Manager;

use Doctrine\ORM\EntityManager;
use ProductoBundle\Entity\Capacidad;
use ProductoBundle\Entity\Subproducto;
use ProductoBundle\Modelo\ModeloCapacidad;
use ProductoBundle\Modelo\ModeloSubproducto;
use ProductoBundle\Modelo\ModeloUsuario;
use DateTime;
use Doctrine\ORM\NoResultException;
class CapacidadManager{
    
    /**
     * @var EntityManager
     */
    protected $em;
    protected $repo;
    public function __construct(EntityManager $entityManager){
        $this->em=$entityManager;
        $this->setRepository();
    }
   
    protected function setRepository()
    {
        $this->repo=$this->em->getRepository(Capacidad::class);
    }

    public function recuperar($id){
       
        $capacidad = $this->repo->find($id);
        if(null==$capacidad)
        {
        	throw new NoResultException();
        }
		$mcapacidad=new ModeloCapacidad();

        $mcapacidad->id=$capacidad->getId();
        $mcapacidad->idMedida=$capacidad->getIdMedida()->getId();
        $mcapacidad->idProducto=$capacidad->getIdProducto()->getId();
        $mcapacidad->idEntidad=$capacidad->getIdEntidad()->getId();
        $mcapacidad->capacidadInstaladaNominal=$capacidad->getCapacidadInstaladaNominalMes();
        $mcapacidad->capacidadInstaladaReal=$capacidad->getCapacidadInstaladaRealMes();
        $mcapacidad->idMagnitud=$capacidad->getIdMedida()->getIdMagnitud()->getId();
        $mcapacidad->porcentajeRendimiento = $capacidad->getPorcentajeRendimiento();
        $mcapacidad->idProductoPrimario = $capacidad->getIdProductoPrimario();
        $mcapacidad->rendimiento = $capacidad->getRendimiento();
        return $mcapacidad;
    }

    public function listaCapacidades($idEmpresa){
        $capacidades=$this->repo->listarXEntidad($idEmpresa)->getQuery()->getResult();
        return $capacidades;
    }
    
    
    public function guardar(ModeloCapacidad $mcapacidad,ModeloUsuario $user)
    {

        $dt = new DateTime();
        $dt->format('Y-m-d H:i:s');
        $capacidad=new Capacidad();
        $capacidad->setIdMedida($this->em->getReference('ProductoBundle:Medida',$mcapacidad->idMedida));
        $capacidad->setIdProducto($this->em->getReference('ProductoBundle:Producto',$mcapacidad->idProducto));
        $capacidad->setIdEntidad($this->em->getReference('ProductoBundle:Entidad',$mcapacidad->idEntidad));
        $capacidad->setCapacidadInstaladaNominalMes($mcapacidad->capacidadInstaladaNominal);
        $capacidad->setCapacidadInstaladaRealMes($mcapacidad->capacidadInstaladaReal);
        $capacidad->setRendimiento($mcapacidad->rendimiento);
        $capacidad->setPorcentajeRendimiento($mcapacidad->porcentajeRendimiento);
        $capacidad->setIdProductoPrimario($mcapacidad->idProductoPrimario);
        $this->em->persist($capacidad);
        $this->em->flush();
        return $capacidad;
    }
    public function actualizar(ModeloCapacidad $mcapacidad,ModeloUsuario $user)
    {
        
        $dt = new DateTime();
        $dt->format('Y-m-d H:i:s');
        $capacidad=$this->repo->find($mcapacidad->id);
        if(null==$capacidad)
        {
            throw new NoResultException();
        }
        $capacidad->setIdMedida($this->em->getReference('ProductoBundle:Medida',$mcapacidad->idMedida));
        $capacidad->setIdProducto($this->em->getReference('ProductoBundle:Producto',$mcapacidad->idProducto));
        $capacidad->setCapacidadInstaladaNominalMes($mcapacidad->capacidadInstaladaNominal);
        $capacidad->setCapacidadInstaladaRealMes($mcapacidad->capacidadInstaladaReal);
        $capacidad->setRendimiento($mcapacidad->rendimiento);
        $capacidad->setPorcentajeRendimiento($mcapacidad->porcentajeRendimiento);
        $capacidad->setIdProductoPrimario($mcapacidad->idProductoPrimario);

        $this->em->persist($capacidad);
        $this->em->flush();
        return $capacidad;
        
    }
    public function baja($id,ModeloUsuario $user){
        $dt = new DateTime();
        $dt->format('Y-m-d H:i:s');
        $capacidad=$this->repo->find($id);
        if(null==$capacidad )
        {
            throw new NoResultException();
        }
        $this->em->remove($capacidad);
        $this->em->flush();
        return $capacidad;
    }
    public function verificaProducto($idProducto,$idEmpresa){
        $capacidades = $this->repo->recuperarXidProducto($idProducto,$idEmpresa)->getQuery()->getResult();
        return $capacidades;
    }
    
    
}