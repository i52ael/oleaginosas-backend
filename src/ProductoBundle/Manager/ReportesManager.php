<?php namespace ProductoBundle\Manager;

use Doctrine\ORM\EntityManager;

class ReportesManager
{
    /**
     * @var EntityManager
     */
    protected $em;
    protected $repo;

    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }
    public function reporteVentas($idTipoTransaccion, $idProducto,$idMedida, $fechaInicio, $fechaFinal)
    {
        $fechaInicio=$fechaInicio."-01";
        $fechaFinal=$fechaFinal."-01";

        $sql = "SELECT (SELECT tt.descripcion     FROM tipo_transaccion AS tt WHERE tt.id = vv.id_tipo_transaccion) AS tipo_transaccion , vv.id_tipo_transaccion,
                              (SELECT pp.nombre FROM producto AS pp WHERE pp.id = vv.id_producto) AS producto  , vv.id_producto ,
                              (SELECT ee.razon_social FROM entidad AS ee WHERE ee.id = vv.id_entidad_socio) AS entidad , vv.id_entidad_socio,
							(SELECT abreviacion FROM medida where id=$idMedida) abreviacion,

						      SUM (vv.cantidad_venta * medida_subproducto* fconversion(vv.id_medida_subproducto, $idMedida)) AS registrado,

                              (SELECT CASE WHEN SUM(ce.compromiso_entidad) IS NOT NULL THEN SUM(ce.compromiso_entidad * fconversion(ce.id_medida_producto,$idMedida)) ELSE 0.00 END
                                  FROM compromiso_entidades AS ce INNER JOIN campania AS c ON c.id = ce.id_campania
                                  WHERE ce.id_tipo_transaccion = vv.id_tipo_transaccion
                                    AND ce.id_producto = vv.id_producto
                                    AND ce.id_entidad = vv.id_entidad_socio
																		AND
																	(
																		date_trunc('month',c.fecha::TIMESTAMP) >= date_trunc('month', '$fechaInicio'::TIMESTAMP)
																		AND
																		date_trunc('month',c.fecha::TIMESTAMP) <= date_trunc('month','$fechaFinal'::TIMESTAMP)
																	)

																) AS comprometido



                              FROM ventas AS vv
																WHERE
																	vv.id_producto=$idProducto
																	AND vv.id_tipo_transaccion=$idTipoTransaccion

																	AND
																	(
																		date_trunc('month',vv.fecha_venta::TIMESTAMP) >= date_trunc('month', '$fechaInicio'::TIMESTAMP)
																		AND
																		date_trunc('month',vv.fecha_venta::TIMESTAMP) <= date_trunc('month','$fechaFinal'::TIMESTAMP)
																	)

                              GROUP BY vv.id_tipo_transaccion, vv.id_producto, vv.id_entidad_socio
                              ORDER BY vv.id_tipo_transaccion, vv.id_producto, vv.id_entidad_socio ;
                ";
         //var_dump($sql); die();
        $statement = $this->em->getConnection()->prepare($sql);
        $statement->execute();
        $ventas = $statement->fetchAll();
        return $ventas;
    }
    public function reporteVentasDetalle($idTipoTransaccion, $idProducto,$idEntidad,$idMedida, $fechaInicio, $fechaFinal)
    {

        $fechaInicio=$fechaInicio."-01";
        $fechaFinal=$fechaFinal."-01";

        $sql = "SELECT (SELECT tt.descripcion     FROM tipo_transaccion AS tt WHERE tt.id = vv.id_tipo_transaccion) AS tipo_transaccion , vv.id_tipo_transaccion,
                              (SELECT pp.nombre FROM producto AS pp WHERE pp.id = vv.id_producto) AS producto  , vv.id_producto ,
                              (SELECT ee.razon_social FROM entidad AS ee WHERE ee.id = vv.id_entidad_socio) AS entidad , vv.id_entidad_socio	,
                              vv.anio_venta,vv.mes_venta,
								(SELECT abreviacion FROM medida where id=$idMedida) abreviacion,
															SUM (vv.cantidad_venta * medida_subproducto* fconversion(vv.id_medida_subproducto, $idMedida)) AS registrado,

                              (SELECT CASE WHEN SUM(ce.compromiso_entidad) IS NOT NULL THEN SUM(ce.compromiso_entidad * fconversion(ce.id_medida_producto,$idMedida)) ELSE 0.00 END
                                  FROM compromiso_entidades AS ce INNER JOIN campania AS c ON c.id = ce.id_campania
                                  WHERE ce.id_tipo_transaccion = vv.id_tipo_transaccion
                                    AND ce.id_producto = vv.id_producto
                                    AND ce.id_entidad = vv.id_entidad_socio
																		AND
																	(
																		date_trunc('month',c.fecha::TIMESTAMP) >= date_trunc('month', '$fechaInicio'::TIMESTAMP)
																		AND
																		date_trunc('month',c.fecha::TIMESTAMP) <= date_trunc('month','$fechaFinal'::TIMESTAMP)
																	)

																) AS comprometido
                              FROM ventas AS vv
																WHERE
																	     vv.id_producto=$idProducto
																	AND vv.id_tipo_transaccion=$idTipoTransaccion
																	and vv.id_entidad_socio=$idEntidad

																	AND
																	(
																		date_trunc('month',vv.fecha_venta::TIMESTAMP) >= date_trunc('month', '$fechaInicio'::TIMESTAMP)
																		AND
																		date_trunc('month',vv.fecha_venta::TIMESTAMP) <= date_trunc('month','$fechaFinal'::TIMESTAMP)
																	)

                              GROUP BY vv.id_tipo_transaccion, vv.id_producto, vv.id_entidad_socio,vv.anio_venta,vv.mes_venta
                              ORDER BY vv.id_tipo_transaccion, vv.id_producto, vv.id_entidad_socio
                ";
        //var_dump($sql); die();
        $statement = $this->em->getConnection()->prepare($sql);
        $statement->execute();
        $ventas = $statement->fetchAll();
        return $ventas;
    }


    public function reporteCompras($idTipoTransaccion, $idProducto,$idMedida, $fechaInicio, $fechaFinal)
    {
        $fechaInicio=$fechaInicio."-01";
        $fechaFinal=$fechaFinal."-01";


         $sql = "SELECT (SELECT tt.descripcion     FROM tipo_transaccion AS tt WHERE tt.id = cc.id_tipo_transaccion) AS tipo_transaccion , cc.id_tipo_transaccion,
                              (SELECT ff.nombre FROM fase AS ff WHERE ff.id = cc.id_fase) AS fase                 ,
                              (SELECT pp.nombre FROM producto AS pp WHERE pp.id = cc.id_producto) AS producto  , cc.id_producto ,
                              (SELECT ee.razon_social FROM entidad AS ee WHERE ee.id = cc.id_entidad) AS entidad , cc.id_entidad	,
								(SELECT abreviacion FROM medida where id=$idMedida) abreviacion,
                              SUM ( cc.cantidad_compra * cc.medida_producto *  fconversion(cc.id_medida_producto, $idMedida)) AS registrado,
                              (SELECT CASE WHEN SUM(ce.compromiso_entidad * fconversion(ce.id_medida_producto, $idMedida)) IS NOT NULL THEN SUM(ce.compromiso_entidad * fconversion(ce.id_medida_producto, $idMedida))ELSE 0.00 END
                                  FROM compromiso_entidades AS ce INNER JOIN campania AS c ON c.id = ce.id_campania
                                  WHERE ce.id_tipo_transaccion = cc.id_tipo_transaccion
                                    AND ce.id_producto = cc.id_producto
                                    AND ce.id_entidad = cc.id_entidad
																		AND
																	(
																		date_trunc('month',c.fecha::TIMESTAMP) >= date_trunc('month', '$fechaInicio'::TIMESTAMP)
																		AND
																		date_trunc('month',c.fecha::TIMESTAMP) <= date_trunc('month','$fechaFinal'::TIMESTAMP)
																	)

																) AS comprometido
                              FROM compras AS cc
																WHERE
																	     cc.id_producto=$idProducto
																	AND cc.id_tipo_transaccion=$idTipoTransaccion
																	AND
																	(
																		date_trunc('month',cc.fecha_compra::TIMESTAMP) >= date_trunc('month', '$fechaInicio'::TIMESTAMP)
																		AND
																		date_trunc('month',cc.fecha_compra::TIMESTAMP) <= date_trunc('month','$fechaFinal'::TIMESTAMP)
																	)

                              GROUP BY cc.id_tipo_transaccion, cc.id_fase, cc.id_producto, cc.id_entidad
                              ORDER BY cc.id_tipo_transaccion, cc.id_fase, cc.id_producto, cc.id_entidad
                ";
        // var_dump($sql); die();
        $statement = $this->em->getConnection()->prepare($sql);
        $statement->execute();
        $compras = $statement->fetchAll();
        return $compras;
    }

    public function reporteProduccion($idTipoTransaccion, $idProducto, $idMedida,$fechaInicio, $fechaFinal)
    {
        $fechaInicio=$fechaInicio."-01";
        $fechaFinal=$fechaFinal."-01";


        $sql = "
        SELECT (SELECT tt.descripcion     FROM tipo_transaccion AS tt WHERE tt.id = pp.id_tipo_transaccion) AS tipo_transaccion , pp.id_tipo_transaccion,
                              (SELECT ff.nombre FROM fase AS ff WHERE ff.id = pp.id_fase) AS fase ,
                              (SELECT pp2.nombre FROM producto AS pp2 WHERE pp2.id = pp.id_producto) AS producto  , pp.id_producto ,
                              (SELECT ee.razon_social FROM entidad AS ee WHERE ee.id = pp.id_entidad) AS entidad , pp.id_entidad	,
								(SELECT abreviacion FROM medida where id=$idMedida) abreviacion,
                              SUM ( pp.cantidad_produccion * pp.medida_producto *  fconversion(pp.id_medida_producto, $idMedida)) AS registrado,
                              (SELECT CASE WHEN SUM(ce.compromiso_entidad * fconversion(ce.id_medida_producto, $idMedida)) IS NOT NULL THEN SUM(ce.compromiso_entidad * fconversion(ce.id_medida_producto, 6))ELSE 0.00 END
                                  FROM compromiso_entidades AS ce INNER JOIN campania AS c ON c.id = ce.id_campania
                                  WHERE ce.id_tipo_transaccion = pp.id_tipo_transaccion
                                    AND ce.id_producto = pp.id_producto
                                    AND ce.id_entidad = pp.id_entidad
																		AND
																	(
																		date_trunc('month',c.fecha::TIMESTAMP) >= date_trunc('month', '$fechaInicio'::TIMESTAMP)
																		AND
																		date_trunc('month',c.fecha::TIMESTAMP) <= date_trunc('month','$fechaFinal'::TIMESTAMP)
																	)

																) AS comprometido
                              FROM produccion2 AS pp
																WHERE
																	     pp.id_producto=$idProducto
																	AND  pp.id_tipo_transaccion=$idTipoTransaccion
																	AND
																	(
																		date_trunc('month',pp.fecha_hasta_periodo::TIMESTAMP) >= date_trunc('month', '$fechaInicio'::TIMESTAMP)
																		AND
																		date_trunc('month',pp.fecha_hasta_periodo::TIMESTAMP) <= date_trunc('month','$fechaFinal'::TIMESTAMP)
																	)

                              GROUP BY pp.id_tipo_transaccion, pp.id_fase, pp.id_producto, pp.id_entidad
                              ORDER BY pp.id_tipo_transaccion, pp.id_fase, pp.id_producto, pp.id_entidad
                ";
        // var_dump($sql); die();
        $statement = $this->em->getConnection()->prepare($sql);
        $statement->execute();
        $compras = $statement->fetchAll();
        return $compras;
    }

    public function reporteComprasDetalle($idTipoTransaccion, $idProducto,$idEntidad,$idMedida, $fechaInicio, $fechaFinal)
    {

        $fechaInicio=$fechaInicio."-01";
        $fechaFinal=$fechaFinal."-01";

        $sql = "  SELECT (SELECT tt.descripcion     FROM tipo_transaccion AS tt WHERE tt.id = cc.id_tipo_transaccion) AS tipo_transaccion , cc.id_tipo_transaccion,
                              (SELECT ff.nombre FROM fase AS ff WHERE ff.id = cc.id_fase) AS fase                 ,
                              (SELECT pp.nombre FROM producto AS pp WHERE pp.id = cc.id_producto) AS producto  , cc.id_producto ,
                              (SELECT ee.razon_social FROM entidad AS ee WHERE ee.id = cc.id_entidad) AS entidad , cc.id_entidad	,
								(SELECT abreviacion FROM medida where id=$idMedida) abreviacion,
																cc.anio_compra,cc.mes_compra													,
                              SUM ( cc.cantidad_compra * cc.medida_producto *  fconversion(cc.id_medida_producto, $idMedida)) AS registrado,

								 (SELECT CASE WHEN SUM(ce.compromiso_entidad * fconversion(ce.id_medida_producto, $idMedida)) IS NOT NULL THEN SUM(ce.compromiso_entidad * fconversion(ce.id_medida_producto, $idMedida))ELSE 0.00 END
                                  FROM compromiso_entidades AS ce INNER JOIN campania AS c ON c.id = ce.id_campania
                                  WHERE ce.id_tipo_transaccion = cc.id_tipo_transaccion
                                    AND ce.id_producto = cc.id_producto
                                    AND ce.id_entidad = cc.id_entidad
																		AND c.mes=cc.mes_compra

																		AND
																	(
																		date_trunc('month',c.fecha::TIMESTAMP) >= date_trunc('month', '$fechaInicio'::TIMESTAMP)
																		AND
																		date_trunc('month',c.fecha::TIMESTAMP) <= date_trunc('month','$fechaFinal'::TIMESTAMP)
																	)

																) AS comprometido

                              FROM compras AS cc
																WHERE
																	     cc.id_producto=$idProducto
																	AND cc.id_tipo_transaccion=$idTipoTransaccion
																	AND cc.id_entidad=$idEntidad
																	AND
																	(
																		date_trunc('month',cc.fecha_compra::TIMESTAMP) >= date_trunc('month', '$fechaInicio'::TIMESTAMP)
																		AND
																		date_trunc('month',cc.fecha_compra::TIMESTAMP) <= date_trunc('month','$fechaFinal'::TIMESTAMP)
																	)

                              GROUP BY cc.id_tipo_transaccion, cc.id_fase, cc.id_producto, cc.id_entidad,cc.anio_compra,cc.mes_compra
                              ORDER BY cc.id_tipo_transaccion, cc.id_fase, cc.id_producto, cc.id_entidad,cc.anio_compra,cc.mes_compra
                ";
         //var_dump($sql); die();
        $statement = $this->em->getConnection()->prepare($sql);
        $statement->execute();
        $compras = $statement->fetchAll();
        return $compras;
    }

    public function reporteProduccionDetalle($idTipoTransaccion, $idProducto,$idEntidad,$idMedida, $fechaInicio, $fechaFinal)
    {

        $fechaInicio=$fechaInicio."-01";
        $fechaFinal=$fechaFinal."-01";

        $sql = " SELECT (SELECT tt.descripcion     FROM tipo_transaccion AS tt WHERE tt.id = pp.id_tipo_transaccion) AS tipo_transaccion , pp.id_tipo_transaccion,
                              (SELECT ff.nombre FROM fase AS ff WHERE ff.id = pp.id_fase) AS fase ,
                              (SELECT pp2.nombre FROM producto AS pp2 WHERE pp2.id = pp.id_producto) AS producto  , pp.id_producto ,
                              (SELECT ee.razon_social FROM entidad AS ee WHERE ee.id = pp.id_entidad) AS entidad , pp.id_entidad	,
								(SELECT abreviacion FROM medida where id=$idMedida) abreviacion,
								date_part('year'::text, pp.fecha_hasta_periodo) anio_produccion,	date_part('month'::text,pp.fecha_hasta_periodo)	 mes_produccion,
                              SUM ( pp.cantidad_produccion * pp.medida_producto *  fconversion(pp.id_medida_producto, $idMedida)) AS registrado,
                              (SELECT CASE WHEN SUM(ce.compromiso_entidad * fconversion(ce.id_medida_producto, $idMedida)) IS NOT NULL THEN SUM(ce.compromiso_entidad * fconversion(ce.id_medida_producto, $idMedida))ELSE 0.00 END
                                  FROM compromiso_entidades AS ce INNER JOIN campania AS c ON c.id = ce.id_campania
                                  WHERE ce.id_tipo_transaccion = pp.id_tipo_transaccion
                                    AND ce.id_producto = pp.id_producto
                                    AND ce.id_entidad = pp.id_entidad
																		AND
																	(
																		date_trunc('month',c.fecha::TIMESTAMP) >= date_trunc('month', '$fechaInicio'::TIMESTAMP)
																		AND
																		date_trunc('month',c.fecha::TIMESTAMP) <= date_trunc('month','$fechaFinal'::TIMESTAMP)
																	)

																) AS comprometido
                              FROM produccion2 AS pp
																WHERE
																	     pp.id_producto=$idProducto
																	AND  pp.id_tipo_transaccion=$idTipoTransaccion
																	AND 	pp.id_entidad=$idEntidad
																	AND
																	(
																		date_trunc('month',pp.fecha_hasta_periodo::TIMESTAMP) >= date_trunc('month', '$fechaInicio'::TIMESTAMP)
																		AND
																		date_trunc('month',pp.fecha_hasta_periodo::TIMESTAMP) <= date_trunc('month','$fechaFinal'::TIMESTAMP)
																	)

                              GROUP BY pp.id_tipo_transaccion, pp.id_fase, pp.id_producto, pp.id_entidad, date_part('year'::text, pp.fecha_hasta_periodo) ,	date_part('month'::text,pp.fecha_hasta_periodo)
                              ORDER BY pp.id_tipo_transaccion, pp.id_fase, pp.id_producto, pp.id_entidad ,	date_part('year'::text, pp.fecha_hasta_periodo) ,	date_part('month'::text,pp.fecha_hasta_periodo)
                ";
        //var_dump($sql); die();
        $statement = $this->em->getConnection()->prepare($sql);
        $statement->execute();
        $compras = $statement->fetchAll();
        return $compras;
    }


    public function reporte01($fechaIni = '', $fechaFin = '')
    {
        $condicion = " WHERE 1=1 ";
        if ($fechaIni !== '' && $fechaFin === '') {
            $fechaFin = $fechaIni;
        } elseif ($fechaIni === '' && $fechaFin !== '') {
            $fechaIni = $fechaFin;
        } elseif ($fechaIni === '' && $fechaFin === '') {
            $fechaIni = date('Y-m-d');
            $fechaFin = date('Y-m-d');
        }
        $condicion .= " AND (aa.fecha_acopio BETWEEN '" . $fechaIni . "' AND '" . $fechaFin . "')";
        $sql = "SELECT
                (SELECT CONCAT(ee.id) FROM entidades AS ee WHERE ee.id = aa.id_entidad_proveedor) AS id_entidades,
                (SELECT CONCAT(ee.razon_social) FROM entidades AS ee WHERE ee.id = aa.id_entidad_proveedor) AS productor,
                (SELECT CONCAT(ee.nombre_regimen_entidad) FROM entidades AS ee WHERE ee.id = aa.id_entidad_proveedor) AS regimen
                , SUM(aa.cantidad_bruta_acopio) AS peso_bruto
                , SUM(aa.cantidad_neta_acopio) AS peso_neto
                FROM acopios AS aa
                {$condicion}
                GROUP BY aa.id_entidad_proveedor
                ORDER BY productor";
        $statement = $this->em->getConnection()->prepare($sql);
        $statement->execute();
        return $statement->fetchAll();
    }

    public function reporte01x($id_productor, $fechaIni, $fechaFin)
    {
        $sql = "SELECT
		        TO_CHAR(aa.fecha_acopio, 'YYYY-MM') AS aniomes
                , SUM(aa.cantidad_bruta_acopio) AS peso_bruto
                , SUM(aa.cantidad_neta_acopio) AS peso_neto
                FROM acopios AS aa
                WHERE (aa.fecha_acopio BETWEEN '" . $fechaIni . "' AND '" . $fechaFin . "') AND aa.id_entidad_proveedor = " . $id_productor . "
                GROUP BY aniomes
                ORDER BY aniomes";
        $statement = $this->em->getConnection()->prepare($sql);
        $statement->execute();
        return $statement->fetchAll();
    }

    public function reporte02($fechaIni = '', $fechaFin = '')
    {
        $condicion = " WHERE 1=1 ";
        if ($fechaIni !== '' && $fechaFin === '') {
            $fechaFin = $fechaIni;
        } elseif ($fechaIni === '' && $fechaFin !== '') {
            $fechaIni = $fechaFin;
        } elseif ($fechaIni === '' && $fechaFin === '') {
            $fechaIni = date('Y-m-d');
            $fechaFin = date('Y-m-d');
        }
        $condicion .= " AND (aa.fecha_acopio BETWEEN '" . $fechaIni . "' AND '" . $fechaFin . "')";
        $sql = "SELECT
                (SELECT CONCAT(ee.id) FROM entidades AS ee WHERE ee.id = aa.id_entidad) AS id_empresas
                , (SELECT CONCAT(ee.razon_social) FROM entidades AS ee WHERE ee.id = aa.id_entidad) AS empresa
                , (SELECT CONCAT(ee.nombre_regimen_entidad) FROM entidades AS ee WHERE ee.id = aa.id_entidad) AS regimen
                , SUM(cantidad_bruta_acopio) AS peso_bruto
                , SUM(cantidad_neta_acopio) AS peso_neto
                FROM acopios AS aa
                {$condicion}
                GROUP BY aa.id_entidad
                ORDER BY empresa";
        $statement = $this->em->getConnection()->prepare($sql);
        $statement->execute                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             ();
        return $statement->fetchAll();
    }

    public function reporte02x($id_empresa, $fechaIni, $fechaFin)
    {
        $sql = "SELECT
		        TO_CHAR(aa.fecha_acopio, 'YYYY-MM') AS aniomes
                , SUM(aa.cantidad_bruta_acopio) AS peso_bruto
                , SUM(aa.cantidad_neta_acopio) AS peso_neto
                FROM acopios AS aa
                WHERE (aa.fecha_acopio BETWEEN '" . $fechaIni . "' AND '" . $fechaFin . "') AND aa.id_entidad = " . $id_empresa . "
                GROUP BY aniomes
                ORDER BY aniomes";
        $statement = $this->em->getConnection()->prepare($sql);
        $statement->execute();
        return $statement->fetchAll();
    }
    public function reporteStock($idProducto,$idMedida,$fecha){
        $fecha=$fecha."-01";
        $sql="
                select p.nombre_producto, e.razon_social,
				(SELECT abreviacion FROM medida where id=$idMedida) abreviacion,

				sum(stock_final * p.medida_producto * fconversion(p.id_medida_producto,$idMedida)) stock
                from produccion2 p
                inner join entidad e on e.id=p.id_entidad
                where
                  p.id_producto=$idProducto
                  AND date_trunc('month',p.fecha_hasta_periodo::TIMESTAMP) =date_trunc('month', '$fecha'::TIMESTAMP)
                  group by p.nombre_producto, e.razon_social order by e.razon_social";
        $statement = $this->em->getConnection()->prepare($sql);
        $statement->execute();
        return $statement->fetchAll();

    }

    public function calculaValoresSaldoExportable($inicio_mes, $inicio_gest, $fin_mes, $fin_gest, $id_producto, $id_entidad){
        $sql = "select * from obtener_saldos_exportables_fechas($inicio_mes, $inicio_gest, $fin_mes, $fin_gest, $id_producto, $id_entidad)";
        $statement = $this->em->getConnection()->prepare($sql);
        $statement->execute();
        $datosExportacion = $statement->fetchAll();
        $resultado = array();

        foreach ($datosExportacion as $row){
            $total_grano_dispo = floatval($row["existencia_grano"]) + floatval($row["compra_ver"]) + floatval($row["compra_inv"]);
            $prod_teorica = $total_grano_dispo * (floatval($row["factor_conversion"])/100);
            $total_oferta = floatval($row["existencia"]) + $prod_teorica;
            $ventas_sme1 = floatval($row["venta_ene"]) + floatval($row["venta_feb"]) +
                floatval($row["venta_mar"]) + floatval($row["venta_abr"]) + floatval($row["venta_may"]) + floatval($row["venta_jun"]);
            $ventas_sem2 = floatval($row["venta_jul"]) + floatval($row["venta_ago"]) +
                floatval($row["venta_sep"]) + floatval($row["venta_oct"]) + floatval($row["venta_nov"]) + floatval($row["venta_dic"]);
            $total_exporta = $total_oferta - $ventas_sme1 - $ventas_sem2;
            array_push($resultado,  array($row["id_producto"], //0
                $row["producto"], //1
                $row["empresa"], //2
                $row["existencia_grano"], //3
                $row["compra_ver"],
                $row["compra_inv"],
                $total_grano_dispo,
                $row["factor_conversion"],
                $row["existencia"],
                $prod_teorica,
                $total_oferta,
                $row["venta_ene"],
                $row["venta_feb"],
                $row["venta_mar"],
                $row["venta_abr"],
                $row["venta_may"],
                $row["venta_jun"],
                $ventas_sme1,
                $row["venta_jul"],
                $row["venta_ago"],
                $row["venta_sep"],
                $row["venta_oct"],
                $row["venta_nov"],
                $row["venta_dic"],
                $ventas_sem2,
                round($total_exporta, 2), //25
                $row["nandina"], // 26
                $row["numero_identificacion"], //27
                $row["id_entidad"] //28
              ));
        }
        return $resultado;
    }

    public function resumenSaldoExportable($inicio_mes, $inicio_gest, $fin_mes, $fin_gest, $id_producto, $id_entidad){

        $datosExportacion = $this->calculaValoresSaldoExportable($inicio_mes, $inicio_gest, $fin_mes, $fin_gest, $id_producto, $id_entidad);
        $resultado = array();

        foreach ($datosExportacion as $row){
          $datosExport = $this->datosExportaciones($inicio_mes, $inicio_gest, $fin_mes, $fin_gest, $row[26], $row[27]);
          $dato = new Reporte();
          $dato->producto = $row[1];
          $dato->entidad = $row[2];
          $dato->teorico_exportable = $row[25];

          if(count($datosExport) > 0){
              $dato->cantidad_autorizada = $datosExport[0][0];
              $dato->cantidad_efec_exporta = $datosExport[0][1];
              $dato->saldo = $datosExport[0][2];
          } else {
              $dato->cantidad_autorizada = 0;
              $dato->cantidad_efec_exporta = 0;
              $dato->saldo = 0;
          }

          array_push($resultado, $dato);
        }

        return $resultado;
    }

    public function datosExportaciones($inicio_mes, $inicio_gest, $fin_mes, $fin_gest, $nandina, $nit){
      $url = "http://192.168.142.109/siexco/ws_cailex/index.php/resumenSaldosExportables/$inicio_mes/$inicio_gest/$fin_mes/$fin_gest/$nit/$nandina";
      $resultado = array();
      $respuesta = $this->servicio_VCI($url);
      if($respuesta != false){
        $arr = json_decode($respuesta,true);
        foreach ($arr as $row){
          if(isset($row['nit'])){
            array_push($resultado, array($row['cantidad_autorizada'], // 0
                $row['cantidad_efectiva_exportada'], //1
                $row['cantidad_autorizada'] - $row['cantidad_efectiva_exportada']) //2
              );
          }
        }
      }
      return $resultado;
    }

    public function resumenExportaciones($inicio_mes, $inicio_gest, $fin_mes, $fin_gest, $productos, $id_entidad){

        $resultado = array();
        $arr = array();
        $enti = "";

        if($id_entidad > 0){
          $sql = "select numero_identificacion from entidad where id=$id_entidad";
          $statement = $this->em->getConnection()->prepare($sql);
          $statement->execute();
          $valor = $statement->fetchAll();
          $enti = $valor[0]['numero_identificacion'];
        } else {
          $enti = $id_entidad;
        }

        if($productos == "0"){

          $url = "http://192.168.142.109/siexco/ws_cailex/index.php/resumenSaldosExportables/$inicio_mes/$inicio_gest/$fin_mes/$fin_gest/$enti/0";
          $resultado = array();
          $respuesta = $this->servicio_VCI($url);
          if($respuesta != false){
            $arr = json_decode($respuesta,true);
            foreach ($arr as $row){
              $dato = new Reporte();
              $sql = "select nombre from producto where nandina='".$row['nandina']."' ";
              $statement = $this->em->getConnection()->prepare($sql);
              $statement->execute();
              $valor = $statement->fetchAll();
              if(!empty($valor)){
                $dato->producto = $valor[0]['nombre'];
              } else {
                $dato->producto = '';
              }
              $dato->nandina = $row['nandina'];

              $nit = (int)$row['nit'];
              $sql = "select razon_social from entidad where numero_identificacion='$nit'";
              $statement = $this->em->getConnection()->prepare($sql);
              $statement->execute();
              $valor = $statement->fetchAll();
              if(!empty($valor)){
                $dato->entidad = $valor[0]['razon_social'];
              } else {
                $dato->entidad = '';
              }
              $dato->nit = $row['nit'];
              $dato->cantidad_autorizada = $row['cantidad_autorizada'];
              $dato->cantidad_efec_exporta = $row['cantidad_efectiva_exportada'];
              $dato->saldo = $row['cantidad_autorizada'] - $row['cantidad_efectiva_exportada'];
              array_push($resultado, $dato);
            }
          }
        } else {
          $list_prod = explode('-', $productos);
          foreach ($list_prod as $item) {
            $sql = "select nandina, nombre from producto where id=$item";
            $statement = $this->em->getConnection()->prepare($sql);
            $statement->execute();
            $valor= $statement->fetchAll();

            $nandina = $valor[0]['nandina'];
            $nombre = $valor[0]['nombre'];

            $url = "http://192.168.142.109/siexco/ws_cailex/index.php/resumenSaldosExportables/$inicio_mes/$inicio_gest/$fin_mes/$fin_gest/$enti/$nandina";
            $respuesta = $this->servicio_VCI($url);
            if($respuesta != false){
              $datos = json_decode($respuesta,true);

              $arr = array_merge($arr,$datos);

              foreach ($arr as $row){

              if(isset($row['nit'])){
                  $nit = $row['nit'];
                  $dato = new Reporte();
                  $dato->producto = $nombre;
                  $dato->nandina = $nandina;
                  $sql = "select razon_social from entidad where numero_identificacion='$nit'";
                  $statement = $this->em->getConnection()->prepare($sql);
                  $statement->execute();
                  $valor = $statement->fetchAll();
                  if(!empty($valor)){
                    $dato->entidad = $valor[0]['razon_social'];
                  } else {
                    $dato->entidad = '';
                  }
                  $dato->nit = $row['nit'];
                  $dato->cantidad_autorizada = $row['cantidad_autorizada'];
                  $dato->cantidad_efec_exporta = $row['cantidad_efectiva_exportada'];
                  $dato->saldo = $row['cantidad_autorizada'] - $row['cantidad_efectiva_exportada'];
                  array_push($resultado, $dato);
                }
              }
            }
          }
        }

        return $resultado;
    }

    function servicio_VCI($webservice){
        $url_webservice = $webservice;
        $curl = curl_init($url_webservice);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        try {
          $curl_respuesta = curl_exec($curl);
        } catch (Exception $e) {
          $curl_respuesta = false;
        }


        if ($curl_respuesta === FALSE){
            $info = curl_gestinfo($curl);
            curl_close($curl);
            return false;
        }
        curl_close($curl);
        return $curl_respuesta;
    }

    public function diferenciaDias($fecha){

        $sql=" select count(*) diferencia from dias_ano where fecha  BETWEEN  '$fecha' and now() AND habil=1;";
        $statement = $this->em->getConnection()->prepare($sql);
        $statement->execute();
        return $statement->fetchAll();

    }
}
class Reporte
{
    public $producto;
    public $entidad;
    public $nit;
    public $nandina;
    public $teorico_exportable;
    public $cantidad_autorizada;
    public $cantidad_efec_exporta;
    public $saldo;
}
