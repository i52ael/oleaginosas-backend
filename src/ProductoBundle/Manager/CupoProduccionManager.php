<?php
namespace ProductoBundle\Manager;

use Doctrine\ORM\Query\ResultSetMapping;
use ProductoBundle\Entity\ProduccionAgricola;
use Doctrine\ORM\EntityManager;

use ProductoBundle\Modelo\ModeloUsuario;
use DateTime;

use Doctrine\ORM\NoResultException;

use ProductoBundle\Modelo\ModeloProduccionAgricola;

class CupoProduccionManager{
    
    /**
     * @var EntityManager
     */
    protected $em;
    protected $repo;
    public function __construct(EntityManager $entityManager){
        $this->em=$entityManager;
        $this->setRepository();
    }
   
    protected function setRepository()
    {
        $this->repo=$this->em->getRepository(ProduccionAgricola::class);
    }

    public function recuperar($id){

       $mcupo=new ModeloProduccionAgricola();
        $cupo =  $this->repo->find($id);
        if(null==$cupo)
        {
        	throw new NoResultException();
        }
        $mcupo->id=$cupo->getId();
        $mcupo->estimado=$cupo->getEstimado();
        $mcupo->idProducto=$cupo->getIdProducto()->getId();
        $mcupo->idMedida=$cupo->getIdMedida()->getId();
        //$mcupo->fechaDesde=$cupo->getFechaDesde()->format('Y-m-d');;
        //$mcupo->fechaHasta=$cupo->getFechaHasta()->format('Y-m-d');;
        $mcupo->gestionAgricola=$cupo->getGestionAgricola();
        return $mcupo;
        
    }

    public function listaXProducto($idProducto){

        $cupos=$this->repo->listarXProducto($idProducto)->getQuery()->getResult();
        return $cupos;
    }
    
    
    public function guardar(ModeloProduccionAgricola $mcupo, ModeloUsuario $user)
    {
        $dt = new DateTime();
        $dt->format('Y-m-d H:i:s');
        $cupo=new  ProduccionAgricola();
        $cupo->setIdUsuarioCreacion($user->idUser);
        $cupo->setFechaCreacion($dt);

        $cupo->setIdUsuarioUltimaModificacion($user->idUser);
        $cupo->setFechaUltimaModificacion($dt);
        $cupo->setFechaDesde($mcupo->fechaDesde);
        $cupo->setFechaHasta($mcupo->fechaHasta);
        $cupo->setEstimado($mcupo->estimado);
        $cupo->setGestionAgricola($mcupo->gestionAgricola);
        $cupo->setIdProducto($this->em->getReference('ProductoBundle:Producto',$mcupo->idProducto));
        $cupo->setIdMedida($this->em->getReference('ProductoBundle:Medida',$mcupo->idMedida));
        $cupo->setEliminado(false);
        $this->em->persist($cupo);
        $this->em->flush();
        return $cupo;
    }
    
    public function actualizar(ModeloCupoProduccion $mcupo,ModeloUsuario $user)
    {
        
        $dt = new DateTime();
        $dt->format('Y-m-d H:i:s');
        $cupo=$this->repo->find($mcupo->id);
        if(null==$cupo)
        {
            throw new NoResultException();
            
        }
        $cupo->setIdUsuarioUltimaModificacion($user->idUser);
        $cupo->setFechaUltimaModificacion($dt);
        $cupo->setFechaDesde($mcupo->fechaDesde);
        $cupo->setFechaHasta($mcupo->fechaHasta);
        $cupo->setEstimado($mcupo->estimado);
        $cupo->setGestionAgricola($mcupo->gestionAgricola);
        $cupo->setIdProducto($this->em->getReference('ProductoBundle:Producto',$mcupo->idProducto));
        $cupo->setIdMedida($this->em->getReference('ProductoBundle:Medida',$mcupo->idMedida));

        $this->em->persist($cupo);
        $this->em->flush();
        return $cupo;
        
    }
    
    public function baja($id,ModeloUsuario $user){
        $dt = new DateTime();
        $dt->format('Y-m-d H:i:s');
        $cupo=$this->repo->find($id);
        if(null==$cupo )
        {
            throw new NoResultException();
        }
        $cupo->setIdUsuarioUltimaModificacion($user->idUser);
        $cupo->setFechaUltimaModificacion($dt);
        $cupo->setEliminado(true);
        $this->em->persist($cupo);
        $this->em->flush();
        return $cupo;
    }
    public function verificaFechas($idProducto,$gestion)
    {
        return $this->repo->verificaXgestion($idProducto,$gestion)->getQuery()->getResult();
    }

    
}