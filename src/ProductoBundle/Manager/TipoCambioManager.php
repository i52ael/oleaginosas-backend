<?php
namespace ProductoBundle\Manager;

use Doctrine\ORM\EntityManager;
use ProductoBundle\Repository\FaseRepository;
use ProductoBundle\Entity\Fase;



class TipoCambioManager 
{
	
	/**
	 * @var FaseRepository
	 */
	/**
	 * @var EntityManager
	 */
    protected $em;
    
	protected $repo;
	
	
	public function __construct(EntityManager $entityManager)
	{
		$this->em=$entityManager;
		$this->setRepository();
	}
	
	protected function setRepository()
	{
		$this->repo = $this->em->getRepository('ProductoBundle:TipoCambio');
	}
		
	
	public function verifica($dt){
		return $this->repo->verificaEntreFechas($dt)->getQuery()->getResult();
	}
	
	
	
}