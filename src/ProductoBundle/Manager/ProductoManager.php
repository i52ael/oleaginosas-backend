<?php
namespace ProductoBundle\Manager;

use Doctrine\ORM\EntityManager;
use ProductoBundle\Entity\Producto;
use ProductoBundle\Modelo\ModeloProducto;
use ProductoBundle\Modelo\ModeloUsuario;
use DateTime;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Doctrine\ORM\NoResultException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
class ProductoManager{
    
    /**
     * @var EntityManager
     */
    protected $em;
    protected $repo;
    public function __construct(EntityManager $entityManager){
        $this->em=$entityManager;
        $this->setRepository();
    }
   
    protected function setRepository()
    {
        $this->repo=$this->em->getRepository(Producto::class);
    }

    public function recuperar($id){
       
        $producto = $this->repo->find($id);        
        if(null==$producto)
        {
        	throw new NoResultException();
        	//throw  new HttpException(500,"no se encontro algo");
        	//throw new ResourceNotFoundException();
        }
		$mproducto=new ModeloProducto();
        $mproducto->descripcion=$producto->getDescripcion();
        $mproducto->nombre=$producto->getNombre();
        $mproducto->idComplejo=$producto->getIdComplejo()->getId();
        $mproducto->idFase=$producto->getIdFase()->getId();
        $mproducto->idMagnitud=$producto->getIdMedida()->getIdMagnitud()->getId();
        $mproducto->idMedida=$producto->getIdMedida()->getId();
        $mproducto->medida=$producto->getMedida();
        $mproducto->id=$producto->getId();
        $mproducto->unidadMedida=$producto->getIdMedida()->getNombre();
        $mproducto->codigo=$producto->getCodigo();
        $mproducto->nandina=$producto->getNandina();
        return $mproducto;
        
    }
    public function listaProductoCount($idComplejo)
    {
        $total=$this->repo->listaXComplejoCount($idComplejo)->getQuery()->getOneOrNullResult();
        return $total;
    }
    public function listaProducto($idComplejo,$page,$limit, $txt='') {
      // $productos=$this->repo->findBy(array("idComplejo"=>$idComplejo),array("nombre"=>"asc"),$limit,($page-1)*$limit);
        $productos=$this->repo->listarXComplejo($idComplejo,$page,$limit, $txt)->getQuery()->getResult();
        return $productos;
    }

    public function listaProductoXFase($idComplejo,$idFase){

        $productos=$this->repo->listarXComplejoXFase($idComplejo,$idFase)->getQuery()->getResult();
        return $productos;
    }
    
    
    public function guardar(ModeloProducto $mproducto,ModeloUsuario $user)
    {
        $dt = new DateTime();
        $dt->format('Y-m-d H:i:s');
        $producto=new Producto();
        $producto->setIdUsuarioCreacion($user->idUser);
        $producto->setIdUsuarioUltimaModificacion($user->idUser);  
        $producto->setFechaCreacion($dt);
        $producto->setFechaUltimaModificacion($dt);
        
        $producto->setNombre($mproducto->nombre);
        $producto->setIdMedida($this->em->getReference('ProductoBundle:Medida',$mproducto->idMedida));
        $producto->setIdFase($this->em->getReference('ProductoBundle:Fase',$mproducto->idFase));
        $producto->setIdComplejo($this->em->getReference('ProductoBundle:Complejo', $mproducto->idComplejo));
        
        $producto->setMedida($mproducto->medida);
        $producto->setVigente(true);
        $producto->setEliminado(false);
        $producto->setDescripcion($mproducto->descripcion);
        $producto->setCodigo($mproducto->codigo);
        $producto->setNandina($mproducto->nandina);
        
        $this->em->persist($producto);
        $this->em->flush();
        return $producto;
    }
    public function actualizar(ModeloProducto $mproducto,ModeloUsuario $user)
    {
        
        $dt = new DateTime();
        $dt->format('Y-m-d H:i:s');
        $producto=$this->repo->find($mproducto->id);
        if(null==$producto)
        {
            throw new NoResultException();
          
        }
        $producto->setIdUsuarioUltimaModificacion($user->idUser);
        $producto->setFechaUltimaModificacion($dt);
        $producto->setNombre($mproducto->nombre);
        $producto->setIdMedida($this->em->getReference('ProductoBundle:Medida',$mproducto->idMedida));
        $producto->setIdFase($this->em->getReference('ProductoBundle:Fase',$mproducto->idFase));
        $producto->setIdComplejo($this->em->getReference('ProductoBundle:Complejo', $mproducto->idComplejo));
        $producto->setMedida($mproducto->medida);
        $producto->setDescripcion($mproducto->descripcion);
        $producto->setCodigo($mproducto->codigo);
        $producto->setNandina($mproducto->nandina);
        $this->em->persist($producto);
        $this->em->flush();
        return $producto;
        
    }
    public function baja($id,ModeloUsuario $user){
        $dt = new DateTime();
        $dt->format('Y-m-d H:i:s');
        $producto=$this->repo->find($id);
        if(null==$producto )
        {
            throw new NoResultException();
            
        }
        $producto->setIdUsuarioUltimaModificacion($user->idUser);
        $producto->setFechaUltimaModificacion($dt);
        $producto->setVigente(false);
        $producto->setEliminado(true);
        $this->em->persist($producto);
        $this->em->flush();
        return $producto;
    }
    
    
}