<?php
namespace ProductoBundle\Manager;

use Doctrine\ORM\EntityManager;
use ProductoBundle\Entity\Entidad;
use ProductoBundle\Modelo\ModeloEntidad;
use ProductoBundle\Modelo\ModeloUsuario;
use DateTime;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Doctrine\ORM\NoResultException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
class EntidadManager{
    
    /**
     * @var EntityManager
     */
    protected $em;
    protected $repo;
    public function __construct(EntityManager $entityManager){
        $this->em=$entityManager;
        $this->setRepository();
    }
   
    protected function setRepository()
    {
        $this->repo=$this->em->getRepository(Entidad::class);
    }

    public function recuperar($id){
       
        $entidad = $this->repo->find($id);        
        if(null==$entidad)
        {
        	throw new NoResultException();
        	
        }
		$mentidad=new ModeloEntidad();
        $mentidad->id=$entidad->getId();
        $mentidad->razonSocial=$entidad->getRazonSocial();
        $mentidad->correo=$entidad->getCorreoElectronico();
        $mentidad->direccion=$entidad->getDireccion();
        $mentidad->fundempresa=$entidad->getFundempresa();
        $mentidad->id=$entidad->getId();
        $mentidad->numeroIdentificacion=$entidad->getNumeroIdentificacion();
        $mentidad->registroSanitario=$entidad->getRegistroSanitario();
        $mentidad->telefonos=$entidad->getTelefonos();
        $mentidad->idComplejo=$entidad->getIdComplejo()->getId();
        $mentidad->idFase=$entidad->getIdFase()->getId();
        $mentidad->idTipoDocumento=$entidad->getIdTipoDocumento()->getId();
        $mentidad->idTipoEntidad=$entidad->getIdTipoEntidad()->getId();
        $mentidad->beneficiario=$entidad->getBeneficiario();
        $mentidad->idRegimen=$entidad->getIdRegimen()->getId();
        $mentidad->idTipoRegistroSanitario = $entidad->getIdTipoRegistroSanitario()->getId();
        
        return $mentidad;
        
    }
    public function listaEntidadCount($idComplejo)
    {
        $total=$this->repo->listaXComplejoCount($idComplejo)->getQuery()->getOneOrNullResult();
        return $total;
    }
    public function listaEntidad($idComplejo,$page,$limit, $txt=''){
      // $productos=$this->repo->findBy(array("idComplejo"=>$idComplejo),array("nombre"=>"asc"),$limit,($page-1)*$limit);
        $productos=$this->repo->listarXComplejo($idComplejo,$page,$limit, $txt)->getQuery()->getResult();
        return $productos;
    }
    
    
    public function guardar(ModeloEntidad $mentidad,ModeloUsuario $user)
    {
        $dt = new DateTime();
        $dt->format('Y-m-d H:i:s');
        $entidad=new Entidad();
        $entidad->setIdUsuarioCreacion($user->idUser);
        $entidad->setIdUsuarioUltimaModificacion($user->idUser);  
        $entidad->setFechaCreacion($dt);
        $entidad->setFechaUltimaModificacion($dt);
        
        $entidad->setRazonSocial($mentidad->razonSocial);
        $entidad->setFundempresa($mentidad->fundempresa);
        $entidad->setRegistroSanitario($mentidad->registroSanitario);
        $entidad->setCorreoElectronico($mentidad->correo);
        $entidad->setTelefonos($mentidad->telefonos);
        $entidad->setDireccion(empty($mentidad->direccion)?'':$mentidad->direccion);
        $entidad->setNumeroIdentificacion($mentidad->numeroIdentificacion);
        $entidad->setIdTipoDocumento($this->em->getReference('ProductoBundle:TipoDocumento',$mentidad->idTipoDocumento));
        $entidad->setIdTipoEntidad($this->em->getReference('ProductoBundle:TipoEntidad',$mentidad->idTipoEntidad));
        $entidad->setIdFase($this->em->getReference('ProductoBundle:Fase',$mentidad->idFase));
        $entidad->setIdComplejo($this->em->getReference('ProductoBundle:Complejo', $mentidad->idComplejo));
        $entidad->setBeneficiario($mentidad->beneficiario);
        $entidad->setIdRegimen($this->em->getReference('ProductoBundle:Regimen',$mentidad->idRegimen));
        $entidad->setIdTipoRegistroSanitario($this->em->getReference('ProductoBundle:TipoRegistroSanitario',$mentidad->idTipoRegistroSanitario));
        $entidad->setIdDepartamento(1);
        $entidad->setIdMunicipio(1);        
        $entidad->setVigente(true);
        $entidad->setEliminado(false);

            
        
        $this->em->persist($entidad);
        $this->em->flush();
        return $entidad;
    }
    
    public function actualizar(ModeloEntidad $mentidad,ModeloUsuario $user)
    {
        
        $dt = new DateTime();
        $dt->format('Y-m-d H:i:s');
        $entidad=$this->repo->find($mentidad->id);
        if(null==$entidad)
        {
            throw new NoResultException();
          
        }
        $entidad->setIdUsuarioUltimaModificacion($user->idUser);
        $entidad->setFechaUltimaModificacion($dt);
        
        $entidad->setRazonSocial($mentidad->razonSocial);
        $entidad->setFundempresa($mentidad->fundempresa);
        $entidad->setRegistroSanitario($mentidad->registroSanitario);
        $entidad->setCorreoElectronico($mentidad->correo);
        $entidad->setTelefonos($mentidad->telefonos);
        $entidad->setDireccion($mentidad->direccion);
        $entidad->setNumeroIdentificacion($mentidad->numeroIdentificacion);
        $entidad->setIdTipoDocumento($this->em->getReference('ProductoBundle:TipoDocumento',$mentidad->idTipoDocumento));
        $entidad->setIdTipoEntidad($this->em->getReference('ProductoBundle:TipoEntidad',$mentidad->idTipoEntidad));
        $entidad->setIdFase($this->em->getReference('ProductoBundle:Fase',$mentidad->idFase));
        $entidad->setBeneficiario($mentidad->beneficiario);
        $entidad->setIdRegimen($this->em->getReference('ProductoBundle:Regimen',$mentidad->idRegimen));
        $entidad->setIdTipoRegistroSanitario($this->em->getReference('ProductoBundle:TipoRegistroSanitario',$mentidad->idTipoRegistroSanitario));
        //$entidad->setIdDepartamento(1);
        $entidad->setIdMunicipio(1);
        
        $this->em->persist($entidad);
        $this->em->flush();
        return $entidad;
        
    }
    public function baja($id,ModeloUsuario $user){
        $dt = new DateTime();
        $dt->format('Y-m-d H:i:s');
        
        $entidad=$this->repo->find($id);
        if(null==$entidad )
        {
            throw new NoResultException();
            
        }
        $entidad->setIdUsuarioUltimaModificacion($user->idUser);
        $entidad->setFechaUltimaModificacion($dt);
        $entidad->setVigente(false);
        $entidad->setEliminado(true);
        $this->em->persist($entidad);
        $this->em->flush();
        return $entidad;
    }
    public function listaXComplejoXFase($idComplejo,$idFase)
    {
    	$ents=$this->repo->listarXComplejoXFase($idComplejo,$idFase)->getQuery()->getResult();
    	return $ents;
    }
    public function recuperarXnit($nit){
        //var_dump($nit);die();
        return $this->repo->findOneBy(array("numeroIdentificacion"=>$nit));
    }
    public function cbxTipoRegistroSanitario()
    {
        $statement = $this->em->getConnection()->query("SELECT
        tipo_registro_sanitario.id AS data,
        tipo_registro_sanitario.abreviacion AS label
        FROM
        tipo_registro_sanitario
        ORDER BY tipo_registro_sanitario.orden");
        return $statement->fetchAll();
    }
    
}