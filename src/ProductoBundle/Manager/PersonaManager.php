<?php
namespace ProductoBundle\Manager;

use Doctrine\ORM\EntityManager;
use ProductoBundle\Entity\Persona;
use ProductoBundle\Modelo\ModeloPersona;
use ProductoBundle\Modelo\ModeloUsuario;
use DateTime;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Doctrine\ORM\NoResultException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
class PersonaManager{
    
    /**
     * @var EntityManager
     */
    protected $em;
    protected $repo;
    public function __construct(EntityManager $entityManager){
        $this->em=$entityManager;
        $this->setRepository();
    }
   
    protected function setRepository()
    {
        $this->repo=$this->em->getRepository(Persona::class);
    }

    public function recuperar($id){
       
        $persona = $this->repo->find($id);
        if(null==$persona)
        {
        	throw new NoResultException();
        }
		$mpersona=new ModeloPersona();
        $mpersona->id=$persona->getId();
        $mpersona->nombres=$persona->getNombres();
        $mpersona->primerApellido=$persona->getPrimerApellido();
        $mpersona->segundoApellido=$persona->getSegundoApellido();
        $mpersona->direccion=$persona->getDireccion();
        $mpersona->telefonos=$persona->getTelefono();
        $mpersona->correo=$persona->getCorreo();
        $mpersona->numeroIdentificacion=$persona->getNumeroIdentificacion();
        $mpersona->idTipoDocumento=$persona->getIdTipoDocumento()->getId();
        return $mpersona;
    }

    public function recuperarNumero($numeroIdentificacion){

        $persona = $this->repo->findOneBy(array("numeroIdentificacion"=>$numeroIdentificacion));
        if(null==$persona)
        {
            return null;
        }
        $mpersona=new ModeloPersona();
        $mpersona->id=$persona->getId();
        $mpersona->nombres=$persona->getNombres();
        $mpersona->primerApellido=$persona->getPrimerApellido();
        $mpersona->segundoApellido=$persona->getSegundoApellido();
        $mpersona->direccion=$persona->getDireccion();
        $mpersona->telefonos=$persona->getTelefono();
        $mpersona->correo=$persona->getCorreo();
        $mpersona->numeroIdentificacion=$persona->getNumeroIdentificacion();
        $mpersona->idTipoDocumento=$persona->getIdTipoDocumento()->getId();

        return $mpersona;

    }

    /*
    public function listaEntidadCount($idComplejo)
    {
        $total=$this->repo->listaXComplejoCount($idComplejo)->getQuery()->getOneOrNullResult();
        return $total;
    }
    public function listaEntidad($idComplejo,$page,$limit){
      // $productos=$this->repo->findBy(array("idComplejo"=>$idComplejo),array("nombre"=>"asc"),$limit,($page-1)*$limit);
        $productos=$this->repo->listarXComplejo($idComplejo,$page,$limit)->getQuery()->getResult();
        return $productos;
    }*/
    
    
    public function guardar(ModeloPersona $mpersona,ModeloUsuario $user)
    {
        $dt = new DateTime();
        $dt->format('Y-m-d H:i:s');
        $persona=new Persona();
        $persona->setIdUsuarioCreacion($user->idUser);
        $persona->setIdUsuarioUltimaModificacion($user->idUser);
        $persona->setFechaCreacion($dt);
        $persona->setFechaUltimaModificacion($dt);

        $persona->setNombres($mpersona->nombres);
        $persona->setPrimerApellido($mpersona->primerApellido);
        $persona->setSegundoApellido($mpersona->segundoApellido);
        $persona->setDireccion($mpersona->direccion);
        $persona->setTelefono($mpersona->telefonos);
        $persona->setCorreo($mpersona->correo);
        $persona->setNumeroIdentificacion($mpersona->numeroIdentificacion);
        $persona->setIdTipoDocumento($this->em->getReference('ProductoBundle:TipoDocumento',$mpersona->idTipoDocumento));

        $persona->setVigente(true);
        $persona->setEliminado(false);
        
        $this->em->persist($persona);
        $this->em->flush();
        return $persona;
    }
    
    public function actualizar(ModeloPersona $mpersona,ModeloUsuario $user)
    {
        
        $dt = new DateTime();
        $dt->format('Y-m-d H:i:s');
        $persona= $this->repo->find($mpersona->id);
        if(null==$persona)
        {
            throw new NoResultException();
          
        }
        $persona->setIdUsuarioUltimaModificacion($user->idUser);
        $persona->setFechaUltimaModificacion($dt);
        $persona->setNombres($mpersona->nombres);
        $persona->setPrimerApellido($mpersona->primerApellido);
        $persona->setSegundoApellido($mpersona->segundoApellido);
        $persona->setDireccion($mpersona->direccion);
        $persona->setTelefono($mpersona->telefonos);
        $persona->setCorreo($mpersona->correo);
        $persona->setNumeroIdentificacion($mpersona->numeroIdentificacion);
        $persona->setIdTipoDocumento($this->em->getReference('ProductoBundle:TipoDocumento',$mpersona->idTipoDocumento));

        $this->em->persist($persona);
        $this->em->flush();
        return $persona;
        
    }
    /*public function baja($id,ModeloUsuario $user){
        $dt = new DateTime();
        $dt->format('Y-m-d H:i:s');
        
        $entidad=$this->repo->find($id);
        if(null==$entidad )
        {
            throw new NoResultException();
            
        }
        $entidad->setIdUsuarioUltimaModificacion($user->idUser);
        $entidad->setFechaUltimaModificacion($dt);
        $entidad->setVigente(false);
        $entidad->setEliminado(true);
        $this->em->persist($entidad);
        $this->em->flush();
        return $entidad;
    }*/
    
    
}