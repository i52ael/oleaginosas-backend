<?php
namespace ProductoBundle\Manager;

use Doctrine\ORM\EntityManager;
use ProductoBundle\Repository\FaseRepository;
use ProductoBundle\Entity\Fase;



class TipoDocumentoManager 
{
	
	/**
	 * @var FaseRepository
	 */
	/**
	 * @var EntityManager
	 */
    protected $em;
    
	protected $repo;
	
	
	public function __construct(EntityManager $entityManager)
	{
		$this->em=$entityManager;
		$this->setRepository();
	}
	
	protected function setRepository()
	{
		$this->repo = $this->em->getRepository('ProductoBundle:TipoDocumento');
	}
		
	
	public function listar($individual){
	    return $this->repo->listar($individual)->getQuery()->getResult();
	}
	
	
	
}