<?php
 namespace ProductoBundle\Manager;
 
 
use Doctrine\ORM\EntityManager;
    
 
 class MagnitudManager {
   
     protected $em;
     
     protected $repo;
     
    
   public function __construct(EntityManager $entityManager)
   {
       $this->em=$entityManager;
       $this->setRepository();
   }
   
    protected function setRepository()
    {
        $this->repo=$this->em->getRepository("ProductoBundle:Magnitud");
    }
    public function listar(){
     return   $this->repo->listar()->getQuery()->getResult();
    }
     
 }
