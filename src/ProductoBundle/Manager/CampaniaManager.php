<?php
namespace ProductoBundle\Manager;

use Doctrine\ORM\EntityManager;
use ProductoBundle\Entity\TipoTransaccion;



class CampaniaManager
{

	/**
	 * @var EntityManager
	 */
    protected $em;
    
	protected $repo;
	
	
	public function __construct(EntityManager $entityManager)
	{
		$this->em=$entityManager;
		$this->setRepository();
	}
	
	protected function setRepository()
	{
		$this->repo = $this->em->getRepository('ProductoBundle:Campania');
	}

	public function listarGestion($gestion){
	    return $this->repo->findBy(array("gestion"=>$gestion));
	}

	public function listaGestionAgricola($gestion){
	    return $this->repo->listarGestiones($gestion)->getQuery()->getResult();
    }

    public function listaGestiones($gestion){
        return $this->repo->listaGestiones($gestion)->getQuery()->getResult();
    }
	
	
}