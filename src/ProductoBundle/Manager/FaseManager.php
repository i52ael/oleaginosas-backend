<?php
namespace ProductoBundle\Manager;

use Doctrine\ORM\EntityManager;
use ProductoBundle\Repository\FaseRepository;
use ProductoBundle\Entity\Fase;



class FaseManager 
{
	
	/**
	 * @var FaseRepository
	 */
	/**
	 * @var EntityManager
	 */
    protected $em;
    
	protected $repo;
	
	
	public function __construct(EntityManager $entityManager)
	{
		$this->em=$entityManager;
		$this->setRepository();
	}
	
	protected function setRepository()
	{
		$this->repo = $this->em->getRepository('ProductoBundle:Fase');
	}
	
	
	public function create(Fase $fase)
	{			
		$this->em->persist($fase);
		$this->em->flush();
		return null;
	}
	
	/**
	 * @param Fase $fase
	 * @return Fase
	 */
	public function updateFase(Fase $fase)
	{
		$this->em->persist($fase);
		$this->em->flush();
		return $fase;
	}
	
	
	public function listar(){
	    return $this->repo->listar()->getQuery()->getResult();
	}
	public function listarXorden($orden){
	    return $this->repo->findBy(array("orden"=>$orden));
    }
	
	
	
}