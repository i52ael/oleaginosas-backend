<?php
namespace ProductoBundle\Manager;

use Doctrine\ORM\EntityManager;
use ProductoBundle\Repository\FaseRepository;
use ProductoBundle\Entity\Fase;



class TipoMonedaManager 
{
	
	/**
	 * @var FaseRepository
	 */
	/**
	 * @var EntityManager
	 */
    protected $em;
    
	protected $repo;
	
	
	public function __construct(EntityManager $entityManager)
	{
		$this->em=$entityManager;
		$this->setRepository();
	}
	
	protected function setRepository()
	{
		$this->repo = $this->em->getRepository('ProductoBundle:TipoMoneda');
	}
		
	
	public function listar(){
	    return $this->repo->listar()->getQuery()->getResult();
	}
	
	
	
}