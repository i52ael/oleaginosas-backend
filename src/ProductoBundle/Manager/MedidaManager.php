<?php
 namespace ProductoBundle\Manager;
 
use Doctrine\ORM\EntityManager;

        
 
 class MedidaManager{
 
     protected $em;
    protected  $repo;
    
   public function __construct(EntityManager $entityManager)
   {
       $this->em=$entityManager;
       $this->setRepository();
   }
   
    protected function setRepository()
    {
        $this->repo=$this->em->getRepository("ProductoBundle:Medida");
    }

    
    public function listXidMagnitud($idMagnitud){
        $medidas=$this->repo->medidasXmagnitud($idMagnitud)->getQuery()->getResult();
        return $medidas;
    }
       
     
 }
