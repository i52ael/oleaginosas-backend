<?php
namespace ProductoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use JMS\DiExtraBundle\Annotation as DI;
use ProductoBundle\Manager\MagnitudManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * @Route("/magnitud")
 **/
class MagnitudController extends Controller{
    protected  $magnitudManager;
    
    /**
     * @DI\InjectParams({
     * "magnitudManager"=@DI\Inject("api.manager.magnitud")
     * })
     * 
     * **/
    public function __construct(MagnitudManager $magnitudManager){
        $this->magnitudManager=$magnitudManager;
        
    }
    /**
     * @Route("/lista")
     * @Method("GET")
     * */
    public function listaMagnitudAction(Request $request){
              
        $magnitudes=$this->magnitudManager->listar();
        return $this->json($magnitudes,Response::HTTP_OK);
    }
   
    
    
}

