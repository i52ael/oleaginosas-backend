<?php
namespace ProductoBundle\Controller;
use ProductoBundle\Manager\TipoTransaccionManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use ProductoBundle\Manager\SubproductoManager;
use JMS\DiExtraBundle\Annotation as DI;
use ProductoBundle\Modelo\ModeloSubproducto;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use ProductoBundle\Entity\Subproducto;

/**
 * @Security("is_granted('ROLE_USER')")
 * @Route("/tipotransaccion")
 * **/
class TipoTransaccionController extends Controller{
    use \ProductoBundle\Helper\Helper;

    protected $tipoTransaccionManager;
    /**
     * @DI\InjectParams({
     * "tipoTransaccionManager"=@DI\Inject("api.manager.tipoTransaccion")
     * })
     *
     */
    public function __construct(TipoTransaccionManager $tipoTransaccionManager){
        
        $this->tipoTransaccionManager=$tipoTransaccionManager;
    }
    
    /**
     * @Route("/lista")
     * @Method("GET")
     * **/
    public function listaTiposTransaccion(){

        $tipos=$this->tipoTransaccionManager->listar();
        return $this->json($tipos,Response::HTTP_OK);
    }
    

    
}