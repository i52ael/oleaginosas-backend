<?php
namespace ProductoBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use ProductoBundle\Manager\SubproductoManager;
use JMS\DiExtraBundle\Annotation as DI;
use ProductoBundle\Modelo\ModeloSubproducto;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use ProductoBundle\Entity\Subproducto;

/**
 * @Security("is_granted('ROLE_USER')")
 * @Route("/subproducto")
 * **/
class SubproductoController extends Controller{
    use \ProductoBundle\Helper\Helper;

    protected $subproductoManager;
    /**
     * @DI\InjectParams({
     * "subproductoManager"=@DI\Inject("api.manager.subproducto")
     * })
     *
     */
    public function __construct(SubproductoManager $subproductoManager){
        
        $this->subproductoManager=$subproductoManager;
    }
    
    /**
     * @Route("/lista/{idEntidad}")
     * @Method("GET")
     * **/
    public function listaSubProductoAction($idEntidad){

        $productos=$this->subproductoManager->listaSubproductos($idEntidad);
        return $this->json($productos,Response::HTTP_OK);
    }
    
    /**
     * @Route("/guardar")
     * @Method("POST") 
     * @ParamConverter("msproducto", converter = "fos_rest.request_body")
     * */
    public function guardarSubProductoAction(ModeloSubproducto $msproducto,Request $request,ConstraintViolationListInterface $constraints){
        
        $user=$this->ObtenerUser($request);
        
        if ($constraints->count()) {
            return $this->json(array("errores"=>'Peticion incorrecta '. $constraints),Response::HTTP_BAD_REQUEST);
        }
        if ($msproducto->id ==null) // si es null es nuevo
        $respuesta=$this->subproductoManager->guardar($msproducto,$user);
        else 
            $respuesta=$this->subproductoManager->actualizar($msproducto,$user);
        return $this->json($respuesta,Response::HTTP_OK);
        
    }
    
    /**
     * @Route("/baja/{id}")
     * @Method("GET")
     **/
    public function bajaAction($id,Request $request)
    {
        $user=$this->ObtenerUser($request);
        $mproducto=$this->subproductoManager->baja($id,$user);
        return $this->json($mproducto);
    }
    
    /**
     * @Route("/recuperar/{id}")
     * @Method("GET")
     **/
    public function recuperarAction($id)
    {
        $mproducto=$this->subproductoManager->recuperar($id);
        return $this->json($mproducto);        
        
    }
    
    
}