<?php
namespace ProductoBundle\Controller;

use ProductoBundle\Entity;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use ProductoBundle\Manager\FaseManager;
use JMS\DiExtraBundle\Annotation as DI;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;


/**
 * @Route("/fase")
 * **/
class FaseController extends Controller{
    
    protected $faseManager;
    /**
     * @DI\InjectParams({
     * "faseManager"=@DI\Inject("api.manager.fase")
     * })
     **/
    
    public function __construct(FaseManager $faseManager){
        $this->faseManager=$faseManager;
    }
    /**
     * @Route("/lista")
     * @Method("GET")
     * */
    public function listaFaseAction(){
        $fases=$this->faseManager->listar();
        return $this->json($fases,Response::HTTP_OK);
    }

    /**
     * @Route("/recuperar-orden/{orden}")
     * @Method("GET")
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function recuperarXorden($orden){
        $fase=$this->faseManager->listarXorden($orden);
        if(null==$fase){
            return $this->json(Response::HTTP_NO_CONTENT);
        }
        return $this->json($fase,Response::HTTP_OK);


    }
    
    
}