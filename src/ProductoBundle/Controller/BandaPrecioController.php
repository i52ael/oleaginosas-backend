<?php
namespace ProductoBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use ProductoBundle\Manager\ProductoManager;
use JMS\DiExtraBundle\Annotation as DI;
use ProductoBundle\Modelo\ModeloProducto;
use Symfony\Component\Translation\Exception\NotFoundResourceException;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use ProductoBundle\Entity\Producto;
use ProductoBundle\Manager\BandaPrecioManager;
use ProductoBundle\Modelo\ModeloBandaPrecio;
use ProductoBundle\Manager\TipoMonedaManager;
use ProductoBundle\Manager\TipoCambioManager;



/**
 * @Security("is_granted('ROLE_USER')")
 */

/**
 * @Route("/banda")
 * **/
class BandaPrecioController extends Controller{
    use \ProductoBundle\Helper\Helper;

    protected $bandaManager;
    protected $tipoMonedaMaganer;
    protected  $tipoCambioManager;
    /**
     * @DI\InjectParams({
     * "bandaManager"=@DI\Inject("api.manager.banda"),
     * "tipoMonedaManager"=@DI\Inject("api.manager.tipoMoneda"),
     * * "tipoCambioManager"=@DI\Inject("api.manager.tipoCambio")
     * })
     *
     */
    public function __construct(BandaPrecioManager $bandaManager,TipoMonedaManager $tipoMonedaManager,TipoCambioManager $tipoCambioManager){
        
        $this->bandaManager=$bandaManager;
        $this->tipoMonedaMaganer=$tipoMonedaManager;
        $this->tipoCambioManager=$tipoCambioManager;
    }
    
    /**
     * @Route("/lista/{idProducto}/{page}/{limit}")
     * @Method("GET")
     * **/
    public function listaBandaAction($idProducto,$page,$limit){
        $total=$this->bandaManager->listaXProductoCount($idProducto);        
        $bandas=$this->bandaManager->listaXProducto($idProducto,$page,$limit);
        return $this->json(array("total"=>$total['total'],"datos"=>$bandas),Response::HTTP_OK);
    }
    
    /**
     * @Route("/guardar")
     * @Method("POST") 
     * @ParamConverter("mbanda", converter = "fos_rest.request_body")
     * */
    public function guardarBandaAction(ModeloBandaPrecio $mbanda,Request $request,ConstraintViolationListInterface $constraints){


        $user=$this->ObtenerUser($request);
			


        if ($constraints->count()) {
            return $this->json(array("error"=>'Peticion incorrecta '. $constraints),Response::HTTP_BAD_REQUEST);
        }
        $dt=date('Y-m-d');
        
        $tipocambio=$this->tipoCambioManager->verifica($dt);
        if(null==$tipocambio)
        {
        	return $this->json(array("mensaje"=>"No se encontro tipo de cambio"),Response::HTTP_PARTIAL_CONTENT);
        }
       //var_dump($tipocambio);
       //die();
		
        $mbanda->idtipoCambio=$tipocambio[0]->getId();
        $result=$this->bandaManager->verificaFechas($mbanda->idProducto,$mbanda->fechaDesde,$mbanda->id);
        if($result!=null){
            return $this->json(array("error"=>"La fecha desde intersecta con el intervalo de fechas de una banda anterior"),Response::HTTP_BAD_REQUEST);
        }

        if ($mbanda->id ==null) // si es null es nuevo
            $respuesta=$this->bandaManager->guardar($mbanda,$user);
        else
        $respuesta=$this->bandaManager->actualizar($mbanda, $user);
        return $this->json($respuesta,Response::HTTP_OK);
        
    }
    
    /**
     * @Route("/baja/{id}")
     * @Method("GET")
     **/
    public function bajaAction($id,Request $request)
    {
        $user=$this->ObtenerUser($request);
        
        $mbanda=$this->bandaManager->baja($id,$user);
        return $this->json($mbanda,Response::HTTP_OK);
        
    }
    
    
    /**
     * @Route("/recuperar/{id}")
     * @Method("GET")
     **/
    public function recuperarAction($id)
    {
        $mproducto=$this->bandaManager->recuperar($id);
        return $this->json($mproducto,Response::HTTP_OK);        
        
    }
    
    /**
     * @Route("/tipomoneda")
     * @Method("GET")
     **/
    public function recuperarTiposMonedaAction()
    {
        $monedas=$this->tipoMonedaMaganer->listar();
        return $this->json($monedas,Response::HTTP_OK);
        
    }

    /**
     * @Route("/fechas/{idProducto}/{fechaDesde}")
     * @Method("GET")
     */
    public function validaFechas($idProducto,$fechaDesde){
        $result=$this->bandaManager->verificaFechas($idProducto,$fechaDesde);

            return $this->json($result,Response::HTTP_OK);

    }
    
    /**
     * @Route("/tipocambio")
     * @Method("GET")
     */
    public function recuperaTipoCambio(){
    	$dt=date('Y-m-d');
    	$result=$this->tipoCambioManager->verifica($dt);
    	
    	return $this->json($result,Response::HTTP_OK);
    	
    }
    
    
}