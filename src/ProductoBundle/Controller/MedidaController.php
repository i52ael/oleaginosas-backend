<?php
namespace ProductoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use  Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use JMS\DiExtraBundle\Annotation as DI;
use ProductoBundle\Manager\MedidaManager;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
/**
 * @Route("/medida")
 * **/
class MedidaController extends Controller{
    
    protected $medidaManager;
    /**
     * @DI\InjectParams({
     * "medidaManager"=@DI\Inject("api.manager.medida")
     * })
     * 
     */
    
    public function __construct(MedidaManager $medidaManager){
        
        $this->medidaManager=$medidaManager;
    }
    /**
     * @Route("/lista/{idMagnitud}")
     * @Method("GET")
     * **/
    public function listaMedidaXMagnitudAction($idMagnitud){
        $medidas=$this->medidaManager->listXidMagnitud($idMagnitud);
        return $this->json($medidas,Response::HTTP_OK);
    }
    
}