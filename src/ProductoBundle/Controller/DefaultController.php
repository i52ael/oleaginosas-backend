<?php

namespace ProductoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use ProductoBundle\Manager\ReportesManager;
use JMS\DiExtraBundle\Annotation as DI;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * @Route("/reportes")
 **/
class DefaultController extends Controller
{
    /**
     * @var ReportesManager
     */
    protected $reportesManager;

    /**
     * @DI\InjectParams({
     *     "reportesManager" = @DI\Inject("api.manager.reportes")
     * })
     *
     * @param ReportesManager $reportesManager
     */
    public function __construct(ReportesManager $reportesManager)
    {
        $this->reportesManager = $reportesManager;
    }
    /**
     * @Route("/ventas/{idTipoTransaccion}/{idProducto}/{idMedida}/{fechaInicio}/{fechaFinal}")
     * @Method("GET")
     * **/
    public function reporteVentasAction($idTipoTransaccion, $idProducto,$idMedida,$fechaInicio ,$fechaFinal)
    {
        $resultados = $this->reportesManager->reporteVentas($idTipoTransaccion, $idProducto,$idMedida, $fechaInicio,$fechaFinal);
        return $this->json($resultados, Response::HTTP_OK);
    }

    /**
     * @Route("/ventas-detalle/{idTipoTransaccion}/{idProducto}/{idEntidad}/{idMedida}/{fechaInicio}/{fechaFinal}")
     * @Method("GET")
     * **/
    public function reporteVentasDetalleAction($idTipoTransaccion, $idProducto,$idEntidad,$idMedida,$fechaInicio ,$fechaFinal)
    {

        $resultados = $this->reportesManager->reporteVentasDetalle($idTipoTransaccion, $idProducto,$idEntidad,$idMedida ,$fechaInicio,$fechaFinal);
        return $this->json($resultados, Response::HTTP_OK);

    }


    /**
     * @Route("/compras/{idTipoTransaccion}/{idProducto}/{idMedida}/{fechaInicio}/{fechaFinal}")
     * @Method("GET")
     * **/
    public function reporteComprasAction($idTipoTransaccion, $idProducto,$idMedida,$fechaInicio ,$fechaFinal)
    {
        $resultados = $this->reportesManager->reporteCompras($idTipoTransaccion, $idProducto,$idMedida, $fechaInicio,$fechaFinal);
        return $this->json($resultados, Response::HTTP_OK);
    }
    /**
     * @Route("/compras-detalle/{idTipoTransaccion}/{idProducto}/{idEntidad}/{idMedida}/{fechaInicio}/{fechaFinal}")
     * @Method("GET")
     * **/
    public function reporteComprasDetalleAction($idTipoTransaccion, $idProducto,$idEntidad,$idMedida,$fechaInicio ,$fechaFinal)
    {

        $resultados = $this->reportesManager->reporteComprasDetalle($idTipoTransaccion, $idProducto,$idEntidad ,$idMedida,$fechaInicio,$fechaFinal);
        return $this->json($resultados, Response::HTTP_OK);
        //return $this->json(array("idtras"=>$idTipoTransaccion, "idProd"=>$idProducto, "identidad"=>$idEntidad,"inicio"=>$fechaInicio,"final"=>$fechaFinal), Response::HTTP_OK);
    }

    /**
     * @Route("/produccioni/{idTipoTransaccion}/{idProducto}/{idMedida}/{fechaInicio}/{fechaFinal}")
     * @Method("GET")
     * **/
    public function reporteProduccionAction($idTipoTransaccion, $idProducto,$idMedida,$fechaInicio ,$fechaFinal)
    {
        $resultados = $this->reportesManager->reporteProduccion($idTipoTransaccion, $idProducto,$idMedida, $fechaInicio,$fechaFinal);
        return $this->json($resultados, Response::HTTP_OK);
    }
    /**
     * @Route("/produccioni-detalle/{idTipoTransaccion}/{idProducto}/{idEntidad}/{idMedida}/{fechaInicio}/{fechaFinal}")
     * @Method("GET")
     * **/
    public function reporteProduccionDetalleAction($idTipoTransaccion, $idProducto,$idEntidad,$idMedida,$fechaInicio ,$fechaFinal)
    {

        $resultados = $this->reportesManager->reporteProduccionDetalle($idTipoTransaccion, $idProducto,$idEntidad,$idMedida ,$fechaInicio,$fechaFinal);
        return $this->json($resultados, Response::HTTP_OK);
        //return $this->json(array("idtras"=>$idTipoTransaccion, "idProd"=>$idProducto, "identidad"=>$idEntidad,"inicio"=>$fechaInicio,"final"=>$fechaFinal), Response::HTTP_OK);
    }

    /**
     * @Route("/reporte01/{fechaIni}/{fechaFin}")
     * @Method("GET")
     * **/
    public function reporte01Action($fechaIni, $fechaFin)
    {
        $resultados = $this->reportesManager->reporte01($fechaIni, $fechaFin);
        return $this->json($resultados, Response::HTTP_OK);
    }

    /**
     * @Route("/reporte01x/{id_productor}/{fechaIni}/{fechaFin}")
     * @Method("GET")
     * **/
    public function reporte01xAction($id_productor, $fechaIni, $fechaFin)
    {
        $resultados = $this->reportesManager->reporte01x($id_productor, $fechaIni, $fechaFin);
        return $this->json($resultados, Response::HTTP_OK);
    }

    /**
     * @Route("/reporte02/{fechaIni}/{fechaFin}")
     * @Method("GET")
     * **/
    public function reporte02Action($fechaIni, $fechaFin)
    {
        $resultados = $this->reportesManager->reporte02($fechaIni, $fechaFin);
        return $this->json($resultados, Response::HTTP_OK);
    }

    /**
     * @Route("/reporte02x/{id_empresa}/{fechaIni}/{fechaFin}")
     * @Method("GET")
     * **/
    public function reporte02xAction($id_empresa, $fechaIni, $fechaFin)
    {
        $resultados = $this->reportesManager->reporte02x($id_empresa, $fechaIni, $fechaFin);
        return $this->json($resultados, Response::HTTP_OK);
    }

    /**
     * @Route("/stock/{idProducto}/{idMedida}/{fecha}")
     * @Method("GET")
     **/
    public function reporteStock($idProducto, $idMedida,$fecha){

        $resultado=$this->reportesManager->reporteStock($idProducto,$idMedida,$fecha);
        return $this->json($resultado, Response::HTTP_OK);
    }

    /**
    * @Route("/resumenSaldoExportable/{inicio_mes}/{inicio_gest}/{fin_mes}/{fin_gest}/{id_producto}/{id_entidad}")
    * @Method("GET")
    */
    public function resumenSaldoExportable($inicio_mes, $inicio_gest, $fin_mes, $fin_gest, $id_producto, $id_entidad){
        $resultado = $this->reportesManager->resumenSaldoExportable($inicio_mes, $inicio_gest, $fin_mes, $fin_gest, $id_producto, $id_entidad);
        return$this->json($resultado, Response::HTTP_OK);
    }

    /**
     * @Route("/resumenExportaciones/{inicio_gest}/{inicio_mes}/{fin_gest}/{fin_mes}/{productos}/{id_entidad}")
     * @Method("GET")
     */
    public function resumenExportaciones($inicio_gest, $inicio_mes, $fin_gest, $fin_mes, $productos, $id_entidad){
        $resultado = $this->reportesManager->resumenExportaciones($inicio_gest, $inicio_mes, $fin_gest, $fin_mes, $productos, $id_entidad);
        return $this->json($resultado, Response::HTTP_OK);
    }

    /**
     * @Route("/diferencia-dia/{fecha}")
     * @Method("GET")
     **/
    public function diferencia($fecha){

        $resultado=$this->reportesManager->diferenciaDias($fecha);
        return $this->json($resultado, Response::HTTP_OK);
    }
}
