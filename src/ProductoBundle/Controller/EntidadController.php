<?php
namespace ProductoBundle\Controller;
use Doctrine\ORM\NoResultException;
use FOS\RestBundle\Controller\Annotations as Rest;
use ProductoBundle\Manager\PersonaManager;
use ProductoBundle\Manager\RegimenManager;
use ProductoBundle\Manager\RelacionEntidadManager;
use ProductoBundle\Modelo\ModeloPersona;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use ProductoBundle\Manager\EntidadManager;
use ProductoBundle\Manager\TipoEntidadManager;
use ProductoBundle\Manager\TipoDocumentoManager;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use ProductoBundle\Modelo\ModeloEntidad;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;



/**
 *  @Security("is_granted('ROLE_USER')")
 * @Route("/entidad")
 * **/
class EntidadController extends Controller{
    use \ProductoBundle\Helper\Helper;

    protected $entidadManager;
    protected $tipoEntidadManager;
    protected $tipoDocumentoManager;
    protected $personaManager;
    protected $relacionManager;
    protected $regimenManager;
    /**
     * @DI\InjectParams({
     * "entidadManager"=@DI\Inject("api.manager.entidad"),
     * "tipoDocumentoManager"=@DI\Inject("api.manager.tipoDocumento"),
     * "tipoEntidadManager"=@DI\Inject("api.manager.tipoEntidad"),
     * "personaManager"=@DI\Inject("api.manager.persona"),
     * "relacionEntidadManager"=@DI\Inject("api.manager.relacion"),
     * "regimenManager"=@DI\Inject("api.manager.regimen")
     * })
     *
     */
    public function __construct(EntidadManager $entidadManager,TipoEntidadManager  $tipoEntidadManager,
                                TipoDocumentoManager $tipoDocumentoManager,PersonaManager $personaManager,
                                RelacionEntidadManager $relacionEntidadManager,RegimenManager $regimenManager){
        
        $this->entidadManager=$entidadManager;
        $this->tipoEntidadManager=$tipoEntidadManager;
        $this->tipoDocumentoManager= $tipoDocumentoManager;
        $this->personaManager=$personaManager;
        $this->relacionManager=$relacionEntidadManager;
        $this->regimenManager=$regimenManager;
    }
    
    /**
     * @Route("/lista/{idComplejo}/{page}/{limit}/{txt}")
     * @Method("GET")
     * **/
    public function listaEntidadAction($idComplejo,$page,$limit, $txt = ''){
        $total=$this->entidadManager->listaEntidadCount($idComplejo);        
        $productos=$this->entidadManager->listaEntidad($idComplejo,$page,$limit, $txt);
        return $this->json(array("total"=>$total['total'],"datos"=>$productos),Response::HTTP_OK);
    }
    
    /**
     * @Route("/guardar")
     * @Method("POST") 
     * @ParamConverter("mempresa", converter = "fos_rest.request_body")
     * */
    public function guardarEmpresaAction(ModeloEntidad $mempresa,Request $request,ConstraintViolationListInterface $constraints){

        $user=$this->ObtenerUser($request);
        if ($constraints->count()) {
            return $this->json(array("errores"=>'Peticion incorrecta '. $constraints),Response::HTTP_BAD_REQUEST);
        }
        $respuesta = $this->guardarEmpresaFn($mempresa,$user);

        return $this->json($respuesta,Response::HTTP_OK);
    }
    public function guardarEmpresaFn( $mempresa, $user)
    {
        if ($mempresa->id == null || $mempresa->id == 0) // si es null es nuevo
            $respuesta=$this->entidadManager->guardar($mempresa,$user);
        else
            $respuesta=$this->entidadManager->actualizar($mempresa,$user);

        $mpersona=new ModeloPersona();
        $mpersona->id=$mempresa->persona['id'];
        $mpersona->nombres=$mempresa->persona['nombres'];
        $mpersona->primerApellido=$mempresa->persona['primerApellido'];
        $mpersona->segundoApellido=$mempresa->persona['segundoApellido'];
        $mpersona->direccion=$mempresa->persona['direccion'];
        $mpersona->numeroIdentificacion=$mempresa->persona['numeroIdentificacion'];
        $mpersona->idTipoDocumento=$mempresa->persona['idTipoDocumento'];
        $mpersona->correo=$mempresa->persona['correo'];
        $mpersona->telefonos=$mempresa->persona['telefonos'];

        if($mpersona->id==null)
        {
            //crear persona
            $respuestaP=$this->personaManager->guardar($mpersona,$user);
            //poner en vigente false si tenia un representante ateriormente
            //ojo, quemamos idTipoPersona a 2 representante legal
            $respuestaR1=$this->relacionManager->quitarVigente($mempresa->id,2,$user);
            //creamos la relacion
            $respuestaR2=$this->relacionManager->guardar($respuesta->getId(),$respuestaP->getId(),2,$user);

        }
        else {
            $respuestap=$this->personaManager->actualizar($mpersona,$user);
            $relacion=$this->relacionManager->recuperaVigente($mempresa->id,2,$user);
            if($relacion==null)
            {
                $respuestaR2=$this->relacionManager->guardar($respuesta->getId(),$respuestap->getId(),2,$user);
            }
            else
                if($relacion->getIdPersona()->getId()!= $mpersona->id)
                {
                    $respuestaR2=$this->relacionManager->guardar($respuesta->getId(),$respuestap->getId(),2,$user);
                }

        }
        return $respuesta;
    }

    /**
     * @Route("/baja/{id}")
     * @Method("GET")
     **/

    public function bajaAction($id,Request $request)
    {
        $user=$this->ObtenerUser($request);
        
        $mentidad=$this->entidadManager->baja($id,$user);
        return $this->json($mentidad);
        
    }
    
    
    /**
     * @Route("/recuperar/{id}")
     * @Method("GET")
     **/
    public function recuperarAction($id)
    {
        $mentidad=$this->entidadManager->recuperar($id);
        return $this->json($mentidad);        
        
    }
    
    /**
     * @Route("/tipoEntidad")
     * @Method("GET")
     * **/
    public function listaTipoEntidadAction(){
        $tipos=$this->tipoEntidadManager->listar();
        return $this->json($tipos,Response::HTTP_OK);
    }
    
    /**
     * @Route("/tipoDocumento/{individual}")
     * @Method("GET")
     * **/
    public function listaTipoDocumentoAction($individual){
        $tipos=$this->tipoDocumentoManager->listar($individual);
        return $this->json($tipos,Response::HTTP_OK);
    }

    /**
     * @Route("/relacion/{idEntidad}")
     * @Method("GET")
     */
    public function recuperarRelacionActiva($idEntidad){
        $relacion=$this->relacionManager->recuperaVigente($idEntidad,2);
        if(null==$relacion)
        {
            throw  new NoResultException();
        }
        return $this->json(array("idPersona"=>$relacion->getIdPersona()->getId(),"idEntidad"=>$relacion->getIdEntidad()->getId(),"idRelacion"=>$relacion->getId()),Response::HTTP_OK);

    }
    
    /**
     * @Route("/lista-fase/{idComplejo}/{idFase}")
     * @Method("GET")
     */
    public function recuperarEntidadesFase($idComplejo,$idFase){
    	$entidades=$this->entidadManager->listaXComplejoXFase($idComplejo,$idFase);
    	return $this->json($entidades,Response::HTTP_OK);
    	
    }

    /**
     * @Route("/regimenes")
     * @Method("GET")
     */
    public function recuperarRegimenes(){
        $regimenes = $this->regimenManager->listar();
        return $this->json($regimenes,Response::HTTP_OK);
    }

    /**
     * @Route("/cbxTipoRegistroSanitario")
     * @Method("GET")
     */
    public function cbxTipoRegistroSanitario(Request $request)
    {
        $result = $this->entidadManager->cbxTipoRegistroSanitario();
        if(!empty($result['error']))
            return $this->json($result['error'], Response::HTTP_SEE_OTHER);
        return $this->json($result,Response::HTTP_OK);
    }
    
}