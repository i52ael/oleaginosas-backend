<?php
namespace ProductoBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use ProductoBundle\Manager\ProductoManager;
use JMS\DiExtraBundle\Annotation as DI;
use ProductoBundle\Modelo\ModeloProducto;
use Symfony\Component\Translation\Exception\NotFoundResourceException;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use ProductoBundle\Entity\Producto;

/**
 * @Security("is_granted('ROLE_USER')")
 * @Route("/producto")
 * **/
class ProductoController extends Controller{
    use \ProductoBundle\Helper\Helper;

    protected $productoManager;
    /**
     * @DI\InjectParams({
     * "productoManager"=@DI\Inject("api.manager.producto")
     * })
     *
     */
    public function __construct(ProductoManager $productoManager){
        
        $this->productoManager=$productoManager;
    }
    
    /**
     * @Route("/lista/{idComplejo}/{page}/{limit}/{txt}")
     * @Method("GET")
     * **/
    public function listaProductoAction($idComplejo,$page,$limit, $txt = ''){
        $total=$this->productoManager->listaProductoCount($idComplejo);        
        $productos=$this->productoManager->listaProducto($idComplejo,$page,$limit, $txt);
        return $this->json(array("total"=>$total['total'],"datos"=>$productos),Response::HTTP_OK);
    }
    
    /**
     * @Route("/guardar")
     * @Method("POST") 
     * @ParamConverter("mproducto", converter = "fos_rest.request_body")
     * */
    public function guardarProductoAction(ModeloProducto $mproducto,Request $request,ConstraintViolationListInterface $constraints){
        
        $user=$this->ObtenerUser($request);
        
        if ($constraints->count()) {
            return $this->json(array("errores"=>'Peticion incorrecta '. $constraints),Response::HTTP_BAD_REQUEST);
        }
        if ($mproducto->id ==null) // si es null es nuevo
        $respuesta=$this->productoManager->guardar($mproducto,$user);
        else 
            $respuesta=$this->productoManager->actualizar($mproducto,$user);
        return $this->json($respuesta,Response::HTTP_OK);
        
    }
    
    /**
     * @Route("/baja/{id}")
     * @Method("GET")
     **/
    public function bajaAction($id,Request $request)
    {
        $user=$this->ObtenerUser($request);
        
        $mproducto=$this->productoManager->baja($id,$user);
        return $this->json($mproducto);
        
    }
    
    
    /**
     * @Route("/recuperar/{id}")
     * @Method("GET")
     **/
    public function recuperarAction($id)
    {
        $mproducto=$this->productoManager->recuperar($id);
        return $this->json($mproducto);        
        
    }

    /**
     * @Route("/recuperar-fase/{idComplejo}/{idFase}")
     * @Method("GET")
     **/
    public function recuperarXFaseAction($idComplejo,$idFase)
    {
        $productos=$this->productoManager->listaProductoXFase($idComplejo,$idFase);
        return $this->json($productos);

    }
    
}