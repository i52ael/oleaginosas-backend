<?php
namespace ProductoBundle\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use ProductoBundle\Manager\PersonaManager;
use JMS\DiExtraBundle\Annotation as DI;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use ProductoBundle\Entity\Producto;

/**
 * @Security("is_granted('ROLE_USER')")
 * @Route("/persona")
 * **/
class PersonaController extends Controller
{
    use \ProductoBundle\Helper\Helper;

    protected $personaManager;

    /**
     * @DI\InjectParams({
     * "personaManager"=@DI\Inject("api.manager.persona")
     * })
     *
     */
    public function __construct(PersonaManager $personaManager)
    {

        $this->personaManager = $personaManager;
    }

    /**
     * @Route("//{idComplejo}/{page}/{limit}")
     * @Method("GET")
     * **/
    /*public function listaProductoAction($idComplejo,$page,$limit){
        $total=$this->productoManager->listaProductoCount($idComplejo);        
        $productos=$this->productoManager->listaProducto($idComplejo,$page,$limit);
        return $this->json(array("total"=>$total['total'],"datos"=>$productos),Response::HTTP_OK);
    }*/

    /**
     * @Route("/guardar")
     * @Method("POST")
     * @ParamConverter("mproducto", converter = "fos_rest.request_body")
     * */
    /*public function guardarPersonaAction(ModeloProducto $mproducto,Request $request,ConstraintViolationListInterface $constraints){
        
        $user=$this->ObtenerUser($request);
        
        if ($constraints->count()) {
            return $this->json(array("errores"=>'Peticion incorrecta '. $constraints),Response::HTTP_BAD_REQUEST);
        }
        if ($mproducto->id ==null) // si es null es nuevo
        $respuesta=$this->productoManager->guardar($mproducto,$user);
        else 
            $respuesta=$this->productoManager->actualizar($mproducto,$user);
        return $this->json($respuesta,Response::HTTP_OK);
        
    }*/

    /**
     * @Route("/baja/{id}")
     * @Method("GET")
     **/
    /*public function bajaAction($id,Request $request)
    {
        $user=$this->ObtenerUser($request);
        
        $mproducto=$this->productoManager->baja($id,$user);
        return $this->json($mproducto);
        
    }
    
    */
    /**
     * @Route("/recuperar/{id}")
     * @Method("GET")
     **/
    public function recuperarAction($id)
    {
        $mpersona = $this->personaManager->recuperar($id);
        return $this->json($mpersona, Response::HTTP_OK);

    }

    /**
     * @param $numeroIdentificacion
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Rest\Route("/recuperarnumero/{numeroIdentificacion}")
     * @Method("GET")
     */
    public function recuperarNumeroAction($numeroIdentificacion)
    {
        $mpersona = $this->personaManager->recuperarNumero($numeroIdentificacion);
        if (null == $mpersona) {
            return $this->json($mpersona, Response::HTTP_NO_CONTENT); //no hay en la base nuestra

        }
        return $this->json($mpersona, Response::HTTP_OK);

    }

    /**
     * @param $numeroIdentificacion
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Rest\Route("/recuperarnumero-agetic/{numeroIdentificacion}")
     * @Method("GET")
     */
    public function recuperarNumeroAgeticAction($numeroIdentificacion)
    {
        $headers = ['Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiI2elVVMDJtQzBqU0ZiMHpWbzY2R0xvRWU0bWVFZDJZbyIsInVzZXIiOiJlYXJnb2xsby1hZ2V0aWMiLCJleHAiOjE2MDcxNzAzMjB9.k-Fh65ipsDGRXvhvuAE-sOvkNeKIl7SAxzM6L2dkxq0'];
        $url = 'https://test.agetic.gob.bo/apigateway/fake/segip/v2/personas/'.$numeroIdentificacion;

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $respuesta = curl_exec($curl);
        if ($respuesta === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }

        $decoded = json_decode($respuesta,true);
        if (isset($decoded->response->status) && $decoded->response->status == 'ERROR') {
            die('error occured: ' . $decoded->response->errormessage);
        }

        curl_close($curl);

        return $this->json($decoded, Response::HTTP_OK);

    }
}