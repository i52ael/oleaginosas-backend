<?php
namespace ProductoBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;

use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

use ProductoBundle\Exception\ApiException;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Debug\Exception\FlattenException;


class ExceptionController extends FOSRestController
{
	
	public function showAction($exception)
	{
		$originException = $exception;
		
		if (!$exception instanceof HttpException) {
			$exception = new HttpException($this->getStatusCode($exception), $this->getStatusText($exception));
		}
		
		if ($exception instanceof HttpException) {
			$exception = new ApiException($this->getStatusText($exception), $this->getStatusCode($exception));
		}
		
		$error = $exception->getErrorDetails();
		
		if ($this->isDebugMode() ) {
			$error['exception'] = FlattenException::create($originException);
		}
		
		$code = $this->getStatusCode($originException);
		
		return $this->json(['error' => $error], $code, ['X-Status-Code' => $code]);
	}
	
	protected function getStatusCode(\Exception $exception)
	{
		// If matched
		if ($statusCode = $this->get('fos_rest.exception.codes_map')->resolveException($exception)) {
			return $statusCode;
		}
		
		// Otherwise, default
		if ($exception instanceof HttpExceptionInterface) {
			return $exception->getStatusCode();
		}
		
		return 500;
	}
	
	protected function getStatusText(\Exception $exception, $default = 'Internal Server Error')
	{
		$code = $this->getStatusCode($exception);
		
		return array_key_exists($code, Response::$statusTexts) ? Response::$statusTexts[$code] : $default;
	}
	
	public function isDebugMode()
	{
		return $this->getParameter('kernel.debug');
	}
}