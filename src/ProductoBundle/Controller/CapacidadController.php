<?php
namespace ProductoBundle\Controller;
use ProductoBundle\Manager\CapacidadManager;
use ProductoBundle\Modelo\ModeloCapacidad;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use ProductoBundle\Manager\SubproductoManager;
use JMS\DiExtraBundle\Annotation as DI;
use ProductoBundle\Modelo\ModeloSubproducto;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use ProductoBundle\Entity\Subproducto;

/**
 * @Security("is_granted('ROLE_USER')")
 * @Route("/capacidad")
 * **/
class CapacidadController extends Controller{
    use \ProductoBundle\Helper\Helper;

    protected $capacidadManager;
    /**
     * @DI\InjectParams({
     * "capacidadManager"=@DI\Inject("api.manager.capacidad")
     * })
     *
     */
    public function __construct(CapacidadManager $capacidadManager){

        $this->capacidadManager=$capacidadManager;
    }
    
    /**
     * @Route("/lista/{idEntidad}")
     * @Method("GET")
     * **/
    public function listaCapacidadAction($idEntidad){

        $capacidades=$this->capacidadManager->listaCapacidades($idEntidad);
        return $this->json($capacidades,Response::HTTP_OK);
    }
    
    /**
     * @Route("/guardar")
     * @Method("POST") 
     * @ParamConverter("mcapacidad", converter = "fos_rest.request_body")
     * */
    public function guardarSubProductoAction(ModeloCapacidad $mcapacidad,Request $request,ConstraintViolationListInterface $constraints){
        
        $user=$this->ObtenerUser($request);
        
        if ($constraints->count()) {
            return $this->json(array("errores"=>'Peticion incorrecta '. $constraints),Response::HTTP_BAD_REQUEST);
        }
        if ($mcapacidad->id ==null) // si es null es nuevo
        {
            $capacidades=$this->capacidadManager->verificaProducto($mcapacidad->idProducto,$mcapacidad->idEntidad);

            if(count($capacidades)>0){
                return $this->json(array("mensaje"=>"Ya existe la capacidad para el producto"),Response::HTTP_ACCEPTED);
            }

            $respuesta = $this->capacidadManager->guardar($mcapacidad, $user);
        }
        else
            $respuesta=$this->capacidadManager->actualizar($mcapacidad,$user);
        return $this->json($respuesta,Response::HTTP_OK);
    }
    
    /**
     * @Route("/baja/{id}")
     * @Method("GET")
     **/
    public function bajaAction($id,Request $request)
    {
        $user=$this->ObtenerUser($request);
        $mcapacidad=$this->capacidadManager->baja($id,$user);
        return $this->json($mcapacidad);
    }
    
    /**
     * @Route("/recuperar/{id}")
     * @Method("GET")
     **/
    public function recuperarAction($id)
    {
        $mcapacidad=$this->capacidadManager->recuperar($id);
        return $this->json($mcapacidad);
        
    }
    
    
}