<?php
namespace ProductoBundle\Controller;
use ProductoBundle\Entity\CompromisoEntidad;
use ProductoBundle\Manager\CampaniaManager;
use ProductoBundle\Manager\CompromisoEntidadManager;
use ProductoBundle\Modelo\ModeloCompromiso;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use ProductoBundle\Entity\Compromiso;
use ProductoBundle\Modelo\ModeloUsuario;

/**
 * @Security("is_granted('ROLE_USER')")
 * @Route("/compromiso")
 * **/
class CompromisoEntidadController extends Controller{
    use \ProductoBundle\Helper\Helper;

    protected $compromisoEntidadManager;
    protected $campaniaManager;
    /**
     * @DI\InjectParams({
     * "compromisoEntidadManager"=@DI\Inject("api.manager.compromiso"),
     * "campaniaManager"=@DI\Inject("api.manager.campania")
     * })
     *
     */
    public function __construct(CompromisoEntidadManager $compromisoEntidadManager,CampaniaManager $campaniaManager){
        
        $this->compromisoEntidadManager=$compromisoEntidadManager;
        $this->campaniaManager=$campaniaManager;
    }
     /**
     * @Route("/lista/{idTipoTransaccion}/{idEntidad}/{idProducto}/{page}/{limit}")
     * @Method("GET")
     * **/
    public function listaProductoAction($idTipoTransaccion,$idEntidad,$idProducto,$page,$limit){
        $total=$this->compromisoEntidadManager->listarCount($idTipoTransaccion,$idEntidad,$idProducto);
        $compromisos=$this->compromisoEntidadManager->listar($idTipoTransaccion,$idEntidad,$idProducto,$page,$limit);
        return $this->json(array("total"=>$total['total'],"datos"=>$compromisos),Response::HTTP_OK);
    }

    /**
     * @Route("/guardar")
     * @Method("POST") 
     * @ParamConverter("mcompromiso", converter = "fos_rest.request_body")
     * */
    public function guardarProductoAction(ModeloCompromiso $mcompromiso,Request $request,ConstraintViolationListInterface $constraints){
        
        $user=$this->ObtenerUser($request);
        
        if ($constraints->count()) {
            return $this->json(array("errores"=>'Peticion incorrecta '. $constraints),Response::HTTP_BAD_REQUEST);
        }

        $result= $this->compromisoEntidadManager->verifica($mcompromiso->idEntidad,$mcompromiso->idProducto,$mcompromiso->idTipoTransaccion,$mcompromiso->idCampania);
        if(null!=$result)
        {
            return $this->json(array("mensaje"=>"Ya existe este compromiso"),Response::HTTP_NO_CONTENT);
        }

        if ($mcompromiso->id ==null) // si es null es nuevo
        $respuesta=$this->compromisoEntidadManager->guardar($mcompromiso,$user);
        else 
            $respuesta=$this->compromisoEntidadManager->actualizar($mcompromiso,$user);
        return $this->json($respuesta,Response::HTTP_OK);
        
    }

    /**
     * @Route("/baja/{id}")
     * @Method("GET")
     **/
    public function bajaAction($id,Request $request)
    {
        $user=$this->ObtenerUser($request);
        
        $mcompromiso=$this->compromisoEntidadManager->baja($id,$user);
        return $this->json($mcompromiso);
    }
    

    /**
     * @Route("/recuperar/{id}")
     * @Method("GET")
     **/
   /* public function recuperarAction($id)
    {
        $mproducto=$this->productoManager->recuperar($id);
        return $this->json($mproducto);        
        
    }
*/
    /**
     * @Route("/campania/{gestion}")
     * @Method("GET")
     **/
    public function recuperarXCampaniaGestionAction(Request $request)
    {
        $anio =  $request->get("gestion");
        $campanias=$this->campaniaManager->listarGestion($anio);
        return $this->json($campanias,Response::HTTP_OK);
    }

    /**
     * @Route("/guardarAll")
     * @Method("POST")
     * */
    public function guardarAll(Request $request){
        $obj = json_decode($request->getContent());
        $index_mes = 1;
        foreach($obj->meses as $key => $row)
        {
            $result = $this->compromisoEntidadManager->getIdCampania($index_mes, $obj->gestion);
            if($request==null)
                return $this->json("Se encontro el ID de la campaña del mes '$key' gestion '$obj->gestion'",Response::HTTP_NOT_FOUND);

            $index_mes++;
            $modelCompromiso = new ModeloCompromiso();
            $modelCompromiso->idEntidad = $obj->id_entidad;
            $modelCompromiso->idTipoTransaccion = $obj->id_tipo_transaccion;
            $modelCompromiso->idProducto = $obj->id_producto;
            $modelCompromiso->idCampania = $result['id'];
            $modelCompromiso->compromiso = $row;
            $res = $this->save($request, $modelCompromiso, $obj->sw_update);
            if(!empty($res['error']))
                return $this->json($res['error'],Response::HTTP_SEE_OTHER);
        }
        return $this->json(1,Response::HTTP_OK);
    }
    private function save($request, $mcompromiso, $sw_update)
    {
        $user=$this->ObtenerUser($request);
        $result = $this->compromisoEntidadManager->verifica($mcompromiso->idEntidad,$mcompromiso->idProducto,$mcompromiso->idTipoTransaccion,$mcompromiso->idCampania);
        if(null!=$result)
            return array('error'=>"Ya existe este compromiso");

        if ($mcompromiso->id ==null) // si es null es nuevo
            $this->compromisoEntidadManager->guardar($mcompromiso,$user);
        else
            $this->compromisoEntidadManager->actualizar($mcompromiso,$user);
        return array('ok' => 1);
    }
    /**
     * @Route("/cbxGestion")
     * @Method("GET")
     **/
    public function cbxGestion()
    {

        $index = 2016;
        $now = intval(\date('Y'));

        $arr = array();
        while ($index <= $now)
        {
            $arr[] = array("data"=>$index, "label"=>$index);
            $index++;
        }
        return $this->json($arr,Response::HTTP_OK);

    }
    
}