<?php
namespace ProductoBundle\Controller;
use ProductoBundle\Manager\CampaniaManager;
use ProductoBundle\Manager\CupoProduccionManager;
use ProductoBundle\Modelo\ModeloProduccionAgricola;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;




/**
 * @Security("is_granted('ROLE_USER')")
 */

/**
 * @Route("/produccion-agricola")
 * **/
class CupoProduccionController extends Controller{
    use \ProductoBundle\Helper\Helper;

    protected $cupoManager;
    protected $campaniaManager;
    /**
     * @DI\InjectParams({
     * "cupoManager"=@DI\Inject("api.manager.cupo"),
     *  "campaniaManager"=@DI\Inject("api.manager.campania")
     * })
     *
     */
    public function __construct(CupoProduccionManager $cupoManager,CampaniaManager $campaniaManager){
        
        $this->cupoManager=$cupoManager;
        $this->campaniaManager=$campaniaManager;
    }
    
    /**
     * @Route("/lista/{idProducto}")
     * @Method("GET")
     * **/
    public function listaCupoAction($idProducto){
        $cupos=$this->cupoManager->listaXProducto($idProducto);
        return $this->json($cupos,Response::HTTP_OK);
    }
    
    /**
     * @Route("/guardar")
     * @Method("POST") 
     * @ParamConverter("mcupo", converter = "fos_rest.request_body")
     * */
    public function guardarBandaAction(ModeloProduccionAgricola $mcupo, Request $request, ConstraintViolationListInterface $constraints){
        $user=$this->ObtenerUser($request);
        if ($constraints->count()) {
            return $this->json(array("error"=>'Peticion incorrecta '. $constraints),Response::HTTP_BAD_REQUEST);
        }

        $result=$this->cupoManager->verificaFechas($mcupo->idProducto,$mcupo->gestionAgricola);
        if($result!=null){
            return $this->json(array("mensaje"=>"Ya existe un registro con esta gestión agricola"),Response::HTTP_ACCEPTED);
        }

        if ($mcupo->id ==null) // si es null es nuevo
            $respuesta=$this->cupoManager->guardar($mcupo,$user);
        else
        $respuesta=$this->cupoManager->actualizar($mcupo, $user);
        return $this->json($respuesta,Response::HTTP_OK);
        
    }
    
    /**
     * @Route("/baja/{id}")
     * @Method("GET")
     **/
    public function bajaAction($id,Request $request)
    {
        $user=$this->ObtenerUser($request);
        
        $mcupo=$this->cupoManager->baja($id,$user);
        return $this->json($mcupo,Response::HTTP_OK);
        
    }
    
    
    /**
     * @Route("/recuperar/{id}")
     * @Method("GET")
     **/
    public function recuperarAction($id)
    {
        $mcupo=$this->cupoManager->recuperar($id);
        return $this->json($mcupo,Response::HTTP_OK);
        
    }

    /**
     * @Route("/fechas/{idProducto}/{fechaDesde}")
     * @Method("GET")
     */
    public function validaFechas($idProducto,$fechaDesde){
        $result=$this->bandaManager->verificaFechas($idProducto,$fechaDesde);

            return $this->json($result,Response::HTTP_OK);

    }

    /**
     * @Route("/campanias")
     * @Method("GET")
     */
    public function cargaCampania(){
        $anio=date("Y");
        $result=$this->campaniaManager->listaGestionAgricola($anio);

        return $this->json($result,Response::HTTP_OK);

    }

    /**
     * @Route("/gestiones")
     * @Method("POST")
     */
    public function listaGestiones(){

        $anio=date("Y");
        $result=$this->campaniaManager->listaGestiones(0/*$anio*/);

        return $this->json($result,Response::HTTP_OK);

    }
    
    
}