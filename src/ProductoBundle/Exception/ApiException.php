<?php 
namespace ProductoBundle\Exception;
use Exception;
class ApiException extends \Exception
{  
    public function getErrorDetails()
    {
        return [
            'code' => $this->getCode() ?: 999,
            'message' => $this->getMessage()?:'API Exception',
        ];
    }
}