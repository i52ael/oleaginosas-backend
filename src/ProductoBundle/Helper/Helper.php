<?php

namespace ProductoBundle\Helper;

use Symfony\Component\HttpFoundation\Response;
use JMS\Serializer\SerializationContext;
use Lexik\Bundle\JWTAuthenticationBundle\TokenExtractor\AuthorizationHeaderTokenExtractor;
use Symfony\Component\HttpFoundation\Request;

use ProductoBundle\Modelo\ModeloUsuario;
trait Helper
{

    /**
     * Set base HTTP headers.
     *
     * @param Response $response
     *
     * @return Response
     */
    public function ObtenerUser(Request $request){

        $user=new ModeloUsuario();

        $extractor = new AuthorizationHeaderTokenExtractor(
            'Bearer',
            'Authorization'
            );
        $token = $extractor->extract($request);
        //$userManager = $this->get('fos_user.user_manager');
        //$user = $userManager->findUserByConfirmationToken($token);
        $data= $this->container->get('lexik_jwt_authentication.encoder')->decode($token);
        $user->idUser=$data["idUser"];
        $user->userNamer=$data["username"];
        $user->email=$data["email"];
        $user->roles=$data["roles"];
        $user->idEntidad=$data["idEntidad"];
        return $user;

    }

}
