<?php
namespace ProductoBundle\Repository;


use Doctrine\ORM\EntityRepository;

class CampaniaRepository extends EntityRepository
{
    public function getListQB()
    {
        $qb = $this->createQueryBuilder('c');
        return $qb;
    }
    public function listar(){
        $qb = $this->createQueryBuilder('c')
        ->select('c.id','c.mes','c.gestion')
        ;
        
        
        return $qb;
    }
    public function listarGestiones($gestion){
        $qb=$this->createQueryBuilder('c')
            ->select('c.gestionAgricola')
            ->where('c.gestion=:gestion')
            ->setParameter('gestion',$gestion)
        ->groupBy('c.gestionAgricola');
        return $qb;
    }
    public function listaGestiones($gestion = 0)
    {
        if($gestion == 0)
            $qb=$this->createQueryBuilder('c')
                ->select('c.gestion')
                ->groupBy('c.gestion')
                ->orderBy('c.gestion');
        else
            $qb=$this->createQueryBuilder('c')
                ->select('c.gestion')
                ->where('c.gestion >=:gestion')
                ->setParameter('gestion',$gestion)
                ->groupBy('c.gestion')
                ->orderBy('c.gestion');
        return $qb;
    }
    
}