<?php
namespace ProductoBundle\Repository;


use Doctrine\ORM\EntityRepository;

class MedidaRepository extends EntityRepository
{
    
    public function medidasXmagnitud($idMagnitud){
        
        $qb = $this->createQueryBuilder('m')
        ->select('m.id, m.nombre,m.abreviacion,(m.idMagnitud) idMagnitud')
        ->where('m.idMagnitud = :id')
        //->andWhere('u.enabled = :enabled')
        ->setParameter('id', $idMagnitud);
        //->setParameter('enabled', $criteria['enabled']);
        
        return $qb;
    }
    
    
}