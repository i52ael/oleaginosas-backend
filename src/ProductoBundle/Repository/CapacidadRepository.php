<?php
namespace ProductoBundle\Repository;


use Doctrine\ORM\EntityRepository;

class CapacidadRepository extends EntityRepository
{
    public function getListQB()
    {
        $qb = $this->createQueryBuilder('c');
        return $qb;
    }
    public function listarXEntidad($idEntidad){
        $qb = $this->createQueryBuilder('s')
        ->select('s.id','e.id idEntidad','e.razonSocial','m.id idMedida','mg.id idMagnitud',
            'm.nombre nombreMedida','p.nombre nombreProducto','s.capacidadInstaladaNominalMes','s.capacidadInstaladaRealMes')
            ->innerJoin('s.idEntidad','e')
            ->innerJoin('s.idProducto','p')
            ->innerJoin('s.idMedida','m')
            ->innerJoin('m.idMagnitud','mg')
        ->where('s.idEntidad = :idEntidad')
        ->setParameter('idEntidad', $idEntidad )
        ;
        return $qb;
    }
    public function  recuperarXidProducto($idProducto,$idEmpresa){
        $qb=$this->createQueryBuilder('c')
            ->select('c')
            ->where('c.idProducto = :idProducto')
            ->andWhere('c.idEntidad=:idEntidad')
            ->setParameter('idProducto',$idProducto)
            ->setParameter('idEntidad',$idEmpresa);


        return $qb;
    }

    
}