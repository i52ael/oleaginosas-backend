<?php
namespace ProductoBundle\Repository;


use Doctrine\ORM\EntityRepository;

class FaseRepository extends EntityRepository
{
    
    public function listar(){
        
        $qb = $this->createQueryBuilder('f')
        ->select('f.id','f.nombre','f.orden');
        return $qb;
    }
    
    
}