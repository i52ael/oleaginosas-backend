<?php
namespace ProductoBundle\Repository;


use Doctrine\ORM\EntityRepository;

class RelacionEntidadRepository extends EntityRepository
{
    
    public function listar(){
        
        $qb = $this->createQueryBuilder('r')
        ->select('r');
        return $qb;
    }
    public function recuperarVigente($idEntidad,$idTipoPersona){
        $qb = $this->createQueryBuilder('r')
            ->select('r')
            ->where('r.idEntidad = :idEntidad')
            ->andWhere('r.idTipoPersona=:idTipoPersona')
            ->andWhere('r.vigente=:vigente')
            ->setParameter('idEntidad', $idEntidad)
            ->setParameter('idTipoPersona', $idTipoPersona)
            ->setParameter('vigente', true)

        ;
        //->setParameter('enabled', $criteria['enabled']);

        return $qb;
    }

    public function recuperarRelacionesVigentes($idPersona){
        $qb = $this->createQueryBuilder('r')
            ->select('r.id','r.vigente','p.nombres','p.primerApellido','p.segundoApellido','p.numeroIdentificacion numeroPersona','p.correo correoPersona',
                    'e.razonSocial','e.numeroIdentificacion numeroEmpresa','e.correoElectronico','p.id idPersona','e.id idEntidad')
            ->innerJoin('r.idPersona','p')
            ->innerJoin('r.idEntidad','e')
            ->where('r.idPersona = :idPersona')
            ->andWhere('r.vigente=:vigente')
            ->setParameter('idPersona', $idPersona)
            ->setParameter('vigente', true)
        ;

        return $qb;
    }
    
    
}