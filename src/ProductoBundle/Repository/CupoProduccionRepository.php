<?php
namespace ProductoBundle\Repository;


use Doctrine\ORM\EntityRepository;

class CupoProduccionRepository extends EntityRepository
{
    public function getListQB()
    {
        $qb = $this->createQueryBuilder('c');
        return $qb;
    }

    public function listarXProducto($idProducto){
        
        $qb = $this->createQueryBuilder('c')
        ->select('c.fechaHasta','c.id','c.estimado','c.gestionAgricola','c.fechaDesde','p.nombre nombreProducto','p.medida','m.nombre nombreMedida','m.abreviacion')
        ->innerJoin('c.idProducto','p')
            ->innerJoin('p.idMedida','m')
        ->where('c.idProducto = :idProducto')
        ->andWhere('c.eliminado=:eliminado')
        ->setParameter('idProducto', $idProducto )
        ->setParameter('eliminado', false)
        ->orderBy('c.fechaDesde','ASC');

        return $qb;
    }
    public function verificaXgestion($idProducto,$gestionAgricola){
        $qb =$this->createQueryBuilder('c')
            ->select('c')
            ->where ('c.gestionAgricola<=:gestiona')
            ->andWhere('c.idProducto =:idProducto')
            ->andWhere('c.eliminado=:eliminado')
            ->setParameter('gestiona',$gestionAgricola)
            ->setParameter('idProducto',$idProducto)
            ->setParameter('eliminado',false);

        return $qb;

    }

    
    
}