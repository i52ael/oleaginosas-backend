<?php
namespace ProductoBundle\Repository;


use Doctrine\ORM\EntityRepository;

class CompromisoEntidadRepository extends EntityRepository
{

    public function listarCount($idTipoTransaccion,$idEntidad,$idProducto){

        $qb = $this->createQueryBuilder('c')
            ->select('count(c.id) total')
            ->where('c.eliminado =:eliminado')
            ->setParameter('eliminado', false);
            if($idProducto!="0") $qb->andWhere('c.idProducto =:idProducto')->setParameter('idProducto', $idProducto);
            if($idTipoTransaccion!="0") $qb->andWhere('c.idTipoTransaccion =:idTipoTransaccion')->setParameter('idTipoTransaccion', $idTipoTransaccion);
            if($idEntidad!="0") $qb->andWhere('c.idEntidad =:idEntidad')->setParameter('idEntidad',$idEntidad  );

        return $qb;
    }
    public function listar($idTipoTransaccion,$idEntidad,$idProducto,$page,$limit){

        $qb = $this->createQueryBuilder('c')
            ->select('c.id','t.descripcion','f.nombre nombreFase','e.razonSocial','cp.mes','cp.gestion','c.compromisoEntidad','p.nombre nombreProducto','m.nombre nombreMedida')
            ->innerJoin('c.idProducto','p')
            ->innerJoin('p.idMedida','m')
            ->innerJoin('c.idEntidad','e')
            ->innerJoin('c.idTipoTransaccion','t')
            ->innerJoin('e.idFase','f')
            ->innerJoin('c.idCampania','cp')
            ->where('c.eliminado =:eliminado')
            ->setParameter('eliminado', false);
            if($idProducto!="0") $qb->andWhere('c.idProducto =:idProducto')->setParameter('idProducto', $idProducto);
            if($idTipoTransaccion!="0") $qb->andWhere('c.idTipoTransaccion =:idTipoTransaccion')->setParameter('idTipoTransaccion', $idTipoTransaccion);
            if($idEntidad!="0") $qb->andWhere('c.idEntidad =:idEntidad')->setParameter('idEntidad',$idEntidad  );

            $qb->setFirstResult(($page-1)*$limit)
            ->setMaxResults($limit)
            ->orderBy('c.fechaCreacion','DESC');

        return $qb;
    }
    public function verifica($idEntidad,$idProducto,$idTipoTransaccion,$idCampania)
    {
        $qb = $this->createQueryBuilder('c')
            ->select('c')
            ->where('c.idProducto =:idProducto')
            ->andWhere('c.idTipoTransaccion =:idTipoTransaccion')
            ->andWhere('c.idEntidad =:idEntidad')
            ->andWhere('c.idCampania =:idCampania')
            ->andWhere('c.eliminado =:eliminado')
            ->setParameter('eliminado', false)
            ->setParameter('idProducto', $idProducto)
            ->setParameter('idTipoTransaccion', $idTipoTransaccion)
            ->setParameter('idEntidad',$idEntidad  )
            ->setParameter('idCampania',$idCampania  );
        return $qb;

    }
    
    
}