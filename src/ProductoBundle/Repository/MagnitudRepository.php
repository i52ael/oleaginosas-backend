<?php
namespace ProductoBundle\Repository;


use Doctrine\ORM\EntityRepository;

class MagnitudRepository extends EntityRepository
{
    public function listar(){
        
        $qb = $this->createQueryBuilder('m')
        ->select('m.id','m.nombre');
        return $qb;
    }
    
    
}