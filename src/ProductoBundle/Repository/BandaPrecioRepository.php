<?php
namespace ProductoBundle\Repository;


use Doctrine\ORM\EntityRepository;

class BandaPrecioRepository extends EntityRepository
{
    public function getListQB()
    {
        $qb = $this->createQueryBuilder('b');
        return $qb;
    }
    public function listaXProductoCount($idProducto){
        $qb = $this->createQueryBuilder('b')
        ->select('count(b.id) total')
        ->where('b.idProducto = :idProducto')
        ->andWhere('b.eliminado=:eliminado')
        ->setParameter('idProducto', $idProducto )
        ->setParameter('eliminado', false)
        ;
        //->setParameter('enabled', $criteria['enabled']);
        
        return $qb;
    }
    public function listarXProducto($idProducto,$page,$limit){
        
        $qb = $this->createQueryBuilder('b')
        ->select('b.id','p.nombre nombreProducto','b.fechaDesde','b.fechaHasta',
        		'b.valorMinimo','b.valorMaximo','m.nombre moneda','m.abreviacion',
        		't.valorbs')
        ->innerJoin('b.idProducto', 'p')
        ->innerJoin('b.idTipoCambio','t')
        ->innerJoin('b.idTipoMoneda','m')
        ->where('b.idProducto = :idProducto')
        ->andWhere('b.eliminado=:eliminado')
        ->setParameter('idProducto', $idProducto )
        ->setParameter('eliminado', false)
        ->orderBy('b.fechaDesde','ASC')
        ->setFirstResult(($page-1)*$limit)
        ->setMaxResults($limit);        
        //->setParameter('enabled', $criteria['enabled']);
        
        return $qb;
    }
    public function verificaEntreFechas($idProducto,$fecha,$id){
        $qb =$this->createQueryBuilder('b')
            ->select('b')
            ->where ('b.fechaDesde<=:fecha')
            ->andWhere('b.fechaHasta>=:fecha')
            ->andWhere('b.idProducto =:idProducto')
            ->andWhere('b.id <>:id')
            ->andWhere('b.eliminado=:eliminado')
            ->setParameter('fecha',$fecha)
            ->setParameter('eliminado',false)
            ->setParameter('idProducto',$idProducto)
            ->setParameter('id',$id);
        return $qb;

    }
    
    
}