<?php
namespace ProductoBundle\Repository;


use Doctrine\ORM\EntityRepository;

class ProductoRepository extends EntityRepository
{
    public function getListQB()
    {
        $qb = $this->createQueryBuilder('p');
        return $qb;
    }
    public function listaXComplejoCount($idComplejo){
        $qb = $this->createQueryBuilder('p')
        ->select('count(p.id) total')
        ->where('p.idComplejo = :id')
        ->andWhere('p.vigente=:vigente')
        ->andWhere('p.eliminado=:eliminado')
        ->setParameter('vigente', true)
        ->setParameter('eliminado', false)
        ->setParameter('id', $idComplejo )
        ;
        //->setParameter('enabled', $criteria['enabled']);
        
        return $qb;
    }
    public function listarXComplejo($idComplejo,$page,$limit, $txt=''){

        if(empty($txt) || $txt == '-1')
        {
            $qb = $this->createQueryBuilder('p')
                ->select('p.id','p.nombre','p.descripcion','p.medida','c.id idComplejo','c.nombre nombreComplejo',
                    'm.id idMedida','m.nombre unidadMedida','f.id idFase','f.nombre fase','p.codigo')
                ->innerJoin('p.idComplejo', 'c')
                ->innerJoin('p.idMedida','m')
                ->innerJoin('p.idFase','f')
                ->where('p.idComplejo = :id')
                ->andWhere('p.vigente=:vigente')
                ->andWhere('p.eliminado=:eliminado')
                //->andWhere('u.enabled = :enabled')
                ->setParameter('id', $idComplejo )
                ->setParameter('vigente', true)
                ->setParameter('eliminado', false)
                ->orderBy('f.orden','ASC')
                ->addOrderBy('p.nombre','ASC')
                ->setFirstResult(($page-1)*$limit)
                ->setMaxResults($limit)
            ;
        }
        else
        {
            $qb = $this->createQueryBuilder('p')
                ->select('p.id','p.nombre','p.descripcion','p.medida','c.id idComplejo','c.nombre nombreComplejo',
                    'm.id idMedida','m.nombre unidadMedida','f.id idFase','f.nombre fase','p.codigo')
                ->innerJoin('p.idComplejo', 'c')
                ->innerJoin('p.idMedida','m')
                ->innerJoin('p.idFase','f')
                ->where('p.idComplejo = :id')
                ->andWhere('p.vigente=:vigente')
                ->andWhere('p.eliminado=:eliminado')
                ->andWhere('LOWER(p.nombre) LIKE LOWER(:txt) OR LOWER(p.codigo) LIKE LOWER(:txt)')
                //->andWhere('u.enabled = :enabled')
                ->setParameter('id', $idComplejo )
                ->setParameter('vigente', true)
                ->setParameter('eliminado', false)
                ->setParameter('txt', '%'.$txt.'%')
                ->orderBy('f.orden','ASC')
                ->addOrderBy('p.nombre','ASC')
                ->setFirstResult(($page-1)*$limit)
                ->setMaxResults($limit)
            ;
        }

        //->setParameter('enabled', $criteria['enabled']);
        
        return $qb;
    }

    public function listarXComplejoXFase($idComplejo,$idFase){

        $qb = $this->createQueryBuilder('p')
            ->select('p.id','p.nombre','p.descripcion','p.medida','c.id idComplejo','c.nombre nombreComplejo','m.id idMedida',
                    'm.nombre unidadMedida','f.id idFase','f.nombre fase','p.codigo')
            ->innerJoin('p.idComplejo', 'c')
            ->innerJoin('p.idMedida','m')
            ->innerJoin('p.idFase','f')
            ->where('p.idComplejo = :id')
            ->andWhere('p.idFase =:idFase')
            ->andWhere('p.vigente=:vigente')
            ->andWhere('p.eliminado=:eliminado')
            ->setParameter('id', $idComplejo )
            ->setParameter('idFase', $idFase )
            ->setParameter('vigente', true)
            ->setParameter('eliminado', false)

            ->orderBy('f.orden','ASC')

        ;
        //->setParameter('enabled', $criteria['enabled']);

        return $qb;
    }




}