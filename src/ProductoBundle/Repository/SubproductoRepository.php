<?php
namespace ProductoBundle\Repository;


use Doctrine\ORM\EntityRepository;

class SubproductoRepository extends EntityRepository
{
    public function getListQB()
    {
        $qb = $this->createQueryBuilder('s');
        return $qb;
    }
    public function listarXEntidad($idEntidad){
        $qb = $this->createQueryBuilder('s')
        ->select('s.id','s.nombreComercial','e.id idEntidad','e.razonSocial','s.densidad','s.medida','m.id idMedida',
            'm.nombre nombreMedida','f.id idFase','f.nombre nombreFase',
            'p.nombre nombreProducto','s.codigo')
        ->innerJoin('s.idEntidad','e')
        ->innerJoin('s.idFase','f')
            ->innerJoin('s.idProducto','p')
            ->innerJoin('s.idMedida','m')
        ->where('s.idEntidad = :idEntidad')
        ->andWhere('s.eliminado=:eliminado')
        ->setParameter('eliminado', false)
        ->setParameter('idEntidad', $idEntidad )
        ;
        return $qb;
    }

    
}