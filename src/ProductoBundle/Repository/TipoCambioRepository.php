<?php
namespace ProductoBundle\Repository;


use Doctrine\ORM\EntityRepository;

class TipoCambioRepository extends EntityRepository
{
    public function getListQB()
    {
        $qb = $this->createQueryBuilder('t');
        return $qb;
    }
    public function verificaEntreFechas($fecha){
    	$qb =$this->createQueryBuilder('t')
    	->select('t')
    	->where ('t.fechaInicio<=:fecha')
    	->andWhere('t.fechaFinal>=:fecha')
    	->setParameter('fecha',$fecha);
    	
    	return $qb;
    	
    }
    
    
}