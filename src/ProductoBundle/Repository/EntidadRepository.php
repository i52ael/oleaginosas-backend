<?php
namespace ProductoBundle\Repository;


use Doctrine\ORM\EntityRepository;

class EntidadRepository extends EntityRepository
{
    public function getListQB()
    {
        $qb = $this->createQueryBuilder('p');
        return $qb;
    }
    public function listaXComplejoCount($idComplejo){
        $qb = $this->createQueryBuilder('p')
        ->select('count(p.id) total')
        ->where('p.idComplejo = :id')
        ->andWhere('p.vigente=:vigente')
        ->andWhere('p.eliminado=:eliminado')
        ->setParameter('vigente', true)
        ->setParameter('eliminado', false)
        ->setParameter('id', $idComplejo )
        ;
        //->setParameter('enabled', $criteria['enabled']);
        
        return $qb;
    }
    public function listarXComplejo($idComplejo,$page,$limit, $txt = ''){

        if(empty($txt) || $txt == '-1')
        {
            $qb = $this->createQueryBuilder('p')
                ->select('p.id','p.razonSocial','c.nombre complejo','p.direccion','p.telefonos','p.celular','p.numeroIdentificacion','te.nombre tipoEntidad',
                    'td.nombre TipoDocumento','f.nombre fase','p.beneficiario','r.id idRegimen','r.nombre nombreRegimen')
                ->innerJoin('p.idComplejo', 'c')
                ->innerJoin('p.idTipoEntidad','te')
                ->innerJoin('p.idTipoDocumento','td')
                ->innerJoin('p.idFase','f')
                ->innerJoin('p.idRegimen','r')
                ->where('p.idComplejo = :id')
                ->andWhere('p.vigente=:vigente')
                ->andWhere('p.eliminado=:eliminado')
                ->orderBy('f.orden','ASC')
                ->addOrderBy('p.razonSocial','ASC')
                //->andWhere('u.enabled = :enabled')
                ->setParameter('id', $idComplejo )
                ->setParameter('vigente', true)
                ->setParameter('eliminado', false)

                ->setFirstResult(($page-1)*$limit)
                ->setMaxResults($limit)
            ;
        }
        else
        {
            $qb = $this->createQueryBuilder('p')
                ->select('p.id','p.razonSocial','c.nombre complejo','p.direccion','p.telefonos','p.celular','p.numeroIdentificacion','te.nombre tipoEntidad',
                    'td.nombre TipoDocumento','f.nombre fase','p.beneficiario','r.id idRegimen','r.nombre nombreRegimen')
                ->innerJoin('p.idComplejo', 'c')
                ->innerJoin('p.idTipoEntidad','te')
                ->innerJoin('p.idTipoDocumento','td')
                ->innerJoin('p.idFase','f')
                ->innerJoin('p.idRegimen','r')
                ->where('p.idComplejo = :id')
                ->andWhere('p.vigente=:vigente')
                ->andWhere('p.eliminado=:eliminado')
                ->andWhere('LOWER(p.razonSocial) LIKE :txt OR LOWER(p.numeroIdentificacion) LIKE :txt')
                ->orderBy('f.orden','ASC')
                ->addOrderBy('p.razonSocial','ASC')
                //->andWhere('u.enabled = :enabled')
                ->setParameter('id', $idComplejo )
                ->setParameter('vigente', true)
                ->setParameter('eliminado', false)
                ->setParameter('txt', '%'.strtolower($txt).'%')

                ->setFirstResult(($page-1)*$limit)
                ->setMaxResults($limit)
            ;
        }
        //->setParameter('enabled', $criteria['enabled']);
        
        return $qb;
    }
    
    public function listarXComplejoXFase($idComplejo,$idFase){
    	
    	$qb = $this->createQueryBuilder('p')
    	->select('p.id','p.razonSocial','c.nombre complejo','p.direccion','p.telefonos','p.celular','p.numeroIdentificacion','te.nombre tipoEntidad',
    			'td.nombre TipoDocumento','f.nombre fase')
    			->innerJoin('p.idComplejo', 'c')
    			->innerJoin('p.idTipoEntidad','te')
    			->innerJoin('p.idTipoDocumento','td')
    			->innerJoin('p.idFase','f')
    			->where('p.idComplejo = :id')
    			->andWhere('p.vigente=:vigente')
    			->andWhere('p.eliminado=:eliminado')
    			->andWhere('p.idFase = :idFase')
    	->setParameter('id', $idComplejo )
    	->setParameter('vigente', true)
    	->setParameter('eliminado', false)
    	->setParameter('idFase', $idFase)
    	
    	->orderBy('p.razonSocial','ASC')    	
    	;
    	//->setParameter('enabled', $criteria['enabled']);
    	
    	return $qb;
    }


    
    
}