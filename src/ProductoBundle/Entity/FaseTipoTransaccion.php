<?php

namespace ProductoBundle\Entity;

/**
 * FaseTipoTransaccion
 */
class FaseTipoTransaccion
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \ProductoBundle\Entity\Fase
     */
    private $idFase;

    /**
     * @var \ProductoBundle\Entity\TipoTransaccion
     */
    private $idTipoTransaccion;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idFase
     *
     * @param \ProductoBundle\Entity\Fase $idFase
     *
     * @return FaseTipoTransaccion
     */
    public function setIdFase(\ProductoBundle\Entity\Fase $idFase = null)
    {
        $this->idFase = $idFase;

        return $this;
    }

    /**
     * Get idFase
     *
     * @return \ProductoBundle\Entity\Fase
     */
    public function getIdFase()
    {
        return $this->idFase;
    }

    /**
     * Set idTipoTransaccion
     *
     * @param \ProductoBundle\Entity\TipoTransaccion $idTipoTransaccion
     *
     * @return FaseTipoTransaccion
     */
    public function setIdTipoTransaccion(\ProductoBundle\Entity\TipoTransaccion $idTipoTransaccion = null)
    {
        $this->idTipoTransaccion = $idTipoTransaccion;

        return $this;
    }

    /**
     * Get idTipoTransaccion
     *
     * @return \ProductoBundle\Entity\TipoTransaccion
     */
    public function getIdTipoTransaccion()
    {
        return $this->idTipoTransaccion;
    }
}

