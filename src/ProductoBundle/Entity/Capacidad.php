<?php

namespace ProductoBundle\Entity;

/**
 * Capacidad
 */
class Capacidad
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $idEntidad;

    /**
     * @var integer
     */
    private $idProducto;

    /**
     * @var string
     */
    private $capacidadInstaladaNominalMes;

    /**
     * @var string
     */
    private $capacidadInstaladaRealMes;

    /**
     * @var integer
     */
    private $idMedida;

    /**
     * @var string
     */
    private $porcentajeRendimiento;

    /**
     * @var integer
     */
    private $idProductoPrimario;

    /**
     * @var integer
     */
    private $rendimiento;
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idEntidad
     *
     * @param integer $idEntidad
     *
     * @return Capacidad
     */
    public function setIdEntidad($idEntidad)
    {
        $this->idEntidad = $idEntidad;

        return $this;
    }

    /**
     * Get idEntidad
     *
     * @return integer
     */
    public function getIdEntidad()
    {
        return $this->idEntidad;
    }

    /**
     * Set idProducto
     *
     * @param integer $idProducto
     *
     * @return Capacidad
     */
    public function setIdProducto($idProducto)
    {
        $this->idProducto = $idProducto;

        return $this;
    }

    /**
     * Get idProducto
     *
     * @return integer
     */
    public function getIdProducto()
    {
        return $this->idProducto;
    }

    /**
     * Set capacidadInstaladaNominalMes
     *
     * @param string $capacidadInstaladaNominalMes
     *
     * @return Capacidad
     */
    public function setCapacidadInstaladaNominalMes($capacidadInstaladaNominalMes)
    {
        $this->capacidadInstaladaNominalMes = $capacidadInstaladaNominalMes;

        return $this;
    }

    /**
     * Get capacidadInstaladaNominalMes
     *
     * @return string
     */
    public function getCapacidadInstaladaNominalMes()
    {
        return $this->capacidadInstaladaNominalMes;
    }

    /**
     * Set capacidadInstaladaRealMes
     *
     * @param string $capacidadInstaladaRealMes
     *
     * @return Capacidad
     */
    public function setCapacidadInstaladaRealMes($capacidadInstaladaRealMes)
    {
        $this->capacidadInstaladaRealMes = $capacidadInstaladaRealMes;

        return $this;
    }

    /**
     * Get capacidadInstaladaRealMes
     *
     * @return string
     */
    public function getCapacidadInstaladaRealMes()
    {
        return $this->capacidadInstaladaRealMes;
    }

    /**
     * Set idMedida
     *
     * @param integer $idMedida
     *
     * @return Capacidad
     */
    public function setIdMedida($idMedida)
    {
        $this->idMedida = $idMedida;

        return $this;
    }

    /**
     * Get idMedida
     *
     * @return integer
     */
    public function getIdMedida()
    {
        return $this->idMedida;
    }

    /**
     * Set PorcentajeRendimiento
     *
     * @param string $PorcentajeRendimiento
     *
     * @return Capacidad
     */
    public function setPorcentajeRendimiento($porcentajeRendimiento)
    {
        $this->porcentajeRendimiento = $porcentajeRendimiento;

        return $this;
    }

    /**
     * Get PorcentajeRendimiento
     *
     * @return string
     */
    public function getPorcentajeRendimiento()
    {
        return $this->porcentajeRendimiento;
    }

    /**
     * Set idProductoPrimario
     *
     * @param integer $idProductoPrimario
     *
     * @return Capacidad
     */
    public function setIdProductoPrimario($idProductoPrimario)
    {
        $this->idProductoPrimario = $idProductoPrimario;

        return $this;
    }

    /**
     * Get idProductoPrimario
     *
     * @return integer
     */
    public function getIdProductoPrimario()
    {
        return $this->idProductoPrimario;
    }

    /**
     * Set rendimiento
     *
     * @param integer $rendimiento
     *
     * @return Capacidad
     */
    public function setRendimiento($rendimiento)
    {
        $this->rendimiento = $rendimiento;

        return $this;
    }

    /**
     * Get rendimiento
     *
     * @return integer
     */
    public function getRendimiento()
    {
        return $this->rendimiento;
    }
}
