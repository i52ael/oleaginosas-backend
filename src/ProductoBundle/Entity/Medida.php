<?php

namespace ProductoBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="ProductoBundle\Repository\MedidaRepository")
 * Medida
 */
class Medida
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $nombre;

    /**
     * @var string
     */
    private $abreviacion;

    /**
     * @var \ProductoBundle\Entity\Magnitud
     */
    private $idMagnitud;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Medida
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set abreviacion
     *
     * @param string $abreviacion
     *
     * @return Medida
     */
    public function setAbreviacion($abreviacion)
    {
        $this->abreviacion = $abreviacion;

        return $this;
    }

    /**
     * Get abreviacion
     *
     * @return string
     */
    public function getAbreviacion()
    {
        return $this->abreviacion;
    }

    /**
     * Set idMagnitud
     *
     * @param \ProductoBundle\Entity\Magnitud $idMagnitud
     *
     * @return Medida
     */
    public function setIdMagnitud(\ProductoBundle\Entity\Magnitud $idMagnitud = null)
    {
        $this->idMagnitud = $idMagnitud;

        return $this;
    }

    /**
     * Get idMagnitud
     *
     * @return \ProductoBundle\Entity\Magnitud
     */
    public function getIdMagnitud()
    {
        return $this->idMagnitud;
    }
}

