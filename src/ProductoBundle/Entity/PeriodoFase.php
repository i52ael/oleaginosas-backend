<?php

namespace ProductoBundle\Entity;

/**
 * PeriodoFase
 */
class PeriodoFase
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $fechaDesde;

    /**
     * @var \DateTime
     */
    private $fechaHasta;

    /**
     * @var \ProductoBundle\Entity\Fase
     */
    private $idFase;

    /**
     * @var \ProductoBundle\Entity\Periodo
     */
    private $idPeriodo;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fechaDesde
     *
     * @param \DateTime $fechaDesde
     *
     * @return PeriodoFase
     */
    public function setFechaDesde($fechaDesde)
    {
        $this->fechaDesde = $fechaDesde;

        return $this;
    }

    /**
     * Get fechaDesde
     *
     * @return \DateTime
     */
    public function getFechaDesde()
    {
        return $this->fechaDesde;
    }

    /**
     * Set fechaHasta
     *
     * @param \DateTime $fechaHasta
     *
     * @return PeriodoFase
     */
    public function setFechaHasta($fechaHasta)
    {
        $this->fechaHasta = $fechaHasta;

        return $this;
    }

    /**
     * Get fechaHasta
     *
     * @return \DateTime
     */
    public function getFechaHasta()
    {
        return $this->fechaHasta;
    }

    /**
     * Set idFase
     *
     * @param \ProductoBundle\Entity\Fase $idFase
     *
     * @return PeriodoFase
     */
    public function setIdFase(\ProductoBundle\Entity\Fase $idFase = null)
    {
        $this->idFase = $idFase;

        return $this;
    }

    /**
     * Get idFase
     *
     * @return \ProductoBundle\Entity\Fase
     */
    public function getIdFase()
    {
        return $this->idFase;
    }

    /**
     * Set idPeriodo
     *
     * @param \ProductoBundle\Entity\Periodo $idPeriodo
     *
     * @return PeriodoFase
     */
    public function setIdPeriodo(\ProductoBundle\Entity\Periodo $idPeriodo = null)
    {
        $this->idPeriodo = $idPeriodo;

        return $this;
    }

    /**
     * Get idPeriodo
     *
     * @return \ProductoBundle\Entity\Periodo
     */
    public function getIdPeriodo()
    {
        return $this->idPeriodo;
    }
}

