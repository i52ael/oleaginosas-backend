<?php

namespace ProductoBundle\Entity;

/**
 * Produccion
 */
class Produccion
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $fechaRegistro = '2017-10-11 17:56:15.769568';

    /**
     * @var \ProductoBundle\Entity\PeriodoFase
     */
    private $idPeriodoFase;

    /**
     * @var \ProductoBundle\Entity\RelacionEntidad
     */
    private $idRelacionEntidad;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fechaRegistro
     *
     * @param \DateTime $fechaRegistro
     *
     * @return Produccion
     */
    public function setFechaRegistro($fechaRegistro)
    {
        $this->fechaRegistro = $fechaRegistro;

        return $this;
    }

    /**
     * Get fechaRegistro
     *
     * @return \DateTime
     */
    public function getFechaRegistro()
    {
        return $this->fechaRegistro;
    }

    /**
     * Set idPeriodoFase
     *
     * @param \ProductoBundle\Entity\PeriodoFase $idPeriodoFase
     *
     * @return Produccion
     */
    public function setIdPeriodoFase(\ProductoBundle\Entity\PeriodoFase $idPeriodoFase = null)
    {
        $this->idPeriodoFase = $idPeriodoFase;

        return $this;
    }

    /**
     * Get idPeriodoFase
     *
     * @return \ProductoBundle\Entity\PeriodoFase
     */
    public function getIdPeriodoFase()
    {
        return $this->idPeriodoFase;
    }

    /**
     * Set idRelacionEntidad
     *
     * @param \ProductoBundle\Entity\RelacionEntidad $idRelacionEntidad
     *
     * @return Produccion
     */
    public function setIdRelacionEntidad(\ProductoBundle\Entity\RelacionEntidad $idRelacionEntidad = null)
    {
        $this->idRelacionEntidad = $idRelacionEntidad;

        return $this;
    }

    /**
     * Get idRelacionEntidad
     *
     * @return \ProductoBundle\Entity\RelacionEntidad
     */
    public function getIdRelacionEntidad()
    {
        return $this->idRelacionEntidad;
    }
}

