<?php

namespace ProductoBundle\Entity;

/**
 * Entidad
 */
class Entidad
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $razonSocial;

    /**
     * @var string
     */
    private $direccion;

    /**
     * @var string
     */
    private $telefonos;

    /**
     * @var string
     */
    private $celular;

    /**
     * @var string
     */
    private $correoElectronico;

    /**
     * @var boolean
     */
    private $vigente;

    /**
     * @var boolean
     */
    private $eliminado;

    /**
     * @var integer
     */
    private $idDepartamento;

    /**
     * @var integer
     */
    private $idMunicipio;

    /**
     * @var \DateTime
     */
    private $fechaCreacion = 'now()';

    /**
     * @var \DateTime
     */
    private $fechaUltimaModificacion;

    /**
     * @var integer
     */
    private $idUsuarioCreacion;

    /**
     * @var integer
     */
    private $idUsuarioUltimaModificacion;

    /**
     * @var string
     */
    private $numeroIdentificacion;

    /**
     * @var string
     */
    private $fundempresa;

    /**
     * @var string
     */
    private $registroSanitario;

    /**
     * @var \ProductoBundle\Entity\TipoEntidad
     */
    private $idTipoEntidad;

    /**
     * @var \ProductoBundle\Entity\TipoDocumento
     */
    private $idTipoDocumento;

    /**
     * @var \ProductoBundle\Entity\Fase
     */
    private $idFase;

    /**
     * @var \ProductoBundle\Entity\Complejo
     */
    private $idComplejo;

    /**
     * @var \ProductoBundle\Entity\TipoRegistroSanitario
     */
    private $idTipoRegistroSanitario;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set razonSocial
     *
     * @param string $razonSocial
     *
     * @return Entidad
     */
    public function setRazonSocial($razonSocial)
    {
        $this->razonSocial = $razonSocial;

        return $this;
    }

    /**
     * Get razonSocial
     *
     * @return string
     */
    public function getRazonSocial()
    {
        return $this->razonSocial;
    }

    /**
     * Set direccion
     *
     * @param string $direccion
     *
     * @return Entidad
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;

        return $this;
    }

    /**
     * Get direccion
     *
     * @return string
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * Set telefonos
     *
     * @param string $telefonos
     *
     * @return Entidad
     */
    public function setTelefonos($telefonos)
    {
        $this->telefonos = $telefonos;

        return $this;
    }

    /**
     * Get telefonos
     *
     * @return string
     */
    public function getTelefonos()
    {
        return $this->telefonos;
    }

    /**
     * Set celular
     *
     * @param string $celular
     *
     * @return Entidad
     */
    public function setCelular($celular)
    {
        $this->celular = $celular;

        return $this;
    }

    /**
     * Get celular
     *
     * @return string
     */
    public function getCelular()
    {
        return $this->celular;
    }

    /**
     * Set correoElectronico
     *
     * @param string $correoElectronico
     *
     * @return Entidad
     */
    public function setCorreoElectronico($correoElectronico)
    {
        $this->correoElectronico = $correoElectronico;

        return $this;
    }

    /**
     * Get correoElectronico
     *
     * @return string
     */
    public function getCorreoElectronico()
    {
        return $this->correoElectronico;
    }

    /**
     * Set vigente
     *
     * @param boolean $vigente
     *
     * @return Entidad
     */
    public function setVigente($vigente)
    {
        $this->vigente = $vigente;

        return $this;
    }

    /**
     * Get vigente
     *
     * @return boolean
     */
    public function getVigente()
    {
        return $this->vigente;
    }

    /**
     * Set eliminado
     *
     * @param boolean $eliminado
     *
     * @return Entidad
     */
    public function setEliminado($eliminado)
    {
        $this->eliminado = $eliminado;

        return $this;
    }

    /**
     * Get eliminado
     *
     * @return boolean
     */
    public function getEliminado()
    {
        return $this->eliminado;
    }

    /**
     * Set idDepartamento
     *
     * @param integer $idDepartamento
     *
     * @return Entidad
     */
    public function setIdDepartamento($idDepartamento)
    {
        $this->idDepartamento = $idDepartamento;

        return $this;
    }

    /**
     * Get idDepartamento
     *
     * @return integer
     */
    public function getIdDepartamento()
    {
        return $this->idDepartamento;
    }

    /**
     * Set idMunicipio
     *
     * @param integer $idMunicipio
     *
     * @return Entidad
     */
    public function setIdMunicipio($idMunicipio)
    {
        $this->idMunicipio = $idMunicipio;

        return $this;
    }

    /**
     * Get idMunicipio
     *
     * @return integer
     */
    public function getIdMunicipio()
    {
        return $this->idMunicipio;
    }

    /**
     * Set fechaCreacion
     *
     * @param \DateTime $fechaCreacion
     *
     * @return Entidad
     */
    public function setFechaCreacion($fechaCreacion)
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    /**
     * Get fechaCreacion
     *
     * @return \DateTime
     */
    public function getFechaCreacion()
    {
        return $this->fechaCreacion;
    }

    /**
     * Set fechaUltimaModificacion
     *
     * @param \DateTime $fechaUltimaModificacion
     *
     * @return Entidad
     */
    public function setFechaUltimaModificacion($fechaUltimaModificacion)
    {
        $this->fechaUltimaModificacion = $fechaUltimaModificacion;

        return $this;
    }

    /**
     * Get fechaUltimaModificacion
     *
     * @return \DateTime
     */
    public function getFechaUltimaModificacion()
    {
        return $this->fechaUltimaModificacion;
    }

    /**
     * Set idUsuarioCreacion
     *
     * @param integer $idUsuarioCreacion
     *
     * @return Entidad
     */
    public function setIdUsuarioCreacion($idUsuarioCreacion)
    {
        $this->idUsuarioCreacion = $idUsuarioCreacion;

        return $this;
    }

    /**
     * Get idUsuarioCreacion
     *
     * @return integer
     */
    public function getIdUsuarioCreacion()
    {
        return $this->idUsuarioCreacion;
    }

    /**
     * Set idUsuarioUltimaModificacion
     *
     * @param integer $idUsuarioUltimaModificacion
     *
     * @return Entidad
     */
    public function setIdUsuarioUltimaModificacion($idUsuarioUltimaModificacion)
    {
        $this->idUsuarioUltimaModificacion = $idUsuarioUltimaModificacion;

        return $this;
    }

    /**
     * Get idUsuarioUltimaModificacion
     *
     * @return integer
     */
    public function getIdUsuarioUltimaModificacion()
    {
        return $this->idUsuarioUltimaModificacion;
    }

    /**
     * Set numeroIdentificacion
     *
     * @param string $numeroIdentificacion
     *
     * @return Entidad
     */
    public function setNumeroIdentificacion($numeroIdentificacion)
    {
        $this->numeroIdentificacion = $numeroIdentificacion;

        return $this;
    }

    /**
     * Get numeroIdentificacion
     *
     * @return string
     */
    public function getNumeroIdentificacion()
    {
        return $this->numeroIdentificacion;
    }

    /**
     * Set fundempresa
     *
     * @param string $fundempresa
     *
     * @return Entidad
     */
    public function setFundempresa($fundempresa)
    {
        $this->fundempresa = $fundempresa;

        return $this;
    }

    /**
     * Get fundempresa
     *
     * @return string
     */
    public function getFundempresa()
    {
        return $this->fundempresa;
    }

    /**
     * Set registroSanitario
     *
     * @param string $registroSanitario
     *
     * @return Entidad
     */
    public function setRegistroSanitario($registroSanitario)
    {
        $this->registroSanitario = $registroSanitario;

        return $this;
    }

    /**
     * Get registroSanitario
     *
     * @return string
     */
    public function getRegistroSanitario()
    {
        return $this->registroSanitario;
    }

    /**
     * Set idTipoEntidad
     *
     * @param \ProductoBundle\Entity\TipoEntidad $idTipoEntidad
     *
     * @return Entidad
     */
    public function setIdTipoEntidad(\ProductoBundle\Entity\TipoEntidad $idTipoEntidad = null)
    {
        $this->idTipoEntidad = $idTipoEntidad;

        return $this;
    }

    /**
     * Get idTipoEntidad
     *
     * @return \ProductoBundle\Entity\TipoEntidad
     */
    public function getIdTipoEntidad()
    {
        return $this->idTipoEntidad;
    }

    /**
     * Set idTipoDocumento
     *
     * @param \ProductoBundle\Entity\TipoDocumento $idTipoDocumento
     *
     * @return Entidad
     */
    public function setIdTipoDocumento(\ProductoBundle\Entity\TipoDocumento $idTipoDocumento = null)
    {
        $this->idTipoDocumento = $idTipoDocumento;

        return $this;
    }

    /**
     * Get idTipoDocumento
     *
     * @return \ProductoBundle\Entity\TipoDocumento
     */
    public function getIdTipoDocumento()
    {
        return $this->idTipoDocumento;
    }

    /**
     * Set idFase
     *
     * @param \ProductoBundle\Entity\Fase $idFase
     *
     * @return Entidad
     */
    public function setIdFase(\ProductoBundle\Entity\Fase $idFase = null)
    {
        $this->idFase = $idFase;

        return $this;
    }

    /**
     * Get idFase
     *
     * @return \ProductoBundle\Entity\Fase
     */
    public function getIdFase()
    {
        return $this->idFase;
    }

    /**
     * Set idComplejo
     *
     * @param \ProductoBundle\Entity\Complejo $idComplejo
     *
     * @return Entidad
     */
    public function setIdComplejo(\ProductoBundle\Entity\Complejo $idComplejo = null)
    {
        $this->idComplejo = $idComplejo;

        return $this;
    }

    /**
     * Get idComplejo
     *
     * @return \ProductoBundle\Entity\Complejo
     */
    public function getIdComplejo()
    {
        return $this->idComplejo;
    }
    /**
     * @var boolean
     */
    private $beneficiario;


    /**
     * Set beneficiario
     *
     * @param boolean $beneficiario
     *
     * @return Entidad
     */
    public function setBeneficiario($beneficiario)
    {
        $this->beneficiario = $beneficiario;

        return $this;
    }

    /**
     * Get beneficiario
     *
     * @return boolean
     */
    public function getBeneficiario()
    {
        return $this->beneficiario;
    }
    /**
     * @var \ProductoBundle\Entity\Regimen
     */
    private $idRegimen;


    /**
     * Set idRegimen
     *
     * @param \ProductoBundle\Entity\Regimen $idRegimen
     *
     * @return Entidad
     */
    public function setIdRegimen(\ProductoBundle\Entity\Regimen $idRegimen = null)
    {
        $this->idRegimen = $idRegimen;

        return $this;
    }

    /**
     * Get idRegimen
     *
     * @return \ProductoBundle\Entity\Regimen
     */
    public function getIdRegimen()
    {
        return $this->idRegimen;
    }

    /**
     * Set idTipoRegistroSanitario
     *
     * @param \ProductoBundle\Entity\TipoRegistroSanitario $idTipoRegistroSanitario
     *
     * @return Entidad
     */
    public function setIdTipoRegistroSanitario(\ProductoBundle\Entity\TipoRegistroSanitario $idTipoRegistroSanitario = null)
    {
        $this->idTipoRegistroSanitario = $idTipoRegistroSanitario;

        return $this;
    }

    /**
     * Get idTipoRegistroSanitario
     *
     * @return \ProductoBundle\Entity\TipoRegistroSanitario
     */
    public function getIdTipoRegistroSanitario()
    {
        return $this->idTipoRegistroSanitario;
    }




}
