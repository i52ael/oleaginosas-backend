<?php

namespace ProductoBundle\Entity;

/**
 * Campania
 */
class Campania
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $mes;

    /**
     * @var string
     */
    private $gestion;

    /**
     * @var string
     */
    private $gestionAgricola;

    /**
     * @var string
     */
    private $campania;


    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set mes
     *
     * @param string $mes
     *
     * @return Campania
     */
    public function setMes($mes)
    {
        $this->mes = $mes;

        return $this;
    }

    /**
     * Get mes
     *
     * @return string
     */
    public function getMes()
    {
        return $this->mes;
    }

    /**
     * Set gestion
     *
     * @param string $gestion
     *
     * @return Campania
     */
    public function setGestion($gestion)
    {
        $this->gestion = $gestion;

        return $this;
    }

    /**
     * Get gestion
     *
     * @return string
     */
    public function getGestion()
    {
        return $this->gestion;
    }

    /**
     * Set gestionAgricola
     *
     * @param string $gestionAgricola
     *
     * @return Campania
     */
    public function setGestionAgricola($gestionAgricola)
    {
        $this->gestionAgricola = $gestionAgricola;

        return $this;
    }

    /**
     * Get gestionAgricola
     *
     * @return string
     */
    public function getGestionAgricola()
    {
        return $this->gestionAgricola;
    }

    /**
     * Set campania
     *
     * @param string $campania
     *
     * @return Campania
     */
    public function setCampania($campania)
    {
        $this->campania = $campania;

        return $this;
    }

    /**
     * Get campania
     *
     * @return string
     */
    public function getCampania()
    {
        return $this->campania;
    }
}

