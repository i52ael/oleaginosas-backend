<?php

namespace ProductoBundle\Entity;

/**
 * CompraDetalle
 */
class CompraDetalle
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $cantidad;

    /**
     * @var string
     */
    private $precioUnitario;

    /**
     * @var string
     */
    private $precioTotal;

    /**
     * @var string
     */
    private $nroFactura;

    /**
     * @var \DateTime
     */
    private $fechaCompra;

    /**
     * @var \ProductoBundle\Entity\Compra
     */
    private $idCompra;

    /**
     * @var \ProductoBundle\Entity\Producto
     */
    private $idProducto;

    /**
     * @var \ProductoBundle\Entity\RelacionEntidad
     */
    private $idRelacionEntidadProveedor;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cantidad
     *
     * @param string $cantidad
     *
     * @return CompraDetalle
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    /**
     * Get cantidad
     *
     * @return string
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * Set precioUnitario
     *
     * @param string $precioUnitario
     *
     * @return CompraDetalle
     */
    public function setPrecioUnitario($precioUnitario)
    {
        $this->precioUnitario = $precioUnitario;

        return $this;
    }

    /**
     * Get precioUnitario
     *
     * @return string
     */
    public function getPrecioUnitario()
    {
        return $this->precioUnitario;
    }

    /**
     * Set precioTotal
     *
     * @param string $precioTotal
     *
     * @return CompraDetalle
     */
    public function setPrecioTotal($precioTotal)
    {
        $this->precioTotal = $precioTotal;

        return $this;
    }

    /**
     * Get precioTotal
     *
     * @return string
     */
    public function getPrecioTotal()
    {
        return $this->precioTotal;
    }

    /**
     * Set nroFactura
     *
     * @param string $nroFactura
     *
     * @return CompraDetalle
     */
    public function setNroFactura($nroFactura)
    {
        $this->nroFactura = $nroFactura;

        return $this;
    }

    /**
     * Get nroFactura
     *
     * @return string
     */
    public function getNroFactura()
    {
        return $this->nroFactura;
    }

    /**
     * Set fechaCompra
     *
     * @param \DateTime $fechaCompra
     *
     * @return CompraDetalle
     */
    public function setFechaCompra($fechaCompra)
    {
        $this->fechaCompra = $fechaCompra;

        return $this;
    }

    /**
     * Get fechaCompra
     *
     * @return \DateTime
     */
    public function getFechaCompra()
    {
        return $this->fechaCompra;
    }

    /**
     * Set idCompra
     *
     * @param \ProductoBundle\Entity\Compra $idCompra
     *
     * @return CompraDetalle
     */
    public function setIdCompra(\ProductoBundle\Entity\Compra $idCompra = null)
    {
        $this->idCompra = $idCompra;

        return $this;
    }

    /**
     * Get idCompra
     *
     * @return \ProductoBundle\Entity\Compra
     */
    public function getIdCompra()
    {
        return $this->idCompra;
    }

    /**
     * Set idProducto
     *
     * @param \ProductoBundle\Entity\Producto $idProducto
     *
     * @return CompraDetalle
     */
    public function setIdProducto(\ProductoBundle\Entity\Producto $idProducto = null)
    {
        $this->idProducto = $idProducto;

        return $this;
    }

    /**
     * Get idProducto
     *
     * @return \ProductoBundle\Entity\Producto
     */
    public function getIdProducto()
    {
        return $this->idProducto;
    }

    /**
     * Set idRelacionEntidadProveedor
     *
     * @param \ProductoBundle\Entity\RelacionEntidad $idRelacionEntidadProveedor
     *
     * @return CompraDetalle
     */
    public function setIdRelacionEntidadProveedor(\ProductoBundle\Entity\RelacionEntidad $idRelacionEntidadProveedor = null)
    {
        $this->idRelacionEntidadProveedor = $idRelacionEntidadProveedor;

        return $this;
    }

    /**
     * Get idRelacionEntidadProveedor
     *
     * @return \ProductoBundle\Entity\RelacionEntidad
     */
    public function getIdRelacionEntidadProveedor()
    {
        return $this->idRelacionEntidadProveedor;
    }
}

