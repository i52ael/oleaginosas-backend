<?php

namespace ProductoBundle\Entity;

/**
 * BandaPrecio
 */
class BandaPrecio
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $fechaDesde;

    /**
     * @var \DateTime
     */
    private $fechaHasta;

    /**
     * @var string
     */
    private $valorMinimo;

    /**
     * @var string
     */
    private $valorMaximo;

    /**
     * @var \ProductoBundle\Entity\Producto
     */
    private $idProducto;

    /**
     * @var \ProductoBundle\Entity\TipoCambio
     */
    private $idTipoCambio;

    /**
     * @var boolean
     */
    private $eliminado;
    
    /**
     * @var \DateTime
     */
    private $fechaCreacion = 'now()';
    
    /**
     * @var \DateTime
     */
    private $fechaUltimaModificacion;
    
    /**
     * @var integer
     */
    private $idUsuarioCreacion;
    
    /**
     * @var integer
     */
    private $idUsuarioUltimaModificacion;
    
    
    /**
     * @var \ProductoBundle\Entity\TipoMoneda
     */
    private $idTipoMoneda;
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fechaDesde
     *
     * @param \DateTime $fechaDesde
     *
     * @return BandaPrecio
     */
    public function setFechaDesde($fechaDesde)
    {
        $this->fechaDesde = $fechaDesde;

        return $this;
    }

    /**
     * Get fechaDesde
     *
     * @return \DateTime
     */
    public function getFechaDesde()
    {
        return $this->fechaDesde;
    }

    /**
     * Set fechaHasta
     *
     * @param \DateTime $fechaHasta
     *
     * @return BandaPrecio
     */
    public function setFechaHasta($fechaHasta)
    {
        $this->fechaHasta = $fechaHasta;

        return $this;
    }

    /**
     * Get fechaHasta
     *
     * @return \DateTime
     */
    public function getFechaHasta()
    {
        return $this->fechaHasta;
    }

    /**
     * Set valorMinimo
     *
     * @param string $valorMinimo
     *
     * @return BandaPrecio
     */
    public function setValorMinimo($valorMinimo)
    {
        $this->valorMinimo = $valorMinimo;

        return $this;
    }

    /**
     * Get valorMinimo
     *
     * @return string
     */
    public function getValorMinimo()
    {
        return $this->valorMinimo;
    }

    /**
     * Set valorMaximo
     *
     * @param string $valorMaximo
     *
     * @return BandaPrecio
     */
    public function setValorMaximo($valorMaximo)
    {
        $this->valorMaximo = $valorMaximo;

        return $this;
    }

    /**
     * Get valorMaximo
     *
     * @return string
     */
    public function getValorMaximo()
    {
        return $this->valorMaximo;
    }

    /**
     * @return boolean
     */
    public function isEliminado()
    {
        return $this->eliminado;
    }

    /**
     * @return DateTime
     */
    public function getFechaCreacion()
    {
        return $this->fechaCreacion;
    }

    /**
     * @return DateTime
     */
    public function getFechaUltimaModificacion()
    {
        return $this->fechaUltimaModificacion;
    }

    /**
     * @return number
     */
    public function getIdUsuarioCreacion()
    {
        return $this->idUsuarioCreacion;
    }

    /**
     * @return number
     */
    public function getIdUsuarioUltimaModificacion()
    {
        return $this->idUsuarioUltimaModificacion;
    }

    /**
     * @return \ProductoBundle\Entity\TipoMoneda
     */
    public function getIdTipoMoneda()
    {
        return $this->idTipoMoneda;
    }

    /**
     * @param boolean $eliminado
     */
    public function setEliminado($eliminado)
    {
        $this->eliminado = $eliminado;
    }

    /**
     * @param DateTime $fechaCreacion
     */
    public function setFechaCreacion($fechaCreacion)
    {
        $this->fechaCreacion = $fechaCreacion;
    }

    /**
     * @param DateTime $fechaUltimaModificacion
     */
    public function setFechaUltimaModificacion($fechaUltimaModificacion)
    {
        $this->fechaUltimaModificacion = $fechaUltimaModificacion;
    }

    /**
     * @param number $idUsuarioCreacion
     */
    public function setIdUsuarioCreacion($idUsuarioCreacion)
    {
        $this->idUsuarioCreacion = $idUsuarioCreacion;
    }

    /**
     * @param number $idUsuarioUltimaModificacion
     */
    public function setIdUsuarioUltimaModificacion($idUsuarioUltimaModificacion)
    {
        $this->idUsuarioUltimaModificacion = $idUsuarioUltimaModificacion;
    }

    /**
     * @param \ProductoBundle\Entity\TipoMoneda $idTipoMoneda
     */
    public function setIdTipoMoneda(\ProductoBundle\Entity\TipoMoneda $idTipoMoneda =null)
    {
        $this->idTipoMoneda = $idTipoMoneda;
    }

    /**
     * Set idProducto
     *
     * @param \ProductoBundle\Entity\Producto $idProducto
     *
     * @return BandaPrecio
     */
    public function setIdProducto(\ProductoBundle\Entity\Producto $idProducto = null)
    {
        $this->idProducto = $idProducto;

        return $this;
    }

    /**
     * Get idProducto
     *
     * @return \ProductoBundle\Entity\Producto
     */
    public function getIdProducto()
    {
        return $this->idProducto;
    }

    /**
     * Set idTipoCambio
     *
     * @param \ProductoBundle\Entity\TipoCambio $idTipoCambio
     *
     * @return BandaPrecio
     */
    public function setIdTipoCambio(\ProductoBundle\Entity\TipoCambio $idTipoCambio = null)
    {
        $this->idTipoCambio = $idTipoCambio;

        return $this;
    }

    /**
     * Get idTipoCambio
     *
     * @return \ProductoBundle\Entity\TipoCambio
     */
    public function getIdTipoCambio()
    {
        return $this->idTipoCambio;
    }
}

