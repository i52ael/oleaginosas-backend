<?php

namespace ProductoBundle\Entity;

/**
 * RelacionEntidad
 */
class RelacionEntidad
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $fechaDesde;

    /**
     * @var \DateTime
     */
    private $fechaHasta;

    /**
     * @var \ProductoBundle\Entity\Persona
     */
    private $idPersona;

    /**
     * @var \ProductoBundle\Entity\Entidad
     */
    private $idEntidad;

    /**
     * @var \ProductoBundle\Entity\TipoPersona
     */
    private $idTipoPersona;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fechaDesde
     *
     * @param \DateTime $fechaDesde
     *
     * @return RelacionEntidad
     */
    public function setFechaDesde($fechaDesde)
    {
        $this->fechaDesde = $fechaDesde;

        return $this;
    }

    /**
     * Get fechaDesde
     *
     * @return \DateTime
     */
    public function getFechaDesde()
    {
        return $this->fechaDesde;
    }

    /**
     * Set fechaHasta
     *
     * @param \DateTime $fechaHasta
     *
     * @return RelacionEntidad
     */
    public function setFechaHasta($fechaHasta)
    {
        $this->fechaHasta = $fechaHasta;

        return $this;
    }

    /**
     * Get fechaHasta
     *
     * @return \DateTime
     */
    public function getFechaHasta()
    {
        return $this->fechaHasta;
    }

    /**
     * Set idPersona
     *
     * @param \ProductoBundle\Entity\Persona $idPersona
     *
     * @return RelacionEntidad
     */
    public function setIdPersona(\ProductoBundle\Entity\Persona $idPersona = null)
    {
        $this->idPersona = $idPersona;

        return $this;
    }

    /**
     * Get idPersona
     *
     * @return \ProductoBundle\Entity\Persona
     */
    public function getIdPersona()
    {
        return $this->idPersona;
    }

    /**
     * Set idEntidad
     *
     * @param \ProductoBundle\Entity\Entidad $idEntidad
     *
     * @return RelacionEntidad
     */
    public function setIdEntidad(\ProductoBundle\Entity\Entidad $idEntidad = null)
    {
        $this->idEntidad = $idEntidad;

        return $this;
    }

    /**
     * Get idEntidad
     *
     * @return \ProductoBundle\Entity\Entidad
     */
    public function getIdEntidad()
    {
        return $this->idEntidad;
    }

    /**
     * Set idTipoPersona
     *
     * @param \ProductoBundle\Entity\TipoPersona $idTipoPersona
     *
     * @return RelacionEntidad
     */
    public function setIdTipoPersona(\ProductoBundle\Entity\TipoPersona $idTipoPersona = null)
    {
        $this->idTipoPersona = $idTipoPersona;

        return $this;
    }

    /**
     * Get idTipoPersona
     *
     * @return \ProductoBundle\Entity\TipoPersona
     */
    public function getIdTipoPersona()
    {
        return $this->idTipoPersona;
    }
    /**
     * @var boolean
     */
    private $vigente;

    /**
     * @var \DateTime
     */
    private $fechaCreacion;

    /**
     * @var \DateTime
     */
    private $fechaUltimaModificacion;

    /**
     * @var integer
     */
    private $idUsuarioUltimaModificacion;

    /**
     * @var integer
     */
    private $idUsuarioCreacion;


    /**
     * Set vigente
     *
     * @param boolean $vigente
     *
     * @return RelacionEntidad
     */
    public function setVigente($vigente)
    {
        $this->vigente = $vigente;

        return $this;
    }

    /**
     * Get vigente
     *
     * @return boolean
     */
    public function getVigente()
    {
        return $this->vigente;
    }

    /**
     * Set fechaCreacion
     *
     * @param \DateTime $fechaCreacion
     *
     * @return RelacionEntidad
     */
    public function setFechaCreacion($fechaCreacion)
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    /**
     * Get fechaCreacion
     *
     * @return \DateTime
     */
    public function getFechaCreacion()
    {
        return $this->fechaCreacion;
    }

    /**
     * Set fechaUltimaModificacion
     *
     * @param \DateTime $fechaUltimaModificacion
     *
     * @return RelacionEntidad
     */
    public function setFechaUltimaModificacion($fechaUltimaModificacion)
    {
        $this->fechaUltimaModificacion = $fechaUltimaModificacion;

        return $this;
    }

    /**
     * Get fechaUltimaModificacion
     *
     * @return \DateTime
     */
    public function getFechaUltimaModificacion()
    {
        return $this->fechaUltimaModificacion;
    }

    /**
     * Set idUsuarioUltimaModificacion
     *
     * @param integer $idUsuarioUltimaModificacion
     *
     * @return RelacionEntidad
     */
    public function setIdUsuarioUltimaModificacion($idUsuarioUltimaModificacion)
    {
        $this->idUsuarioUltimaModificacion = $idUsuarioUltimaModificacion;

        return $this;
    }

    /**
     * Get idUsuarioUltimaModificacion
     *
     * @return integer
     */
    public function getIdUsuarioUltimaModificacion()
    {
        return $this->idUsuarioUltimaModificacion;
    }

    /**
     * Set idUsuarioCreacion
     *
     * @param integer $idUsuarioCreacion
     *
     * @return RelacionEntidad
     */
    public function setIdUsuarioCreacion($idUsuarioCreacion)
    {
        $this->idUsuarioCreacion = $idUsuarioCreacion;

        return $this;
    }

    /**
     * Get idUsuarioCreacion
     *
     * @return integer
     */
    public function getIdUsuarioCreacion()
    {
        return $this->idUsuarioCreacion;
    }
}
