<?php

namespace ProductoBundle\Entity;

/**
 * Subproducto
 */
class Subproducto
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $nombreComercial;

    /**
     * @var string
     */
    private $medida;

    /**
     * @var boolean
     */
    private $eliminado;

    /**
     * @var \DateTime
     */
    private $fechaCreacion = 'now()';

    /**
     * @var \DateTime
     */
    private $fechaUltimaModificacion;

    /**
     * @var integer
     */
    private $idUsuarioCreacion;

    /**
     * @var integer
     */
    private $idUsuarioUltimaModificacion;

    /**
     * @var string
     */
    private $densidad;

    /**
     * @var \ProductoBundle\Entity\Medida
     */
    private $idMedida;

    /**
     * @var \ProductoBundle\Entity\Fase
     */
    private $idFase;

    /**
     * @var \ProductoBundle\Entity\Producto
     */
    private $idProducto;

    /**
     * @var \ProductoBundle\Entity\Entidad
     */
    private $idEntidad;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombreComercial
     *
     * @param string $nombreComercial
     *
     * @return Subproducto
     */
    public function setNombreComercial($nombreComercial)
    {
        $this->nombreComercial = $nombreComercial;

        return $this;
    }

    /**
     * Get nombreComercial
     *
     * @return string
     */
    public function getNombreComercial()
    {
        return $this->nombreComercial;
    }

    /**
     * Set medida
     *
     * @param string $medida
     *
     * @return Subproducto
     */
    public function setMedida($medida)
    {
        $this->medida = $medida;

        return $this;
    }

    /**
     * Get medida
     *
     * @return string
     */
    public function getMedida()
    {
        return $this->medida;
    }

    /**
     * Set eliminado
     *
     * @param boolean $eliminado
     *
     * @return Subproducto
     */
    public function setEliminado($eliminado)
    {
        $this->eliminado = $eliminado;

        return $this;
    }

    /**
     * Get eliminado
     *
     * @return boolean
     */
    public function getEliminado()
    {
        return $this->eliminado;
    }

    /**
     * Set fechaCreacion
     *
     * @param \DateTime $fechaCreacion
     *
     * @return Subproducto
     */
    public function setFechaCreacion($fechaCreacion)
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    /**
     * Get fechaCreacion
     *
     * @return \DateTime
     */
    public function getFechaCreacion()
    {
        return $this->fechaCreacion;
    }

    /**
     * Set fechaUltimaModificacion
     *
     * @param \DateTime $fechaUltimaModificacion
     *
     * @return Subproducto
     */
    public function setFechaUltimaModificacion($fechaUltimaModificacion)
    {
        $this->fechaUltimaModificacion = $fechaUltimaModificacion;

        return $this;
    }

    /**
     * Get fechaUltimaModificacion
     *
     * @return \DateTime
     */
    public function getFechaUltimaModificacion()
    {
        return $this->fechaUltimaModificacion;
    }

    /**
     * Set idUsuarioCreacion
     *
     * @param integer $idUsuarioCreacion
     *
     * @return Subproducto
     */
    public function setIdUsuarioCreacion($idUsuarioCreacion)
    {
        $this->idUsuarioCreacion = $idUsuarioCreacion;

        return $this;
    }

    /**
     * Get idUsuarioCreacion
     *
     * @return integer
     */
    public function getIdUsuarioCreacion()
    {
        return $this->idUsuarioCreacion;
    }

    /**
     * Set idUsuarioUltimaModificacion
     *
     * @param integer $idUsuarioUltimaModificacion
     *
     * @return Subproducto
     */
    public function setIdUsuarioUltimaModificacion($idUsuarioUltimaModificacion)
    {
        $this->idUsuarioUltimaModificacion = $idUsuarioUltimaModificacion;

        return $this;
    }

    /**
     * Get idUsuarioUltimaModificacion
     *
     * @return integer
     */
    public function getIdUsuarioUltimaModificacion()
    {
        return $this->idUsuarioUltimaModificacion;
    }

    /**
     * Set densidad
     *
     * @param string $densidad
     *
     * @return Subproducto
     */
    public function setDensidad($densidad)
    {
        $this->densidad = $densidad;

        return $this;
    }

    /**
     * Get densidad
     *
     * @return string
     */
    public function getDensidad()
    {
        return $this->densidad;
    }

    /**
     * Set idMedida
     *
     * @param \ProductoBundle\Entity\Medida $idMedida
     *
     * @return Subproducto
     */
    public function setIdMedida(\ProductoBundle\Entity\Medida $idMedida = null)
    {
        $this->idMedida = $idMedida;

        return $this;
    }

    /**
     * Get idMedida
     *
     * @return \ProductoBundle\Entity\Medida
     */
    public function getIdMedida()
    {
        return $this->idMedida;
    }

    /**
     * Set idFase
     *
     * @param \ProductoBundle\Entity\Fase $idFase
     *
     * @return Subproducto
     */
    public function setIdFase(\ProductoBundle\Entity\Fase $idFase = null)
    {
        $this->idFase = $idFase;

        return $this;
    }

    /**
     * Get idFase
     *
     * @return \ProductoBundle\Entity\Fase
     */
    public function getIdFase()
    {
        return $this->idFase;
    }

    /**
     * Set idProducto
     *
     * @param \ProductoBundle\Entity\Producto $idProducto
     *
     * @return Subproducto
     */
    public function setIdProducto(\ProductoBundle\Entity\Producto $idProducto = null)
    {
        $this->idProducto = $idProducto;

        return $this;
    }

    /**
     * Get idProducto
     *
     * @return \ProductoBundle\Entity\Producto
     */
    public function getIdProducto()
    {
        return $this->idProducto;
    }

    /**
     * Set idEntidad
     *
     * @param \ProductoBundle\Entity\Entidad $idEntidad
     *
     * @return Subproducto
     */
    public function setIdEntidad(\ProductoBundle\Entity\Entidad $idEntidad = null)
    {
        $this->idEntidad = $idEntidad;

        return $this;
    }

    /**
     * Get idEntidad
     *
     * @return \ProductoBundle\Entity\Entidad
     */
    public function getIdEntidad()
    {
        return $this->idEntidad;
    }

    /**
     * @var string
     */
    private $codigo;


    /**
     * Set codigo
     *
     * @param string $codigo
     *
     * @return Subproducto
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo
     *
     * @return string
     */
    public function getCodigo()
    {
        return $this->codigo;
    }
}
