<?php

namespace ProductoBundle\Entity;

/**
 * ProduccionDetalle
 */
class ProduccionDetalle
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $cantidadProduccion;

    /**
     * @var string
     */
    private $stockFinal;

    /**
     * @var \ProductoBundle\Entity\Produccion
     */
    private $idProduccion;

    /**
     * @var \ProductoBundle\Entity\Producto
     */
    private $idProducto;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cantidadProduccion
     *
     * @param string $cantidadProduccion
     *
     * @return ProduccionDetalle
     */
    public function setCantidadProduccion($cantidadProduccion)
    {
        $this->cantidadProduccion = $cantidadProduccion;

        return $this;
    }

    /**
     * Get cantidadProduccion
     *
     * @return string
     */
    public function getCantidadProduccion()
    {
        return $this->cantidadProduccion;
    }

    /**
     * Set stockFinal
     *
     * @param string $stockFinal
     *
     * @return ProduccionDetalle
     */
    public function setStockFinal($stockFinal)
    {
        $this->stockFinal = $stockFinal;

        return $this;
    }

    /**
     * Get stockFinal
     *
     * @return string
     */
    public function getStockFinal()
    {
        return $this->stockFinal;
    }

    /**
     * Set idProduccion
     *
     * @param \ProductoBundle\Entity\Produccion $idProduccion
     *
     * @return ProduccionDetalle
     */
    public function setIdProduccion(\ProductoBundle\Entity\Produccion $idProduccion = null)
    {
        $this->idProduccion = $idProduccion;

        return $this;
    }

    /**
     * Get idProduccion
     *
     * @return \ProductoBundle\Entity\Produccion
     */
    public function getIdProduccion()
    {
        return $this->idProduccion;
    }

    /**
     * Set idProducto
     *
     * @param \ProductoBundle\Entity\Producto $idProducto
     *
     * @return ProduccionDetalle
     */
    public function setIdProducto(\ProductoBundle\Entity\Producto $idProducto = null)
    {
        $this->idProducto = $idProducto;

        return $this;
    }

    /**
     * Get idProducto
     *
     * @return \ProductoBundle\Entity\Producto
     */
    public function getIdProducto()
    {
        return $this->idProducto;
    }
}

