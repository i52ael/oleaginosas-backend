<?php

namespace ProductoBundle\Entity;

/**
 * TipoCambio
 */
class TipoCambio
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $fechaInicio;

    /**
     * @var \DateTime
     */
    private $fechaFinal;

    /**
     * @var string
     */
    private $valorbs;

    /**
     * @var \ProductoBundle\Entity\TipoMoneda
     */
    private $idTipoMoneda;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fechaInicio
     *
     * @param \DateTime $fechaInicio
     *
     * @return TipoCambio
     */
    public function setFechaInicio($fechaInicio)
    {
        $this->fechaInicio = $fechaInicio;

        return $this;
    }

    /**
     * Get fechaInicio
     *
     * @return \DateTime
     */
    public function getFechaInicio()
    {
        return $this->fechaInicio;
    }

    /**
     * Set fechaFinal
     *
     * @param \DateTime $fechaFinal
     *
     * @return TipoCambio
     */
    public function setFechaFinal($fechaFinal)
    {
        $this->fechaFinal = $fechaFinal;

        return $this;
    }

    /**
     * Get fechaFinal
     *
     * @return \DateTime
     */
    public function getFechaFinal()
    {
        return $this->fechaFinal;
    }

    /**
     * Set valorbs
     *
     * @param string $valorbs
     *
     * @return TipoCambio
     */
    public function setValorbs($valorbs)
    {
        $this->valorbs = $valorbs;

        return $this;
    }

    /**
     * Get valorbs
     *
     * @return string
     */
    public function getValorbs()
    {
        return $this->valorbs;
    }

    /**
     * Set idTipoMoneda
     *
     * @param \ProductoBundle\Entity\TipoMoneda $idTipoMoneda
     *
     * @return TipoCambio
     */
    public function setIdTipoMoneda(\ProductoBundle\Entity\TipoMoneda $idTipoMoneda = null)
    {
        $this->idTipoMoneda = $idTipoMoneda;

        return $this;
    }

    /**
     * Get idTipoMoneda
     *
     * @return \ProductoBundle\Entity\TipoMoneda
     */
    public function getIdTipoMoneda()
    {
        return $this->idTipoMoneda;
    }
}

