<?php

namespace ProductoBundle\Entity;

/**
 * TipoRegistroSanitario
 */
class TipoRegistroSanitario
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $orden;


    /**
     * @var string
     */
    private $nombre;

    /**
     * @var string
     */
    private $abreviacion;

    /**
     * 
     * @var boolean
     */
    private $estado;
    /**
     * @return boolean
     */


    /**
     * @param boolean $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return TipoEntidad
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set abreviacion
     *
     * @param string $abreviacion
     *
     * @return TipoEntidad
     */
    public function setAbreviacion($abreviacion)
    {
        $this->abreviacion = $abreviacion;

        return $this;
    }

    /**
     * Get abreviacion
     *
     * @return string
     */
    public function getAbreviacion()
    {
        return $this->abreviacion;
    }
}

