<?php

namespace ProductoBundle\Entity;

/**
 * DiasAno
 */
class DiasAno
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $fecha;

    /**
     * @var string
     */
    private $nroDia;

    /**
     * @var string
     */
    private $habil;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     *
     * @return DiasAno
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set nroDia
     *
     * @param string $nroDia
     *
     * @return DiasAno
     */
    public function setNroDia($nroDia)
    {
        $this->nroDia = $nroDia;

        return $this;
    }

    /**
     * Get nroDia
     *
     * @return string
     */
    public function getNroDia()
    {
        return $this->nroDia;
    }

    /**
     * Set habil
     *
     * @param string $habil
     *
     * @return DiasAno
     */
    public function setHabil($habil)
    {
        $this->habil = $habil;

        return $this;
    }

    /**
     * Get habil
     *
     * @return string
     */
    public function getHabil()
    {
        return $this->habil;
    }
}

