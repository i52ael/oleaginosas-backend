<?php

namespace ProductoBundle\Entity;

/**
 * CompromisoEntidad
 */
class CompromisoEntidad
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $compromisoEntidad;

    /**
     * @var integer
     */
    private $idUsuarioCreacion;

    /**
     * @var integer
     */
    private $idUsuarioUltimaModificacion;

    /**
     * @var \DateTime
     */
    private $fechaCreacion;

    /**
     * @var \DateTime
     */
    private $fechaUltimaModificacion;

    /**
     * @var \ProductoBundle\Entity\Entidad
     */
    private $idEntidad;

    /**
     * @var \ProductoBundle\Entity\Campania
     */
    private $idCampania;

    /**
     * @var \ProductoBundle\Entity\TipoTransaccion
     */
    private $idTipoTransaccion;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set compromisoEntidad
     *
     * @param string $compromisoEntidad
     *
     * @return CompromisoEntidad
     */
    public function setCompromisoEntidad($compromisoEntidad)
    {
        $this->compromisoEntidad = $compromisoEntidad;

        return $this;
    }

    /**
     * Get compromisoEntidad
     *
     * @return string
     */
    public function getCompromisoEntidad()
    {
        return $this->compromisoEntidad;
    }

    /**
     * Set idUsuarioCreacion
     *
     * @param integer $idUsuarioCreacion
     *
     * @return CompromisoEntidad
     */
    public function setIdUsuarioCreacion($idUsuarioCreacion)
    {
        $this->idUsuarioCreacion = $idUsuarioCreacion;

        return $this;
    }

    /**
     * Get idUsuarioCreacion
     *
     * @return integer
     */
    public function getIdUsuarioCreacion()
    {
        return $this->idUsuarioCreacion;
    }

    /**
     * Set idUsuarioUltimaModificacion
     *
     * @param integer $idUsuarioUltimaModificacion
     *
     * @return CompromisoEntidad
     */
    public function setIdUsuarioUltimaModificacion($idUsuarioUltimaModificacion)
    {
        $this->idUsuarioUltimaModificacion = $idUsuarioUltimaModificacion;

        return $this;
    }

    /**
     * Get idUsuarioUltimaModificacion
     *
     * @return integer
     */
    public function getIdUsuarioUltimaModificacion()
    {
        return $this->idUsuarioUltimaModificacion;
    }

    /**
     * Set fechaCreacion
     *
     * @param \DateTime $fechaCreacion
     *
     * @return CompromisoEntidad
     */
    public function setFechaCreacion($fechaCreacion)
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    /**
     * Get fechaCreacion
     *
     * @return \DateTime
     */
    public function getFechaCreacion()
    {
        return $this->fechaCreacion;
    }

    /**
     * Set fechaUltimaModificacion
     *
     * @param \DateTime $fechaUltimaModificacion
     *
     * @return CompromisoEntidad
     */
    public function setFechaUltimaModificacion($fechaUltimaModificacion)
    {
        $this->fechaUltimaModificacion = $fechaUltimaModificacion;

        return $this;
    }

    /**
     * Get fechaUltimaModificacion
     *
     * @return \DateTime
     */
    public function getFechaUltimaModificacion()
    {
        return $this->fechaUltimaModificacion;
    }

    /**
     * Set idEntidad
     *
     * @param \ProductoBundle\Entity\Entidad $idEntidad
     *
     * @return CompromisoEntidad
     */
    public function setIdEntidad(\ProductoBundle\Entity\Entidad $idEntidad = null)
    {
        $this->idEntidad = $idEntidad;

        return $this;
    }

    /**
     * Get idEntidad
     *
     * @return \ProductoBundle\Entity\Entidad
     */
    public function getIdEntidad()
    {
        return $this->idEntidad;
    }

    /**
     * Set idCampania
     *
     * @param \ProductoBundle\Entity\Campania $idCampania
     *
     * @return CompromisoEntidad
     */
    public function setIdCampania(\ProductoBundle\Entity\Campania $idCampania = null)
    {
        $this->idCampania = $idCampania;

        return $this;
    }

    /**
     * Get idCampania
     *
     * @return \ProductoBundle\Entity\Campania
     */
    public function getIdCampania()
    {
        return $this->idCampania;
    }

    /**
     * Set idTipoTransaccion
     *
     * @param \ProductoBundle\Entity\TipoTransaccion $idTipoTransaccion
     *
     * @return CompromisoEntidad
     */
    public function setIdTipoTransaccion(\ProductoBundle\Entity\TipoTransaccion $idTipoTransaccion = null)
    {
        $this->idTipoTransaccion = $idTipoTransaccion;

        return $this;
    }

    /**
     * Get idTipoTransaccion
     *
     * @return \ProductoBundle\Entity\TipoTransaccion
     */
    public function getIdTipoTransaccion()
    {
        return $this->idTipoTransaccion;
    }
    /**
     * @var \ProductoBundle\Entity\Producto
     */
    private $idProducto;


    /**
     * Set idProducto
     *
     * @param \ProductoBundle\Entity\Producto $idProducto
     *
     * @return CompromisoEntidad
     */
    public function setIdProducto(\ProductoBundle\Entity\Producto $idProducto = null)
    {
        $this->idProducto = $idProducto;

        return $this;
    }

    /**
     * Get idProducto
     *
     * @return \ProductoBundle\Entity\Producto
     */
    public function getIdProducto()
    {
        return $this->idProducto;
    }
    /**
     * @var boolean
     */
    private $eliminado;


    /**
     * Set eliminado
     *
     * @param boolean $eliminado
     *
     * @return CompromisoEntidad
     */
    public function setEliminado($eliminado)
    {
        $this->eliminado = $eliminado;

        return $this;
    }

    /**
     * Get eliminado
     *
     * @return boolean
     */
    public function getEliminado()
    {
        return $this->eliminado;
    }
}
