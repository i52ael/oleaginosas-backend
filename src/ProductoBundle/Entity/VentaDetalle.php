<?php

namespace ProductoBundle\Entity;

/**
 * VentaDetalle
 */
class VentaDetalle
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $cantidad;

    /**
     * @var string
     */
    private $precioUnitario;

    /**
     * @var string
     */
    private $precioTotal;

    /**
     * @var string
     */
    private $nroFactura;

    /**
     * @var \DateTime
     */
    private $fechaVenta;

    /**
     * @var \ProductoBundle\Entity\Venta
     */
    private $idVenta;

    /**
     * @var \ProductoBundle\Entity\Producto
     */
    private $idProducto;

    /**
     * @var \ProductoBundle\Entity\RelacionEntidad
     */
    private $idRelacionEntidadCliente;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cantidad
     *
     * @param string $cantidad
     *
     * @return VentaDetalle
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    /**
     * Get cantidad
     *
     * @return string
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * Set precioUnitario
     *
     * @param string $precioUnitario
     *
     * @return VentaDetalle
     */
    public function setPrecioUnitario($precioUnitario)
    {
        $this->precioUnitario = $precioUnitario;

        return $this;
    }

    /**
     * Get precioUnitario
     *
     * @return string
     */
    public function getPrecioUnitario()
    {
        return $this->precioUnitario;
    }

    /**
     * Set precioTotal
     *
     * @param string $precioTotal
     *
     * @return VentaDetalle
     */
    public function setPrecioTotal($precioTotal)
    {
        $this->precioTotal = $precioTotal;

        return $this;
    }

    /**
     * Get precioTotal
     *
     * @return string
     */
    public function getPrecioTotal()
    {
        return $this->precioTotal;
    }

    /**
     * Set nroFactura
     *
     * @param string $nroFactura
     *
     * @return VentaDetalle
     */
    public function setNroFactura($nroFactura)
    {
        $this->nroFactura = $nroFactura;

        return $this;
    }

    /**
     * Get nroFactura
     *
     * @return string
     */
    public function getNroFactura()
    {
        return $this->nroFactura;
    }

    /**
     * Set fechaVenta
     *
     * @param \DateTime $fechaVenta
     *
     * @return VentaDetalle
     */
    public function setFechaVenta($fechaVenta)
    {
        $this->fechaVenta = $fechaVenta;

        return $this;
    }

    /**
     * Get fechaVenta
     *
     * @return \DateTime
     */
    public function getFechaVenta()
    {
        return $this->fechaVenta;
    }

    /**
     * Set idVenta
     *
     * @param \ProductoBundle\Entity\Venta $idVenta
     *
     * @return VentaDetalle
     */
    public function setIdVenta(\ProductoBundle\Entity\Venta $idVenta = null)
    {
        $this->idVenta = $idVenta;

        return $this;
    }

    /**
     * Get idVenta
     *
     * @return \ProductoBundle\Entity\Venta
     */
    public function getIdVenta()
    {
        return $this->idVenta;
    }

    /**
     * Set idProducto
     *
     * @param \ProductoBundle\Entity\Producto $idProducto
     *
     * @return VentaDetalle
     */
    public function setIdProducto(\ProductoBundle\Entity\Producto $idProducto = null)
    {
        $this->idProducto = $idProducto;

        return $this;
    }

    /**
     * Get idProducto
     *
     * @return \ProductoBundle\Entity\Producto
     */
    public function getIdProducto()
    {
        return $this->idProducto;
    }

    /**
     * Set idRelacionEntidadCliente
     *
     * @param \ProductoBundle\Entity\RelacionEntidad $idRelacionEntidadCliente
     *
     * @return VentaDetalle
     */
    public function setIdRelacionEntidadCliente(\ProductoBundle\Entity\RelacionEntidad $idRelacionEntidadCliente = null)
    {
        $this->idRelacionEntidadCliente = $idRelacionEntidadCliente;

        return $this;
    }

    /**
     * Get idRelacionEntidadCliente
     *
     * @return \ProductoBundle\Entity\RelacionEntidad
     */
    public function getIdRelacionEntidadCliente()
    {
        return $this->idRelacionEntidadCliente;
    }
}

