<?php

namespace ProductoBundle\Entity;

/**
 * ProduccionAgricola
 */
class ProduccionAgricola
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $estimado;

    /**
     * @var \DateTime
     */
    private $fechaDesde;

    /**
     * @var \DateTime
     */
    private $fechaHasta;

    /**
     * @var integer
     */
    private $idUsuarioCreacion;

    /**
     * @var integer
     */
    private $idUsuarioUltimaModificacion;

    /**
     * @var \DateTime
     */
    private $fechaUltimaModificacion;

    /**
     * @var boolean
     */
    private $eliminado;

    /**
     * @var \DateTime
     */
    private $fechaCreacion = 'now()';

    /**
     * @var string
     */
    private $gestionAgricola;

    /**
     * @var \ProductoBundle\Entity\Medida
     */
    private $idMedida;

    /**
     * @var \ProductoBundle\Entity\Producto
     */
    private $idProducto;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set estimado
     *
     * @param string $estimado
     *
     * @return ProduccionAgricola
     */
    public function setEstimado($estimado)
    {
        $this->estimado = $estimado;

        return $this;
    }

    /**
     * Get estimado
     *
     * @return string
     */
    public function getEstimado()
    {
        return $this->estimado;
    }

    /**
     * Set fechaDesde
     *
     * @param \DateTime $fechaDesde
     *
     * @return ProduccionAgricola
     */
    public function setFechaDesde($fechaDesde)
    {
        $this->fechaDesde = $fechaDesde;

        return $this;
    }

    /**
     * Get fechaDesde
     *
     * @return \DateTime
     */
    public function getFechaDesde()
    {
        return $this->fechaDesde;
    }

    /**
     * Set fechaHasta
     *
     * @param \DateTime $fechaHasta
     *
     * @return ProduccionAgricola
     */
    public function setFechaHasta($fechaHasta)
    {
        $this->fechaHasta = $fechaHasta;

        return $this;
    }

    /**
     * Get fechaHasta
     *
     * @return \DateTime
     */
    public function getFechaHasta()
    {
        return $this->fechaHasta;
    }

    /**
     * Set idUsuarioCreacion
     *
     * @param integer $idUsuarioCreacion
     *
     * @return ProduccionAgricola
     */
    public function setIdUsuarioCreacion($idUsuarioCreacion)
    {
        $this->idUsuarioCreacion = $idUsuarioCreacion;

        return $this;
    }

    /**
     * Get idUsuarioCreacion
     *
     * @return integer
     */
    public function getIdUsuarioCreacion()
    {
        return $this->idUsuarioCreacion;
    }

    /**
     * Set idUsuarioUltimaModificacion
     *
     * @param integer $idUsuarioUltimaModificacion
     *
     * @return ProduccionAgricola
     */
    public function setIdUsuarioUltimaModificacion($idUsuarioUltimaModificacion)
    {
        $this->idUsuarioUltimaModificacion = $idUsuarioUltimaModificacion;

        return $this;
    }

    /**
     * Get idUsuarioUltimaModificacion
     *
     * @return integer
     */
    public function getIdUsuarioUltimaModificacion()
    {
        return $this->idUsuarioUltimaModificacion;
    }

    /**
     * Set fechaUltimaModificacion
     *
     * @param \DateTime $fechaUltimaModificacion
     *
     * @return ProduccionAgricola
     */
    public function setFechaUltimaModificacion($fechaUltimaModificacion)
    {
        $this->fechaUltimaModificacion = $fechaUltimaModificacion;

        return $this;
    }

    /**
     * Get fechaUltimaModificacion
     *
     * @return \DateTime
     */
    public function getFechaUltimaModificacion()
    {
        return $this->fechaUltimaModificacion;
    }

    /**
     * Set eliminado
     *
     * @param boolean $eliminado
     *
     * @return ProduccionAgricola
     */
    public function setEliminado($eliminado)
    {
        $this->eliminado = $eliminado;

        return $this;
    }

    /**
     * Get eliminado
     *
     * @return boolean
     */
    public function getEliminado()
    {
        return $this->eliminado;
    }

    /**
     * Set fechaCreacion
     *
     * @param \DateTime $fechaCreacion
     *
     * @return ProduccionAgricola
     */
    public function setFechaCreacion($fechaCreacion)
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    /**
     * Get fechaCreacion
     *
     * @return \DateTime
     */
    public function getFechaCreacion()
    {
        return $this->fechaCreacion;
    }

    /**
     * Set gestionAgricola
     *
     * @param string $gestionAgricola
     *
     * @return ProduccionAgricola
     */
    public function setGestionAgricola($gestionAgricola)
    {
        $this->gestionAgricola = $gestionAgricola;

        return $this;
    }

    /**
     * Get gestionAgricola
     *
     * @return string
     */
    public function getGestionAgricola()
    {
        return $this->gestionAgricola;
    }

    /**
     * Set idMedida
     *
     * @param \ProductoBundle\Entity\Medida $idMedida
     *
     * @return ProduccionAgricola
     */
    public function setIdMedida(\ProductoBundle\Entity\Medida $idMedida = null)
    {
        $this->idMedida = $idMedida;

        return $this;
    }

    /**
     * Get idMedida
     *
     * @return \ProductoBundle\Entity\Medida
     */
    public function getIdMedida()
    {
        return $this->idMedida;
    }

    /**
     * Set idProducto
     *
     * @param \ProductoBundle\Entity\Producto $idProducto
     *
     * @return ProduccionAgricola
     */
    public function setIdProducto(\ProductoBundle\Entity\Producto $idProducto = null)
    {
        $this->idProducto = $idProducto;

        return $this;
    }

    /**
     * Get idProducto
     *
     * @return \ProductoBundle\Entity\Producto
     */
    public function getIdProducto()
    {
        return $this->idProducto;
    }
}

