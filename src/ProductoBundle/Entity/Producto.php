<?php

namespace ProductoBundle\Entity;

/**
 * Producto
 */
class Producto
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $nombre;

    /**
     * @var boolean
     */
    private $vigente;

    /**
     * @var boolean
     */
    private $eliminado;

    /**
     * @var \DateTime
     */
    private $fechaCreacion = 'now()';

    /**
     * @var \DateTime
     */
    private $fechaUltimaModificacion;

    /**
     * @var integer
     */
    private $idUsuarioCreacion;

    /**
     * @var integer
     */
    private $idUsuarioUltimaModificacion;

    /**
     * @var string
     */
    private $medida;

    /**
     * @var string
     */
    private $descripcion;

    /**
     * @var \ProductoBundle\Entity\Fase
     */
    private $idFase;

    /**
     * @var \ProductoBundle\Entity\Complejo
     */
    private $idComplejo;

    /**
     * @var \ProductoBundle\Entity\Medida
     */
    private $idMedida;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Producto
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set vigente
     *
     * @param boolean $vigente
     *
     * @return Producto
     */
    public function setVigente($vigente)
    {
        $this->vigente = $vigente;

        return $this;
    }

    /**
     * Get vigente
     *
     * @return boolean
     */
    public function getVigente()
    {
        return $this->vigente;
    }

    /**
     * Set eliminado
     *
     * @param boolean $eliminado
     *
     * @return Producto
     */
    public function setEliminado($eliminado)
    {
        $this->eliminado = $eliminado;

        return $this;
    }

    /**
     * Get eliminado
     *
     * @return boolean
     */
    public function getEliminado()
    {
        return $this->eliminado;
    }

    /**
     * Set fechaCreacion
     *
     * @param \DateTime $fechaCreacion
     *
     * @return Producto
     */
    public function setFechaCreacion($fechaCreacion)
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    /**
     * Get fechaCreacion
     *
     * @return \DateTime
     */
    public function getFechaCreacion()
    {
        return $this->fechaCreacion;
    }

    /**
     * Set fechaUltimaModificacion
     *
     * @param \DateTime $fechaUltimaModificacion
     *
     * @return Producto
     */
    public function setFechaUltimaModificacion($fechaUltimaModificacion)
    {
        $this->fechaUltimaModificacion = $fechaUltimaModificacion;

        return $this;
    }

    /**
     * Get fechaUltimaModificacion
     *
     * @return \DateTime
     */
    public function getFechaUltimaModificacion()
    {
        return $this->fechaUltimaModificacion;
    }

    /**
     * Set idUsuarioCreacion
     *
     * @param integer $idUsuarioCreacion
     *
     * @return Producto
     */
    public function setIdUsuarioCreacion($idUsuarioCreacion)
    {
        $this->idUsuarioCreacion = $idUsuarioCreacion;

        return $this;
    }

    /**
     * Get idUsuarioCreacion
     *
     * @return integer
     */
    public function getIdUsuarioCreacion()
    {
        return $this->idUsuarioCreacion;
    }

    /**
     * Set idUsuarioUltimaModificacion
     *
     * @param integer $idUsuarioUltimaModificacion
     *
     * @return Producto
     */
    public function setIdUsuarioUltimaModificacion($idUsuarioUltimaModificacion)
    {
        $this->idUsuarioUltimaModificacion = $idUsuarioUltimaModificacion;

        return $this;
    }

    /**
     * Get idUsuarioUltimaModificacion
     *
     * @return integer
     */
    public function getIdUsuarioUltimaModificacion()
    {
        return $this->idUsuarioUltimaModificacion;
    }

    /**
     * Set medida
     *
     * @param string $medida
     *
     * @return Producto
     */
    public function setMedida($medida)
    {
        $this->medida = $medida;

        return $this;
    }

    /**
     * Get medida
     *
     * @return string
     */
    public function getMedida()
    {
        return $this->medida;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Producto
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set idFase
     *
     * @param \ProductoBundle\Entity\Fase $idFase
     *
     * @return Producto
     */
    public function setIdFase(\ProductoBundle\Entity\Fase $idFase = null)
    {
        $this->idFase = $idFase;

        return $this;
    }

    /**
     * Get idFase
     *
     * @return \ProductoBundle\Entity\Fase
     */
    public function getIdFase()
    {
        return $this->idFase;
    }

    /**
     * Set idComplejo
     *
     * @param \ProductoBundle\Entity\Complejo $idComplejo
     *
     * @return Producto
     */
    public function setIdComplejo(\ProductoBundle\Entity\Complejo $idComplejo = null)
    {
        $this->idComplejo = $idComplejo;

        return $this;
    }

    /**
     * Get idComplejo
     *
     * @return \ProductoBundle\Entity\Complejo
     */
    public function getIdComplejo()
    {
        return $this->idComplejo;
    }

    /**
     * Set idMedida
     *
     * @param \ProductoBundle\Entity\Medida $idMedida
     *
     * @return Producto
     */
    public function setIdMedida(\ProductoBundle\Entity\Medida $idMedida = null)
    {
        $this->idMedida = $idMedida;

        return $this;
    }

    /**
     * Get idMedida
     *
     * @return \ProductoBundle\Entity\Medida
     */
    public function getIdMedida()
    {
        return $this->idMedida;
    }
    /**
     * @var string
     */
    private $codigo;


    /**
     * Set codigo
     *
     * @param string $codigo
     *
     * @return Producto
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo
     *
     * @return string
     */
    public function getCodigo()
    {
        return $this->codigo;
    }
    private $nandina;
    
    public function setNandina($nandina)
    {
    	$this->nandina = $nandina;
    	
    	return $this;
    }
    
    /**
     * Get codigo
     *
     * @return string
     */
    public function getNandina()
    {
    	return $this->nandina;
    }
    
}
