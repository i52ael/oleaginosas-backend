<?php

namespace ProductoBundle\Entity;

/**
 * Compra
 */
class Compra
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $fechaRegistro = '2017-10-11 17:56:17.040326';

    /**
     * @var \ProductoBundle\Entity\PeriodoFase
     */
    private $idPeriodoFase;

    /**
     * @var \ProductoBundle\Entity\RelacionEntidad
     */
    private $idRelacionEntidad;

    /**
     * @var \ProductoBundle\Entity\TipoCambio
     */
    private $idTipoCambio;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fechaRegistro
     *
     * @param \DateTime $fechaRegistro
     *
     * @return Compra
     */
    public function setFechaRegistro($fechaRegistro)
    {
        $this->fechaRegistro = $fechaRegistro;

        return $this;
    }

    /**
     * Get fechaRegistro
     *
     * @return \DateTime
     */
    public function getFechaRegistro()
    {
        return $this->fechaRegistro;
    }

    /**
     * Set idPeriodoFase
     *
     * @param \ProductoBundle\Entity\PeriodoFase $idPeriodoFase
     *
     * @return Compra
     */
    public function setIdPeriodoFase(\ProductoBundle\Entity\PeriodoFase $idPeriodoFase = null)
    {
        $this->idPeriodoFase = $idPeriodoFase;

        return $this;
    }

    /**
     * Get idPeriodoFase
     *
     * @return \ProductoBundle\Entity\PeriodoFase
     */
    public function getIdPeriodoFase()
    {
        return $this->idPeriodoFase;
    }

    /**
     * Set idRelacionEntidad
     *
     * @param \ProductoBundle\Entity\RelacionEntidad $idRelacionEntidad
     *
     * @return Compra
     */
    public function setIdRelacionEntidad(\ProductoBundle\Entity\RelacionEntidad $idRelacionEntidad = null)
    {
        $this->idRelacionEntidad = $idRelacionEntidad;

        return $this;
    }

    /**
     * Get idRelacionEntidad
     *
     * @return \ProductoBundle\Entity\RelacionEntidad
     */
    public function getIdRelacionEntidad()
    {
        return $this->idRelacionEntidad;
    }

    /**
     * Set idTipoCambio
     *
     * @param \ProductoBundle\Entity\TipoCambio $idTipoCambio
     *
     * @return Compra
     */
    public function setIdTipoCambio(\ProductoBundle\Entity\TipoCambio $idTipoCambio = null)
    {
        $this->idTipoCambio = $idTipoCambio;

        return $this;
    }

    /**
     * Get idTipoCambio
     *
     * @return \ProductoBundle\Entity\TipoCambio
     */
    public function getIdTipoCambio()
    {
        return $this->idTipoCambio;
    }
}

