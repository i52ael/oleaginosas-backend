<?php
namespace ProductoBundle\Modelo;

use JMS\Serializer\Annotation as Serial;
use Symfony\Component\Validator\Constraints as Assert;



class ModeloUsuario{
    
    public $idUser;
    public $userName;
    public $email;
    public $roles;
    
}