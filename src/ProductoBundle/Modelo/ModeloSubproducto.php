<?php
namespace ProductoBundle\Modelo;

use JMS\Serializer\Annotation as Serial;
use Symfony\Component\Validator\Constraints as Assert;



class ModeloSubproducto{
    
     /**
     * @var int id
     * @Serial\Type("integer")
     **/
     public $id;
    /**
     * @var string $nombre
     *
     * @Assert\Length(min=3)
     * @Assert\NotNull()
     * @Assert\Length(min=2, max=200)
     * @Serial\Type("string")
     */
    public $nombreComercial;
    /**
     * @var integer $idcomplejo
     * @Assert\NotNull()
     * @Serial\Type("integer")
     */
    public $idFase;
    
    /**
     * @var integer $idMedida
     *
     * @Assert\NotNull()
     * @Serial\Type("integer")
     */
    
    
    public $idMedida;
    
    /**
     * @var string $medida
     *
     * @Assert\NotNull()
     * @Serial\Type("float")
     */
        
    public $medida;
    
    /**
     * @var string $idProducto
     *
     * @Assert\Length(min=1)
     * @Assert\NotNull()
     * @Serial\Type("integer")
     */

    public $idProducto;

    /**
     * @var string $densidad
     *
     * @Assert\Length(min=1)
     * @Serial\Type("float")
     */

    public $densidad;

    /**
     * @var string $integer
     *
     * @Assert\Length(min=1)
     * @Serial\Type("integer")
     */

    public $idEntidad;

    /**
     * @var string $idMagnitud
     *
     * @Assert\Length(min=1)
     * @Serial\Type("integer")
     */
    public $idMagnitud;

    /**
     * @var string $codigo
     *
     * @Assert\Length(min=1)
     * @Serial\Type("string")
     */
    public $codigo;

}