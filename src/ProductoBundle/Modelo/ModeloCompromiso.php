<?php

namespace ProductoBundle\Modelo;

use JMS\Serializer\Annotation as Serial;
use Symfony\Component\Validator\Constraints as Assert;



class ModeloCompromiso{

    /**
     * @var int id
     * @Serial\Type("integer")
     **/
    public $id;
    /**
     * @var string $idProducto
     *
     * @Assert\NotNull()
     * @Serial\Type("integer")
     */
    public $idProducto;

    /**
     * @var string $idTipoTransaccion
     * @Assert\NotNull()
     * @Serial\Type("integer")
     ***/

    public $idTipoTransaccion;

    /**
     * @var string $idCampania
     * @Assert\NotNull()
     * @Serial\Type("integer")
     ***/

    public $idCampania;

    /**
     * @var string $idEntidad
     * @Assert\NotNull()
     * @Serial\Type("integer")
     ***/

    public $idEntidad;

    /**
     * @var string $compromiso
     * @Assert\NotNull()
     * @Serial\Type("float")
     */

    public $compromiso;



}