<?php
namespace ProductoBundle\Modelo;

use JMS\Serializer\Annotation as Serial;
use Symfony\Component\Validator\Constraints as Assert;



class ModeloEntidad{
    
     /**
     * @var int id
     * @Serial\Type("integer")
     **/
     public $id;
    /**
     * @var string $nombre
     *
     * @Assert\Length(min=3)
     * @Assert\NotNull()
     * @Assert\Length(min=2, max=200)
     * @Serial\Type("string")
     */
    public $razonSocial;
    
    /**
     * @var string $descripcion
     *
     * @Assert\NotNull()
     * @Assert\Length(min=5, max=35)
     * @Serial\Type("string")
     */
    
    public $numeroIdentificacion;
    
    /**
     * @var integer $idComplejo
     *
     * @Assert\NotNull()
     * @Serial\Type("integer")
     */
    
    public $idComplejo;
    
    /**
     * @var integer $idFase
     *
     * @Assert\NotNull()
     * @Serial\Type("integer")
     */
    
    public $idFase;
    
    /**
     * @var integer $idMedida
     *
     * @Assert\NotNull()
     * @Serial\Type("integer")
     */
    
    
    public $idTipoEntidad;
    
    /**
     * @var string $idTipoDocumento
     *
     * @Assert\NotNull()
     * @Serial\Type("integer")
     */
        
    public $idTipoDocumento;
    
    /**
     * @var string $direccion
     *
     * @Assert\Length(min=1)
     * @Assert\NotNull()
     * @Serial\Type("string")
     */
    
    public $direccion;
    /**
     * @var string $telefonos
     *
     * @Serial\Type("string")
     */
    
    public $telefonos;
    
    /**
     * @var string $registroSanitario
     *
     * @Serial\Type("string")
     */
    
    public $registroSanitario;
    
    /**
     * @var string $fundempresa
     *
     * @Serial\Type("string")
     */
    
    public $fundempresa;
    
    /**
     * @var string $correo
     *
     * @Serial\Type("string")
     */
    
    public $correo;
    /**
     * @var object $persona
     * @Serial\Type("array")
     */
    public  $persona;
    /**
     * @var object $beneficiario
     * @Serial\Type("boolean")
     */
    public  $beneficiario;

    /**
     * @var integer $idRegimen
     * @Serial\Type("integer")
     *
     */
    public $idRegimen;

    /**
     * @var integer $idTipoRegistroSanitario
     * @Serial\Type("integer")
     *
     */
    public $idTipoRegistroSanitario;
    
}