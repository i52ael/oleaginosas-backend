<?php
namespace ProductoBundle\Modelo;

use JMS\Serializer\Annotation as Serial;
use Symfony\Component\Validator\Constraints as Assert;



class ModeloBandaPrecio{
    
     /**
     * @var int id
     * @Serial\Type("integer")
     **/
     public $id;
    /**
     * @var string $nombre
     * @Assert\NotNull()
     * @Serial\Type("integer")
     */
    public $idProducto;
    
    /**
     * @var string nombre
     * @Serial\Type("string")
     ***/
    
    public $nombreProducto;
    /**
     * @var string $descripcion
     * @Assert\NotNull()
     * @Serial\Type("DateTime<'Y-m-d'>")
     */
    public $fechaDesde;
    /**
     * @var integer $idcomplejo
     *
     * @Assert\NotNull()
     * @Serial\Type("DateTime<'Y-m-d'>")
     */
    
    public $fechaHasta;
    
    /**
     * @var integer $idcase
     *
     * @Assert\NotNull()
     * @Serial\Type("integer")
     */
    
    public $idtipoCambio;
    
    /**
     * @var integer $idcase
     *
     * @Assert\NotNull()
     * @Serial\Type("integer")
     */
    
    public $idTipoMoneda;
    
    
    /**
     * @var integer $idMedida
     *
     * @Assert\NotNull()
     * @Serial\Type("float")
     */    
    
    public $valorMinimo;
    
    /**
     * @var string $medida
     *
     * @Assert\NotNull()
     * @Serial\Type("float")
     */
        
    public $valorMaximo;
    
    
    
}