<?php
namespace ProductoBundle\Modelo;

use JMS\Serializer\Annotation as Serial;
use Symfony\Component\Validator\Constraints as Assert;



class ModeloCapacidad{
    
     /**
     * @var int id
     * @Serial\Type("integer")
     **/
     public $id;

    /**
     * @var integer $idMagnitud
       * @Serial\Type("integer")
     */


    public $idMagnitud;

    /**
     * @var integer $idMedida
     *
     * @Assert\NotNull()
     * @Serial\Type("integer")
     */

    public $idMedida;
   /**
     * @var string $idProducto
     *
     * @Assert\Length(min=1)
     * @Assert\NotNull()
     * @Serial\Type("integer")
     */

    public $idProducto;

    /**
     * @var string $integer
     *
     * @Assert\Length(min=1)
     * @Serial\Type("integer")
     */

    public $idEntidad;

    /**
     * @var string $capacidadInstaladaNominal
     *
     * @Assert\Length(min=1)
     * @Serial\Type("float")
     */
    public $capacidadInstaladaNominal;

    /**
     * @var string $capacidadInstaladaReal
     *
     * @Assert\Length(min=1)
     * @Serial\Type("float")
     */
    public $capacidadInstaladaReal;

    /**
     * @var string $porcentajeRendimiento
     *
     * @Assert\Length(min=1)
     * @Serial\Type("float")
     */
    public $porcentajeRendimiento;

    /**
     * @var string $idProductoPrimario
     *
     * @Assert\Length(min=1)
     * @Serial\Type("integer")
     */
    public $idProductoPrimario;

    /**
     * @var string $rendimiento
     *
     * @Assert\Length(min=0)
     * @Serial\Type("boolean")
     */
    public $rendimiento;
}