<?php
namespace ProductoBundle\Modelo;

use JMS\Serializer\Annotation as Serial;
use Symfony\Component\Validator\Constraints as Assert;



class ModeloPersona{
    
     /**
     * @var int id
     * @Serial\Type("integer")
     **/
     public $id;
    /**
     * @var string $nombre
     *
     * @Assert\Length(min=3)
     * @Assert\NotNull()
     * @Assert\Length(min=2, max=200)
     * @Serial\Type("string")
     */
    public $nombres;

    /**
     * @var string $primerApellido
     *
     * @Assert\Length(min=3)
     * @Assert\NotNull()
     * @Assert\Length(min=2, max=200)
     * @Serial\Type("string")
     */
    public $primerApellido;

    /**
     * @var string $segundoApellido
     *
     * @Assert\Length(min=3)
     * @Assert\Length(min=2, max=200)
     * @Serial\Type("string")
     */
    public $segundoApellido;



    /**
     * @var string $descripcion
     * @Assert\NotNull()
     * @Assert\Length(min=5, max=35)
     * @Serial\Type("string")
     */
    
    public $numeroIdentificacion;
    
    /**
     * @var integer $idTipoDocumento
     *
     * @Assert\NotNull()
     * @Serial\Type("integer")
     */


    public $idTipoDocumento;
    
    /**
     * @var string $direccion
     *
     * @Assert\Length(min=1)
     * @Assert\NotNull()
     * @Serial\Type("string")
     */
    
    public $direccion;
    /**
     * @var string $telefonos
     *
     * @Serial\Type("string")
     */
    
    public $telefonos;
    /**
     * @var string $correo     *
     * @Serial\Type("string")
     */
    
    public  $correo;

}