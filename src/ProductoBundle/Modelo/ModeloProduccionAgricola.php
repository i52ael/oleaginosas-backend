<?php

namespace ProductoBundle\Modelo;

use JMS\Serializer\Annotation as Serial;
use Symfony\Component\Validator\Constraints as Assert;



class ModeloProduccionAgricola{

    /**
     * @var int id
     * @Serial\Type("integer")
     **/
    public $id;
    /**
     * @var string $idPriducto
     *
     * @Assert\NotNull()
     * @Serial\Type("integer")
     */
    public $idProducto;

    /**
     * @var string idMedida
     * @Serial\Type("integer")
     ***/

    public $idMedida;
    /**
     * @var string $desde

     * @Serial\Type("DateTime<'Y-m-d'>")
     */
    public $fechaDesde;

    /**
     * @var integer $hasta
     *
     * @Serial\Type("DateTime<'Y-m-d'>")

     */
    public $fechaHasta;


    /**
     * @var string $estimado
     *
     * @Assert\NotNull()
     * @Serial\Type("float")
     */

    public $estimado;

    /**
     * @var string $produccion_agricola
     *
     * @Assert\NotNull()
     * @Serial\Type("string")
     */

    public $gestionAgricola;



}