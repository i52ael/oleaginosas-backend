<?php
namespace ProductoBundle\Modelo;

use JMS\Serializer\Annotation as Serial;
use Symfony\Component\Validator\Constraints as Assert;



class ModeloProducto{
    
     /**
     * @var int id
     * @Serial\Type("integer")
     **/
     public $id;
    /**
     * @var string $nombre
     *
     * @Assert\Length(min=3)
     * @Assert\NotNull()
     * @Assert\Length(min=2, max=200)
     * @Serial\Type("string")
     */
    public $nombre;
    
    /**
     * @var string $descripcion
     *
     * @Assert\NotNull()
     * @Assert\Length(min=5, max=255)
     * @Serial\Type("string")
     */
    
    public $descripcion;
    
    /**
     * @var integer $idcomplejo
     *
     * @Assert\NotNull()
     * @Serial\Type("integer")
     */
    
    public $idComplejo;
    
    /**
     * @var integer $idcase
     *
     * @Assert\NotNull()
     * @Serial\Type("float")
     */
    
    public $idFase;
    
    /**
     * @var integer $idMedida
     *
     * @Assert\NotNull()
     * @Serial\Type("integer")
     */
    
    
    public $idMedida;
    
    /**
     * @var string $medida
     *
     * @Assert\NotNull()
     * @Serial\Type("float")
     */
        
    public $medida;
    
    /**
     * @var string $idMagnitud
     *
     * @Assert\Length(min=1)
     * @Assert\NotNull()
     * @Serial\Type("integer")
     */
    
    public $idMagnitud;
    /**
     * @var string $idMagnitud
     *
     * @Serial\Type("string")
     */
    
    public $unidadMedida;

    /**
     * @var string $codigo
     *
     * @Serial\Type("string")
     */
    public $codigo;

    /**
     * @var string $nandina
     *
     * @Serial\Type("string")
     */
    public $nandina;
    
}