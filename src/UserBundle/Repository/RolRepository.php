<?php
/**
 * Created by PhpStorm.
 * User: Ivan Mujica
 * Date: 04/12/2017
 * Time: 04:22 PM
 */
namespace  UserBundle\Repository;

class RolRepository extends \Doctrine\ORM\EntityRepository
{
    public function getRoles(){
        $this->createQueryBuilder('r')
            ->select('r');
    }
    public function getRolesModulo($modulo){
            $qb=$this->createQueryBuilder('r')
                ->select('r')
                ->where("r.modulo=:modulo")
                ->setParameter("modulo",$modulo)
                ->orderBy("r.rol","ASC");
            return $qb;
    }

    public function getModulo(){
        $qb=$this->createQueryBuilder('r')
            ->select('r.modulo')
            ->groupBy("r.modulo")
            ->orderBy("r.modulo","ASC")
            ;

        return $qb;
    }


}