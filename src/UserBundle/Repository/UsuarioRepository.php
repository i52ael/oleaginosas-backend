<?php
/**
 * Created by PhpStorm.
 * User: Ivan Mujica
 * Date: 04/12/2017
 * Time: 04:22 PM
 */
namespace  UserBundle\Repository;

class UsuarioRepository extends \Doctrine\ORM\EntityRepository
{
    public function getUsers(){
        $this->createQueryBuilder('u')
            ->select('u');
    }

}