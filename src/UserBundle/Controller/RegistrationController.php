<?php

namespace UserBundle\Controller;

use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use FOS\UserBundle\Controller\RegistrationController as BaseController;
use ProductoBundle\Manager\PersonaManager;
use ProductoBundle\Manager\RelacionEntidadManager;
use ProductoBundle\Modelo\ModeloPersona;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\FOSUserEvents;
use Symfony\Component\Form\FormInterface;
use UserBundle\Service\UsuarioManager;
use JMS\DiExtraBundle\Annotation as DI;

class RegistrationController extends BaseController
{
    use \UserBundle\Helper\ControllerHelper;
    use \ProductoBundle\Helper\Helper;

    protected $usuarioMamanger;
    protected  $personaManager;
    protected  $relacionManager;
    /**
     * @DI\InjectParams({

     *  "usuarioManager"=@DI\Inject("api.manager.usuario"),
     *  "personaManager"=@DI\Inject("api.manager.persona"),
     *  "relacionManager"=@DI\Inject("api.manager.relacion"),

     * })
     *
     */

    public function __construct(UsuarioManager $usuarioManager,PersonaManager $personaManager,RelacionEntidadManager $relacionManager)
    {
        $this->usuarioMamanger=$usuarioManager;
        $this->personaManager=$personaManager;
        $this->relacionManager=$relacionManager;
    }
    /**
     * @Route("/register")
     * @Method("POST")
     */

    public function registerAction(Request $request)
    {
        // @var $formFactory \FOS\UserBundle\Form\Factory\FactoryInterface


    }

  /*  public function registerAction(Request $request)
    {
        // @var $formFactory \FOS\UserBundle\Form\Factory\FactoryInterface
        
        $formFactory = $this->get('fos_user.registration.form.factory');
        // @var $userManager \FOS\UserBundle\Model\UserManagerInterface
        $userManager = $this->get('fos_user.user_manager');
        //@var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface
        $dispatcher = $this->get('event_dispatcher');

        $user = $userManager->createUser();
        $user->setEnabled(true);

        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        $form = $formFactory->createForm(array('csrf_protection' => false));
        $form->setData($user);

        $this->processForm($request, $form);

        if ($form->isValid()) {
            $event = new FormEvent($form, $request);
            $dispatcher->dispatch(FOSUserEvents::REGISTRATION_SUCCESS, $event);
           // $user->setRoles(array("ROLE_IVAN","ROLE_ADMIN","ROLE_USER"));
            $user->setRoles(array("ROLE_ENTIDAD","ROLE_USER"));
            $userManager->updateUser($user);

            $response = new Response($this->serialize('User created.=>'), Response::HTTP_CREATED);
        } else {
            throw $this->throwApiProblemValidationException($form);
        }

        return $this->setBaseHeaders($response);
    }
    */



    /**
     * @param Request $request
     * @param FormInterface $form
     */
    private function processForm(Request $request, FormInterface $form)
    {
        $data = json_decode($request->getContent(), true);
        if ($data === null) {
            throw new BadRequestHttpException();
        }

        $form->submit($data);
    }

    /**
     * Returns response in case of invalid request.
     *
     * @param FormInterface $form
     *
     * @return ApiProblemException
     * @todo Make custom response for invalid request
     */
    private function throwApiProblemValidationException(FormInterface $form)
    {
        $errors = $this->getErrorsFromForm($form);

        throw new BadRequestHttpException($this->serialize($errors));
    }

    /**
     * Returns form errors.
     *
     * @param FormInterface $form
     *
     * @return array
     */
    private function getErrorsFromForm(FormInterface $form)
    {
        $errors = [];

        foreach ($form->getErrors() as $key => $error) {
            $template = $error->getMessageTemplate();
            $parameters = $error->getMessageParameters();

            foreach ($parameters as $var => $value) {
                $template = str_replace($var, $value, $template);
            }
            $errors[$key] = $template;
        }

        foreach ($form->all() as $childForm) {
            if ($childForm instanceof FormInterface) {
                if ($childErrors = $this->getErrorsFromForm($childForm)) {
                    $errors[$childForm->getName()] = $childErrors;
                }
            }
        }

        return $errors;
    }

    /**
     * @Route("/registrar", name="user_register")
     * @Method("POST")
     */
    public function registrarAction(Request $request)
    {

        $contenido = $request->getContent();
        $usuario=json_decode($contenido,true);
        $persona=$usuario['persona'];
        $tipo=$usuario['tipoUsuario'];

        $user=$this->ObtenerUser($request);
        if ($tipo=='e'){
            if($persona['id']==null){
                return $this->json(array("mensaje"=>"Para registrar un nuevo usuario de tipo entidad, debe estar registrado previamente como representante."),Response::HTTP_PARTIAL_CONTENT);
            }
            $relaciones = $this->relacionManager->relacionesVigentesPersona($persona['id']);
            if(count($relaciones)==0){
                return $this->json(array("mensaje"=>"Para registrar un nuevo usuario de tipo entidad, debe estar registrado previamente como representante de alguna entidad."),Response::HTTP_PARTIAL_CONTENT);
            }

            $arr_usuario=$this->usuarioMamanger->recuperaUsuario($persona['id']);
            if(!empty($arr_usuario)){
                return $this->json(array("mensaje"=>"Esta persona ya esta asociada a una cuenta de usuario."),Response::HTTP_PARTIAL_CONTENT);
            }
        }

        if($tipo=='f'){
            $personaB=$this->personaManager->recuperarNumero($persona['numeroIdentificacion']);
            if($personaB!=null){
                return $this->json(array("mensaje"=>"Esta persona ya se encuentra registrada en el sistema"),Response::HTTP_PARTIAL_CONTENT);
            }
        }
        //verificar si no existe un CI con ese numero

        try{
            $mpersona=new ModeloPersona();

            $mpersona->id=$persona['id'];
            $mpersona->numeroIdentificacion=$persona['numeroIdentificacion'];
            $mpersona->nombres=$persona['nombres'];
            $mpersona->primerApellido=$persona['primerApellido'];
            $mpersona->segundoApellido=$persona['segundoApellido'];
            $mpersona->idTipoDocumento=$persona['idTipoDocumento'];
            $mpersona->direccion=$persona['direccion'];
            $mpersona->telefonos=$persona['telefonos'];
            $mpersona->correo=$persona['correo'];
            if ($mpersona->id==null){
                $personaGuardada=$this->personaManager->guardar($mpersona,$user);
            }
            else{
                $personaGuardada=$this->personaManager->actualizar($mpersona,$user);
            }

            $userManager = $this->get('fos_user.user_manager');
            $user = $userManager->createUser();
            $user->setEnabled(true);
            $user->setEmailCanonical($personaGuardada->getCorreo());
            $user->setEmail($personaGuardada->getCorreo());
            $user->setPlainPassword($usuario['password']);
            $user->setUsername($usuario['username']);
            $user->setUsernameCanonical($usuario['username']);
            $user->setIdPersona($personaGuardada->getId());
            $user->setTipo($tipo);
            $user->setRoles(array("ROLE_USER"));
            $userManager->updateUser($user);

            return $this->json($persona['id'],Response::HTTP_OK);
        }
        catch (UniqueConstraintViolationException $ex){
            return $this->json(array("Algun dato de tipo identificador ya existe en el sistema. No se pudo guardar el registro."),Response::HTTP_BAD_REQUEST);
        }

    }

    /**
     * @Route("/actualizar", name="user_actualizar")
     * @Method("POST")
     */
    public function actualizarAction(Request $request)
    {

        $contenido = $request->getContent();
        $usuario=json_decode($contenido,true);
        $persona=$usuario['persona'];
        $tipo=$usuario['tipoUsuario'];

        $user=$this->ObtenerUser($request);

        try{
            $mpersona=new ModeloPersona();

            $mpersona->id=$persona['id'];
            $mpersona->numeroIdentificacion=$persona['numeroIdentificacion'];
            $mpersona->nombres=$persona['nombres'];
            $mpersona->primerApellido=$persona['primerApellido'];
            $mpersona->segundoApellido=$persona['segundoApellido'];
            $mpersona->idTipoDocumento=$persona['idTipoDocumento'];
            $mpersona->direccion=$persona['direccion'];
            $mpersona->telefonos=$persona['telefonos'];
            $mpersona->correo=$persona['correo'];

            if ($mpersona->id==null){
                $personaGuardada=$this->personaManager->guardar($mpersona,$user);
            }
            else{
                $personaGuardada=$this->personaManager->actualizar($mpersona,$user);
            }


            $userManager = $this->get('fos_user.user_manager');

            $user = $userManager->findUserBy(array("id"=>$usuario["id"]));

            $user->setEnabled(true);
            $user->setEmailCanonical($personaGuardada->getCorreo());
            $user->setEmail($personaGuardada->getCorreo());
                if($usuario['password']!=null){
                    $user->setPlainPassword($usuario['password']);
                }

            $user->setUsername($usuario['username']);
            $user->setUsernameCanonical($usuario['username']);
            $user->setIdPersona($personaGuardada->getId());

            $user->setTipo($tipo);
            $user->setRoles(array("ROLE_USER"));
            $userManager->updateUser($user);

            return $this->json($persona['id'],Response::HTTP_OK);
        }
        catch (UniqueConstraintViolationException $ex){
            return $this->json(array("Algun dato de tipo identificador ya existe en el sistema. No se pudo guardar el registro."),Response::HTTP_BAD_REQUEST);
        }



    }


    /**
     * @Route("/quitar/{id}", name="user_quitar")
     * @Method("GET")
     */
    public function quitarAction($id)
    {
            $userManager = $this->get('fos_user.user_manager');
            $user = $userManager->findUserBy(array("id"=>$id));
            $user->setEnabled(false);
            $userManager->updateUser($user);

            return $this->json($id,Response::HTTP_OK);
    }

}
