<?php

namespace UserBundle\Controller;

use ProductoBundle\Manager\EntidadManager;
use ProductoBundle\Manager\PersonaManager;
use ProductoBundle\Manager\RelacionEntidadManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use UserBundle\Entity\User;
use UserBundle\Service\MyUserManager;
use Lexik\Bundle\JWTAuthenticationBundle\TokenExtractor\AuthorizationHeaderTokenExtractor;

use JMS\DiExtraBundle\Annotation as DI;
use UserBundle\Service\UsuarioManager;

class LoginController extends Controller
{

    use \UserBundle\Helper\ControllerHelper;
    protected $relacionManager;
    protected $personaManager;
    protected  $entidadManager;
    protected $usuarioManager;
    /**
     * @DI\InjectParams({
     * "personaManager"=@DI\Inject("api.manager.persona"),
     * "relacionEntidadManager"=@DI\Inject("api.manager.relacion"),
     * "entidadManager"=@DI\Inject("api.manager.entidad"),
     * "usuarioManager"=@DI\Inject("api.manager.usuario")
     * })
     *
     */
    public  function __construct(RelacionEntidadManager $relacionEntidadManager,PersonaManager $personaManager,EntidadManager $entidadManager,UsuarioManager $usuarioManager){
            $this->relacionManager=$relacionEntidadManager;
            $this->personaManager=$personaManager;
            $this->entidadManager=$entidadManager;
            $this->usuarioManager=$usuarioManager;
    }
    
    /**
     * @Route("/login", name="user_login")
     * @Method("POST")
     */
    public function loginAction(Request $request)
    {
        $contenido = $request->getContent();
        $datos = json_decode($contenido);

        $usernameOrEmail = $datos->username;
        $password = $datos->password;

        /** @var MyUserManager */
        $userManager = $this->get('my_user_manager');
        $user = $userManager->findUserByUsernameOrEmail($usernameOrEmail);
        if (!$user) {
            throw $this->createNotFoundException();
        }

        $isValid = $this->get('security.password_encoder')
            ->isPasswordValid($user, $password);

        if (!$isValid) {
            throw new BadCredentialsException();
        }

        $relacion = json_decode($contenido);
        if($user->getTipo()=='e')
        {
            if ($relacion->idRelacion == null) {
                $vigentes = $this->relacionManager->relacionesVigentesPersona($user->getIdPersona());
                if (count($vigentes) <> 1) {
                    $response = new  Response($this->serialize($vigentes), Response::HTTP_PARTIAL_CONTENT);
                    return $this->setBaseHeaders($response);
                } else {
                        $relacion = $this->relacionManager->recuperar($vigentes[0]['id']);
                }
            } else {
                $relacion = $this->relacionManager->recuperar($relacion->idRelacion);
                $idPersona = $relacion->getIdPersona()->getId();
            }

            if ($relacion->getIdPersona()->getId() == $user->getIdPersona()) {

            $token = $this->getToken($user, $relacion->getId(), $relacion->getIdEntidad()->getId(), $relacion->getIdEntidad()->getRazonSocial(),
                $relacion->getIdPersona()->getNombres() . " " . $relacion->getIdPersona()->getPrimerApellido() . " " . $relacion->getIdPersona()->getSegundoApellido());
            $response = new Response($this->serialize(['token' => $token]), Response::HTTP_OK);
            return $this->setBaseHeaders($response);
            } else {
                throw new BadCredentialsException();
            }
        }
        if($user->getTipo()=='f'){

            $persona=$this->personaManager->recuperar($user->getIdPersona());

            $token = $this->getToken($user, null,null,"MDPyEP - VCIE",
                $persona->nombres." ". $persona->primerApellido." ".$persona->segundoApellido);
            $response = new Response($this->serialize(['token' => $token]), Response::HTTP_OK);
            return $this->setBaseHeaders($response);


        }

    }

    /**
     * @Route("/loginNit", name="user_loginnit")
     * @Method("POST")
     */
    public function loginNitAction(Request $request)
    {
        $contenido = $request->getContent();
        $datos = json_decode($contenido);

        $headers = array('Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiI2elVVMDJtQzBqU0ZiMHpWbzY2R0xvRWU0bWVFZDJZbyIsInVzZXIiOiJlYXJnb2xsby1hZ2V0aWMiLCJleHAiOjE2MDcxNzAzMjB9.k-Fh65ipsDGRXvhvuAE-sOvkNeKIl7SAxzM6L2dkxq0'
                                ,
                            'Content-Type: application/json',
                            'accept: application/json'
                        );

        $url = 'https://test.agetic.gob.bo/apigateway/fake/impuestos/v1/login';
        $curl_post_data = array(
            "nit" => $datos->nit, "usuario" => $datos->usuario, "clave" => $datos->password
        );
        $curl = curl_init($url);
        $json=json_encode($curl_post_data);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $json);
        $respuesta = curl_exec($curl);
         if ($respuesta === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }

        $decoded = json_decode($respuesta);
        if (isset($decoded->response->status) && $decoded->response->status == 'ERROR') {
            die('error occured: ' . $decoded->response->errormessage);
        }
        curl_close($curl);

        if($decoded->Autenticado){
            $entidad= $this->entidadManager->recuperarXnit($curl_post_data['nit']);
            if(null==$entidad){
                return $this->json(array("codigo"=>1,"mensaje"=>"Nit válido, Pero la entidad no esta registrada en nuestra base de datos local."), Response::HTTP_OK);
            }
            $relacion=$this->relacionManager->relacionVigenteEntidad($entidad->getId());
            //var_dump($entidad->getId());
            //var_dump($relacion->getIdPersona()->getId()); die();
            if(null==$relacion){
                return $this->json(array("codigo"=>1,"mensaje"=>"Esta entidad no tiene una relación activa."), Response::HTTP_OK);
            }

            $user= $this->usuarioManager->recuperaUsuario($relacion->getIdPersona()->getId());

            $token = $this->getToken($user, $relacion->getId(), $relacion->getIdEntidad()->getId(), $relacion->getIdEntidad()->getRazonSocial(),
                $relacion->getIdPersona()->getNombres() . " " . $relacion->getIdPersona()->getPrimerApellido() . " " . $relacion->getIdPersona()->getSegundoApellido());
            $response = new Response($this->serialize(['token' => $token,"codigo"=>2]), Response::HTTP_OK);
            return $this->setBaseHeaders($response);

        }
        else {
            //throw new BadCredentialsException();
            return $this->json(array("codigo"=>1,"mensaje"=>"Credenciales  no validas"),Response::HTTP_UNAUTHORIZED);
        }

    }

    
    /**
     *
     * @Route("/user")
     * @Method("GET")
     * **/
    public function user(Request $request){
        
        $extractor = new AuthorizationHeaderTokenExtractor(
            'Bearer',
            'Authorization'
            );
        $token = $extractor->extract($request);
        $data= $this->container->get('lexik_jwt_authentication.encoder')->decode($token);
        return $this->json(array("datos"=>$data));
    }

    /**
     *
     * @Route("/usuario")
     * @Method("GET")
     * **/
    public function usuario(Request $request){

        $extractor = new AuthorizationHeaderTokenExtractor(
            'Bearer',
            'Authorization'
        );
        $token = $extractor->extract($request);
        $data= $this->container->get('lexik_jwt_authentication.encoder')->decode($token);
        return $this->json(array("username"=>$data['username'],"nombrePersona"=>$data['nombrePersona'],
                                                "email"=>$data['email'],
                                                "razonSocial"=>$data['razonSocial'],"tipo"=>$data['tipo'],
                                                "roles"=>$data['roles']),Response::HTTP_OK);
    }

    /**
     * Returns token for user.
     *
     * @param User $user
     *
     * @return array
     */
    public function getToken(User $user,$idRelacion,$idEntidad,$razonSocial,$nombrePersona)
    {
        return $this->container->get('lexik_jwt_authentication.encoder')
                ->encode([
                    'username' => $user->getUsername(),
                    'exp' => $this->getTokenExpiryDateTime(),
                    'idUser'=>$user->getId(),
                    'idPersona'=>$user->getIdPersona(),
                    'nombrePersona'=>$nombrePersona,
                    'tipo'=>$user->getTipo(),
                    'email'=>$user->getEmail(),
                    'roles'=>$user->getRoles(),
                    'idRelacion'=>$idRelacion,
                    'idEntidad'=>$idEntidad,
                    'razonSocial'=>$razonSocial
                ]);
    }


    /**
     * Returns token expiration datetime.
     *
     * @return string Unixtmestamp
     */
    private function getTokenExpiryDateTime()
    {
        $tokenTtl = $this->container->getParameter('lexik_jwt_authentication.token_ttl');
        $now = new \DateTime();
        $now->add(new \DateInterval('PT'.$tokenTtl.'S'));

        return $now->format('U');
    }


}
