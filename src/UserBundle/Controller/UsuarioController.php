<?php
namespace UserBundle\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use ProductoBundle\Manager\PersonaManager;
use JMS\DiExtraBundle\Annotation as DI;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use ProductoBundle\Entity\Producto;
use UserBundle\Service\UsuarioManager;

/**
 * @Security("is_granted('ROLE_USER')")
 * **/
class UsuarioController extends Controller{
    use \ProductoBundle\Helper\Helper;

    protected $usuarioManager;
    /**
     * @DI\InjectParams({
     * "usuarioManager"=@DI\Inject("api.manager.usuario")
     * })
     *
     */
    public function __construct(UsuarioManager $usuarioManager){
        $this->usuarioManager=$usuarioManager;
    }
    /**
     *
     * @Route("/lista/{page}/{limit}")
     * @Method("GET")
     * **/
    public function recuperarUsuarios($page,$limit){
        $total=$this->usuarioManager->listarUsuariosCount();
        $usuarios =$this->usuarioManager->listarUsuarios($page,$limit);
        return $this->json(array("total"=>$total[0]["total"],"datos"=>$usuarios),Response::HTTP_OK);
    }
    /**
     *
     * @Route("/cambiarPassword")
     * @Method("POST")
     * **/
    public function cambiarPassword(Request $request){
        $passwordActual = $request->request->get('actual_password');
        $passwordNuevo = $request->request->get('nuevo_password');
        $email = $request->request->get('email');
        $mensaje = '';

        /** @var MyUserManager */
        $userManager = $this->get('my_user_manager');
        $user = $userManager->findUserByUsernameOrEmail($email);
        if (!$user) {
            throw $this->createNotFoundException();
        }

        $isValid = $this->get('security.password_encoder')
            ->isPasswordValid($user, $passwordActual);

        if (!$isValid) {
            // throw new BadCredentialsException();
            return $this->json("La contraseña es incorrecta",404);
        }

        $userManager = $this->get('fos_user.user_manager');
        $save_user = $userManager->findUserBy(array("id"=>$user->getId()));
        if($passwordNuevo){
            $save_user->setPlainPassword($passwordNuevo);
        }
        $userManager->updateUser($user);

        //return $this->json(array($passwordActual,$passwordNuevo,$email,$mensaje,$user->getId()),Response::HTTP_OK);
        return $this->json('La contraseña se actualizó correctamente',Response::HTTP_OK);
    }
}
