<?php
namespace UserBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use JMS\DiExtraBundle\Annotation as DI;
use UserBundle\Service\UsuarioManager;
use UserBundle\Service\RolManager;
use UserBundle\Entity\User;

/**
 * @Route("/rol")
 * **/
class RolController extends Controller{
    use \ProductoBundle\Helper\Helper;

    protected $rolManager;
    protected $usuarioManager;
    /**
     * @DI\InjectParams({
     * "rolManager"=@DI\Inject("api.manager.rol"),
     * "usuarioManager"=@DI\Inject("api.manager.usuario"),
     * })
     *
     */
    public function __construct(RolManager $rolManager,UsuarioManager $usuarioManager)
    {
        $this->rolManager = $rolManager;
        $this->usuarioManager=$usuarioManager;
    }
    /**
     *
     * @Route("/lista/{modulo}")
     * @Method("GET")
     * **/
    public function RecuperaRoles($modulo){
        $roles= $this->rolManager->recuperaRoles($modulo);
        return $this->json($roles,Response::HTTP_OK);
    }

    /**
     *
     * @Route("/lista")
     * @Method("GET")
     * **/
    public function RecuperaModulos(){
        $modulos= $this->rolManager->recuperaModulos();
        return $this->json($modulos,Response::HTTP_OK);

    }

    /**
     *
     * @Route("/usuario/{id}")
     * @Method("GET")
     * **/
    public function recuperaUsuario($id){
        $user= $this->usuarioManager->recuperaUsuarioXid($id);
        return $this->json($user,Response::HTTP_OK);

    }

    /**
     * @Route("/usuariorol/{id}")
     * @Method("GET")
     */
    public function recuperaRolesUsuario($id){
        $usuario= $this->usuarioManager->recuperaUsuarioXid(intval($id));
        $roles=array();
        foreach ( $usuario->getRoles() as $role)
        {
            array_push($roles,array("rol"=>$role));
        }

        return $this->json($roles,Response::HTTP_OK);
    }

    /**
     * @Route("/agregar/{id}/{rol}")
     * @Method("GET")
     */
    public function agregarRol($id,$rol)
    {
        $user= $this->usuarioManager->recuperaUsuarioXid($id);
        $user->addRole($rol);
        $userManager = $this->get('fos_user.user_manager');


        //$user->setRoles(array("ROLE_USER"));
        $userManager->updateUser($user);
        return $this->json($user,Response::HTTP_OK);
    }

    /**
     * @Route("/eliminar/{id}/{rol}")
     * @Method("GET")
     */
    public function quitarRol($id,$rol)
    {
        $user= $this->usuarioManager->recuperaUsuarioXid($id);
        $user->removeRole($rol);
        $userManager = $this->get('fos_user.user_manager');

        $userManager->updateUser($user);
        return $this->json($user,Response::HTTP_OK);

    }
    
}