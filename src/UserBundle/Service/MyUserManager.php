<?php

namespace UserBundle\Service;


use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query\ResultSetMapping;
use FOS\UserBundle\Doctrine\UserManager;
use FOS\UserBundle\Util\CanonicalFieldsUpdater;
use FOS\UserBundle\Util\PasswordUpdaterInterface;
use UserBundle\Entity\User;

class MyUserManager extends UserManager
{
    /**
     * @inheritdoc
     */
    protected  $em;


    public function findUserByUsernameOrEmail($usernameOrEmail)
    {
        $user = parent::findUserByUsernameOrEmail($usernameOrEmail);
        if (null === $user) {
            $userRepo = $this->objectManager->getRepository('UserBundle:User');
            $userAddOnEmail = $userRepo->findOneBy(['email' => $usernameOrEmail]);
            if ($userAddOnEmail) {
                $user = $userAddOnEmail->getUser();
            }
        }

        return $user;
    }

}