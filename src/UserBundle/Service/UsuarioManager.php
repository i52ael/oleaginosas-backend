<?php
namespace UserBundle\Service;

use Doctrine\ORM\EntityManager;
use ProductoBundle\Entity\Persona;

use UserBundle\Entity\User;

class UsuarioManager{
    
    /**
     * @var EntityManager
     */
    protected $em;
    protected $repo;
    public function __construct(EntityManager $entityManager){
        $this->em=$entityManager;
        $this->setRepository();
    }
   
    protected function setRepository()
    {
        $this->repo=$this->em->getRepository(User::class);
    }

    public function listarUsuariosCount(){

        $sql = "Select count(usuario.id) total 
                                    from usuario
                                    inner join persona on persona.id=usuario.id_persona 
                                    where enabled=true";

        $statement = $this->em->getConnection()->prepare($sql);
        $statement->execute();
        $total = $statement->fetchAll();
        return $total;
    }
    public function listarUsuarios($page,$limit){

        $sql = "Select usuario.id, username, email, last_login, roles, username_canonical,email_canonical,enabled,tipo,id_persona,
                                      persona.nombres,
                                    persona.primer_apellido,persona.segundo_apellido, persona.numero_identificacion 
                                    from usuario
                                    inner join persona on persona.id=usuario.id_persona 
                                    where enabled=true".
            "  order by username ASC ".
            "  limit ". $limit.
            "  offset " . ($page-1) * $limit
        ;

        $statement = $this->em->getConnection()->prepare($sql);
        $statement->execute();
        $usuarios = $statement->fetchAll();
        return $usuarios;
    }
    public function recuperaUsuario($idPersona){

        $user= $this->repo->findOneBy(array("id_persona"=>$idPersona));
        return $user;

    }
    public function recuperaUsuarioXid($id){
        return $this->repo->findOneBy(array("id"=>$id));
    }

    public function guardarUsuario($usuario){
        $usuario= new User();

        //$usuario->set

    }
    public function guardar($musuario,Persona $personaGuardada)
    {

    }



    
}