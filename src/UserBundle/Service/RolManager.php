<?php
namespace UserBundle\Service;

use Doctrine\ORM\EntityManager;
use ProductoBundle\Entity\Rol;
use UserBundle\Entity\User;

class RolManager{
    
    /**
     * @var EntityManager
     */
    protected $em;
    protected $repo;
    protected $repo2;
    public function __construct(EntityManager $entityManager){
        $this->em=$entityManager;
        $this->setRepository();
    }
    protected function setRepository()
    {
        $this->repo=$this->em->getRepository(Rol::class);

    }

    public function recuperaRoles($modulo){
        return $this->repo->getRolesModulo($modulo)->getQuery()->getResult();
    }
    public function recuperaModulos(){
        return $this->repo->getModulo()->getQuery()->getResult();
    }

    
}