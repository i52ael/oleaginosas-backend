<?php

namespace CorreoBundle\Entity;

/**
 * Notificaciones
 */
class Notificaciones
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var \Time
     */
    private $horaAEnviar;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set horaAEnviar
     *
     * @param \Time $horaAEnviar
     *
     * @return Notificaciones
     */
    public function setHoraAEnviar($horaAEnviar)
    {
        $this->horaAEnviar = $horaAEnviar;

        return $this;
    }

    /**
     * Get horaAEnviar
     *
     * @return \Time
     */
    public function getHoraAEnviar()
    {
        return $this->horaAEnviar;
    }
}

