<?php

namespace CorreoBundle\Command;

use CorreoBundle\Entity\Notificaciones;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class NotificacionCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('notificaciones:enviar');
        $this->setDescription('Envía mensajes de notificación, por correo electrónico.');
        $this->setHelp('Permite enviar uno o varios mensajes, a uno o varios destinatarios, vía correo electrónico.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // $notificaciones = $this->getDoctrine()->getRepository(Notificaciones::class)->find();
        // var_dump($notificaciones);
        $correo = $this->getContainer()->get('correo.mensajesgenerador');
        $output->writeln(['ENVIAR MENSAJES']);
        $output->writeln(['====== ========']);
        $output->write($correo->nuevoMensaje(0));
    }
}