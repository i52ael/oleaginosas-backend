<?php

namespace CorreoBundle\Services;

class MensajesGenerador
{
    public function nuevoMensaje($index = 0)
    {
        $messages = [
            0 => 'MENSAJE INFORMATIVO. Este mensaje contiene informacion importante.',
            1 => 'MENSAJE DE CONFIRMACION. Confirme que esta es su direccion de correo electrónico.',
            2 => 'MENSAJE DE BIENVENIDA. A partir de la fecha, usted será notificado de las siguientes actividades:',
        ];
        return $messages[$index];
    }
}